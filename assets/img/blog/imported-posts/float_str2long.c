#include <stddef.h>
#include <string.h>
#include <math.h>
#include <stdio.h>

long str2long(char *p)
{
  size_t l = strlen(p);
  long acc = 0;
  
  for (size_t i=0; i<l; i++)
    {
      int digit = p[i] - '0';
      long pow10 = pow(10, l - 1U - i);
      acc += digit * pow10;;
    }
  return acc;
}

int main(){
  printf("%ld %ld %ld %ld\n", 
	 str2long("0"),
	 str2long("123"), 
	 str2long("999"), 
	 str2long("123456789123456789"));
} 
	 
