/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.1.0
 * Git version: ea4762b
 * Options:   --max-pointer-depth 2 --max-funcs 2 --max-array-dim 2 --max-array-len-per-dim 4 --no-longlong --no-math64 --no-structs --no-volatiles --no-bitfields --no-argc --no-unions
 * Seed:      2442080782
 */


#define NO_LONGLONG

#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
/* --- GLOBAL VARIABLES --- */
static uint8_t g_7 = 8UL;
static uint32_t g_11 = 1UL;
static int16_t g_15[4] = {0xDEADL, 0xDEADL, 0xDEADL, 0xDEADL};
static uint16_t g_20[1][3] = {{0UL, 0UL, 0UL}};
static int32_t g_22 = 0x348F4A8FL;
static uint32_t g_40 = 4294967295UL;
static int16_t g_57 = 5L;
static int32_t g_64 = 1L;
static uint16_t g_66 = 0x8509L;
static int16_t g_69 = 0x5761L;
static int32_t g_71[1][3] = {{0x900F1D91L, 0x900F1D91L, 0x900F1D91L}};
static int32_t g_116 = 0x817FF2A7L;
static int16_t g_121 = 0x1919L;
static uint32_t g_122 = 0xF8503667L;
static int8_t g_131 = 0x51L;
static int8_t *g_130 = &g_131;
static int8_t *g_135 = (void*)0;
static int8_t **g_134 = &g_135;
static const int8_t g_139 = 0x96L;
static uint8_t g_144 = 0x5FL;
static int8_t g_168 = (-1L);
static uint32_t g_169 = 0UL;
static int32_t g_213[3][3] = {{9L, 7L, 9L}, {9L, 7L, 9L}, {9L, 7L, 9L}};
static uint8_t g_214 = 250UL;
static uint32_t g_220[3] = {4294967295UL, 4294967295UL, 4294967295UL};
static int32_t *g_246 = &g_22;
static uint8_t g_256 = 0xDBL;
static uint16_t *g_274 = &g_20[0][2];
static uint32_t *g_308 = (void*)0;
static uint32_t **g_307[4] = {&g_308, (void*)0, &g_308, (void*)0};
static int32_t g_323 = (-1L);
static int16_t g_324[1][3] = {{0xF9F8L, 0xF9F8L, 0xF9F8L}};
static uint32_t g_325[1] = {1UL};
static int32_t g_330 = (-1L);
static uint16_t g_406 = 0xFC37L;
static int8_t g_438 = 6L;


/* --- FORWARD DECLARATIONS --- */
static uint16_t  func_1(void);
static int32_t  func_2(int32_t  p_3, uint16_t  p_4, uint32_t  p_5, uint8_t  p_6);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_7 g_15 g_20 g_11 g_40 g_22 g_69 g_66 g_71 g_130 g_131 g_64 g_168 g_214 g_220 g_169 g_246 g_256 g_57 g_139 g_323 g_330 g_406 g_325 g_213 g_274 g_438 g_144 g_324
 * writes: g_11 g_15 g_20 g_22 g_40 g_57 g_64 g_69 g_7 g_71 g_66 g_214 g_220 g_169 g_246 g_256 g_330 g_131 g_323 g_406 g_324
 */
static uint16_t  func_1(void)
{ /* block id: 0 */
    uint32_t *l_10 = &g_11;
    int16_t *l_14 = &g_15[0];
    int32_t l_18 = 0x2949F4FFL;
    uint16_t *l_19 = &g_20[0][0];
    int32_t l_395 = 0x0BBA3064L;
    int32_t l_402[1];
    int32_t l_405 = 0x6FBCFE45L;
    int32_t **l_463[3][3] = {{&g_246, &g_246, &g_246}, {&g_246, &g_246, &g_246}, {&g_246, &g_246, &g_246}};
    int32_t ***l_462 = &l_463[1][1];
    int i, j;
    for (i = 0; i < 1; i++)
        l_402[i] = 0x6C6504D1L;
    if (func_2(g_7, ((((*l_14) = (safe_lshift_func_uint8_t_u_u((g_7 , g_7), (((*l_10) = g_7) , (safe_rshift_func_int8_t_s_u(g_7, g_7)))))) && 0xD9B4L) , (((*l_19) &= (safe_sub_func_uint32_t_u_u(l_18, (~g_15[0])))) >= g_11)), l_18, g_7))
    { /* block id: 282 */
        return l_18;
    }
    else
    { /* block id: 284 */
        uint32_t l_386 = 0x438B3E22L;
        int32_t l_399 = (-1L);
        int32_t l_400 = 0xDB46B1EAL;
        int32_t l_401[4];
        int16_t l_403 = 0x5634L;
        uint16_t *l_476 = &g_406;
        int32_t l_477 = 0x19A32693L;
        int i;
        for (i = 0; i < 4; i++)
            l_401[i] = 1L;
        for (g_69 = 0; (g_69 < (-5)); g_69 = safe_sub_func_uint32_t_u_u(g_69, 6))
        { /* block id: 287 */
            int32_t *l_384 = &g_330;
            int32_t *l_385 = &g_330;
            int32_t **l_389 = (void*)0;
            int32_t **l_390 = (void*)0;
            uint32_t *l_396[2];
            int8_t l_397 = 0x19L;
            int8_t l_398[4] = {1L, (-1L), 1L, (-1L)};
            int32_t l_404 = (-6L);
            uint32_t l_419 = 0xD4214D1CL;
            uint16_t l_440[4] = {0xC878L, 0x08D1L, 0xC878L, 0x08D1L};
            int i;
            for (i = 0; i < 2; i++)
                l_396[i] = &g_40;
            l_386--;
            g_246 = &g_323;
            (*l_385) &= (+(*g_246));
            if ((((*g_246) = 0x6395968AL) < ((safe_lshift_func_int8_t_s_s(((l_395 > l_395) <= (--g_406)), (l_402[0] &= 0x08L))) >= ((safe_mod_func_int16_t_s_s((safe_div_func_int16_t_s_s(((l_395 , l_395) > ((*g_130) = l_18)), (*l_385))), (safe_mod_func_int8_t_s_s(((safe_lshift_func_uint16_t_u_u((safe_lshift_func_uint8_t_u_u(l_419, g_214)), 10)) <= l_400), (-7L))))) != g_325[0]))))
            { /* block id: 295 */
                int32_t l_424 = 4L;
                int32_t l_425 = 0x4E25238DL;
                l_425 ^= ((((l_402[0] , (safe_mul_func_uint32_t_u_u(((safe_div_func_uint32_t_u_u(0xA923E03BL, ((*l_385) ^= (-1L)))) == g_213[0][0]), (((l_424 < (*g_246)) , ((255UL >= l_401[1]) >= l_424)) < (*g_274))))) , 0xCDL) == 0x61L) >= l_424);
            }
            else
            { /* block id: 298 */
                int16_t l_430 = (-9L);
                int8_t *l_431 = &l_397;
                int16_t *l_439 = &g_324[0][2];
                uint8_t l_448 = 247UL;
                (*g_246) = (safe_mul_func_int16_t_s_s((safe_rshift_func_int8_t_s_u(((((*l_14) ^= l_400) && ((l_430 , (l_430 , l_402[0])) == ((*l_431) |= ((*g_130) = l_386)))) < ((safe_div_func_int16_t_s_s((0x97L && 0xB1L), 5L)) <= ((*l_439) = (safe_rshift_func_int16_t_s_u((safe_div_func_int8_t_s_s(g_438, g_144)), 2))))), 5)), l_440[0]));
                for (l_405 = 0; (l_405 <= 0); l_405 += 1)
                { /* block id: 306 */
                    int i;
                    if ((safe_unary_minus_func_int16_t_s((4294967286UL | ((l_402[l_405] ^ ((safe_mod_func_uint32_t_u_u((((l_402[l_405] | (0x7EL != g_324[0][1])) , (safe_sub_func_uint16_t_u_u((((*g_130) && (safe_rshift_func_int16_t_s_s((((*l_385) != l_402[0]) != l_402[0]), l_400))) , (*g_274)), l_18))) < l_448), (*g_246))) == 0xF2L)) | (*l_385))))))
                    { /* block id: 307 */
                        return l_402[l_405];
                    }
                    else
                    { /* block id: 309 */
                        uint16_t l_449 = 65530UL;
                        int i;
                        l_402[l_405] ^= l_449;
                        (*l_385) = (0xACE73012L > l_430);
                        return (*g_274);
                    }
                }
            }
        }
        l_399 |= (l_477 = ((safe_div_func_int16_t_s_s((safe_mul_func_int16_t_s_s(0L, ((((safe_rshift_func_int16_t_s_s((~(-1L)), g_323)) == 0x01A7L) & l_386) , ((safe_mul_func_uint16_t_u_u(((*l_476) ^= (((safe_div_func_uint16_t_u_u((l_401[0] && 3UL), (*g_274))) , l_400) || 6UL)), (*g_274))) == g_15[0])))), (*g_274))) | 65535UL));
    }
    return (*g_274);
}


/* ------------------------------------------ */
/* 
 * reads : g_15 g_40 g_22 g_20 g_11 g_7 g_69 g_66 g_71 g_130 g_131 g_64 g_168 g_214 g_220 g_169 g_246 g_256 g_57 g_139
 * writes: g_22 g_40 g_57 g_64 g_69 g_7 g_71 g_66 g_214 g_220 g_169 g_246 g_256 g_11 g_330 g_131
 */
static int32_t  func_2(int32_t  p_3, uint16_t  p_4, uint32_t  p_5, uint8_t  p_6)
{ /* block id: 4 */
    int32_t *l_21 = &g_22;
    uint16_t *l_100 = &g_20[0][0];
    int32_t l_117 = (-6L);
    int32_t l_118 = 0x5B119D2BL;
    int32_t l_119[2][1];
    int8_t *l_127 = (void*)0;
    int8_t l_232 = (-5L);
    int32_t **l_243 = &l_21;
    int32_t l_372 = 0xB752946BL;
    int32_t *l_373[4][3] = {{(void*)0, (void*)0, &l_117}, {(void*)0, (void*)0, &l_117}, {(void*)0, (void*)0, &l_117}, {(void*)0, (void*)0, &l_117}};
    uint8_t *l_377[1][3];
    int16_t *l_378 = (void*)0;
    int16_t *l_379[1];
    uint8_t l_380 = 0x4CL;
    uint32_t l_381[1][3];
    int i, j;
    for (i = 0; i < 2; i++)
    {
        for (j = 0; j < 1; j++)
            l_119[i][j] = (-1L);
    }
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 3; j++)
            l_377[i][j] = (void*)0;
    }
    for (i = 0; i < 1; i++)
        l_379[i] = &g_121;
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 3; j++)
            l_381[i][j] = 4294967295UL;
    }
    if (((*l_21) = g_15[0]))
    { /* block id: 6 */
        int32_t **l_25 = (void*)0;
        int32_t **l_26 = &l_21;
        (*l_21) = (safe_lshift_func_uint8_t_u_u(p_5, 7));
        (*l_26) = l_21;
        g_22 = 0x91023F12L;
        if (p_5)
            goto lbl_374;
    }
    else
    { /* block id: 10 */
        uint32_t l_37 = 0x5ECCE35AL;
        uint32_t *l_38 = (void*)0;
        uint32_t *l_39 = &g_40;
        int8_t l_45[3];
        int16_t *l_56 = &g_57;
        uint32_t l_60 = 4UL;
        int8_t *l_61[2];
        int32_t l_62[1][4];
        int32_t *l_63 = &g_64;
        uint16_t *l_65[4][3] = {{&g_20[0][0], &g_20[0][0], &g_20[0][1]}, {&g_20[0][0], &g_20[0][0], &g_20[0][1]}, {&g_20[0][0], &g_20[0][0], &g_20[0][1]}, {&g_20[0][0], &g_20[0][0], &g_20[0][1]}};
        int32_t l_67[1];
        int16_t *l_68 = &g_69;
        uint8_t *l_70 = &g_7;
        int32_t **l_72 = (void*)0;
        int32_t **l_73 = &l_21;
        uint32_t l_86[3];
        int8_t l_192 = 0x26L;
        uint32_t **l_306 = &l_39;
        int i, j;
        for (i = 0; i < 3; i++)
            l_45[i] = 0xCEL;
        for (i = 0; i < 2; i++)
            l_61[i] = &l_45[1];
        for (i = 0; i < 1; i++)
        {
            for (j = 0; j < 4; j++)
                l_62[i][j] = 0xDA78404DL;
        }
        for (i = 0; i < 1; i++)
            l_67[i] = 0xF04B2B3AL;
        for (i = 0; i < 3; i++)
            l_86[i] = 0xA02B236EL;
        g_71[0][0] ^= (safe_add_func_uint8_t_u_u((g_15[3] <= ((safe_div_func_uint8_t_u_u((safe_mod_func_uint8_t_u_u(((*l_70) = (((safe_sub_func_uint8_t_u_u(l_37, (((*l_68) &= ((++(*l_39)) && ((l_67[0] &= (((((*l_63) = (((p_4 & (l_62[0][0] = (safe_div_func_int32_t_s_s(((0xB5B0L | l_45[2]) > (safe_mod_func_int32_t_s_s((((*l_21) = ((safe_div_func_uint32_t_u_u(((safe_rshift_func_uint16_t_u_u((safe_mul_func_int8_t_s_s((safe_lshift_func_uint8_t_u_u((p_3 && (((*l_56) = (l_21 == (void*)0)) <= ((safe_lshift_func_uint16_t_u_s((*l_21), g_20[0][0])) != p_4))), p_4)), 2UL)), p_4)) , l_60), (*l_21))) <= 0xBDEDL)) & p_6), g_20[0][1]))), p_3)))) != g_11) || p_6)) & p_4) ^ g_20[0][2]) == 0xAF6DDF93L)) , g_7))) , g_22))) | p_5) , 0UL)), g_20[0][1])), g_66)) < g_20[0][2])), 0xB5L));
        (*l_73) = l_21;
        for (p_5 = 0; (p_5 > 45); p_5++)
        { /* block id: 23 */
            int8_t l_85[3][3] = {{0xF8L, 0L, 0xF8L}, {0xF8L, 0L, 0xF8L}, {0xF8L, 0L, 0xF8L}};
            int32_t l_89 = 0x9069C78CL;
            int32_t **l_101 = (void*)0;
            int32_t l_115 = (-1L);
            int32_t l_120[1][2];
            int i, j;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 2; j++)
                    l_120[i][j] = 6L;
            }
            for (g_57 = 0; (g_57 >= 7); g_57 = safe_add_func_uint8_t_u_u(g_57, 8))
            { /* block id: 26 */
                int8_t **l_80 = (void*)0;
                for (g_22 = 0; (g_22 == 29); g_22 = safe_add_func_int32_t_s_s(g_22, 8))
                { /* block id: 29 */
                    int8_t ***l_81 = &l_80;
                    (*l_81) = l_80;
                    return g_69;
                }
            }
        }
        if ((((safe_sub_func_int32_t_s_s((-10L), (0xB7L < (*g_130)))) != (*l_63)) & (-1L)))
        { /* block id: 123 */
            int8_t l_210[1][4];
            int32_t l_217 = 9L;
            int32_t l_218 = 7L;
            int32_t l_219[1][3];
            int32_t **l_239 = &l_21;
            int32_t **l_240 = (void*)0;
            int i, j;
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 4; j++)
                    l_210[i][j] = 0xFAL;
            }
            for (i = 0; i < 1; i++)
            {
                for (j = 0; j < 3; j++)
                    l_219[i][j] = 0xDA2B0385L;
            }
            if (((((p_5 <= (safe_add_func_int8_t_s_s((&g_71[0][0] == (void*)0), (safe_unary_minus_func_int8_t_s((((((*l_68) = ((safe_div_func_int32_t_s_s(g_7, p_4)) ^ (+(((safe_lshift_func_uint16_t_u_u((safe_mul_func_uint16_t_u_u(g_131, p_3)), (p_4 | (safe_div_func_int8_t_s_s((safe_sub_func_int32_t_s_s((!(~(safe_mul_func_uint16_t_u_u((*l_63), 1UL)))), l_192)), p_6))))) >= 0xA6AEL) & (-1L))))) , 1L) & 0x04L) >= g_168)))))) , 0xF7L) >= 7L) || 0xB5C2L))
            { /* block id: 125 */
                int32_t *l_230[3][2] = {{(void*)0, (void*)0}, {(void*)0, (void*)0}, {(void*)0, (void*)0}};
                int i, j;
                for (p_5 = 10; (p_5 >= 29); ++p_5)
                { /* block id: 128 */
                    int32_t *l_197 = &l_119[1][0];
                    int32_t l_211 = 0x4A309CDAL;
                    int32_t l_212 = 1L;
                    uint32_t l_223 = 0UL;
                    (*l_197) ^= ((*l_63) |= (safe_rshift_func_uint16_t_u_u((*l_21), 3)));
                    for (g_66 = 0; (g_66 <= 2); g_66 += 1)
                    { /* block id: 133 */
                        int32_t *l_198 = &l_119[1][0];
                        int32_t *l_199 = &l_117;
                        int32_t *l_200 = &l_119[1][0];
                        int32_t *l_201 = &g_22;
                        int32_t *l_202 = &l_62[0][1];
                        int32_t *l_203 = (void*)0;
                        int32_t *l_204 = &l_119[0][0];
                        int32_t *l_205 = (void*)0;
                        int32_t *l_206 = &l_62[0][2];
                        int32_t *l_207 = &l_62[0][0];
                        int32_t *l_208 = &l_117;
                        int32_t *l_209[3];
                        int i;
                        for (i = 0; i < 3; i++)
                            l_209[i] = (void*)0;
                        (*l_197) = l_45[g_66];
                        ++g_214;
                        --g_220[1];
                        l_223++;
                    }
                    return p_6;
                }
                for (l_117 = (-15); (l_117 < 15); ++l_117)
                { /* block id: 143 */
                    for (g_169 = 0; (g_169 > 46); g_169 = safe_add_func_int32_t_s_s(g_169, 5))
                    { /* block id: 146 */
                        const int32_t l_231 = 1L;
                        l_230[0][0] = (void*)0;
                        if (l_231)
                            break;
                    }
                    (**l_73) |= (0L > p_5);
                    return l_232;
                }
            }
            else
            { /* block id: 153 */
                uint32_t l_235 = 0UL;
                int32_t l_245 = 9L;
                int32_t *l_252 = &l_119[0][0];
                if ((safe_lshift_func_int8_t_s_s(l_235, p_3)))
                { /* block id: 154 */
                    int32_t ***l_241 = (void*)0;
                    int32_t ***l_242[2];
                    int i;
                    for (i = 0; i < 2; i++)
                        l_242[i] = &l_240;
                    if ((((((safe_unary_minus_func_int16_t_s((0L <= p_3))) , (++(*l_39))) != ((l_239 = (void*)0) != (l_243 = l_240))) , p_3) , 9L))
                    { /* block id: 158 */
                        int8_t l_244 = (-1L);
                        g_64 &= ((**l_73) |= l_244);
                        (*l_73) = &g_22;
                        g_22 = (l_245 = ((*g_130) <= 0xE3L));
                        g_246 = (void*)0;
                    }
                    else
                    { /* block id: 165 */
                        uint8_t **l_247 = &l_70;
                        uint8_t **l_249 = &l_70;
                        uint8_t ***l_248 = &l_249;
                        (*l_21) = 0x4F9DCC43L;
                        (*g_246) = (*g_246);
                        (*l_248) = (l_247 = &l_70);
                    }
                }
                else
                { /* block id: 171 */
                    int32_t *l_255[2];
                    int i;
                    for (i = 0; i < 2; i++)
                        l_255[i] = &l_219[0][2];
                    for (p_6 = 3; (p_6 < 28); ++p_6)
                    { /* block id: 174 */
                        l_252 = ((*l_243) = (void*)0);
                        if ((*g_246))
                            break;
                    }
                    for (g_66 = (-14); (g_66 < 53); g_66 = safe_add_func_uint16_t_u_u(g_66, 8))
                    { /* block id: 181 */
                        if (p_4)
                            break;
                        (*l_63) &= p_3;
                    }
                    ++g_256;
                    if ((!0xCA150083L))
                    { /* block id: 186 */
                        return p_5;
                    }
                    else
                    { /* block id: 188 */
                        (*l_73) = &g_64;
                        (**l_243) = 1L;
                        (*l_239) = &l_245;
                    }
                }
            }
            for (p_4 = 9; (p_4 >= 27); ++p_4)
            { /* block id: 197 */
                int32_t *l_263 = &l_219[0][2];
                uint16_t *l_273 = &g_20[0][2];
                int32_t l_275 = (-2L);
            }
        }
        else
        { /* block id: 233 */
            uint8_t l_316 = 0x1BL;
            int32_t l_319[3][3];
            int32_t *l_328 = &l_67[0];
            uint32_t l_331 = 1UL;
            uint32_t *l_359 = &l_86[0];
            int32_t *l_368 = &l_119[1][0];
            int32_t *l_369 = &g_330;
            int i, j;
            for (i = 0; i < 3; i++)
            {
                for (j = 0; j < 3; j++)
                    l_319[i][j] = 0L;
            }
            (*l_63) = (0x28L < p_4);
            for (p_4 = 0; (p_4 < 51); p_4 = safe_add_func_int8_t_s_s(p_4, 4))
            { /* block id: 237 */
                int32_t l_315[2];
                int i;
                for (i = 0; i < 2; i++)
                    l_315[i] = 0x646F1837L;
                for (g_11 = 0; (g_11 <= 0); g_11 += 1)
                { /* block id: 240 */
                    int32_t l_320 = 0x4FE8CBA4L;
                    int32_t l_321 = 0xF64FB653L;
                    int32_t l_322 = (-1L);
                    int i, j;
                }
            }
            (*g_246) = (safe_div_func_int16_t_s_s(g_20[0][2], p_4));
            (*g_246) = (safe_add_func_uint16_t_u_u((safe_rshift_func_int8_t_s_u((**l_243), (+(safe_rshift_func_int8_t_s_s(((((*l_359) = 4294967293UL) , ((safe_lshift_func_uint8_t_u_s(((*l_21) , g_66), (safe_mul_func_uint8_t_u_u(((safe_mul_func_uint16_t_u_u((*l_63), (*l_328))) | (safe_lshift_func_uint8_t_u_u(((((*l_70) = ((((*l_368) = (*g_246)) <= ((*l_369) = 0x3BE51396L)) == (p_4 , 0xE25AL))) || g_57) , g_66), 3))), (*l_63))))) != 1UL)) & (*l_21)), 6))))), 0x7266L));
        }
    }
lbl_374:
    g_330 = (safe_sub_func_int8_t_s_s(((0xD3E1314EL == (&p_4 == &g_20[0][2])) > (((*g_130) = l_372) < p_4)), g_139));
    l_381[0][0] &= ((((g_57 = ((-8L) ^ ((((*g_130) = (((g_256 = (safe_sub_func_uint32_t_u_u(p_5, 0x24227976L))) , 5UL) , p_6)) && p_6) != (0x45L & ((p_6 , p_3) <= p_4))))) > p_6) , g_40) != l_380);
    return p_3;
}




/* ---------------------------------------- */
int main (void)
{
    int i, j;
    int print_hash_value = 0;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_7, "g_7", print_hash_value);
    transparent_crc(g_11, "g_11", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_15[i], "g_15[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc(g_20[i][j], "g_20[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_22, "g_22", print_hash_value);
    transparent_crc(g_40, "g_40", print_hash_value);
    transparent_crc(g_57, "g_57", print_hash_value);
    transparent_crc(g_64, "g_64", print_hash_value);
    transparent_crc(g_66, "g_66", print_hash_value);
    transparent_crc(g_69, "g_69", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc(g_71[i][j], "g_71[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_116, "g_116", print_hash_value);
    transparent_crc(g_121, "g_121", print_hash_value);
    transparent_crc(g_122, "g_122", print_hash_value);
    transparent_crc(g_131, "g_131", print_hash_value);
    transparent_crc(g_139, "g_139", print_hash_value);
    transparent_crc(g_144, "g_144", print_hash_value);
    transparent_crc(g_168, "g_168", print_hash_value);
    transparent_crc(g_169, "g_169", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc(g_213[i][j], "g_213[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_214, "g_214", print_hash_value);
    for (i = 0; i < 3; i++)
    {
        transparent_crc(g_220[i], "g_220[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_256, "g_256", print_hash_value);
    transparent_crc(g_323, "g_323", print_hash_value);
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc(g_324[i][j], "g_324[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    for (i = 0; i < 1; i++)
    {
        transparent_crc(g_325[i], "g_325[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    transparent_crc(g_330, "g_330", print_hash_value);
    transparent_crc(g_406, "g_406", print_hash_value);
    transparent_crc(g_438, "g_438", print_hash_value);
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 0
breakdown:
   depth: 0, occurrence: 105
XXX total union variables: 0

XXX max expression depth: 42
breakdown:
   depth: 1, occurrence: 75
   depth: 2, occurrence: 24
   depth: 3, occurrence: 2
   depth: 5, occurrence: 1
   depth: 7, occurrence: 1
   depth: 10, occurrence: 1
   depth: 14, occurrence: 1
   depth: 16, occurrence: 4
   depth: 17, occurrence: 1
   depth: 19, occurrence: 1
   depth: 21, occurrence: 1
   depth: 22, occurrence: 1
   depth: 42, occurrence: 1

XXX total number of pointers: 117

XXX times a variable address is taken: 45
XXX times a pointer is dereferenced on RHS: 45
breakdown:
   depth: 1, occurrence: 43
   depth: 2, occurrence: 2
XXX times a pointer is dereferenced on LHS: 98
breakdown:
   depth: 1, occurrence: 87
   depth: 2, occurrence: 11
XXX times a pointer is compared with null: 6
XXX times a pointer is compared with address of another variable: 1
XXX times a pointer is compared with another pointer: 2
XXX times a pointer is qualified to be dereferenced: 1130

XXX max dereference level: 3
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 247
   level: 2, occurrence: 50
   level: 3, occurrence: 3
XXX number of pointers point to pointers: 32
XXX number of pointers point to scalars: 85
XXX number of pointers point to structs: 0
XXX percent of pointers has null in alias set: 28.2
XXX average alias set size: 1.28

XXX times a non-volatile is read: 332
XXX times a non-volatile is write: 281
XXX times a volatile is read: 0
XXX    times read thru a pointer: 0
XXX times a volatile is write: 0
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 0
XXX percentage of non-volatile access: 100

XXX forward jumps: 1
XXX backward jumps: 1

XXX stmts: 75
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 6
   depth: 1, occurrence: 11
   depth: 2, occurrence: 11
   depth: 3, occurrence: 8
   depth: 4, occurrence: 14
   depth: 5, occurrence: 25

XXX percentage a fresh-made variable is used: 18.5
XXX percentage an existing variable is used: 81.5
********************* end of statistics **********************/

