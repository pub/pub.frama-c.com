typedef unsigned int size_t;

void* memcpy(void* region1, const void* region2, size_t n)
{
  const char* first = (const char*)region2;
  const char* last = ((const char*)region2) + n;
  char* dest = (char*)region1;
  while (first != last)
    {
      *dest = *first;
      dest++;
      first++;
    }
  return region1;
}
/*
void* memset (void* dest, int val, size_t len)
{
  unsigned char *ptr = (unsigned char*)dest;
  while (len-- > 0)
    *ptr++ = val;
  return dest;
}*/

