unsigned char deflated_buffer[40] = {
  120,
  156,
  67,
  84,
  200,
  77,
  204,
  211,
  81,
  72,
  4,
  81,
  153,
  105,
  249,
  57,
  41,
  32,
  118,
  82,
  98,
  30,
  16,
  34,
  88,
  122,
  10,
  1,
  64,
  50,
  55,
  81,
  143,
  1,
  8,
  0,
  154,
  31,
  14,
  251,
}; 

#include "zlib.h"
#include <stdio.h>

#define CHUNK_OUT 300

z_stream my_strm;
int ret, flush;

main(){
  unsigned char out_buffer[CHUNK_OUT];

  printf("out_buffer: %p\n", out_buffer);

  /* allocate inflate state */
  my_strm.zalloc = Z_NULL;
  my_strm.zfree = Z_NULL;
  my_strm.opaque = Z_NULL;
  my_strm.avail_in = 0;
  my_strm.next_in = Z_NULL;

  ret = inflateInit(&my_strm);

  if (ret != Z_OK)
    return ret;

  my_strm.next_in = deflated_buffer;
  my_strm.avail_in = 40;
  my_strm.next_out = out_buffer;
  my_strm.avail_out = CHUNK_OUT;
	
  ret = inflate(&my_strm, Z_FINISH);

  if (ret == Z_STREAM_ERROR) return ret;
  switch (ret) {
  case Z_NEED_DICT:
    ret = Z_DATA_ERROR;     /* and fall through */
  case Z_DATA_ERROR:
  case Z_MEM_ERROR:
    (void)inflateEnd(&my_strm);
    return ret;
  }
  return Z_OK;
}
 
