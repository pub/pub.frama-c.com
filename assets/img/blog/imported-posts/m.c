typedef unsigned int size_t;

void *
memcpy(void *dst, void *src, size_t length)
{
  int i;
  unsigned char *dp = dst;
  unsigned char *sp = src;

  for (i=0; i<length; i++)
    {
      *dp++ = *sp++;
    }
  return dst;
}

struct S { int *p; int *q; int i; double d; int j; };

extern int Frama_C_entropy_source;

/*@ assigns \result \from a, b, Frama_C_entropy_source;
    assigns Frama_C_entropy_source \from Frama_C_entropy_source;
 */
void *Frama_C_nondet_ptr(void *a, void *b);

/*@ assigns \result \from min, max, Frama_C_entropy_source;
    assigns Frama_C_entropy_source \from Frama_C_entropy_source;
 */
int Frama_C_interval(int min, int max);

/*@
  requires min <= max;
  ensures min <= \result <= max;
  assigns \result \from min, max, Frama_C_entropy_source;
  assigns Frama_C_entropy_source \from Frama_C_entropy_source;
 */
double Frama_C_double_interval(double min, double max);

int a, b;

main(){
  struct S s, d;
  s.p = &a;
  s.q = Frama_C_nondet_ptr(&a, &b);
  s.i = Frama_C_interval(17, 42);
  s.j = 456;
  s.d = Frama_C_double_interval(3.0, 7.0);
  memcpy(&d, &s, sizeof(struct S));
  return 0;
}
