/*
 * This is a RANDOMLY GENERATED PROGRAM.
 *
 * Generator: csmith 2.1.0
 * Git version: e876ed1
 * Options:   --no-volatiles --no-argc -s 0
 * Seed:      0
 */

#include "csmith.h"


static long __undefined;

/* --- Struct/Union Declarations --- */
#pragma pack(push)
#pragma pack(1)
struct S0 {
   int8_t  f0;
};
#pragma pack(pop)

#pragma pack(push)
#pragma pack(1)
struct S1 {
   int8_t  f0;
};
#pragma pack(pop)

struct S2 {
   signed f0 : 29;
   const signed f1 : 3;
   signed : 0;
   signed f2 : 12;
   unsigned f3 : 1;
   signed f4 : 27;
};

#pragma pack(push)
#pragma pack(1)
struct S3 {
   int64_t  f0;
   const struct S2  f1;
   int32_t  f2;
   uint32_t  f3;
};
#pragma pack(pop)

/* --- GLOBAL VARIABLES --- */
static struct S1 g_13 = {-1L};
static uint32_t g_28 = 0xD71B36C9L;
static int64_t g_56 = (-1L);
static int64_t g_58 = 0L;
static int32_t g_60[7][10] = {{0x42FAFECEL, 0x7B11082EL, 0x35D7D231L, 0x5762CC68L, 0xCD7820EAL, 0x5762CC68L, 0x35D7D231L, 0x7B11082EL, 0x42FAFECEL, 0xDAF8D98CL}, {0x42FAFECEL, 0x7B11082EL, 0x35D7D231L, 0x5762CC68L, 0xCD7820EAL, 0x5762CC68L, 0x35D7D231L, 0x7B11082EL, 0x42FAFECEL, 0xDAF8D98CL}, {0x42FAFECEL, 0x7B11082EL, 0x35D7D231L, 0x5762CC68L, 0xCD7820EAL, 0x5762CC68L, 0x35D7D231L, 0x7B11082EL, 0x42FAFECEL, 0xDAF8D98CL}, {0x42FAFECEL, 0x7B11082EL, 0x35D7D231L, 0x5762CC68L, 0xCD7820EAL, 0x5762CC68L, 0x35D7D231L, 0x7B11082EL, 0x42FAFECEL, 0xDAF8D98CL}, {0x42FAFECEL, 0x7B11082EL, 0x35D7D231L, 0x5762CC68L, 0xCD7820EAL, 0x5762CC68L, 0x35D7D231L, 0x7B11082EL, 0x42FAFECEL, 0xDAF8D98CL}, {0x42FAFECEL, 0x7B11082EL, 0x35D7D231L, 0x5762CC68L, 0xCD7820EAL, 0x5762CC68L, 0x35D7D231L, 0x7B11082EL, 0x42FAFECEL, 0xDAF8D98CL}, {0x42FAFECEL, 0x7B11082EL, 0x35D7D231L, 0x5762CC68L, 0xCD7820EAL, 0x5762CC68L, 0x35D7D231L, 0x7B11082EL, 0x42FAFECEL, 0xDAF8D98CL}};
static uint32_t g_65 = 0x2A26B131L;
static uint32_t *g_64 = &g_65;
static struct S3 g_76 = {1L,{-19329,0,62,0,-7041},0L,18446744073709551614UL};
static struct S3 *g_75 = &g_76;
static int8_t g_96 = (-1L);
static uint64_t g_103 = 0x7879CFBC8947EB49LL;
static int32_t *g_111 = (void*)0;
static int32_t ** const g_110[5] = {&g_111, &g_111, &g_111, &g_111, &g_111};
static int16_t g_113[5][2] = {{0x4CA1L, 0xCEA7L}, {0x4CA1L, 0xCEA7L}, {0x4CA1L, 0xCEA7L}, {0x4CA1L, 0xCEA7L}, {0x4CA1L, 0xCEA7L}};
static int16_t g_115 = (-3L);
static int32_t g_116 = 1L;
static uint32_t g_117 = 4294967295UL;
static struct S0 g_119 = {1L};
static uint8_t g_125 = 0x48L;
static uint64_t *g_192[5] = {&g_103, &g_103, &g_103, &g_103, &g_103};
static uint64_t **g_191 = &g_192[3];
static struct S1 g_228 = {0x07L};
static uint32_t g_261 = 1UL;
static struct S1 g_287 = {0xC2L};
static int32_t g_288 = (-7L);
static struct S2 g_415 = {-6669,-0,35,0,-10915};
static struct S2 *g_414 = &g_415;
static uint16_t g_482 = 0UL;
static int64_t g_575 = 0L;
static int64_t * const * const g_597 = (void*)0;
static int64_t * const * const *g_596 = &g_597;
static int32_t g_631 = 0xBE5563CCL;
static uint16_t g_642 = 0x41BFL;
static struct S1 *g_656 = &g_228;
static const int64_t *g_676 = &g_575;
static const int64_t **g_675 = &g_676;
static const uint64_t ***g_681 = (void*)0;
static const uint64_t ****g_680 = &g_681;
static struct S0 *g_777 = &g_119;
static struct S0 * const *g_776 = &g_777;
static int16_t *g_807[4][7][6] = {{{&g_115, (void*)0, &g_115, (void*)0, &g_115, &g_113[2][0]}, {&g_115, (void*)0, &g_115, (void*)0, &g_115, &g_113[2][0]}, {&g_115, (void*)0, &g_115, (void*)0, &g_115, &g_113[2][0]}, {&g_115, (void*)0, &g_115, (void*)0, &g_115, &g_113[2][0]}, {&g_115, (void*)0, &g_115, (void*)0, &g_115, &g_113[2][0]}, {&g_115, (void*)0, &g_115, (void*)0, &g_115, &g_113[2][0]}, {&g_115, (void*)0, &g_115, (void*)0, &g_115, &g_113[2][0]}}, {{&g_115, (void*)0, &g_115, (void*)0, &g_115, &g_113[2][0]}, {&g_115, (void*)0, &g_115, (void*)0, &g_115, &g_113[2][0]}, {&g_115, (void*)0, &g_115, (void*)0, &g_115, &g_113[2][0]}, {&g_115, (void*)0, &g_115, (void*)0, &g_115, &g_113[2][0]}, {&g_115, (void*)0, &g_115, (void*)0, &g_115, &g_113[2][0]}, {&g_115, (void*)0, &g_115, (void*)0, &g_115, &g_113[2][0]}, {&g_115, (void*)0, &g_115, (void*)0, &g_115, &g_113[2][0]}}, {{&g_115, (void*)0, &g_115, (void*)0, &g_115, &g_113[2][0]}, {&g_115, (void*)0, &g_115, (void*)0, &g_115, &g_113[2][0]}, {&g_115, (void*)0, &g_115, (void*)0, &g_115, &g_113[2][0]}, {&g_115, (void*)0, &g_115, (void*)0, &g_115, &g_113[2][0]}, {&g_115, (void*)0, &g_115, (void*)0, &g_115, &g_113[2][0]}, {&g_115, (void*)0, &g_115, (void*)0, &g_115, &g_113[2][0]}, {&g_115, (void*)0, &g_115, (void*)0, &g_115, &g_113[2][0]}}, {{&g_115, (void*)0, &g_115, (void*)0, &g_115, &g_113[2][0]}, {&g_115, (void*)0, &g_115, (void*)0, &g_115, &g_113[2][0]}, {&g_115, (void*)0, &g_115, (void*)0, &g_115, &g_113[2][0]}, {&g_115, (void*)0, &g_115, (void*)0, &g_115, &g_113[2][0]}, {&g_115, (void*)0, &g_115, (void*)0, &g_115, &g_113[2][0]}, {&g_115, (void*)0, &g_115, (void*)0, &g_115, &g_113[2][0]}, {&g_115, (void*)0, &g_115, (void*)0, &g_115, &g_113[2][0]}}};
static int16_t **g_806[1] = {&g_807[2][1][1]};
static int16_t ***g_805 = &g_806[0];
static int16_t ** const *g_931[5] = {(void*)0, (void*)0, (void*)0, (void*)0, (void*)0};
static const uint32_t g_965 = 0x75DA97C6L;
static const uint32_t *g_964 = &g_965;
static uint32_t g_967 = 4UL;
static uint32_t * const g_966 = &g_967;
static uint32_t g_1068 = 3UL;
static struct S1 g_1075 = {4L};
static uint64_t g_1095 = 0x774C93F58D9276F2LL;
static uint32_t *g_1189 = &g_117;
static uint32_t **g_1188 = &g_1189;
static int32_t g_1210[4] = {0x23D7C147L, 0xC203AA93L, 0x23D7C147L, 0xC203AA93L};
static int64_t g_1217[1][3] = {{(-1L), (-1L), (-1L)}};


/* --- FORWARD DECLARATIONS --- */
static struct S0  func_1(void);
static int32_t  func_2(struct S3  p_3, int32_t  p_4, int64_t  p_5, int16_t  p_6);
static struct S3  func_7(struct S1  p_8, int16_t  p_9, uint16_t  p_10, const struct S2  p_11, uint16_t  p_12);
static struct S2  func_14(int64_t  p_15, int32_t  p_16, uint32_t  p_17, int16_t  p_18, uint32_t  p_19);
static int64_t  func_20(uint8_t  p_21, const uint32_t  p_22, struct S1  p_23, struct S2  p_24);
static struct S1  func_32(uint32_t * p_33);
static uint32_t * func_34(uint16_t  p_35, uint32_t * const  p_36);
static uint32_t * func_37(uint32_t  p_38, struct S0  p_39, const uint32_t * p_40, uint32_t * p_41);
static const uint8_t  func_47(int32_t  p_48);
static const int16_t  func_52(int64_t  p_53);


/* --- FUNCTIONS --- */
/* ------------------------------------------ */
/* 
 * reads : g_13 g_28 g_56 g_60 g_64 g_75 g_76.f1.f4 g_76.f3 g_96 g_76.f2 g_76.f1.f1 g_110 g_113 g_76.f1.f2 g_65 g_76.f1.f3 g_119 g_125 g_116 g_191 g_261 g_117 g_288 g_115 g_76.f1 g_228.f0 g_103 g_287 g_76 g_414 g_415.f4 g_228 g_482 g_192 g_58 g_111 g_415.f0 g_596 g_415 g_642 g_597 g_656 g_675 g_680 g_676 g_575 g_631 g_776 g_805 g_964 g_966 g_777 g_967 g_1075 g_965 g_1095 g_1188 g_1189 g_1210
 * writes: g_28 g_56 g_58 g_60 g_64 g_75 g_96 g_76.f2 g_103 g_113 g_115 g_116 g_117 g_125 g_191 g_228 g_119 g_111 g_261 g_65 g_287.f0 g_287 g_414 g_76.f0 g_76.f3 g_642 g_656 g_675 g_680 g_482 g_931 g_631 g_1068 g_13 g_1210
 */
static struct S0  func_1(void)
{ /* block id: 0 */
    uint32_t l_25 = 4294967290UL;
    uint32_t *l_26[6];
    uint32_t *l_27[3][3] = {{&g_28, &g_28, &g_28}, {&g_28, &g_28, &g_28}, {&g_28, &g_28, &g_28}};
    uint32_t *l_29 = &g_28;
    uint64_t *l_102 = &g_103;
    int16_t *l_112 = &g_113[2][0];
    int16_t *l_114[5];
    uint8_t l_118 = 0x86L;
    uint32_t **l_963 = &l_27[2][0];
    int32_t l_988 = 0xAEDD27D2L;
    int32_t *l_1209 = &g_1210[3];
    struct S0 l_1213[7] = {{9L}, {-1L}, {9L}, {-1L}, {9L}, {-1L}, {9L}};
    const struct S0 *l_1215 = (void*)0;
    const struct S0 **l_1214 = &l_1215;
    int32_t *l_1216[2][9] = {{&g_116, (void*)0, &g_116, (void*)0, &g_116, (void*)0, &g_116, (void*)0, &g_116}, {&g_116, (void*)0, &g_116, (void*)0, &g_116, (void*)0, &g_116, (void*)0, &g_116}};
    int32_t *l_1218 = (void*)0;
    int i, j;
    for (i = 0; i < 6; i++)
        l_26[i] = (void*)0;
    for (i = 0; i < 5; i++)
        l_114[i] = &g_115;
    (*l_1209) ^= func_2(func_7(g_13, g_13.f0, g_13.f0, func_14(func_20(l_25, ((*l_29)--), func_32(func_34((((((*l_963) = func_37((safe_unary_minus_func_int32_t_s((safe_rshift_func_uint16_t_u_s((safe_add_func_uint8_t_u_u(func_47(l_25), (safe_mul_func_uint16_t_u_u(((safe_add_func_uint64_t_u_u(((*l_102) = 0xC3DD168C87B4EC18LL), (safe_mul_func_int16_t_s_s((((g_117 = (safe_sub_func_int16_t_s_s(((safe_lshift_func_int16_t_s_u((g_116 = (g_115 = ((*l_112) ^= (((g_76.f1.f1 , g_110[2]) != &g_111) , 0xF5F5L)))), l_25)) || g_76.f1.f1), g_76.f1.f2))) < l_118) >= (-8L)), g_65)))) < l_25), g_13.f0)))), g_76.f1.f3)))), g_119, l_26[1], &g_65)) == g_964) == 0x24AE7014CC3713C7LL) | l_25), g_966)), g_415), l_988, g_967, l_118, l_25), l_25), l_118, (*g_676), l_988);
    l_1218 = l_1209;
    return (**g_776);
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int32_t  func_2(struct S3  p_3, int32_t  p_4, int64_t  p_5, int16_t  p_6)
{ /* block id: 665 */
    int16_t l_1206 = 0x5E39L;
    int32_t *l_1207 = (void*)0;
    int32_t *l_1208[7] = {(void*)0, (void*)0, &g_116, (void*)0, (void*)0, &g_116, (void*)0};
    int i;
    p_3.f2 &= l_1206;
    return p_3.f3;
}


/* ------------------------------------------ */
/* 
 * reads : g_631 g_575 g_76.f1 g_191 g_192 g_60 g_58 g_287.f0 g_415.f0 g_1075 g_656 g_776 g_777 g_115 g_964 g_965 g_13.f0 g_1095 g_676 g_56 g_64 g_75 g_76.f3 g_96 g_76.f2 g_116 g_642 g_119 g_482 g_65 g_13 g_103 g_1188 g_1189 g_117 g_228.f0
 * writes: g_58 g_125 g_119.f0 g_103 g_1068 g_60 g_287.f0 g_631 g_13 g_656 g_56 g_64 g_75 g_96 g_76.f2 g_116 g_642 g_482 g_65 g_119
 */
static struct S3  func_7(struct S1  p_8, int16_t  p_9, uint16_t  p_10, const struct S2  p_11, uint16_t  p_12)
{ /* block id: 559 */
    uint32_t l_1023[2];
    int32_t l_1038 = 0x4F29CAE0L;
    int32_t l_1040 = 0x03C360D2L;
    int32_t l_1041 = (-3L);
    int32_t l_1042 = 0x26500479L;
    int32_t l_1043[10][6] = {{0xE3CC1DDDL, (-7L), (-1L), 0xE40DA30FL, (-1L), (-7L)}, {0xE3CC1DDDL, (-7L), (-1L), 0xE40DA30FL, (-1L), (-7L)}, {0xE3CC1DDDL, (-7L), (-1L), 0xE40DA30FL, (-1L), (-7L)}, {0xE3CC1DDDL, (-7L), (-1L), 0xE40DA30FL, (-1L), (-7L)}, {0xE3CC1DDDL, (-7L), (-1L), 0xE40DA30FL, (-1L), (-7L)}, {0xE3CC1DDDL, (-7L), (-1L), 0xE40DA30FL, (-1L), (-7L)}, {0xE3CC1DDDL, (-7L), (-1L), 0xE40DA30FL, (-1L), (-7L)}, {0xE3CC1DDDL, (-7L), (-1L), 0xE40DA30FL, (-1L), (-7L)}, {0xE3CC1DDDL, (-7L), (-1L), 0xE40DA30FL, (-1L), (-7L)}, {0xE3CC1DDDL, (-7L), (-1L), 0xE40DA30FL, (-1L), (-7L)}};
    uint64_t l_1113 = 0x3DC1588847E898A5LL;
    struct S1 l_1155 = {-2L};
    uint64_t l_1158 = 18446744073709551613UL;
    uint32_t l_1167 = 0x8E6554FCL;
    uint64_t ***l_1176 = (void*)0;
    uint64_t ****l_1175[2][8] = {{&l_1176, &l_1176, &l_1176, &l_1176, &l_1176, &l_1176, &l_1176, &l_1176}, {&l_1176, &l_1176, &l_1176, &l_1176, &l_1176, &l_1176, &l_1176, &l_1176}};
    uint64_t **** const *l_1174[8] = {&l_1175[0][3], (void*)0, &l_1175[0][3], (void*)0, &l_1175[0][3], (void*)0, &l_1175[0][3], (void*)0};
    int32_t *l_1187[2];
    uint8_t l_1190 = 1UL;
    struct S1 l_1195[10][8] = {{{0x5AL}, {0xF0L}, {-1L}, {0xEEL}, {0x5AL}, {-1L}, {0x3CL}, {-1L}}, {{0x5AL}, {0xF0L}, {-1L}, {0xEEL}, {0x5AL}, {-1L}, {0x3CL}, {-1L}}, {{0x5AL}, {0xF0L}, {-1L}, {0xEEL}, {0x5AL}, {-1L}, {0x3CL}, {-1L}}, {{0x5AL}, {0xF0L}, {-1L}, {0xEEL}, {0x5AL}, {-1L}, {0x3CL}, {-1L}}, {{0x5AL}, {0xF0L}, {-1L}, {0xEEL}, {0x5AL}, {-1L}, {0x3CL}, {-1L}}, {{0x5AL}, {0xF0L}, {-1L}, {0xEEL}, {0x5AL}, {-1L}, {0x3CL}, {-1L}}, {{0x5AL}, {0xF0L}, {-1L}, {0xEEL}, {0x5AL}, {-1L}, {0x3CL}, {-1L}}, {{0x5AL}, {0xF0L}, {-1L}, {0xEEL}, {0x5AL}, {-1L}, {0x3CL}, {-1L}}, {{0x5AL}, {0xF0L}, {-1L}, {0xEEL}, {0x5AL}, {-1L}, {0x3CL}, {-1L}}, {{0x5AL}, {0xF0L}, {-1L}, {0xEEL}, {0x5AL}, {-1L}, {0x3CL}, {-1L}}};
    struct S3 l_1205 = {0xA4BA59FCAC4C7941LL,{-13085,-0,-29,0,4797},1L,18446744073709551607UL};
    int i, j;
    for (i = 0; i < 2; i++)
        l_1023[i] = 4294967292UL;
    for (i = 0; i < 2; i++)
        l_1187[i] = &l_1043[5][4];
    if (l_1023[0])
    { /* block id: 560 */
        int8_t l_1044 = 0xD7L;
        int32_t l_1045[3];
        uint64_t **l_1058[6][9] = {{&g_192[3], (void*)0, &g_192[0], (void*)0, &g_192[3], &g_192[1], &g_192[3], (void*)0, &g_192[0]}, {&g_192[3], (void*)0, &g_192[0], (void*)0, &g_192[3], &g_192[1], &g_192[3], (void*)0, &g_192[0]}, {&g_192[3], (void*)0, &g_192[0], (void*)0, &g_192[3], &g_192[1], &g_192[3], (void*)0, &g_192[0]}, {&g_192[3], (void*)0, &g_192[0], (void*)0, &g_192[3], &g_192[1], &g_192[3], (void*)0, &g_192[0]}, {&g_192[3], (void*)0, &g_192[0], (void*)0, &g_192[3], &g_192[1], &g_192[3], (void*)0, &g_192[0]}, {&g_192[3], (void*)0, &g_192[0], (void*)0, &g_192[3], &g_192[1], &g_192[3], (void*)0, &g_192[0]}};
        uint32_t **l_1088 = &g_64;
        uint32_t ***l_1087 = &l_1088;
        struct S1 l_1089[3] = {{-1L}, {-1L}, {-1L}};
        int16_t l_1112 = 0x3E55L;
        int i, j;
        for (i = 0; i < 3; i++)
            l_1045[i] = 0L;
        for (p_10 = 0; (p_10 == 12); p_10 = safe_add_func_int16_t_s_s(p_10, 1))
        { /* block id: 563 */
            uint32_t l_1033 = 9UL;
            int32_t *l_1036 = &g_631;
            int32_t l_1039[1][5] = {{0x3AD01BE5L, 0xBCF22934L, 0x3AD01BE5L, 0xBCF22934L, 0x3AD01BE5L}};
            uint32_t l_1066 = 0x81D80D0AL;
            int i, j;
            for (g_58 = 0; (g_58 <= 4); g_58 += 1)
            { /* block id: 566 */
                int32_t *l_1026 = &g_116;
                int32_t *l_1027 = (void*)0;
                int32_t *l_1028 = (void*)0;
                int32_t *l_1029 = &g_60[0][8];
                int32_t *l_1030 = &g_116;
                int32_t *l_1031 = &g_60[0][9];
                int32_t *l_1032 = &g_631;
                int32_t **l_1037[6] = {&l_1031, &l_1031, &l_1036, &l_1031, &l_1031, &l_1036};
                uint32_t l_1046 = 0x63F8B6B9L;
                uint8_t *l_1049 = &g_125;
                int8_t *l_1067 = &g_119.f0;
                uint16_t *l_1070 = &g_642;
                uint16_t **l_1069 = &l_1070;
                int i;
                l_1033++;
                l_1036 = l_1036;
                l_1046++;
                if ((((((*l_1049) = g_631) | (safe_rshift_func_int16_t_s_u(func_52(((safe_div_func_uint16_t_u_u((safe_mod_func_uint32_t_u_u(g_575, 1UL)), (safe_add_func_uint8_t_u_u(((void*)0 != l_1058[2][7]), (g_76.f1 , ((safe_unary_minus_func_int16_t_s((safe_sub_func_uint64_t_u_u((g_1068 = ((**g_191) = (safe_div_func_uint16_t_u_u(((safe_lshift_func_int8_t_s_u(l_1066, p_11.f0)) & ((*l_1067) = 0x0CL)), l_1023[0])))), l_1040)))) || p_10)))))) >= p_11.f0)), g_58))) | p_8.f0) , p_9))
                { /* block id: 574 */
                    uint16_t ***l_1071 = (void*)0;
                    uint16_t ***l_1072 = (void*)0;
                    uint16_t ***l_1073[5][2][7] = {{{&l_1069, (void*)0, &l_1069, &l_1069, (void*)0, &l_1069, (void*)0}, {&l_1069, (void*)0, &l_1069, &l_1069, (void*)0, &l_1069, (void*)0}}, {{&l_1069, (void*)0, &l_1069, &l_1069, (void*)0, &l_1069, (void*)0}, {&l_1069, (void*)0, &l_1069, &l_1069, (void*)0, &l_1069, (void*)0}}, {{&l_1069, (void*)0, &l_1069, &l_1069, (void*)0, &l_1069, (void*)0}, {&l_1069, (void*)0, &l_1069, &l_1069, (void*)0, &l_1069, (void*)0}}, {{&l_1069, (void*)0, &l_1069, &l_1069, (void*)0, &l_1069, (void*)0}, {&l_1069, (void*)0, &l_1069, &l_1069, (void*)0, &l_1069, (void*)0}}, {{&l_1069, (void*)0, &l_1069, &l_1069, (void*)0, &l_1069, (void*)0}, {&l_1069, (void*)0, &l_1069, &l_1069, (void*)0, &l_1069, (void*)0}}};
                    int i, j, k;
                    l_1069 = l_1069;
                }
                else
                { /* block id: 576 */
                    uint64_t l_1074 = 18446744073709551615UL;
                    int32_t l_1084 = 0xC863E4C8L;
                    uint32_t **l_1086 = (void*)0;
                    uint32_t ***l_1085 = &l_1086;
                    for (g_287.f0 = 1; (g_287.f0 >= 0); g_287.f0 -= 1)
                    { /* block id: 579 */
                        int i;
                        (*l_1031) = l_1023[g_287.f0];
                    }
                    (*l_1036) = l_1074;
                    if (func_52((func_20(((((((g_76.f1.f3 & (l_1045[1] = (func_20(g_415.f0, (*l_1036), ((*g_656) = g_1075), p_11) >= ((((*l_1031) = ((safe_mod_func_int16_t_s_s((safe_mod_func_uint8_t_u_u((safe_mul_func_uint8_t_u_u(l_1044, (safe_div_func_int32_t_s_s((0xB51CL || ((((void*)0 != (*g_776)) , (void*)0) != (void*)0)), 4294967289UL)))), g_115)), l_1074)) , (*l_1036))) ^ p_11.f4) , l_1084)))) , l_1085) == l_1087) > 0x6F641405L) || p_11.f4) != p_11.f0), (*g_964), l_1089[0], p_11) || l_1041)))
                    { /* block id: 586 */
                        (*g_656) = p_8;
                    }
                    else
                    { /* block id: 588 */
                        struct S1 **l_1090 = &g_656;
                        (*l_1090) = &p_8;
                    }
                }
            }
            if (l_1045[0])
                break;
            for (g_13.f0 = (-14); (g_13.f0 <= (-6)); ++g_13.f0)
            { /* block id: 596 */
                struct S3 l_1098 = {0x9E3A02D6A308386ELL,{-14269,1,62,0,5110},0xAF7B7FF6L,7UL};
                int32_t *l_1099 = &g_116;
                int32_t *l_1100 = (void*)0;
                int32_t *l_1101 = &l_1043[5][3];
                int32_t *l_1102 = &l_1039[0][0];
                int32_t *l_1103 = &l_1045[1];
                int32_t *l_1104 = &l_1045[1];
                int32_t *l_1105 = &l_1041;
                int32_t *l_1106 = &g_60[4][2];
                int32_t *l_1107 = (void*)0;
                int32_t *l_1108 = &g_60[0][0];
                int32_t *l_1109 = &l_1045[0];
                int32_t *l_1110 = &g_60[0][8];
                int32_t *l_1111[9][10][2] = {{{&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}}, {{&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}}, {{&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}}, {{&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}}, {{&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}}, {{&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}}, {{&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}}, {{&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}}, {{&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}, {&g_631, &l_1098.f2}}};
                int i, j, k;
                (*l_1099) = ((*l_1036) = ((p_12 <= (func_47(((*l_1036) = (((safe_add_func_uint32_t_u_u(g_1095, (safe_div_func_int64_t_s_s(p_8.f0, p_9)))) == p_11.f1) < (l_1089[0].f0 ^ (l_1098 , (*g_676)))))) == p_11.f0)) , l_1040));
                if ((*l_1099))
                    break;
                --l_1113;
            }
        }
    }
    else
    { /* block id: 604 */
        uint64_t l_1127 = 0xD084064A417B99C7LL;
        int32_t l_1128[10][9][2] = {{{0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}}, {{0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}}, {{0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}}, {{0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}}, {{0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}}, {{0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}}, {{0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}}, {{0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}}, {{0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}}, {{0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}, {0xC869AF3AL, 1L}}};
        uint64_t **l_1156 = &g_192[3];
        uint32_t **l_1183 = &g_64;
        uint32_t ***l_1182 = &l_1183;
        int32_t l_1184 = (-5L);
        int i, j, k;
        for (g_642 = (-5); (g_642 != 18); g_642 = safe_add_func_int16_t_s_s(g_642, 2))
        { /* block id: 607 */
            uint8_t l_1131 = 0x3FL;
            uint64_t **l_1150 = &g_192[3];
            int32_t l_1153 = 0L;
            int32_t l_1164[3];
            uint32_t *l_1177[2];
            int32_t *l_1185 = (void*)0;
            int32_t *l_1186[5][7][4] = {{{&g_60[0][8], &l_1128[6][0][0], &g_60[0][8], &l_1164[2]}, {&g_60[0][8], &l_1128[6][0][0], &g_60[0][8], &l_1164[2]}, {&g_60[0][8], &l_1128[6][0][0], &g_60[0][8], &l_1164[2]}, {&g_60[0][8], &l_1128[6][0][0], &g_60[0][8], &l_1164[2]}, {&g_60[0][8], &l_1128[6][0][0], &g_60[0][8], &l_1164[2]}, {&g_60[0][8], &l_1128[6][0][0], &g_60[0][8], &l_1164[2]}, {&g_60[0][8], &l_1128[6][0][0], &g_60[0][8], &l_1164[2]}}, {{&g_60[0][8], &l_1128[6][0][0], &g_60[0][8], &l_1164[2]}, {&g_60[0][8], &l_1128[6][0][0], &g_60[0][8], &l_1164[2]}, {&g_60[0][8], &l_1128[6][0][0], &g_60[0][8], &l_1164[2]}, {&g_60[0][8], &l_1128[6][0][0], &g_60[0][8], &l_1164[2]}, {&g_60[0][8], &l_1128[6][0][0], &g_60[0][8], &l_1164[2]}, {&g_60[0][8], &l_1128[6][0][0], &g_60[0][8], &l_1164[2]}, {&g_60[0][8], &l_1128[6][0][0], &g_60[0][8], &l_1164[2]}}, {{&g_60[0][8], &l_1128[6][0][0], &g_60[0][8], &l_1164[2]}, {&g_60[0][8], &l_1128[6][0][0], &g_60[0][8], &l_1164[2]}, {&g_60[0][8], &l_1128[6][0][0], &g_60[0][8], &l_1164[2]}, {&g_60[0][8], &l_1128[6][0][0], &g_60[0][8], &l_1164[2]}, {&g_60[0][8], &l_1128[6][0][0], &g_60[0][8], &l_1164[2]}, {&g_60[0][8], &l_1128[6][0][0], &g_60[0][8], &l_1164[2]}, {&g_60[0][8], &l_1128[6][0][0], &g_60[0][8], &l_1164[2]}}, {{&g_60[0][8], &l_1128[6][0][0], &g_60[0][8], &l_1164[2]}, {&g_60[0][8], &l_1128[6][0][0], &g_60[0][8], &l_1164[2]}, {&g_60[0][8], &l_1128[6][0][0], &g_60[0][8], &l_1164[2]}, {&g_60[0][8], &l_1128[6][0][0], &g_60[0][8], &l_1164[2]}, {&g_60[0][8], &l_1128[6][0][0], &g_60[0][8], &l_1164[2]}, {&g_60[0][8], &l_1128[6][0][0], &g_60[0][8], &l_1164[2]}, {&g_60[0][8], &l_1128[6][0][0], &g_60[0][8], &l_1164[2]}}, {{&g_60[0][8], &l_1128[6][0][0], &g_60[0][8], &l_1164[2]}, {&g_60[0][8], &l_1128[6][0][0], &g_60[0][8], &l_1164[2]}, {&g_60[0][8], &l_1128[6][0][0], &g_60[0][8], &l_1164[2]}, {&g_60[0][8], &l_1128[6][0][0], &g_60[0][8], &l_1164[2]}, {&g_60[0][8], &l_1128[6][0][0], &g_60[0][8], &l_1164[2]}, {&g_60[0][8], &l_1128[6][0][0], &g_60[0][8], &l_1164[2]}, {&g_60[0][8], &l_1128[6][0][0], &g_60[0][8], &l_1164[2]}}};
            int i, j, k;
            for (i = 0; i < 3; i++)
                l_1164[i] = 0x43042B49L;
            for (i = 0; i < 2; i++)
                l_1177[i] = &l_1023[0];
            for (l_1040 = (-12); (l_1040 < 14); l_1040 = safe_add_func_int64_t_s_s(l_1040, 1))
            { /* block id: 610 */
                uint8_t l_1120[6] = {0xB6L, 0xF0L, 0xB6L, 0xF0L, 0xB6L, 0xF0L};
                uint32_t * const l_1135[4][10] = {{(void*)0, &g_28, &g_261, &g_28, &g_65, &g_76.f3, &g_261, (void*)0, &g_76.f3, &g_967}, {(void*)0, &g_28, &g_261, &g_28, &g_65, &g_76.f3, &g_261, (void*)0, &g_76.f3, &g_967}, {(void*)0, &g_28, &g_261, &g_28, &g_65, &g_76.f3, &g_261, (void*)0, &g_76.f3, &g_967}, {(void*)0, &g_28, &g_261, &g_28, &g_65, &g_76.f3, &g_261, (void*)0, &g_76.f3, &g_967}};
                int32_t *l_1137 = &l_1042;
                uint64_t ** const l_1146[8][6] = {{&g_192[0], &g_192[4], &g_192[0], (void*)0, &g_192[1], &g_192[3]}, {&g_192[0], &g_192[4], &g_192[0], (void*)0, &g_192[1], &g_192[3]}, {&g_192[0], &g_192[4], &g_192[0], (void*)0, &g_192[1], &g_192[3]}, {&g_192[0], &g_192[4], &g_192[0], (void*)0, &g_192[1], &g_192[3]}, {&g_192[0], &g_192[4], &g_192[0], (void*)0, &g_192[1], &g_192[3]}, {&g_192[0], &g_192[4], &g_192[0], (void*)0, &g_192[1], &g_192[3]}, {&g_192[0], &g_192[4], &g_192[0], (void*)0, &g_192[1], &g_192[3]}, {&g_192[0], &g_192[4], &g_192[0], (void*)0, &g_192[1], &g_192[3]}};
                int32_t *l_1165 = &l_1164[2];
                int32_t *l_1166[2];
                int i, j;
                for (i = 0; i < 2; i++)
                    l_1166[i] = &l_1042;
                --l_1120[1];
                for (l_1113 = 22; (l_1113 > 51); ++l_1113)
                { /* block id: 614 */
                    int32_t *l_1126[10] = {(void*)0, (void*)0, (void*)0, (void*)0, (void*)0, (void*)0, (void*)0, (void*)0, (void*)0, (void*)0};
                    uint64_t **l_1157 = (void*)0;
                    int i;
                    for (l_1042 = 1; (l_1042 >= 0); l_1042 -= 1)
                    { /* block id: 617 */
                        struct S3 l_1125[4] = {{0xC479EB3457855743LL,{20849,0,1,0,8329},-1L,1UL}, {0xC479EB3457855743LL,{20849,0,1,0,8329},-1L,1UL}, {0xC479EB3457855743LL,{20849,0,1,0,8329},-1L,1UL}, {0xC479EB3457855743LL,{20849,0,1,0,8329},-1L,1UL}};
                        int i;
                        return l_1125[3];
                    }
                    l_1128[6][0][0] = (l_1127 = 1L);
                    for (p_10 = (-4); (p_10 > 58); p_10 = safe_add_func_uint16_t_u_u(p_10, 9))
                    { /* block id: 624 */
                        uint8_t l_1134[2][7] = {{0xB8L, 246UL, 1UL, 246UL, 0xB8L, 0x61L, 0xB8L}, {0xB8L, 246UL, 1UL, 246UL, 0xB8L, 0x61L, 0xB8L}};
                        int32_t **l_1136 = (void*)0;
                        int32_t **l_1138 = &l_1126[6];
                        int i, j;
                        l_1131--;
                        l_1137 = func_34(g_965, func_34(l_1134[0][4], l_1135[0][2]));
                        (*l_1138) = (void*)0;
                    }
                    for (g_103 = 0; (g_103 > 16); g_103 = safe_add_func_int64_t_s_s(g_103, 2))
                    { /* block id: 631 */
                        uint64_t ** const l_1145 = &g_192[3];
                        uint16_t *l_1147 = &g_482;
                        uint8_t *l_1151 = (void*)0;
                        uint8_t *l_1152[7] = {&l_1120[5], &g_125, &l_1120[5], &g_125, &l_1120[5], &g_125, &l_1120[5]};
                        const int32_t l_1154[5][8][6] = {{{0xAB0CC9B2L, 0x86D89478L, 0x141703FBL, (-6L), 0L, 0xBDECCC14L}, {0xAB0CC9B2L, 0x86D89478L, 0x141703FBL, (-6L), 0L, 0xBDECCC14L}, {0xAB0CC9B2L, 0x86D89478L, 0x141703FBL, (-6L), 0L, 0xBDECCC14L}, {0xAB0CC9B2L, 0x86D89478L, 0x141703FBL, (-6L), 0L, 0xBDECCC14L}, {0xAB0CC9B2L, 0x86D89478L, 0x141703FBL, (-6L), 0L, 0xBDECCC14L}, {0xAB0CC9B2L, 0x86D89478L, 0x141703FBL, (-6L), 0L, 0xBDECCC14L}, {0xAB0CC9B2L, 0x86D89478L, 0x141703FBL, (-6L), 0L, 0xBDECCC14L}, {0xAB0CC9B2L, 0x86D89478L, 0x141703FBL, (-6L), 0L, 0xBDECCC14L}}, {{0xAB0CC9B2L, 0x86D89478L, 0x141703FBL, (-6L), 0L, 0xBDECCC14L}, {0xAB0CC9B2L, 0x86D89478L, 0x141703FBL, (-6L), 0L, 0xBDECCC14L}, {0xAB0CC9B2L, 0x86D89478L, 0x141703FBL, (-6L), 0L, 0xBDECCC14L}, {0xAB0CC9B2L, 0x86D89478L, 0x141703FBL, (-6L), 0L, 0xBDECCC14L}, {0xAB0CC9B2L, 0x86D89478L, 0x141703FBL, (-6L), 0L, 0xBDECCC14L}, {0xAB0CC9B2L, 0x86D89478L, 0x141703FBL, (-6L), 0L, 0xBDECCC14L}, {0xAB0CC9B2L, 0x86D89478L, 0x141703FBL, (-6L), 0L, 0xBDECCC14L}, {0xAB0CC9B2L, 0x86D89478L, 0x141703FBL, (-6L), 0L, 0xBDECCC14L}}, {{0xAB0CC9B2L, 0x86D89478L, 0x141703FBL, (-6L), 0L, 0xBDECCC14L}, {0xAB0CC9B2L, 0x86D89478L, 0x141703FBL, (-6L), 0L, 0xBDECCC14L}, {0xAB0CC9B2L, 0x86D89478L, 0x141703FBL, (-6L), 0L, 0xBDECCC14L}, {0xAB0CC9B2L, 0x86D89478L, 0x141703FBL, (-6L), 0L, 0xBDECCC14L}, {0xAB0CC9B2L, 0x86D89478L, 0x141703FBL, (-6L), 0L, 0xBDECCC14L}, {0xAB0CC9B2L, 0x86D89478L, 0x141703FBL, (-6L), 0L, 0xBDECCC14L}, {0xAB0CC9B2L, 0x86D89478L, 0x141703FBL, (-6L), 0L, 0xBDECCC14L}, {0xAB0CC9B2L, 0x86D89478L, 0x141703FBL, (-6L), 0L, 0xBDECCC14L}}, {{0xAB0CC9B2L, 0x86D89478L, 0x141703FBL, (-6L), 0L, 0xBDECCC14L}, {0xAB0CC9B2L, 0x86D89478L, 0x141703FBL, (-6L), 0L, 0xBDECCC14L}, {0xAB0CC9B2L, 0x86D89478L, 0x141703FBL, (-6L), 0L, 0xBDECCC14L}, {0xAB0CC9B2L, 0x86D89478L, 0x141703FBL, (-6L), 0L, 0xBDECCC14L}, {0xAB0CC9B2L, 0x86D89478L, 0x141703FBL, (-6L), 0L, 0xBDECCC14L}, {0xAB0CC9B2L, 0x86D89478L, 0x141703FBL, (-6L), 0L, 0xBDECCC14L}, {0xAB0CC9B2L, 0x86D89478L, 0x141703FBL, (-6L), 0L, 0xBDECCC14L}, {0xAB0CC9B2L, 0x86D89478L, 0x141703FBL, (-6L), 0L, 0xBDECCC14L}}, {{0xAB0CC9B2L, 0x86D89478L, 0x141703FBL, (-6L), 0L, 0xBDECCC14L}, {0xAB0CC9B2L, 0x86D89478L, 0x141703FBL, (-6L), 0L, 0xBDECCC14L}, {0xAB0CC9B2L, 0x86D89478L, 0x141703FBL, (-6L), 0L, 0xBDECCC14L}, {0xAB0CC9B2L, 0x86D89478L, 0x141703FBL, (-6L), 0L, 0xBDECCC14L}, {0xAB0CC9B2L, 0x86D89478L, 0x141703FBL, (-6L), 0L, 0xBDECCC14L}, {0xAB0CC9B2L, 0x86D89478L, 0x141703FBL, (-6L), 0L, 0xBDECCC14L}, {0xAB0CC9B2L, 0x86D89478L, 0x141703FBL, (-6L), 0L, 0xBDECCC14L}, {0xAB0CC9B2L, 0x86D89478L, 0x141703FBL, (-6L), 0L, 0xBDECCC14L}}};
                        uint32_t l_1159 = 0UL;
                        int i, j, k;
                        l_1159 = ((safe_sub_func_int8_t_s_s((safe_sub_func_uint32_t_u_u((((((l_1145 != l_1146[1][1]) || (--(*l_1147))) , func_20((g_115 , ((l_1153 &= ((void*)0 != l_1150)) && l_1038)), l_1154[4][5][2], l_1155, p_11)) , l_1156) != l_1157), (*l_1137))), g_631)) < l_1158);
                        l_1164[2] |= ((((safe_rshift_func_int8_t_s_s(func_20(l_1155.f0, l_1153, ((*g_656) = (*g_656)), p_11), 2)) || 0x5A573FD2L) || (safe_lshift_func_uint8_t_u_s(p_11.f0, 6))) ^ (*l_1137));
                    }
                }
                --l_1167;
                (*l_1137) &= (safe_div_func_uint64_t_u_u((safe_mul_func_int16_t_s_s((l_1184 = (l_1174[2] != ((&g_117 != l_1177[0]) , ((safe_rshift_func_int16_t_s_u(((l_1128[9][1][1] = ((+p_10) <= (l_1182 == (void*)0))) <= (-1L)), 6)) , &g_680)))), p_10)), p_11.f1));
            }
            l_1128[5][0][1] &= (l_1184 = p_12);
            l_1187[1] = (void*)0;
            (**g_776) = (**g_776);
        }
        l_1128[2][1][0] &= ((g_103 | (p_9 |= (l_1184 = 0x288EL))) > ((void*)0 == g_1188));
    }
    --l_1190;
    for (g_76.f2 = (-20); (g_76.f2 == (-8)); g_76.f2 = safe_add_func_int16_t_s_s(g_76.f2, 6))
    { /* block id: 656 */
        uint16_t *l_1196[8] = {(void*)0, &g_642, (void*)0, &g_642, (void*)0, &g_642, (void*)0, &g_642};
        uint32_t *l_1202 = &g_65;
        struct S2 l_1203 = {3874,1,55,0,3533};
        uint32_t l_1204 = 1UL;
        int i;
        if (p_10)
            break;
        l_1204 = (((p_12 < (p_10 = (l_1195[6][2] , g_116))) > (((((p_11.f3 && func_20(g_415.f0, ((*l_1202) = (((safe_mul_func_int16_t_s_s(p_12, (p_9 = p_11.f2))) < p_11.f2) == (safe_unary_minus_func_uint16_t_u(((safe_lshift_func_uint16_t_u_u((p_11.f0 & (*g_1189)), 5)) | 0x590AF2B17DC423CFLL))))), p_8, l_1203)) && p_11.f4) >= g_228.f0) < l_1203.f3) | p_11.f2)) & 0x1533E8E2L);
    }
    (**g_776) = (*g_777);
    return l_1205;
}


/* ------------------------------------------ */
/* 
 * reads : g_56 g_60 g_64 g_75 g_76.f1.f4 g_76.f3 g_96 g_76.f2 g_28 g_125 g_966 g_967 g_191 g_192 g_103 g_777 g_119 g_76.f1.f2 g_76.f1 g_656 g_13
 * writes: g_56 g_58 g_60 g_64 g_75 g_96 g_76.f2 g_656 g_28 g_125 g_103 g_287.f0
 */
static struct S2  func_14(int64_t  p_15, int32_t  p_16, uint32_t  p_17, int16_t  p_18, uint32_t  p_19)
{ /* block id: 539 */
    uint32_t l_991 = 0x28FC699CL;
    int32_t *l_992[6][5][5] = {{{(void*)0, &g_76.f2, (void*)0, &g_60[0][8], &g_60[0][8]}, {(void*)0, &g_76.f2, (void*)0, &g_60[0][8], &g_60[0][8]}, {(void*)0, &g_76.f2, (void*)0, &g_60[0][8], &g_60[0][8]}, {(void*)0, &g_76.f2, (void*)0, &g_60[0][8], &g_60[0][8]}, {(void*)0, &g_76.f2, (void*)0, &g_60[0][8], &g_60[0][8]}}, {{(void*)0, &g_76.f2, (void*)0, &g_60[0][8], &g_60[0][8]}, {(void*)0, &g_76.f2, (void*)0, &g_60[0][8], &g_60[0][8]}, {(void*)0, &g_76.f2, (void*)0, &g_60[0][8], &g_60[0][8]}, {(void*)0, &g_76.f2, (void*)0, &g_60[0][8], &g_60[0][8]}, {(void*)0, &g_76.f2, (void*)0, &g_60[0][8], &g_60[0][8]}}, {{(void*)0, &g_76.f2, (void*)0, &g_60[0][8], &g_60[0][8]}, {(void*)0, &g_76.f2, (void*)0, &g_60[0][8], &g_60[0][8]}, {(void*)0, &g_76.f2, (void*)0, &g_60[0][8], &g_60[0][8]}, {(void*)0, &g_76.f2, (void*)0, &g_60[0][8], &g_60[0][8]}, {(void*)0, &g_76.f2, (void*)0, &g_60[0][8], &g_60[0][8]}}, {{(void*)0, &g_76.f2, (void*)0, &g_60[0][8], &g_60[0][8]}, {(void*)0, &g_76.f2, (void*)0, &g_60[0][8], &g_60[0][8]}, {(void*)0, &g_76.f2, (void*)0, &g_60[0][8], &g_60[0][8]}, {(void*)0, &g_76.f2, (void*)0, &g_60[0][8], &g_60[0][8]}, {(void*)0, &g_76.f2, (void*)0, &g_60[0][8], &g_60[0][8]}}, {{(void*)0, &g_76.f2, (void*)0, &g_60[0][8], &g_60[0][8]}, {(void*)0, &g_76.f2, (void*)0, &g_60[0][8], &g_60[0][8]}, {(void*)0, &g_76.f2, (void*)0, &g_60[0][8], &g_60[0][8]}, {(void*)0, &g_76.f2, (void*)0, &g_60[0][8], &g_60[0][8]}, {(void*)0, &g_76.f2, (void*)0, &g_60[0][8], &g_60[0][8]}}, {{(void*)0, &g_76.f2, (void*)0, &g_60[0][8], &g_60[0][8]}, {(void*)0, &g_76.f2, (void*)0, &g_60[0][8], &g_60[0][8]}, {(void*)0, &g_76.f2, (void*)0, &g_60[0][8], &g_60[0][8]}, {(void*)0, &g_76.f2, (void*)0, &g_60[0][8], &g_60[0][8]}, {(void*)0, &g_76.f2, (void*)0, &g_60[0][8], &g_60[0][8]}}};
    struct S0 l_993 = {-1L};
    struct S1 **l_994[6][8][4] = {{{&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}}, {{&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}}, {{&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}}, {{&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}}, {{&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}}, {{&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}, {&g_656, &g_656, &g_656, (void*)0}}};
    const int8_t l_1018 = 1L;
    struct S2 l_1022 = {1536,0,35,0,1720};
    int i, j, k;
    if ((func_52(func_47((p_16 = l_991))) <= (l_993 , ((g_656 = &g_13) == &g_13))))
    { /* block id: 542 */
        uint8_t *l_1001 = &g_125;
        int32_t l_1004 = 0x46800134L;
        int8_t *l_1017 = &g_287.f0;
        int32_t l_1019 = 1L;
        int8_t *l_1020[2][5][2];
        int i, j, k;
        for (i = 0; i < 2; i++)
        {
            for (j = 0; j < 5; j++)
            {
                for (k = 0; k < 2; k++)
                    l_1020[i][j][k] = &g_96;
            }
        }
        for (g_28 = 27; (g_28 < 41); ++g_28)
        { /* block id: 545 */
            l_992[2][0][3] = l_992[1][0][1];
        }
        p_16 = ((l_1004 = (safe_mod_func_int16_t_s_s((safe_lshift_func_uint8_t_u_s(((*l_1001)++), ((l_1004 >= (l_1019 &= ((*g_966) , (((safe_mod_func_int64_t_s_s((safe_sub_func_int64_t_s_s(l_1004, (((**g_191)++) >= (safe_mul_func_uint8_t_u_u((safe_mul_func_int8_t_s_s(((*l_1017) = ((*g_777) , (safe_mod_func_int16_t_s_s(0x2062L, 1L)))), 0UL)), l_1018))))), l_1004)) & l_1004) < 7UL)))) ^ g_76.f1.f2))), 2L))) || g_103);
    }
    else
    { /* block id: 554 */
        struct S2 l_1021 = {8735,0,-31,0,4158};
        return l_1021;
    }
    p_16 ^= func_20(((func_47(p_17) , (18446744073709551615UL < p_17)) , 1UL), (g_76.f1 , p_17), (*g_656), l_1022);
    return l_1022;
}


/* ------------------------------------------ */
/* 
 * reads :
 * writes:
 */
static int64_t  func_20(uint8_t  p_21, const uint32_t  p_22, struct S1  p_23, struct S2  p_24)
{ /* block id: 537 */
    return p_22;
}


/* ------------------------------------------ */
/* 
 * reads : g_103 g_676 g_575 g_675 g_60 g_125 g_656 g_228
 * writes: g_103 g_60 g_125 g_631
 */
static struct S1  func_32(uint32_t * p_33)
{ /* block id: 526 */
    uint32_t l_976 = 18446744073709551615UL;
    uint32_t l_982 = 1UL;
    int32_t l_984 = (-1L);
    for (g_103 = (-26); (g_103 > 7); g_103++)
    { /* block id: 529 */
        int32_t l_975 = 0x5446757FL;
        int32_t l_981 = 0x6D22C26FL;
        uint8_t *l_983[7];
        int32_t *l_985 = &g_631;
        int32_t *l_986 = (void*)0;
        int32_t *l_987 = &g_60[0][8];
        int i;
        for (i = 0; i < 7; i++)
            l_983[i] = &g_125;
        (*l_987) = ((*l_985) = (((l_984 = (g_125 |= ((((safe_rshift_func_uint16_t_u_u((0xE4F0B188AB326F56LL >= l_975), l_976)) ^ (*g_676)) & func_52((**g_675))) , (safe_lshift_func_int16_t_s_s(((l_975 = 2L) || l_976), (((safe_mul_func_int16_t_s_s(l_981, l_982)) , l_982) == l_976)))))) <= 0x20L) != l_981));
    }
    return (*g_656);
}


/* ------------------------------------------ */
/* 
 * reads : g_776 g_777 g_119 g_76.f2
 * writes: g_76.f2
 */
static uint32_t * func_34(uint16_t  p_35, uint32_t * const  p_36)
{ /* block id: 522 */
    int32_t l_968 = 0x10AA2179L;
    int32_t l_969[5][2][4] = {{{(-1L), 0x6D609352L, 0xF8C06029L, 0x6D609352L}, {(-1L), 0x6D609352L, 0xF8C06029L, 0x6D609352L}}, {{(-1L), 0x6D609352L, 0xF8C06029L, 0x6D609352L}, {(-1L), 0x6D609352L, 0xF8C06029L, 0x6D609352L}}, {{(-1L), 0x6D609352L, 0xF8C06029L, 0x6D609352L}, {(-1L), 0x6D609352L, 0xF8C06029L, 0x6D609352L}}, {{(-1L), 0x6D609352L, 0xF8C06029L, 0x6D609352L}, {(-1L), 0x6D609352L, 0xF8C06029L, 0x6D609352L}}, {{(-1L), 0x6D609352L, 0xF8C06029L, 0x6D609352L}, {(-1L), 0x6D609352L, 0xF8C06029L, 0x6D609352L}}};
    int32_t *l_970 = &g_76.f2;
    int i, j, k;
    (*l_970) &= (l_968 & (l_969[4][1][2] ^= ((**g_776) , p_35)));
    return &g_65;
}


/* ------------------------------------------ */
/* 
 * reads : g_96 g_125 g_60 g_76.f1.f3 g_119 g_56 g_113 g_116 g_13.f0 g_65 g_191 g_13 g_261 g_117 g_288 g_115 g_76.f1 g_228.f0 g_103 g_287 g_64 g_75 g_76.f3 g_76.f2 g_76 g_414 g_415.f4 g_228 g_482 g_192 g_58 g_111 g_415.f0 g_596 g_415 g_642 g_597 g_656 g_675 g_680 g_676 g_575 g_631 g_776 g_805
 * writes: g_96 g_125 g_76.f2 g_56 g_60 g_116 g_191 g_228 g_103 g_119 g_111 g_261 g_65 g_287.f0 g_115 g_287 g_58 g_64 g_75 g_113 g_414 g_76.f0 g_76.f3 g_642 g_656 g_117 g_675 g_680 g_482 g_931
 */
static uint32_t * func_37(uint32_t  p_38, struct S0  p_39, const uint32_t * p_40, uint32_t * p_41)
{ /* block id: 38 */
    struct S0 *l_131 = &g_119;
    int32_t l_133 = (-1L);
    int8_t l_162 = (-1L);
    int32_t l_176 = 0x8E7D84EAL;
    int32_t l_177 = 1L;
    int32_t l_178 = 0x49259ABBL;
    int32_t l_182[9] = {0xCE0B2961L, 0x09DC6AD9L, 0xCE0B2961L, 0x09DC6AD9L, 0xCE0B2961L, 0x09DC6AD9L, 0xCE0B2961L, 0x09DC6AD9L, 0xCE0B2961L};
    int32_t *l_235 = &l_178;
    uint8_t l_279 = 1UL;
    int16_t *l_299 = &g_115;
    uint32_t l_448 = 0xD5CD2EFDL;
    int32_t l_454 = 0x1B39453BL;
    int16_t l_492 = 0xE7B9L;
    int32_t l_574 = 0xD413C57EL;
    int64_t *l_601[10] = {&g_58, (void*)0, &g_58, (void*)0, &g_58, (void*)0, &g_58, (void*)0, &g_58, (void*)0};
    int64_t **l_600[4];
    int64_t ***l_599[8][6][5] = {{{&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}, {&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}, {&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}, {&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}, {&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}, {&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}}, {{&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}, {&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}, {&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}, {&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}, {&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}, {&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}}, {{&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}, {&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}, {&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}, {&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}, {&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}, {&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}}, {{&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}, {&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}, {&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}, {&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}, {&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}, {&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}}, {{&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}, {&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}, {&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}, {&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}, {&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}, {&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}}, {{&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}, {&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}, {&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}, {&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}, {&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}, {&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}}, {{&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}, {&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}, {&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}, {&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}, {&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}, {&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}}, {{&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}, {&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}, {&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}, {&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}, {&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}, {&l_600[3], &l_600[2], &l_600[3], (void*)0, &l_600[1]}}};
    const int32_t *l_612 = &g_60[3][2];
    uint32_t **l_687 = &g_64;
    uint8_t l_802 = 0x30L;
    uint8_t l_821 = 254UL;
    struct S2 l_837 = {-5759,0,-51,0,-5049};
    int64_t l_901 = 0x273ECEF4D6CA39D7LL;
    int8_t l_913 = (-4L);
    int32_t l_915 = 0x078B0EB0L;
    uint64_t l_952 = 0x953A4118D5237816LL;
    uint8_t l_955 = 253UL;
    int i, j, k;
    for (i = 0; i < 4; i++)
        l_600[i] = &l_601[1];
    for (g_96 = 23; (g_96 > 8); --g_96)
    { /* block id: 41 */
        uint8_t *l_124[4][2][2] = {{{&g_125, &g_125}, {&g_125, &g_125}}, {{&g_125, &g_125}, {&g_125, &g_125}}, {{&g_125, &g_125}, {&g_125, &g_125}}, {{&g_125, &g_125}, {&g_125, &g_125}}};
        int32_t l_126 = 4L;
        int32_t l_172 = 1L;
        int32_t l_173 = 0x299B2AD0L;
        int32_t l_174 = 0x61CA15E8L;
        int32_t l_179 = 0x86176E38L;
        int32_t l_180 = (-6L);
        int32_t l_181 = (-2L);
        int32_t l_183 = 8L;
        int32_t l_184 = 1L;
        int32_t l_185 = 0xC6768EADL;
        int32_t *l_209 = &l_185;
        struct S0 *l_210 = (void*)0;
        int32_t *l_233[3][2][8] = {{{&l_174, &g_116, &g_76.f2, &l_181, &l_176, &l_181, &g_76.f2, &g_116}, {&l_174, &g_116, &g_76.f2, &l_181, &l_176, &l_181, &g_76.f2, &g_116}}, {{&l_174, &g_116, &g_76.f2, &l_181, &l_176, &l_181, &g_76.f2, &g_116}, {&l_174, &g_116, &g_76.f2, &l_181, &l_176, &l_181, &g_76.f2, &g_116}}, {{&l_174, &g_116, &g_76.f2, &l_181, &l_176, &l_181, &g_76.f2, &g_116}, {&l_174, &g_116, &g_76.f2, &l_181, &l_176, &l_181, &g_76.f2, &g_116}}};
        int i, j, k;
        if ((safe_div_func_int16_t_s_s(((l_126 = ((g_125 |= 1UL) ^ g_60[1][1])) , g_76.f1.f3), p_38)))
        { /* block id: 44 */
            int32_t *l_127 = &l_126;
            (*l_127) |= p_38;
            for (g_76.f2 = (-4); (g_76.f2 >= 7); g_76.f2 = safe_add_func_uint32_t_u_u(g_76.f2, 1))
            { /* block id: 48 */
                uint32_t l_130 = 1UL;
                struct S0 **l_132 = &l_131;
                (*l_127) = l_130;
                (*l_132) = l_131;
                l_133 = (*l_127);
            }
        }
        else
        { /* block id: 53 */
            int64_t *l_144 = &g_56;
            int32_t l_147 = 0x35580C9BL;
            int32_t l_169 = 3L;
            int32_t l_170[4] = {(-10L), 1L, (-10L), 1L};
            int32_t l_186 = 0xD038AAF2L;
            uint8_t l_188 = 0x35L;
            uint16_t l_206 = 0x2405L;
            uint32_t *l_231 = (void*)0;
            int32_t *l_234 = &l_133;
            const int32_t *l_238 = &l_177;
            int i;
            if ((p_38 <= (safe_sub_func_int32_t_s_s(((((((((safe_div_func_uint8_t_u_u((safe_div_func_uint16_t_u_u(0x7E4FL, 6UL)), (safe_rshift_func_int16_t_s_u((safe_rshift_func_int8_t_s_u((g_119 , func_52((p_39 , ((*l_144) &= l_126)))), (safe_add_func_int16_t_s_s((((g_119 , (-5L)) && 0x7B4D5CE2L) != 3L), g_113[1][0])))), 15)))) , g_116) || g_113[2][0]) && 0UL) && p_39.f0) < 0xC475L) >= l_133) < p_39.f0), l_147))))
            { /* block id: 55 */
                uint32_t **l_160 = &g_64;
                uint32_t ***l_161 = &l_160;
                int32_t l_165 = 0L;
                int32_t l_171 = 0x6EEDD9E1L;
                int32_t l_175[6] = {9L, 9L, (-4L), 9L, 9L, (-4L)};
                int i;
                for (g_125 = (-9); (g_125 < 42); ++g_125)
                { /* block id: 58 */
                    for (g_116 = (-17); (g_116 < (-12)); g_116 = safe_add_func_uint16_t_u_u(g_116, 8))
                    { /* block id: 61 */
                        if (p_38)
                            break;
                        if (l_147)
                            break;
                        if (l_147)
                            break;
                        return &g_65;
                    }
                }
                l_133 |= (l_147 | p_38);
                if ((((((p_39.f0 , (safe_div_func_uint8_t_u_u((l_133 = (safe_div_func_int32_t_s_s((safe_lshift_func_uint8_t_u_u(((((*l_161) = l_160) != &p_40) || ((l_147 | l_162) >= 0xFEL)), 1)), ((p_39.f0 > (safe_lshift_func_int8_t_s_s(l_165, p_38))) && 4294967295UL)))), g_13.f0))) , l_126) && p_38) & g_65) <= 1UL))
                { /* block id: 71 */
                    uint32_t l_166 = 4UL;
                    int32_t l_167 = (-4L);
                    int32_t *l_168[2];
                    int64_t l_187 = 0L;
                    int i;
                    for (i = 0; i < 2; i++)
                        l_168[i] = &g_116;
                    l_167 ^= (l_162 & l_166);
                    ++l_188;
                }
                else
                { /* block id: 74 */
                    uint32_t l_218 = 0xAF2F86AFL;
                    uint32_t **l_223 = (void*)0;
                    for (l_179 = 0; (l_179 <= 6); l_179 += 1)
                    { /* block id: 77 */
                        uint64_t ***l_193 = &g_191;
                        int32_t *l_194 = &l_178;
                        int32_t *l_195 = &l_126;
                        int32_t *l_196 = &l_185;
                        int32_t *l_197 = &l_182[2];
                        int32_t *l_198 = (void*)0;
                        int32_t *l_199 = &g_60[1][3];
                        int32_t *l_200 = &l_184;
                        int32_t *l_201 = &l_181;
                        int32_t *l_202 = &g_60[2][1];
                        int32_t *l_203 = &l_174;
                        int32_t *l_204 = (void*)0;
                        int32_t *l_205 = &l_175[5];
                        struct S0 **l_211 = &l_210;
                        int i, j;
                        (*l_193) = g_191;
                        l_206++;
                        l_209 = &g_60[l_179][(l_179 + 2)];
                        (*l_211) = l_210;
                    }
                    for (l_147 = (-29); (l_147 > (-29)); l_147++)
                    { /* block id: 85 */
                        int32_t *l_214 = &l_165;
                        int32_t l_215 = 0L;
                        int32_t *l_216 = &l_177;
                        int32_t *l_217[1];
                        uint32_t ***l_224 = (void*)0;
                        uint32_t ***l_225 = &l_223;
                        int32_t l_226 = 0xE0309361L;
                        struct S1 *l_227 = &g_228;
                        int i;
                        for (i = 0; i < 1; i++)
                            l_217[i] = &g_116;
                        --l_218;
                        (*l_216) &= 0x7C09D144L;
                        l_226 &= ((safe_lshift_func_int16_t_s_s(0xF593L, (((*l_161) = &g_64) == ((*l_225) = l_223)))) , 0x47ABF410L);
                        (*l_227) = g_13;
                    }
                }
                for (g_103 = 0; (g_103 != 38); ++g_103)
                { /* block id: 96 */
                    if (p_38)
                    { /* block id: 97 */
                        return l_231;
                    }
                    else
                    { /* block id: 99 */
                        int32_t **l_232 = &l_209;
                        l_233[1][0][5] = ((*l_232) = (g_60[0][8] , (void*)0));
                    }
                    l_235 = l_234;
                }
            }
            else
            { /* block id: 105 */
                int32_t **l_236 = &l_233[1][0][0];
                const int32_t **l_237[9] = {(void*)0, (void*)0, (void*)0, (void*)0, (void*)0, (void*)0, (void*)0, (void*)0, (void*)0};
                int i;
                (*l_131) = p_39;
                (*l_236) = (void*)0;
                l_238 = p_40;
            }
            for (l_183 = (-8); (l_183 < (-5)); l_183++)
            { /* block id: 112 */
                l_209 = (l_234 = &l_178);
                if ((*l_235))
                    continue;
            }
            l_170[0] = ((*l_235) &= (safe_sub_func_uint8_t_u_u(((p_39.f0 , &l_234) != &g_111), p_39.f0)));
        }
        l_233[1][0][5] = &l_183;
    }
lbl_536:
    for (g_116 = (-28); (g_116 >= (-22)); ++g_116)
    { /* block id: 124 */
        int32_t *l_245[3];
        struct S1 *l_286 = &g_287;
        int i;
        for (i = 0; i < 3; i++)
            l_245[i] = &l_176;
        l_177 |= (*l_235);
        if (p_39.f0)
            break;
        for (g_56 = (-30); (g_56 <= 6); ++g_56)
        { /* block id: 129 */
            uint16_t l_275 = 0UL;
            int32_t l_277 = (-1L);
            int32_t l_278 = (-6L);
            for (g_103 = 0; (g_103 == 44); ++g_103)
            { /* block id: 132 */
                uint32_t l_254 = 18446744073709551614UL;
                int32_t l_259 = 1L;
                for (l_176 = 13; (l_176 < 16); l_176 = safe_add_func_int32_t_s_s(l_176, 7))
                { /* block id: 135 */
                    uint16_t l_252 = 65531UL;
                    int32_t *l_257 = &g_60[0][7];
                    int32_t l_260 = 0L;
                    if (l_252)
                    { /* block id: 136 */
                        int16_t l_253 = 0x0065L;
                        l_254++;
                        l_257 = &l_177;
                    }
                    else
                    { /* block id: 139 */
                        int32_t **l_258 = &g_111;
                        (*l_257) ^= (((*l_258) = p_41) != l_245[2]);
                    }
                    g_261++;
                }
            }
            for (g_228.f0 = 12; (g_228.f0 < 21); g_228.f0++)
            { /* block id: 148 */
                int8_t *l_268 = (void*)0;
                int8_t *l_269[1];
                int32_t l_272 = 0L;
                int i;
                for (i = 0; i < 1; i++)
                    l_269[i] = (void*)0;
                if ((safe_mul_func_uint8_t_u_u(g_76.f1.f3, (l_275 ^= (((*l_235) = p_38) || (safe_rshift_func_uint16_t_u_u(l_272, (safe_lshift_func_uint8_t_u_s(g_117, p_39.f0)))))))))
                { /* block id: 151 */
                    int32_t **l_276 = &g_111;
                    (*l_276) = (l_235 = (l_245[0] = p_41));
                    ++l_279;
                }
                else
                { /* block id: 156 */
                    for (g_65 = (-7); (g_65 != 24); g_65++)
                    { /* block id: 159 */
                        struct S1 *l_285 = &g_228;
                        struct S1 **l_284[8][6][3] = {{{(void*)0, &l_285, (void*)0}, {(void*)0, &l_285, (void*)0}, {(void*)0, &l_285, (void*)0}, {(void*)0, &l_285, (void*)0}, {(void*)0, &l_285, (void*)0}, {(void*)0, &l_285, (void*)0}}, {{(void*)0, &l_285, (void*)0}, {(void*)0, &l_285, (void*)0}, {(void*)0, &l_285, (void*)0}, {(void*)0, &l_285, (void*)0}, {(void*)0, &l_285, (void*)0}, {(void*)0, &l_285, (void*)0}}, {{(void*)0, &l_285, (void*)0}, {(void*)0, &l_285, (void*)0}, {(void*)0, &l_285, (void*)0}, {(void*)0, &l_285, (void*)0}, {(void*)0, &l_285, (void*)0}, {(void*)0, &l_285, (void*)0}}, {{(void*)0, &l_285, (void*)0}, {(void*)0, &l_285, (void*)0}, {(void*)0, &l_285, (void*)0}, {(void*)0, &l_285, (void*)0}, {(void*)0, &l_285, (void*)0}, {(void*)0, &l_285, (void*)0}}, {{(void*)0, &l_285, (void*)0}, {(void*)0, &l_285, (void*)0}, {(void*)0, &l_285, (void*)0}, {(void*)0, &l_285, (void*)0}, {(void*)0, &l_285, (void*)0}, {(void*)0, &l_285, (void*)0}}, {{(void*)0, &l_285, (void*)0}, {(void*)0, &l_285, (void*)0}, {(void*)0, &l_285, (void*)0}, {(void*)0, &l_285, (void*)0}, {(void*)0, &l_285, (void*)0}, {(void*)0, &l_285, (void*)0}}, {{(void*)0, &l_285, (void*)0}, {(void*)0, &l_285, (void*)0}, {(void*)0, &l_285, (void*)0}, {(void*)0, &l_285, (void*)0}, {(void*)0, &l_285, (void*)0}, {(void*)0, &l_285, (void*)0}}, {{(void*)0, &l_285, (void*)0}, {(void*)0, &l_285, (void*)0}, {(void*)0, &l_285, (void*)0}, {(void*)0, &l_285, (void*)0}, {(void*)0, &l_285, (void*)0}, {(void*)0, &l_285, (void*)0}}};
                        int i, j, k;
                        l_286 = (void*)0;
                        if (g_288)
                            break;
                    }
                    for (p_39.f0 = 0; (p_39.f0 <= 6); p_39.f0 += 1)
                    { /* block id: 165 */
                        struct S0 **l_289 = (void*)0;
                        struct S0 **l_290 = &l_131;
                        (*l_290) = &p_39;
                    }
                }
            }
            for (g_287.f0 = 0; g_287.f0 < 9; g_287.f0 += 1)
            {
                l_182[g_287.f0] = 0x69411491L;
            }
        }
    }
    if (((p_39.f0 <= ((*l_131) , (0x1CF6L < (!((*l_299) &= ((safe_mul_func_uint8_t_u_u(func_52((1L | ((safe_rshift_func_uint16_t_u_u((g_76.f1.f3 , (((*l_235) , (safe_mod_func_int8_t_s_s(p_38, (safe_div_func_int8_t_s_s((p_39.f0 == ((void*)0 != l_235)), g_60[3][2]))))) >= p_39.f0)), p_38)) || p_38))), g_113[2][0])) >= (*l_235))))))) != (*l_235)))
    { /* block id: 174 */
        const struct S1 l_302 = {0xA4L};
        int32_t l_316 = 1L;
        uint64_t **l_329 = (void*)0;
        int32_t l_356 = 0xAC4B7426L;
        int32_t l_381 = 0x6EB08D93L;
        int32_t l_383 = 1L;
        int32_t l_386 = 0xB6681478L;
        uint64_t l_391[2][3] = {{18446744073709551615UL, 0xD8B6E1676639DBE0LL, 18446744073709551615UL}, {18446744073709551615UL, 0xD8B6E1676639DBE0LL, 18446744073709551615UL}};
        const int32_t *l_406 = &g_60[0][3];
        uint8_t l_436 = 0x67L;
        struct S3 *l_486 = &g_76;
        int16_t l_557 = 1L;
        uint32_t l_561 = 0UL;
        int32_t l_567 = (-6L);
        int32_t l_568 = 0xFFA91CF5L;
        int32_t l_570 = 0xE63A837DL;
        int32_t l_571 = (-7L);
        int32_t l_572 = 2L;
        int32_t l_573[2];
        uint8_t l_576[5] = {0x8EL, 252UL, 0x8EL, 252UL, 0x8EL};
        uint64_t ****l_590 = (void*)0;
        uint64_t ***l_592 = &g_191;
        uint64_t ****l_591 = &l_592;
        int64_t *l_593 = &g_56;
        int i, j;
        for (i = 0; i < 2; i++)
            l_573[i] = 5L;
lbl_360:
        (*l_235) |= p_39.f0;
        for (l_178 = 0; (l_178 <= 6); l_178 += 1)
        { /* block id: 178 */
            uint32_t **l_303 = (void*)0;
            int8_t *l_314 = &g_119.f0;
            uint8_t *l_315[1][8][2] = {{{&l_279, &g_125}, {&l_279, &g_125}, {&l_279, &g_125}, {&l_279, &g_125}, {&l_279, &g_125}, {&l_279, &g_125}, {&l_279, &g_125}, {&l_279, &g_125}}};
            int32_t l_332 = 0x7B0FE012L;
            int64_t *l_347 = &g_76.f0;
            int64_t **l_346 = &l_347;
            int32_t l_388 = 9L;
            int32_t l_389 = 0x67B2373DL;
            int32_t l_390 = 3L;
            const int32_t *l_413 = &l_356;
            uint64_t l_440 = 0UL;
            int32_t l_447 = 0xEB32F37AL;
            int i, j, k;
            if ((safe_div_func_int16_t_s_s(p_38, (l_302 , (((void*)0 != l_303) && (((safe_lshift_func_uint8_t_u_s((g_76.f1 , 1UL), 2)) & p_38) != (l_316 = (safe_div_func_int8_t_s_s(((*l_314) &= (safe_add_func_int16_t_s_s((safe_mod_func_uint8_t_u_u(l_302.f0, (safe_div_func_int8_t_s_s((((0xC4L && 0L) ^ 0x2971L) , g_115), p_39.f0)))), 1UL))), l_302.f0)))))))))
            { /* block id: 181 */
                int32_t l_333 = 0x88130779L;
                struct S0 *l_334[5] = {&g_119, &g_119, &g_119, &g_119, &g_119};
                uint64_t l_357 = 18446744073709551607UL;
                int32_t l_378 = 0xA8E17AFCL;
                int32_t l_379 = 0L;
                int32_t l_380 = 0x09884D23L;
                int32_t l_382 = 0x0F8F43F3L;
                int32_t l_384 = 1L;
                int32_t l_385[1][10] = {{1L, 1L, 4L, 4L, 1L, 1L, 1L, 4L, 4L, 1L}};
                int16_t l_387 = 2L;
                uint8_t *l_397 = (void*)0;
                struct S2 **l_416 = &g_414;
                int i, j;
                for (g_228.f0 = 1; (g_228.f0 >= 0); g_228.f0 -= 1)
                { /* block id: 184 */
                    int8_t **l_319 = &l_314;
                    uint64_t ** const l_328 = &g_192[3];
                    int8_t *l_330 = (void*)0;
                    int8_t *l_331 = &g_119.f0;
                    int i, j;
                    if (((safe_lshift_func_uint16_t_u_s((&g_96 != ((*l_319) = (void*)0)), ((safe_mod_func_int64_t_s_s((safe_lshift_func_uint16_t_u_s((safe_lshift_func_int16_t_s_s((g_113[g_228.f0][g_228.f0] & func_52(((safe_rshift_func_int16_t_s_s(((((*l_331) |= (l_328 != l_329)) <= 0xE2L) , func_52(l_332)), 11)) == (*l_235)))), 8)), l_333)), p_38)) != p_39.f0))) , p_39.f0))
                    { /* block id: 187 */
                        struct S0 **l_335 = &l_131;
                        struct S0 **l_336 = &l_334[1];
                        (*l_336) = ((*l_335) = l_334[3]);
                    }
                    else
                    { /* block id: 190 */
                        const int32_t *l_338 = &g_60[0][8];
                        const int32_t **l_337 = &l_338;
                        int32_t *l_341 = &l_133;
                        int64_t ***l_342 = (void*)0;
                        int64_t *l_345 = &g_58;
                        int64_t **l_344 = &l_345;
                        int64_t ***l_343 = &l_344;
                        int i, j;
                        (*l_337) = p_40;
                        if ((*l_235))
                            break;
                        l_346 = (((*l_341) = (g_60[(g_228.f0 + 1)][(l_178 + 1)] |= (safe_lshift_func_int8_t_s_s(g_103, 0)))) , ((*l_343) = (void*)0));
                        return &g_261;
                    }
                    g_287 = g_287;
                }
                if ((safe_div_func_uint64_t_u_u((g_287.f0 < 0x07FD64A67530FD64LL), (p_38 ^ (l_316 = g_287.f0)))))
                { /* block id: 202 */
                    struct S1 *l_370 = &g_13;
                    struct S1 **l_369[10][6] = {{(void*)0, &l_370, (void*)0, (void*)0, &l_370, &l_370}, {(void*)0, &l_370, (void*)0, (void*)0, &l_370, &l_370}, {(void*)0, &l_370, (void*)0, (void*)0, &l_370, &l_370}, {(void*)0, &l_370, (void*)0, (void*)0, &l_370, &l_370}, {(void*)0, &l_370, (void*)0, (void*)0, &l_370, &l_370}, {(void*)0, &l_370, (void*)0, (void*)0, &l_370, &l_370}, {(void*)0, &l_370, (void*)0, (void*)0, &l_370, &l_370}, {(void*)0, &l_370, (void*)0, (void*)0, &l_370, &l_370}, {(void*)0, &l_370, (void*)0, (void*)0, &l_370, &l_370}, {(void*)0, &l_370, (void*)0, (void*)0, &l_370, &l_370}};
                    int32_t l_377[7][3] = {{(-4L), 4L, 1L}, {(-4L), 4L, 1L}, {(-4L), 4L, 1L}, {(-4L), 4L, 1L}, {(-4L), 4L, 1L}, {(-4L), 4L, 1L}, {(-4L), 4L, 1L}};
                    int i, j;
                    for (g_103 = 0; (g_103 <= 1); g_103 += 1)
                    { /* block id: 205 */
                        int32_t *l_350 = &g_116;
                        int32_t *l_351 = &g_60[5][0];
                        int32_t *l_352 = &l_177;
                        int32_t *l_353 = &l_316;
                        int32_t *l_354 = &g_60[3][5];
                        int32_t *l_355[9][5] = {{&g_76.f2, &l_182[5], &l_133, &l_177, &l_133}, {&g_76.f2, &l_182[5], &l_133, &l_177, &l_133}, {&g_76.f2, &l_182[5], &l_133, &l_177, &l_133}, {&g_76.f2, &l_182[5], &l_133, &l_177, &l_133}, {&g_76.f2, &l_182[5], &l_133, &l_177, &l_133}, {&g_76.f2, &l_182[5], &l_133, &l_177, &l_133}, {&g_76.f2, &l_182[5], &l_133, &l_177, &l_133}, {&g_76.f2, &l_182[5], &l_133, &l_177, &l_133}, {&g_76.f2, &l_182[5], &l_133, &l_177, &l_133}};
                        int i, j;
                        l_357++;
                        if (g_125)
                            goto lbl_360;
                        (*l_353) = 0L;
                    }
                    for (l_333 = 0; (l_333 <= 1); l_333 += 1)
                    { /* block id: 212 */
                        int32_t l_375[5];
                        int32_t *l_376[1][6] = {{(void*)0, &g_76.f2, (void*)0, &g_76.f2, (void*)0, &g_76.f2}};
                        int i, j;
                        for (i = 0; i < 5; i++)
                            l_375[i] = 0x6A64D3B0L;
                        l_375[4] &= (safe_lshift_func_int8_t_s_u((((safe_add_func_int32_t_s_s(g_113[(l_333 + 1)][l_333], ((safe_mul_func_uint16_t_u_u((safe_add_func_uint16_t_u_u(p_38, 65526UL)), l_356)) < p_38))) != ((void*)0 != l_369[2][3])) , (!(safe_mul_func_int16_t_s_s((safe_mul_func_uint8_t_u_u(func_47(p_38), g_287.f0)), 1L)))), l_333));
                        l_391[1][2]--;
                        l_378 = p_39.f0;
                        l_386 = 0x1D392CCFL;
                    }
                    if (l_383)
                        break;
                    for (g_228.f0 = 1; (g_228.f0 >= 0); g_228.f0 -= 1)
                    { /* block id: 221 */
                        int32_t *l_396 = &l_382;
                        int8_t *l_398 = (void*)0;
                        int8_t *l_399 = &l_162;
                        int32_t **l_402 = &l_235;
                        int32_t **l_403 = &l_396;
                        int i, j;
                        (*l_396) |= (safe_lshift_func_uint16_t_u_u(p_38, func_52(func_47(p_39.f0))));
                        (*l_403) = ((*l_402) = (((((*g_75) , ((func_47(l_385[0][5]) < (l_397 == l_397)) < (((*l_399) |= 0xBAL) & (g_119.f0 != (safe_rshift_func_int16_t_s_u((g_113[(g_228.f0 + 3)][g_228.f0] = ((void*)0 == &l_378)), p_39.f0)))))) , g_76.f0) != (-1L)) , p_41));
                    }
                }
                else
                { /* block id: 228 */
                    int32_t * const l_407[9] = {&l_379, &l_379, &l_382, &l_379, &l_379, &l_382, &l_379, &l_379, &l_382};
                    int i;
                    for (g_58 = 0; (g_58 < (-12)); g_58 = safe_sub_func_int8_t_s_s(g_58, 7))
                    { /* block id: 231 */
                        int32_t **l_408[8][5][6] = {{{(void*)0, &l_235, &l_235, &l_235, (void*)0, &g_111}, {(void*)0, &l_235, &l_235, &l_235, (void*)0, &g_111}, {(void*)0, &l_235, &l_235, &l_235, (void*)0, &g_111}, {(void*)0, &l_235, &l_235, &l_235, (void*)0, &g_111}, {(void*)0, &l_235, &l_235, &l_235, (void*)0, &g_111}}, {{(void*)0, &l_235, &l_235, &l_235, (void*)0, &g_111}, {(void*)0, &l_235, &l_235, &l_235, (void*)0, &g_111}, {(void*)0, &l_235, &l_235, &l_235, (void*)0, &g_111}, {(void*)0, &l_235, &l_235, &l_235, (void*)0, &g_111}, {(void*)0, &l_235, &l_235, &l_235, (void*)0, &g_111}}, {{(void*)0, &l_235, &l_235, &l_235, (void*)0, &g_111}, {(void*)0, &l_235, &l_235, &l_235, (void*)0, &g_111}, {(void*)0, &l_235, &l_235, &l_235, (void*)0, &g_111}, {(void*)0, &l_235, &l_235, &l_235, (void*)0, &g_111}, {(void*)0, &l_235, &l_235, &l_235, (void*)0, &g_111}}, {{(void*)0, &l_235, &l_235, &l_235, (void*)0, &g_111}, {(void*)0, &l_235, &l_235, &l_235, (void*)0, &g_111}, {(void*)0, &l_235, &l_235, &l_235, (void*)0, &g_111}, {(void*)0, &l_235, &l_235, &l_235, (void*)0, &g_111}, {(void*)0, &l_235, &l_235, &l_235, (void*)0, &g_111}}, {{(void*)0, &l_235, &l_235, &l_235, (void*)0, &g_111}, {(void*)0, &l_235, &l_235, &l_235, (void*)0, &g_111}, {(void*)0, &l_235, &l_235, &l_235, (void*)0, &g_111}, {(void*)0, &l_235, &l_235, &l_235, (void*)0, &g_111}, {(void*)0, &l_235, &l_235, &l_235, (void*)0, &g_111}}, {{(void*)0, &l_235, &l_235, &l_235, (void*)0, &g_111}, {(void*)0, &l_235, &l_235, &l_235, (void*)0, &g_111}, {(void*)0, &l_235, &l_235, &l_235, (void*)0, &g_111}, {(void*)0, &l_235, &l_235, &l_235, (void*)0, &g_111}, {(void*)0, &l_235, &l_235, &l_235, (void*)0, &g_111}}, {{(void*)0, &l_235, &l_235, &l_235, (void*)0, &g_111}, {(void*)0, &l_235, &l_235, &l_235, (void*)0, &g_111}, {(void*)0, &l_235, &l_235, &l_235, (void*)0, &g_111}, {(void*)0, &l_235, &l_235, &l_235, (void*)0, &g_111}, {(void*)0, &l_235, &l_235, &l_235, (void*)0, &g_111}}, {{(void*)0, &l_235, &l_235, &l_235, (void*)0, &g_111}, {(void*)0, &l_235, &l_235, &l_235, (void*)0, &g_111}, {(void*)0, &l_235, &l_235, &l_235, (void*)0, &g_111}, {(void*)0, &l_235, &l_235, &l_235, (void*)0, &g_111}, {(void*)0, &l_235, &l_235, &l_235, (void*)0, &g_111}}};
                        int32_t **l_409 = &l_235;
                        int i, j, k;
                        l_406 = p_40;
                        (*l_409) = l_407[3];
                        (*l_409) = (void*)0;
                        (*l_409) = p_41;
                    }
                }
                for (l_316 = 0; (l_316 < 8); ++l_316)
                { /* block id: 240 */
                    const int32_t **l_412 = &l_406;
                    (*l_412) = p_40;
                    l_413 = p_40;
                }
                (*l_416) = g_414;
            }
            else
            { /* block id: 245 */
                uint16_t l_425 = 65535UL;
                int32_t l_428 = (-10L);
                int32_t l_429 = 0xFA8EB076L;
                int32_t l_434 = 0x8901F33DL;
                int32_t *l_445 = &l_389;
                int32_t *l_446[10][2][10] = {{{&l_182[8], (void*)0, &l_389, &l_178, (void*)0, &l_316, &l_389, &l_177, (void*)0, &l_182[5]}, {&l_182[8], (void*)0, &l_389, &l_178, (void*)0, &l_316, &l_389, &l_177, (void*)0, &l_182[5]}}, {{&l_182[8], (void*)0, &l_389, &l_178, (void*)0, &l_316, &l_389, &l_177, (void*)0, &l_182[5]}, {&l_182[8], (void*)0, &l_389, &l_178, (void*)0, &l_316, &l_389, &l_177, (void*)0, &l_182[5]}}, {{&l_182[8], (void*)0, &l_389, &l_178, (void*)0, &l_316, &l_389, &l_177, (void*)0, &l_182[5]}, {&l_182[8], (void*)0, &l_389, &l_178, (void*)0, &l_316, &l_389, &l_177, (void*)0, &l_182[5]}}, {{&l_182[8], (void*)0, &l_389, &l_178, (void*)0, &l_316, &l_389, &l_177, (void*)0, &l_182[5]}, {&l_182[8], (void*)0, &l_389, &l_178, (void*)0, &l_316, &l_389, &l_177, (void*)0, &l_182[5]}}, {{&l_182[8], (void*)0, &l_389, &l_178, (void*)0, &l_316, &l_389, &l_177, (void*)0, &l_182[5]}, {&l_182[8], (void*)0, &l_389, &l_178, (void*)0, &l_316, &l_389, &l_177, (void*)0, &l_182[5]}}, {{&l_182[8], (void*)0, &l_389, &l_178, (void*)0, &l_316, &l_389, &l_177, (void*)0, &l_182[5]}, {&l_182[8], (void*)0, &l_389, &l_178, (void*)0, &l_316, &l_389, &l_177, (void*)0, &l_182[5]}}, {{&l_182[8], (void*)0, &l_389, &l_178, (void*)0, &l_316, &l_389, &l_177, (void*)0, &l_182[5]}, {&l_182[8], (void*)0, &l_389, &l_178, (void*)0, &l_316, &l_389, &l_177, (void*)0, &l_182[5]}}, {{&l_182[8], (void*)0, &l_389, &l_178, (void*)0, &l_316, &l_389, &l_177, (void*)0, &l_182[5]}, {&l_182[8], (void*)0, &l_389, &l_178, (void*)0, &l_316, &l_389, &l_177, (void*)0, &l_182[5]}}, {{&l_182[8], (void*)0, &l_389, &l_178, (void*)0, &l_316, &l_389, &l_177, (void*)0, &l_182[5]}, {&l_182[8], (void*)0, &l_389, &l_178, (void*)0, &l_316, &l_389, &l_177, (void*)0, &l_182[5]}}, {{&l_182[8], (void*)0, &l_389, &l_178, (void*)0, &l_316, &l_389, &l_177, (void*)0, &l_182[5]}, {&l_182[8], (void*)0, &l_389, &l_178, (void*)0, &l_316, &l_389, &l_177, (void*)0, &l_182[5]}}};
                int i, j, k;
                for (l_386 = 0; (l_386 <= 1); l_386 += 1)
                { /* block id: 248 */
                    int64_t l_423[10][8] = {{0x6F830DA3ABB5D249LL, 0x4ABF20A43A70069FLL, 0xA016EAE044D6DA28LL, 0xB80001A53E0ADA16LL, (-3L), 0xB80001A53E0ADA16LL, 0xA016EAE044D6DA28LL, 0x4ABF20A43A70069FLL}, {0x6F830DA3ABB5D249LL, 0x4ABF20A43A70069FLL, 0xA016EAE044D6DA28LL, 0xB80001A53E0ADA16LL, (-3L), 0xB80001A53E0ADA16LL, 0xA016EAE044D6DA28LL, 0x4ABF20A43A70069FLL}, {0x6F830DA3ABB5D249LL, 0x4ABF20A43A70069FLL, 0xA016EAE044D6DA28LL, 0xB80001A53E0ADA16LL, (-3L), 0xB80001A53E0ADA16LL, 0xA016EAE044D6DA28LL, 0x4ABF20A43A70069FLL}, {0x6F830DA3ABB5D249LL, 0x4ABF20A43A70069FLL, 0xA016EAE044D6DA28LL, 0xB80001A53E0ADA16LL, (-3L), 0xB80001A53E0ADA16LL, 0xA016EAE044D6DA28LL, 0x4ABF20A43A70069FLL}, {0x6F830DA3ABB5D249LL, 0x4ABF20A43A70069FLL, 0xA016EAE044D6DA28LL, 0xB80001A53E0ADA16LL, (-3L), 0xB80001A53E0ADA16LL, 0xA016EAE044D6DA28LL, 0x4ABF20A43A70069FLL}, {0x6F830DA3ABB5D249LL, 0x4ABF20A43A70069FLL, 0xA016EAE044D6DA28LL, 0xB80001A53E0ADA16LL, (-3L), 0xB80001A53E0ADA16LL, 0xA016EAE044D6DA28LL, 0x4ABF20A43A70069FLL}, {0x6F830DA3ABB5D249LL, 0x4ABF20A43A70069FLL, 0xA016EAE044D6DA28LL, 0xB80001A53E0ADA16LL, (-3L), 0xB80001A53E0ADA16LL, 0xA016EAE044D6DA28LL, 0x4ABF20A43A70069FLL}, {0x6F830DA3ABB5D249LL, 0x4ABF20A43A70069FLL, 0xA016EAE044D6DA28LL, 0xB80001A53E0ADA16LL, (-3L), 0xB80001A53E0ADA16LL, 0xA016EAE044D6DA28LL, 0x4ABF20A43A70069FLL}, {0x6F830DA3ABB5D249LL, 0x4ABF20A43A70069FLL, 0xA016EAE044D6DA28LL, 0xB80001A53E0ADA16LL, (-3L), 0xB80001A53E0ADA16LL, 0xA016EAE044D6DA28LL, 0x4ABF20A43A70069FLL}, {0x6F830DA3ABB5D249LL, 0x4ABF20A43A70069FLL, 0xA016EAE044D6DA28LL, 0xB80001A53E0ADA16LL, (-3L), 0xB80001A53E0ADA16LL, 0xA016EAE044D6DA28LL, 0x4ABF20A43A70069FLL}};
                    int32_t l_435 = (-10L);
                    int i, j;
                    for (l_133 = 4; (l_133 >= 0); l_133 -= 1)
                    { /* block id: 251 */
                        struct S3 l_419[9][1][9] = {{{{0x0B82DA26440F5263LL,{3916,-0,49,0,11043},0L,1UL}, {7L,{-14873,-0,-58,0,7634},3L,18446744073709551610UL}, {1L,{20131,1,10,0,-11539},0x44FA6FA3L,0x3D84D2C8L}, {0xA6642A46128A7D53LL,{2018,0,-28,0,-9797},0x5A14CF5EL,0xA1C66547L}, {-4L,{-9931,0,7,0,-6381},0x44B5EA75L,0x6BC8E85CL}, {0xFD730CA24AB212BELL,{18325,-1,56,0,-3238},0L,0x469A6C37L}, {-4L,{-9931,0,7,0,-6381},0x44B5EA75L,0x6BC8E85CL}, {0xA6642A46128A7D53LL,{2018,0,-28,0,-9797},0x5A14CF5EL,0xA1C66547L}, {1L,{20131,1,10,0,-11539},0x44FA6FA3L,0x3D84D2C8L}}}, {{{0x0B82DA26440F5263LL,{3916,-0,49,0,11043},0L,1UL}, {7L,{-14873,-0,-58,0,7634},3L,18446744073709551610UL}, {1L,{20131,1,10,0,-11539},0x44FA6FA3L,0x3D84D2C8L}, {0xA6642A46128A7D53LL,{2018,0,-28,0,-9797},0x5A14CF5EL,0xA1C66547L}, {-4L,{-9931,0,7,0,-6381},0x44B5EA75L,0x6BC8E85CL}, {0xFD730CA24AB212BELL,{18325,-1,56,0,-3238},0L,0x469A6C37L}, {-4L,{-9931,0,7,0,-6381},0x44B5EA75L,0x6BC8E85CL}, {0xA6642A46128A7D53LL,{2018,0,-28,0,-9797},0x5A14CF5EL,0xA1C66547L}, {1L,{20131,1,10,0,-11539},0x44FA6FA3L,0x3D84D2C8L}}}, {{{0x0B82DA26440F5263LL,{3916,-0,49,0,11043},0L,1UL}, {7L,{-14873,-0,-58,0,7634},3L,18446744073709551610UL}, {1L,{20131,1,10,0,-11539},0x44FA6FA3L,0x3D84D2C8L}, {0xA6642A46128A7D53LL,{2018,0,-28,0,-9797},0x5A14CF5EL,0xA1C66547L}, {-4L,{-9931,0,7,0,-6381},0x44B5EA75L,0x6BC8E85CL}, {0xFD730CA24AB212BELL,{18325,-1,56,0,-3238},0L,0x469A6C37L}, {-4L,{-9931,0,7,0,-6381},0x44B5EA75L,0x6BC8E85CL}, {0xA6642A46128A7D53LL,{2018,0,-28,0,-9797},0x5A14CF5EL,0xA1C66547L}, {1L,{20131,1,10,0,-11539},0x44FA6FA3L,0x3D84D2C8L}}}, {{{0x0B82DA26440F5263LL,{3916,-0,49,0,11043},0L,1UL}, {7L,{-14873,-0,-58,0,7634},3L,18446744073709551610UL}, {1L,{20131,1,10,0,-11539},0x44FA6FA3L,0x3D84D2C8L}, {0xA6642A46128A7D53LL,{2018,0,-28,0,-9797},0x5A14CF5EL,0xA1C66547L}, {-4L,{-9931,0,7,0,-6381},0x44B5EA75L,0x6BC8E85CL}, {0xFD730CA24AB212BELL,{18325,-1,56,0,-3238},0L,0x469A6C37L}, {-4L,{-9931,0,7,0,-6381},0x44B5EA75L,0x6BC8E85CL}, {0xA6642A46128A7D53LL,{2018,0,-28,0,-9797},0x5A14CF5EL,0xA1C66547L}, {1L,{20131,1,10,0,-11539},0x44FA6FA3L,0x3D84D2C8L}}}, {{{0x0B82DA26440F5263LL,{3916,-0,49,0,11043},0L,1UL}, {7L,{-14873,-0,-58,0,7634},3L,18446744073709551610UL}, {1L,{20131,1,10,0,-11539},0x44FA6FA3L,0x3D84D2C8L}, {0xA6642A46128A7D53LL,{2018,0,-28,0,-9797},0x5A14CF5EL,0xA1C66547L}, {-4L,{-9931,0,7,0,-6381},0x44B5EA75L,0x6BC8E85CL}, {0xFD730CA24AB212BELL,{18325,-1,56,0,-3238},0L,0x469A6C37L}, {-4L,{-9931,0,7,0,-6381},0x44B5EA75L,0x6BC8E85CL}, {0xA6642A46128A7D53LL,{2018,0,-28,0,-9797},0x5A14CF5EL,0xA1C66547L}, {1L,{20131,1,10,0,-11539},0x44FA6FA3L,0x3D84D2C8L}}}, {{{0x0B82DA26440F5263LL,{3916,-0,49,0,11043},0L,1UL}, {7L,{-14873,-0,-58,0,7634},3L,18446744073709551610UL}, {1L,{20131,1,10,0,-11539},0x44FA6FA3L,0x3D84D2C8L}, {0xA6642A46128A7D53LL,{2018,0,-28,0,-9797},0x5A14CF5EL,0xA1C66547L}, {-4L,{-9931,0,7,0,-6381},0x44B5EA75L,0x6BC8E85CL}, {0xFD730CA24AB212BELL,{18325,-1,56,0,-3238},0L,0x469A6C37L}, {-4L,{-9931,0,7,0,-6381},0x44B5EA75L,0x6BC8E85CL}, {0xA6642A46128A7D53LL,{2018,0,-28,0,-9797},0x5A14CF5EL,0xA1C66547L}, {1L,{20131,1,10,0,-11539},0x44FA6FA3L,0x3D84D2C8L}}}, {{{0x0B82DA26440F5263LL,{3916,-0,49,0,11043},0L,1UL}, {7L,{-14873,-0,-58,0,7634},3L,18446744073709551610UL}, {1L,{20131,1,10,0,-11539},0x44FA6FA3L,0x3D84D2C8L}, {0xA6642A46128A7D53LL,{2018,0,-28,0,-9797},0x5A14CF5EL,0xA1C66547L}, {-4L,{-9931,0,7,0,-6381},0x44B5EA75L,0x6BC8E85CL}, {0xFD730CA24AB212BELL,{18325,-1,56,0,-3238},0L,0x469A6C37L}, {-4L,{-9931,0,7,0,-6381},0x44B5EA75L,0x6BC8E85CL}, {0xA6642A46128A7D53LL,{2018,0,-28,0,-9797},0x5A14CF5EL,0xA1C66547L}, {1L,{20131,1,10,0,-11539},0x44FA6FA3L,0x3D84D2C8L}}}, {{{0x0B82DA26440F5263LL,{3916,-0,49,0,11043},0L,1UL}, {7L,{-14873,-0,-58,0,7634},3L,18446744073709551610UL}, {1L,{20131,1,10,0,-11539},0x44FA6FA3L,0x3D84D2C8L}, {0xA6642A46128A7D53LL,{2018,0,-28,0,-9797},0x5A14CF5EL,0xA1C66547L}, {-4L,{-9931,0,7,0,-6381},0x44B5EA75L,0x6BC8E85CL}, {0xFD730CA24AB212BELL,{18325,-1,56,0,-3238},0L,0x469A6C37L}, {-4L,{-9931,0,7,0,-6381},0x44B5EA75L,0x6BC8E85CL}, {0xA6642A46128A7D53LL,{2018,0,-28,0,-9797},0x5A14CF5EL,0xA1C66547L}, {1L,{20131,1,10,0,-11539},0x44FA6FA3L,0x3D84D2C8L}}}, {{{0x0B82DA26440F5263LL,{3916,-0,49,0,11043},0L,1UL}, {7L,{-14873,-0,-58,0,7634},3L,18446744073709551610UL}, {1L,{20131,1,10,0,-11539},0x44FA6FA3L,0x3D84D2C8L}, {0xA6642A46128A7D53LL,{2018,0,-28,0,-9797},0x5A14CF5EL,0xA1C66547L}, {-4L,{-9931,0,7,0,-6381},0x44B5EA75L,0x6BC8E85CL}, {0xFD730CA24AB212BELL,{18325,-1,56,0,-3238},0L,0x469A6C37L}, {-4L,{-9931,0,7,0,-6381},0x44B5EA75L,0x6BC8E85CL}, {0xA6642A46128A7D53LL,{2018,0,-28,0,-9797},0x5A14CF5EL,0xA1C66547L}, {1L,{20131,1,10,0,-11539},0x44FA6FA3L,0x3D84D2C8L}}}};
                        int32_t *l_420 = (void*)0;
                        int32_t *l_421 = (void*)0;
                        int32_t *l_422[2][1];
                        int16_t l_424 = 1L;
                        int i, j, k;
                        for (i = 0; i < 2; i++)
                        {
                            for (j = 0; j < 1; j++)
                                l_422[i][j] = &l_176;
                        }
                        g_60[l_178][(l_386 + 8)] = (safe_rshift_func_uint16_t_u_u(g_113[l_386][l_386], 7));
                        l_316 = (l_419[6][0][4] , 4L);
                        ++l_425;
                    }
                    l_390 |= g_60[l_178][l_178];
                    for (g_228.f0 = 0; (g_228.f0 <= 1); g_228.f0 += 1)
                    { /* block id: 259 */
                        int32_t *l_430 = &l_182[5];
                        int32_t *l_431 = &l_182[8];
                        int32_t *l_432 = (void*)0;
                        int32_t *l_433[7];
                        struct S3 **l_439 = &g_75;
                        int16_t **l_444 = &l_299;
                        int16_t ***l_443 = &l_444;
                        int i;
                        for (i = 0; i < 7; i++)
                            l_433[i] = &l_389;
                        l_436++;
                        (*l_439) = &g_76;
                        ++l_440;
                        (*l_443) = &l_299;
                    }
                }
                ++l_448;
                return &g_261;
            }
            if ((*l_235))
            { /* block id: 269 */
                int32_t l_451[7] = {0L, 0L, 0x5640E0A4L, 0L, 0L, 0x5640E0A4L, 0L};
                int32_t l_464 = 0x34EA3BA7L;
                int32_t l_465[8];
                const int16_t *l_475[2];
                const int16_t **l_474 = &l_475[1];
                int i;
                for (i = 0; i < 8; i++)
                    l_465[i] = 0xB7DD79B2L;
                for (i = 0; i < 2; i++)
                    l_475[i] = (void*)0;
                if (((func_47(l_451[1]) > (safe_div_func_uint64_t_u_u(l_454, g_415.f4))) && (safe_rshift_func_int16_t_s_u(func_52(l_451[1]), 7))))
                { /* block id: 270 */
                    int64_t l_463[7][6] = {{2L, 2L, 0L, 0xAD68A61375C96C76LL, 4L, 0xAD68A61375C96C76LL}, {2L, 2L, 0L, 0xAD68A61375C96C76LL, 4L, 0xAD68A61375C96C76LL}, {2L, 2L, 0L, 0xAD68A61375C96C76LL, 4L, 0xAD68A61375C96C76LL}, {2L, 2L, 0L, 0xAD68A61375C96C76LL, 4L, 0xAD68A61375C96C76LL}, {2L, 2L, 0L, 0xAD68A61375C96C76LL, 4L, 0xAD68A61375C96C76LL}, {2L, 2L, 0L, 0xAD68A61375C96C76LL, 4L, 0xAD68A61375C96C76LL}, {2L, 2L, 0L, 0xAD68A61375C96C76LL, 4L, 0xAD68A61375C96C76LL}};
                    int32_t l_466[6][9][4] = {{{0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}}, {{0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}}, {{0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}}, {{0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}}, {{0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}}, {{0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}, {0x99AD8EF5L, 0x0B0D003DL, 6L, 0xD9BB628FL}}};
                    int i, j, k;
                    if ((!((g_228 , l_447) < g_76.f1.f0)))
                    { /* block id: 271 */
                        int32_t *l_457 = &l_383;
                        int32_t *l_458 = &l_389;
                        int32_t *l_459 = &l_390;
                        int32_t *l_460 = &l_182[2];
                        int32_t *l_461 = &g_60[0][8];
                        int32_t *l_462[6] = {&l_389, &l_389, &l_447, &l_389, &l_389, &l_447};
                        uint32_t l_467 = 0x16F8B90AL;
                        int i;
                        ++l_467;
                    }
                    else
                    { /* block id: 273 */
                        int32_t *l_470 = &l_447;
                        const int16_t *l_472[1];
                        const int16_t **l_471 = &l_472[0];
                        const int16_t ***l_473[3];
                        int32_t **l_483 = &g_111;
                        int32_t **l_484[8][2] = {{(void*)0, &l_235}, {(void*)0, &l_235}, {(void*)0, &l_235}, {(void*)0, &l_235}, {(void*)0, &l_235}, {(void*)0, &l_235}, {(void*)0, &l_235}, {(void*)0, &l_235}};
                        int i, j;
                        for (i = 0; i < 1; i++)
                            l_472[i] = &g_113[0][0];
                        for (i = 0; i < 3; i++)
                            l_473[i] = &l_471;
                        (*l_470) ^= l_464;
                        l_466[1][5][0] &= (((p_39.f0 , func_47(((l_474 = l_471) == (void*)0))) & (safe_sub_func_int16_t_s_s((safe_lshift_func_int16_t_s_s(g_117, 14)), (safe_mod_func_uint8_t_u_u(0x1BL, ((*l_470) ^ ((p_38 == (-1L)) , 0xCDD33B24L))))))) < g_482);
                        l_235 = ((*l_483) = p_41);
                    }
                    l_389 = (safe_unary_minus_func_int8_t_s(0x8AL));
                    (*l_131) = (*l_131);
                    return &g_261;
                }
                else
                { /* block id: 283 */
                    struct S3 **l_487 = &l_486;
                    (*l_487) = l_486;
                }
            }
            else
            { /* block id: 286 */
                l_381 = p_38;
            }
            if (p_38)
                break;
        }
        for (l_356 = 4; (l_356 >= 1); l_356 -= 1)
        { /* block id: 293 */
            uint32_t **l_497[9][2] = {{&g_64, &g_64}, {&g_64, &g_64}, {&g_64, &g_64}, {&g_64, &g_64}, {&g_64, &g_64}, {&g_64, &g_64}, {&g_64, &g_64}, {&g_64, &g_64}, {&g_64, &g_64}};
            int32_t l_502 = 0x6069987CL;
            int32_t l_566 = 0x93F96888L;
            int32_t l_569[1];
            int32_t **l_579 = &g_111;
            int i, j;
            for (i = 0; i < 1; i++)
                l_569[i] = 1L;
            if ((*l_235))
            { /* block id: 294 */
                int16_t l_493 = 5L;
                int32_t l_532 = (-9L);
                (*l_235) |= func_47(func_47(p_38));
                if ((((safe_add_func_int8_t_s_s((((safe_rshift_func_int16_t_s_s(func_52((1UL && g_103)), 15)) , (p_39.f0 , g_60[5][5])) || (func_47(((*l_235) |= (0x2DC9DB21FF5F84A0LL != 0UL))) >= ((g_76.f0 = 0xF25EC14DC0C14756LL) & 18446744073709551615UL))), l_492)) && 0xB41FE537ACF50A80LL) == l_493))
                { /* block id: 298 */
                    struct S2 l_494 = {10963,1,42,0,-2624};
                    for (g_76.f3 = 0; (g_76.f3 <= 4); g_76.f3 += 1)
                    { /* block id: 301 */
                        uint32_t ***l_498 = &l_497[3][0];
                        int8_t *l_499 = &g_228.f0;
                        int32_t **l_503 = (void*)0;
                        int32_t **l_504 = &g_111;
                        int i;
                        (*l_504) = ((((l_494 , (func_47(((safe_mul_func_uint8_t_u_u((&p_41 == ((*l_498) = l_497[3][0])), ((*l_499) = p_39.f0))) , l_494.f4)) == ((((safe_mul_func_int8_t_s_s(((l_502 ^ ((**g_191) = 2UL)) & (0L == 5UL)), g_76.f1.f2)) == p_39.f0) && g_482) & (-7L)))) <= g_415.f4) | g_76.f1.f0) , &l_176);
                        (*l_235) |= (safe_sub_func_int16_t_s_s((safe_rshift_func_uint16_t_u_u(func_52(g_228.f0), 3)), ((*l_131) , ((l_494.f4 != ((*l_486) , (((safe_add_func_int32_t_s_s(((**l_504) = (safe_sub_func_uint8_t_u_u(0x00L, g_58))), p_39.f0)) != 0xC68C47A0L) > p_39.f0))) || g_113[2][0]))));
                        l_235 = p_41;
                        if (l_494.f4)
                            break;
                    }
                }
                else
                { /* block id: 311 */
                    int32_t *l_531[10][6][4] = {{{&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}}, {{&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}}, {{&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}}, {{&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}}, {{&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}}, {{&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}}, {{&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}}, {{&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}}, {{&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}}, {{&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}, {&l_178, &l_176, &l_178, &g_116}}};
                    struct S0 *l_558 = (void*)0;
                    int i, j, k;
                    if ((safe_lshift_func_uint16_t_u_s((((safe_add_func_uint32_t_u_u(1UL, ((*g_64) , 0x70B3D5BDL))) ^ (safe_div_func_uint16_t_u_u(func_52(((l_177 = (safe_lshift_func_int16_t_s_s(func_52(((g_76.f1.f1 >= g_119.f0) || (((safe_lshift_func_uint8_t_u_u((safe_sub_func_uint32_t_u_u(p_38, ((++(**g_191)) || (func_52(p_38) == ((g_13.f0 >= g_76.f1.f4) , p_38))))), p_38)) >= g_119.f0) <= 0x458DL))), 7))) < g_56)), l_502))) , 1UL), g_76.f1.f4)))
                    { /* block id: 314 */
                        uint16_t l_533 = 3UL;
                        ++l_533;
                    }
                    else
                    { /* block id: 316 */
                        int32_t l_541[7];
                        uint32_t l_548 = 0x78B5FB88L;
                        int i;
                        for (i = 0; i < 7; i++)
                            l_541[i] = 0x8A15FD45L;
                        l_182[5] ^= ((*l_235) = (-5L));
                        if (l_356)
                            goto lbl_536;
                        l_532 ^= (safe_mul_func_int8_t_s_s((g_76.f1.f3 < (func_52(g_76.f1.f3) <= 0x86CDA0E4L)), (!(safe_lshift_func_int8_t_s_s((g_228.f0 , ((l_541[5] |= g_113[2][0]) & (0x20F3L != (safe_mod_func_uint32_t_u_u((p_39.f0 ^ (((*l_235) = (safe_div_func_uint64_t_u_u(((safe_div_func_uint32_t_u_u(4294967295UL, 0xF90AA0F6L)) > l_548), g_415.f0))) & p_39.f0)), 0x159CB124L))))), p_38)))));
                    }
                    (*l_235) = (safe_lshift_func_uint8_t_u_u(((~(((safe_sub_func_uint64_t_u_u(func_52(p_39.f0), 9UL)) && (((**g_191) = (func_52((2UL != (*l_235))) && (((safe_div_func_uint8_t_u_u(p_38, (safe_mul_func_int16_t_s_s((l_386 &= g_76.f2), p_38)))) != l_557) && (**g_191)))) , p_39.f0)) ^ p_38)) < g_76.f3), g_76.f1.f1));
                    l_558 = &p_39;
                }
                for (g_119.f0 = 26; (g_119.f0 != 18); g_119.f0--)
                { /* block id: 331 */
                    if (g_56)
                        goto lbl_360;
                }
            }
            else
            { /* block id: 334 */
                int32_t *l_562 = &l_383;
                int32_t *l_563 = &g_116;
                int32_t *l_564 = &l_133;
                int32_t *l_565[2];
                int i;
                for (i = 0; i < 2; i++)
                    l_565[i] = &l_381;
                (*l_235) ^= l_561;
                ++l_576[4];
            }
            (*l_579) = p_41;
            if ((*g_111))
                break;
        }
        (*l_235) = ((safe_rshift_func_int16_t_s_u((-1L), (safe_div_func_uint64_t_u_u((safe_mul_func_uint8_t_u_u(0xCAL, ((g_482 == g_76.f1.f1) != 1UL))), (safe_div_func_int64_t_s_s(((*l_593) |= ((**g_191) == (safe_mul_func_uint16_t_u_u((&g_191 == (g_117 , ((*l_591) = &g_191))), g_261)))), p_39.f0)))))) > p_39.f0);
    }
    else
    { /* block id: 344 */
        struct S0 l_598 = {8L};
        int64_t ***l_608 = (void*)0;
        const uint32_t l_611 = 0x3C019AF0L;
        int32_t l_626 = 0x6F4F68A2L;
        int32_t l_628[1];
        struct S1 *l_654 = &g_228;
        int32_t *l_697 = (void*)0;
        int32_t l_738 = 0L;
        struct S2 **l_764[5];
        uint16_t l_880 = 1UL;
        int32_t l_914[5] = {(-2L), 0xCA351644L, (-2L), 0xCA351644L, (-2L)};
        uint32_t **l_920 = (void*)0;
        uint32_t *l_922 = &g_117;
        uint32_t **l_921 = &l_922;
        int16_t *l_926 = &l_492;
        int16_t *l_927 = &l_492;
        int16_t *l_928 = &l_492;
        int16_t *l_929 = &l_492;
        int16_t *l_930[10][2] = {{&g_113[3][1], (void*)0}, {&g_113[3][1], (void*)0}, {&g_113[3][1], (void*)0}, {&g_113[3][1], (void*)0}, {&g_113[3][1], (void*)0}, {&g_113[3][1], (void*)0}, {&g_113[3][1], (void*)0}, {&g_113[3][1], (void*)0}, {&g_113[3][1], (void*)0}, {&g_113[3][1], (void*)0}};
        int16_t ** const l_925[2][7] = {{&l_927, &l_930[8][1], &l_929, &l_930[8][1], &l_927, &l_926, &l_927}, {&l_927, &l_930[8][1], &l_929, &l_930[8][1], &l_927, &l_926, &l_927}};
        int16_t ** const *l_924[4][10] = {{(void*)0, &l_925[1][6], &l_925[0][6], &l_925[1][1], &l_925[1][6], &l_925[1][6], &l_925[1][6], &l_925[1][1], &l_925[0][6], &l_925[1][6]}, {(void*)0, &l_925[1][6], &l_925[0][6], &l_925[1][1], &l_925[1][6], &l_925[1][6], &l_925[1][6], &l_925[1][1], &l_925[0][6], &l_925[1][6]}, {(void*)0, &l_925[1][6], &l_925[0][6], &l_925[1][1], &l_925[1][6], &l_925[1][6], &l_925[1][6], &l_925[1][1], &l_925[0][6], &l_925[1][6]}, {(void*)0, &l_925[1][6], &l_925[0][6], &l_925[1][1], &l_925[1][6], &l_925[1][6], &l_925[1][6], &l_925[1][1], &l_925[0][6], &l_925[1][6]}};
        int16_t ** const **l_923[10] = {&l_924[2][8], &l_924[0][1], &l_924[1][4], &l_924[1][4], &l_924[0][1], &l_924[2][8], &l_924[0][1], &l_924[1][4], &l_924[1][4], &l_924[0][1]};
        int8_t *l_944 = &g_96;
        int i, j;
        for (i = 0; i < 1; i++)
            l_628[i] = 0x3AE536DDL;
        for (i = 0; i < 5; i++)
            l_764[i] = &g_414;
        if (((safe_div_func_int8_t_s_s((g_596 != (l_598 , l_599[0][3][3])), (safe_mul_func_uint8_t_u_u((safe_add_func_int64_t_s_s((safe_rshift_func_int8_t_s_s((g_96 |= (((void*)0 != l_608) | (p_38 != (l_598.f0 ^ (safe_rshift_func_uint8_t_u_s(((g_60[0][8] & ((*l_235) , 255UL)) <= 0xCC25L), 6)))))), l_611)), (*l_235))), (-4L))))) >= (*l_235)))
        { /* block id: 346 */
            int16_t l_618 = 0xB0ADL;
            int32_t l_624[2];
            int16_t l_637 = 0x7ACFL;
            int64_t ** const l_661[7][1] = {{&l_601[1]}, {&l_601[1]}, {&l_601[1]}, {&l_601[1]}, {&l_601[1]}, {&l_601[1]}, {&l_601[1]}};
            int32_t l_684[6][7] = {{0x485D5557L, (-4L), (-5L), 0xFF14E1C3L, (-4L), 0x066BF22EL, (-4L)}, {0x485D5557L, (-4L), (-5L), 0xFF14E1C3L, (-4L), 0x066BF22EL, (-4L)}, {0x485D5557L, (-4L), (-5L), 0xFF14E1C3L, (-4L), 0x066BF22EL, (-4L)}, {0x485D5557L, (-4L), (-5L), 0xFF14E1C3L, (-4L), 0x066BF22EL, (-4L)}, {0x485D5557L, (-4L), (-5L), 0xFF14E1C3L, (-4L), 0x066BF22EL, (-4L)}, {0x485D5557L, (-4L), (-5L), 0xFF14E1C3L, (-4L), 0x066BF22EL, (-4L)}};
            int32_t *l_732 = (void*)0;
            int32_t *l_733 = &l_178;
            struct S2 l_760 = {22993,0,20,0,10910};
            int64_t l_800 = 0xFE41B577EFF9FFCBLL;
            int i, j;
            for (i = 0; i < 2; i++)
                l_624[i] = 0x041B7FABL;
            if (p_38)
            { /* block id: 347 */
                const int32_t **l_613 = &l_612;
                uint8_t *l_616 = (void*)0;
                uint8_t *l_617 = &g_125;
                (*l_613) = l_612;
                (*l_235) |= (p_39.f0 > ((*g_414) , func_52((l_618 = (safe_sub_func_uint8_t_u_u(((*l_617) = 0x3DL), 0xD0L))))));
            }
            else
            { /* block id: 352 */
                int32_t *l_619 = &l_133;
                int32_t **l_620[7];
                int i;
                for (i = 0; i < 7; i++)
                    l_620[i] = &g_111;
                l_612 = l_619;
            }
            for (g_58 = 0; (g_58 < (-26)); --g_58)
            { /* block id: 357 */
                int8_t l_625[5][3] = {{0xC3L, 0xC3L, 0L}, {0xC3L, 0xC3L, 0L}, {0xC3L, 0xC3L, 0L}, {0xC3L, 0xC3L, 0L}, {0xC3L, 0xC3L, 0L}};
                int32_t l_627 = (-1L);
                int32_t l_629 = 0x9FCDCEE9L;
                int32_t l_630 = 8L;
                int32_t l_632 = 0xA88789C9L;
                int32_t l_633 = (-4L);
                int32_t l_634 = (-5L);
                int32_t l_635 = (-1L);
                int32_t l_636 = 0x739C8623L;
                int32_t l_638 = 0x494DC427L;
                int32_t l_639 = 0xD84CF5F3L;
                int32_t l_640[7] = {(-1L), 0x0FF96024L, (-1L), 0x0FF96024L, (-1L), 0x0FF96024L, (-1L)};
                int32_t l_641 = 0xDC194E33L;
                uint32_t *l_662 = &g_65;
                const struct S2 l_694 = {15046,0,-49,0,-9870};
                uint64_t ** const l_709 = &g_192[3];
                int i, j;
                for (l_574 = 1; (l_574 >= 0); l_574 -= 1)
                { /* block id: 360 */
                    int32_t *l_623[8][4][8] = {{{&g_116, &l_182[5], &l_182[6], &g_60[0][8], &g_76.f2, &g_116, (void*)0, &g_76.f2}, {&g_116, &l_182[5], &l_182[6], &g_60[0][8], &g_76.f2, &g_116, (void*)0, &g_76.f2}, {&g_116, &l_182[5], &l_182[6], &g_60[0][8], &g_76.f2, &g_116, (void*)0, &g_76.f2}, {&g_116, &l_182[5], &l_182[6], &g_60[0][8], &g_76.f2, &g_116, (void*)0, &g_76.f2}}, {{&g_116, &l_182[5], &l_182[6], &g_60[0][8], &g_76.f2, &g_116, (void*)0, &g_76.f2}, {&g_116, &l_182[5], &l_182[6], &g_60[0][8], &g_76.f2, &g_116, (void*)0, &g_76.f2}, {&g_116, &l_182[5], &l_182[6], &g_60[0][8], &g_76.f2, &g_116, (void*)0, &g_76.f2}, {&g_116, &l_182[5], &l_182[6], &g_60[0][8], &g_76.f2, &g_116, (void*)0, &g_76.f2}}, {{&g_116, &l_182[5], &l_182[6], &g_60[0][8], &g_76.f2, &g_116, (void*)0, &g_76.f2}, {&g_116, &l_182[5], &l_182[6], &g_60[0][8], &g_76.f2, &g_116, (void*)0, &g_76.f2}, {&g_116, &l_182[5], &l_182[6], &g_60[0][8], &g_76.f2, &g_116, (void*)0, &g_76.f2}, {&g_116, &l_182[5], &l_182[6], &g_60[0][8], &g_76.f2, &g_116, (void*)0, &g_76.f2}}, {{&g_116, &l_182[5], &l_182[6], &g_60[0][8], &g_76.f2, &g_116, (void*)0, &g_76.f2}, {&g_116, &l_182[5], &l_182[6], &g_60[0][8], &g_76.f2, &g_116, (void*)0, &g_76.f2}, {&g_116, &l_182[5], &l_182[6], &g_60[0][8], &g_76.f2, &g_116, (void*)0, &g_76.f2}, {&g_116, &l_182[5], &l_182[6], &g_60[0][8], &g_76.f2, &g_116, (void*)0, &g_76.f2}}, {{&g_116, &l_182[5], &l_182[6], &g_60[0][8], &g_76.f2, &g_116, (void*)0, &g_76.f2}, {&g_116, &l_182[5], &l_182[6], &g_60[0][8], &g_76.f2, &g_116, (void*)0, &g_76.f2}, {&g_116, &l_182[5], &l_182[6], &g_60[0][8], &g_76.f2, &g_116, (void*)0, &g_76.f2}, {&g_116, &l_182[5], &l_182[6], &g_60[0][8], &g_76.f2, &g_116, (void*)0, &g_76.f2}}, {{&g_116, &l_182[5], &l_182[6], &g_60[0][8], &g_76.f2, &g_116, (void*)0, &g_76.f2}, {&g_116, &l_182[5], &l_182[6], &g_60[0][8], &g_76.f2, &g_116, (void*)0, &g_76.f2}, {&g_116, &l_182[5], &l_182[6], &g_60[0][8], &g_76.f2, &g_116, (void*)0, &g_76.f2}, {&g_116, &l_182[5], &l_182[6], &g_60[0][8], &g_76.f2, &g_116, (void*)0, &g_76.f2}}, {{&g_116, &l_182[5], &l_182[6], &g_60[0][8], &g_76.f2, &g_116, (void*)0, &g_76.f2}, {&g_116, &l_182[5], &l_182[6], &g_60[0][8], &g_76.f2, &g_116, (void*)0, &g_76.f2}, {&g_116, &l_182[5], &l_182[6], &g_60[0][8], &g_76.f2, &g_116, (void*)0, &g_76.f2}, {&g_116, &l_182[5], &l_182[6], &g_60[0][8], &g_76.f2, &g_116, (void*)0, &g_76.f2}}, {{&g_116, &l_182[5], &l_182[6], &g_60[0][8], &g_76.f2, &g_116, (void*)0, &g_76.f2}, {&g_116, &l_182[5], &l_182[6], &g_60[0][8], &g_76.f2, &g_116, (void*)0, &g_76.f2}, {&g_116, &l_182[5], &l_182[6], &g_60[0][8], &g_76.f2, &g_116, (void*)0, &g_76.f2}, {&g_116, &l_182[5], &l_182[6], &g_60[0][8], &g_76.f2, &g_116, (void*)0, &g_76.f2}}};
                    int i, j, k;
                    ++g_642;
                }
                for (l_176 = 0; (l_176 >= 6); l_176++)
                { /* block id: 365 */
                    struct S2 l_648 = {-195,-1,16,0,6602};
                    struct S1 *l_651[7] = {&g_287, &g_13, &g_287, &g_13, &g_287, &g_13, &g_287};
                    struct S1 **l_655[6];
                    uint32_t *l_657 = (void*)0;
                    uint32_t *l_658 = &g_117;
                    const int32_t **l_659 = (void*)0;
                    const int32_t *l_660 = &l_636;
                    int i;
                    for (i = 0; i < 6; i++)
                        l_655[i] = &l_654;
                    for (p_39.f0 = 7; (p_39.f0 >= 0); p_39.f0 -= 1)
                    { /* block id: 368 */
                        int32_t *l_647 = &l_182[5];
                        int i;
                        l_647 = l_647;
                        l_182[(p_39.f0 + 1)] ^= p_38;
                        if (p_38)
                            break;
                    }
                    l_632 = ((*g_75) , (0UL && (g_261 && ((l_648 , ((*l_658) = (0xFA7B87D9L | (safe_rshift_func_uint16_t_u_u((l_651[0] == ((safe_mod_func_int8_t_s_s(p_38, (*l_235))) , (g_656 = l_654))), l_628[0]))))) ^ 0x0F38D854L))));
                    l_660 = p_40;
                }
                if (func_52(p_38))
                { /* block id: 378 */
                    int32_t l_664 = 0x69514CEFL;
                    int32_t l_665[9] = {0xF8E9F378L, (-6L), 0xF8E9F378L, (-6L), 0xF8E9F378L, (-6L), 0xF8E9F378L, (-6L), 0xF8E9F378L};
                    uint32_t l_666 = 0xBA4E8AA5L;
                    int i;
                    if ((l_661[6][0] != (*g_596)))
                    { /* block id: 379 */
                        return l_662;
                    }
                    else
                    { /* block id: 381 */
                        int32_t *l_663[10] = {&l_182[0], &l_630, &l_182[0], &l_630, &l_182[0], &l_630, &l_182[0], &l_630, &l_182[0], &l_630};
                        const int64_t ***l_677[8][4] = {{&g_675, &g_675, &g_675, &g_675}, {&g_675, &g_675, &g_675, &g_675}, {&g_675, &g_675, &g_675, &g_675}, {&g_675, &g_675, &g_675, &g_675}, {&g_675, &g_675, &g_675, &g_675}, {&g_675, &g_675, &g_675, &g_675}, {&g_675, &g_675, &g_675, &g_675}, {&g_675, &g_675, &g_675, &g_675}};
                        int i, j;
                        --l_666;
                        (*l_235) = ((**g_191) && (((safe_add_func_int16_t_s_s((((l_626 , (func_52((func_52(p_38) & ((*l_235) , (safe_add_func_int8_t_s_s(((*g_656) , (safe_rshift_func_uint16_t_u_u(((g_675 = g_675) == ((*l_235) , (*g_596))), 4))), p_38))))) || g_415.f1)) & 0x502038B0L) , l_637), p_38)) == l_664) & 1L));
                    }
                }
                else
                { /* block id: 386 */
                    uint64_t ****l_683 = (void*)0;
                    uint32_t l_705 = 0x0B2F9A3EL;
                    for (l_635 = 15; (l_635 < (-24)); l_635 = safe_sub_func_uint16_t_u_u(l_635, 1))
                    { /* block id: 389 */
                        const uint64_t *****l_682 = &g_680;
                        (*l_235) = 0x9D7B1428L;
                        (*l_235) |= (((((*l_682) = g_680) == l_683) && (+func_52((l_684[4][5] = (l_634 = (**g_675)))))) & g_575);
                        (*l_235) |= 1L;
                    }
                    for (l_635 = 0; (l_635 <= 0); l_635 += 1)
                    { /* block id: 399 */
                        struct S3 l_693[10][4] = {{{0xEF154F9131211011LL,{-15333,1,13,0,-5184},0xA69D1C8FL,0x2BFD5E07L}, {0x5741F402F56346BBLL,{-17586,-0,-39,0,-5481},1L,0xFA8AFFF3L}, {0L,{7806,-1,36,0,-8954},-1L,0xC970ED06L}, {0x5741F402F56346BBLL,{-17586,-0,-39,0,-5481},1L,0xFA8AFFF3L}}, {{0xEF154F9131211011LL,{-15333,1,13,0,-5184},0xA69D1C8FL,0x2BFD5E07L}, {0x5741F402F56346BBLL,{-17586,-0,-39,0,-5481},1L,0xFA8AFFF3L}, {0L,{7806,-1,36,0,-8954},-1L,0xC970ED06L}, {0x5741F402F56346BBLL,{-17586,-0,-39,0,-5481},1L,0xFA8AFFF3L}}, {{0xEF154F9131211011LL,{-15333,1,13,0,-5184},0xA69D1C8FL,0x2BFD5E07L}, {0x5741F402F56346BBLL,{-17586,-0,-39,0,-5481},1L,0xFA8AFFF3L}, {0L,{7806,-1,36,0,-8954},-1L,0xC970ED06L}, {0x5741F402F56346BBLL,{-17586,-0,-39,0,-5481},1L,0xFA8AFFF3L}}, {{0xEF154F9131211011LL,{-15333,1,13,0,-5184},0xA69D1C8FL,0x2BFD5E07L}, {0x5741F402F56346BBLL,{-17586,-0,-39,0,-5481},1L,0xFA8AFFF3L}, {0L,{7806,-1,36,0,-8954},-1L,0xC970ED06L}, {0x5741F402F56346BBLL,{-17586,-0,-39,0,-5481},1L,0xFA8AFFF3L}}, {{0xEF154F9131211011LL,{-15333,1,13,0,-5184},0xA69D1C8FL,0x2BFD5E07L}, {0x5741F402F56346BBLL,{-17586,-0,-39,0,-5481},1L,0xFA8AFFF3L}, {0L,{7806,-1,36,0,-8954},-1L,0xC970ED06L}, {0x5741F402F56346BBLL,{-17586,-0,-39,0,-5481},1L,0xFA8AFFF3L}}, {{0xEF154F9131211011LL,{-15333,1,13,0,-5184},0xA69D1C8FL,0x2BFD5E07L}, {0x5741F402F56346BBLL,{-17586,-0,-39,0,-5481},1L,0xFA8AFFF3L}, {0L,{7806,-1,36,0,-8954},-1L,0xC970ED06L}, {0x5741F402F56346BBLL,{-17586,-0,-39,0,-5481},1L,0xFA8AFFF3L}}, {{0xEF154F9131211011LL,{-15333,1,13,0,-5184},0xA69D1C8FL,0x2BFD5E07L}, {0x5741F402F56346BBLL,{-17586,-0,-39,0,-5481},1L,0xFA8AFFF3L}, {0L,{7806,-1,36,0,-8954},-1L,0xC970ED06L}, {0x5741F402F56346BBLL,{-17586,-0,-39,0,-5481},1L,0xFA8AFFF3L}}, {{0xEF154F9131211011LL,{-15333,1,13,0,-5184},0xA69D1C8FL,0x2BFD5E07L}, {0x5741F402F56346BBLL,{-17586,-0,-39,0,-5481},1L,0xFA8AFFF3L}, {0L,{7806,-1,36,0,-8954},-1L,0xC970ED06L}, {0x5741F402F56346BBLL,{-17586,-0,-39,0,-5481},1L,0xFA8AFFF3L}}, {{0xEF154F9131211011LL,{-15333,1,13,0,-5184},0xA69D1C8FL,0x2BFD5E07L}, {0x5741F402F56346BBLL,{-17586,-0,-39,0,-5481},1L,0xFA8AFFF3L}, {0L,{7806,-1,36,0,-8954},-1L,0xC970ED06L}, {0x5741F402F56346BBLL,{-17586,-0,-39,0,-5481},1L,0xFA8AFFF3L}}, {{0xEF154F9131211011LL,{-15333,1,13,0,-5184},0xA69D1C8FL,0x2BFD5E07L}, {0x5741F402F56346BBLL,{-17586,-0,-39,0,-5481},1L,0xFA8AFFF3L}, {0L,{7806,-1,36,0,-8954},-1L,0xC970ED06L}, {0x5741F402F56346BBLL,{-17586,-0,-39,0,-5481},1L,0xFA8AFFF3L}}};
                        int i, j;
                        if (l_628[l_635])
                            break;
                        (*l_235) &= ((safe_add_func_int16_t_s_s(((l_687 != &p_40) < l_628[l_635]), (-5L))) >= (safe_div_func_int64_t_s_s(0x5D3F371066EFC4FFLL, l_628[l_635])));
                        (*l_235) = (safe_mul_func_int8_t_s_s((((safe_unary_minus_func_uint16_t_u(((p_38 || l_628[l_635]) , l_628[l_635]))) > (l_693[1][3] , (0xDF61C129L != (l_694 , g_415.f0)))) > (safe_mod_func_int16_t_s_s((!g_288), (~(-9L))))), g_119.f0));
                        l_697 = &l_628[l_635];
                    }
                    for (g_228.f0 = (-11); (g_228.f0 >= (-13)); g_228.f0 = safe_sub_func_uint8_t_u_u(g_228.f0, 1))
                    { /* block id: 407 */
                        int32_t **l_700 = &l_697;
                        uint16_t *l_706 = (void*)0;
                        int32_t *l_722 = &g_76.f2;
                        (*l_700) = (void*)0;
                        l_626 = ((safe_add_func_int64_t_s_s(((g_642 = (((-3L) != (safe_lshift_func_uint16_t_u_s((p_38 < l_705), 1))) & ((*l_235) = l_633))) >= (+(safe_mul_func_uint8_t_u_u((l_709 == ((safe_mul_func_uint8_t_u_u(((safe_add_func_uint32_t_u_u(l_705, func_52((*g_676)))) ^ l_624[1]), 0UL)) , (void*)0)), p_38)))), p_39.f0)) < l_705);
                        if (p_39.f0)
                            break;
                        (*l_722) ^= (safe_div_func_int64_t_s_s((safe_sub_func_uint32_t_u_u((g_113[3][0] || (l_624[1] && (p_38 , (((safe_sub_func_int8_t_s_s(0x54L, g_287.f0)) < ((safe_div_func_uint64_t_u_u(p_38, ((**l_709) = 5UL))) , p_38)) && ((~(+g_76.f1.f0)) , g_65))))), 0x320207BAL)), p_39.f0));
                    }
                }
            }
            for (g_116 = 0; (g_116 == (-30)); --g_116)
            { /* block id: 420 */
                uint16_t *l_727 = &g_482;
                uint16_t *l_730 = &g_642;
                uint16_t l_731 = 65527UL;
                int32_t **l_734 = &l_733;
                int8_t *l_735 = &g_228.f0;
                int32_t l_759 = (-10L);
                int32_t l_782 = 1L;
                int32_t l_783 = 0x1619267DL;
                int32_t l_784 = 0x416A7987L;
                int32_t l_785 = 8L;
                int32_t l_786 = 0xFD159AF5L;
                int32_t l_787 = 0x12BBB2E3L;
                int32_t l_789 = 3L;
                int32_t l_790[2][5][8] = {{{0x3058D5B9L, (-1L), 5L, 1L, 0x541AEA78L, 1L, 5L, (-1L)}, {0x3058D5B9L, (-1L), 5L, 1L, 0x541AEA78L, 1L, 5L, (-1L)}, {0x3058D5B9L, (-1L), 5L, 1L, 0x541AEA78L, 1L, 5L, (-1L)}, {0x3058D5B9L, (-1L), 5L, 1L, 0x541AEA78L, 1L, 5L, (-1L)}, {0x3058D5B9L, (-1L), 5L, 1L, 0x541AEA78L, 1L, 5L, (-1L)}}, {{0x3058D5B9L, (-1L), 5L, 1L, 0x541AEA78L, 1L, 5L, (-1L)}, {0x3058D5B9L, (-1L), 5L, 1L, 0x541AEA78L, 1L, 5L, (-1L)}, {0x3058D5B9L, (-1L), 5L, 1L, 0x541AEA78L, 1L, 5L, (-1L)}, {0x3058D5B9L, (-1L), 5L, 1L, 0x541AEA78L, 1L, 5L, (-1L)}, {0x3058D5B9L, (-1L), 5L, 1L, 0x541AEA78L, 1L, 5L, (-1L)}}};
                uint32_t l_791 = 0x2E1CB4B5L;
                int i, j, k;
                if ((0x53L | func_52((safe_sub_func_uint16_t_u_u(((*l_727) = g_76.f3), (safe_mul_func_int16_t_s_s(g_287.f0, func_47((((*l_735) |= (((*l_730) = p_39.f0) | func_52((((l_731 > (func_52(p_39.f0) == ((l_732 = &l_628[0]) != ((*l_734) = l_733)))) | g_76.f3) & 0xCFEA63A9L)))) , p_38)))))))))
                { /* block id: 426 */
                    return &g_261;
                }
                else
                { /* block id: 428 */
                    uint8_t *l_739 = &g_125;
                    uint8_t l_744 = 248UL;
                    struct S0 **l_778 = &g_777;
                    int32_t l_780 = 0xBA2D7392L;
                    int32_t l_781[9] = {0x983FD7DBL, 0x8484CEE5L, 0x983FD7DBL, 0x8484CEE5L, 0x983FD7DBL, 0x8484CEE5L, 0x983FD7DBL, 0x8484CEE5L, 0x983FD7DBL};
                    int i;
                    (*l_235) |= (safe_mod_func_int16_t_s_s(((((*l_739) = l_738) > (safe_lshift_func_uint16_t_u_s((g_117 < ((*l_735) |= (p_39.f0 || g_113[3][0]))), ((((safe_lshift_func_int8_t_s_s(g_76.f0, 4)) , 0xABL) & p_38) == 0x1AAC72E2L)))) ^ p_39.f0), 1L));
                    if ((*l_732))
                        break;
                    if (l_744)
                    { /* block id: 433 */
                        struct S3 **l_755[10][3][3] = {{{&g_75, &g_75, &g_75}, {&g_75, &g_75, &g_75}, {&g_75, &g_75, &g_75}}, {{&g_75, &g_75, &g_75}, {&g_75, &g_75, &g_75}, {&g_75, &g_75, &g_75}}, {{&g_75, &g_75, &g_75}, {&g_75, &g_75, &g_75}, {&g_75, &g_75, &g_75}}, {{&g_75, &g_75, &g_75}, {&g_75, &g_75, &g_75}, {&g_75, &g_75, &g_75}}, {{&g_75, &g_75, &g_75}, {&g_75, &g_75, &g_75}, {&g_75, &g_75, &g_75}}, {{&g_75, &g_75, &g_75}, {&g_75, &g_75, &g_75}, {&g_75, &g_75, &g_75}}, {{&g_75, &g_75, &g_75}, {&g_75, &g_75, &g_75}, {&g_75, &g_75, &g_75}}, {{&g_75, &g_75, &g_75}, {&g_75, &g_75, &g_75}, {&g_75, &g_75, &g_75}}, {{&g_75, &g_75, &g_75}, {&g_75, &g_75, &g_75}, {&g_75, &g_75, &g_75}}, {{&g_75, &g_75, &g_75}, {&g_75, &g_75, &g_75}, {&g_75, &g_75, &g_75}}};
                        const uint64_t l_758[7] = {0xB91C03533F6FF16ALL, 0xB91C03533F6FF16ALL, 0x7C3D7CC721ABED07LL, 0xB91C03533F6FF16ALL, 0xB91C03533F6FF16ALL, 0x7C3D7CC721ABED07LL, 0xB91C03533F6FF16ALL};
                        struct S2 **l_763 = &g_414;
                        int i, j, k;
                        l_759 &= ((*l_732) |= ((safe_div_func_uint32_t_u_u(0x46B53445L, (safe_div_func_int16_t_s_s(0xBD64L, g_76.f1.f0)))) < (safe_sub_func_int16_t_s_s((safe_lshift_func_uint16_t_u_u(0UL, (safe_rshift_func_int16_t_s_s(((*l_299) &= 0x91E6L), (&g_75 == l_755[0][1][1]))))), (((g_119 , (safe_lshift_func_uint8_t_u_u(func_47(((**l_734) = l_744)), g_76.f1.f0))) > l_758[0]) , g_76.f1.f0)))));
                        if ((*l_235))
                            break;
                        (**l_734) = ((l_760 , (((safe_div_func_int8_t_s_s(((((l_764[3] = l_763) != ((((*l_612) >= (safe_mod_func_int32_t_s_s((safe_sub_func_int16_t_s_s(((l_758[0] == ((g_415.f3 <= (g_76.f1.f3 && p_39.f0)) > p_38)) , ((*l_733) > 0xBE79L)), p_38)), 0xAD774CA1L))) ^ p_39.f0) , (void*)0)) | (**l_734)) , 0x39L), g_60[0][8])) >= 0xC254L) >= 0L)) <= g_116);
                    }
                    else
                    { /* block id: 441 */
                        int32_t *l_779[7] = {&g_76.f2, &g_76.f2, &g_631, &g_76.f2, &g_76.f2, &g_631, &g_76.f2};
                        int8_t l_788 = 0x9BL;
                        int i;
                        (*l_733) = ((safe_mul_func_int16_t_s_s((safe_mul_func_int8_t_s_s(p_38, (g_125 = p_39.f0))), (safe_mod_func_int8_t_s_s(((safe_unary_minus_func_uint16_t_u(((*l_727) ^= 65532UL))) , g_631), (~0x6EL))))) >= (func_52((g_776 != l_778)) || (**g_191)));
                        --l_791;
                    }
                }
                if (func_52((safe_mod_func_uint8_t_u_u(func_47((*l_235)), (~(0x9C8BL & (**l_734)))))))
                { /* block id: 448 */
                    int32_t *l_796 = &l_784;
                    int32_t l_797 = (-1L);
                    int32_t *l_798 = (void*)0;
                    int32_t *l_799[7][7] = {{(void*)0, (void*)0, &l_684[2][3], (void*)0, (void*)0, &l_684[2][3], (void*)0}, {(void*)0, (void*)0, &l_684[2][3], (void*)0, (void*)0, &l_684[2][3], (void*)0}, {(void*)0, (void*)0, &l_684[2][3], (void*)0, (void*)0, &l_684[2][3], (void*)0}, {(void*)0, (void*)0, &l_684[2][3], (void*)0, (void*)0, &l_684[2][3], (void*)0}, {(void*)0, (void*)0, &l_684[2][3], (void*)0, (void*)0, &l_684[2][3], (void*)0}, {(void*)0, (void*)0, &l_684[2][3], (void*)0, (void*)0, &l_684[2][3], (void*)0}, {(void*)0, (void*)0, &l_684[2][3], (void*)0, (void*)0, &l_684[2][3], (void*)0}};
                    int32_t l_801 = 0x75AC33ACL;
                    int i, j;
                    l_802++;
                    if (func_47(p_38))
                    { /* block id: 450 */
                        int16_t ** const *l_808 = &g_806[0];
                        (*l_796) |= (((*g_656) , g_805) == l_808);
                        (*l_734) = p_41;
                    }
                    else
                    { /* block id: 453 */
                        uint8_t *l_817 = &l_802;
                        int32_t l_818 = (-2L);
                        (*l_732) ^= (*l_796);
                        (*l_732) = ((safe_div_func_uint64_t_u_u(((**g_191) = (safe_lshift_func_uint16_t_u_s((p_38 <= (safe_rshift_func_int16_t_s_s(0x4AFBL, 0))), 7))), (-7L))) ^ (safe_mod_func_uint32_t_u_u(((((-1L) <= (*l_733)) <= ((p_38 >= p_39.f0) ^ g_76.f3)) >= ((*l_817) = g_60[5][9])), l_818)));
                        if ((*l_733))
                            break;
                        (*l_734) = &l_177;
                    }
                }
                else
                { /* block id: 461 */
                    int32_t *l_819 = &l_182[1];
                    int32_t *l_820 = (void*)0;
                    l_821--;
                }
            }
        }
        else
        { /* block id: 465 */
            int32_t *l_824 = &l_182[8];
            int32_t *l_825 = (void*)0;
            int32_t *l_826 = &g_116;
            int32_t *l_827 = (void*)0;
            int32_t l_828 = (-1L);
            int32_t *l_829 = (void*)0;
            int32_t *l_830 = &l_133;
            int32_t *l_831 = (void*)0;
            int32_t *l_832[1][2][10] = {{{&g_60[0][2], &l_738, &g_60[0][2], &l_738, &g_60[0][2], &l_738, &g_60[0][2], &l_738, &g_60[0][2], &l_738}, {&g_60[0][2], &l_738, &g_60[0][2], &l_738, &g_60[0][2], &l_738, &g_60[0][2], &l_738, &g_60[0][2], &l_738}}};
            uint8_t l_833 = 0xAAL;
            int8_t *l_836 = &g_96;
            uint8_t *l_866 = &l_833;
            int8_t *l_867 = &g_287.f0;
            uint16_t l_879[5][9] = {{0x5540L, 0x0C6BL, 0xE5CFL, 65535UL, 0xD90EL, 65535UL, 0xE5CFL, 0x0C6BL, 0x5540L}, {0x5540L, 0x0C6BL, 0xE5CFL, 65535UL, 0xD90EL, 65535UL, 0xE5CFL, 0x0C6BL, 0x5540L}, {0x5540L, 0x0C6BL, 0xE5CFL, 65535UL, 0xD90EL, 65535UL, 0xE5CFL, 0x0C6BL, 0x5540L}, {0x5540L, 0x0C6BL, 0xE5CFL, 65535UL, 0xD90EL, 65535UL, 0xE5CFL, 0x0C6BL, 0x5540L}, {0x5540L, 0x0C6BL, 0xE5CFL, 65535UL, 0xD90EL, 65535UL, 0xE5CFL, 0x0C6BL, 0x5540L}};
            struct S2 *l_883 = (void*)0;
            int i, j, k;
            (*l_235) |= p_38;
            l_833++;
            if ((((g_60[6][2] != ((*l_836) = (*l_612))) <= p_38) < (l_837 , (safe_mod_func_uint64_t_u_u((((((*l_235) = p_39.f0) <= (safe_lshift_func_uint8_t_u_u((safe_add_func_int32_t_s_s((g_76.f1.f0 && (safe_sub_func_int32_t_s_s((safe_div_func_uint16_t_u_u((safe_sub_func_int8_t_s_s(p_39.f0, ((safe_lshift_func_int8_t_s_u(((*l_867) = (safe_mul_func_int8_t_s_s(g_58, ((*l_866) |= (safe_add_func_uint64_t_u_u((safe_mod_func_uint64_t_u_u((safe_add_func_uint32_t_u_u((safe_mul_func_int16_t_s_s(((safe_rshift_func_uint16_t_u_u((((safe_add_func_int8_t_s_s(4L, 1L)) && p_39.f0) > (*l_824)), p_38)) , p_39.f0), (*l_612))), p_38)), p_39.f0)), p_39.f0)))))), 2)) | p_38))), p_38)), g_228.f0))), p_39.f0)), p_39.f0))) , (**g_675)) || 4UL), p_38)))))
            { /* block id: 472 */
                uint64_t l_876[3][8] = {{18446744073709551615UL, 4UL, 0UL, 0UL, 4UL, 18446744073709551615UL, 4UL, 0UL}, {18446744073709551615UL, 4UL, 0UL, 0UL, 4UL, 18446744073709551615UL, 4UL, 0UL}, {18446744073709551615UL, 4UL, 0UL, 0UL, 4UL, 18446744073709551615UL, 4UL, 0UL}};
                int i, j;
                (*l_235) = (safe_add_func_uint8_t_u_u((safe_lshift_func_int16_t_s_s(((*l_824) = (safe_lshift_func_int16_t_s_s((-4L), (safe_mul_func_uint16_t_u_u(g_415.f0, (((*l_299) |= ((!l_876[0][4]) == (p_38 != 0L))) != ((l_879[2][5] & p_38) , p_39.f0))))))), 4)), p_38));
                (*l_824) = l_598.f0;
            }
            else
            { /* block id: 477 */
                ++l_880;
                g_414 = l_883;
            }
        }
        if ((*l_235))
        { /* block id: 482 */
            int64_t l_887 = (-7L);
            int8_t l_888 = (-8L);
            uint16_t l_889 = 65535UL;
            for (g_125 = 0; (g_125 == 46); ++g_125)
            { /* block id: 485 */
                int32_t *l_886[9][7][3] = {{{&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}}, {{&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}}, {{&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}}, {{&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}}, {{&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}}, {{&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}}, {{&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}}, {{&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}}, {{&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}, {&l_178, &l_133, &l_178}}};
                int i, j, k;
                l_889++;
            }
        }
        else
        { /* block id: 488 */
            int32_t *l_892 = (void*)0;
            int32_t **l_893[6][5][8] = {{{&l_235, (void*)0, &l_697, (void*)0, &l_235, (void*)0, &l_697, (void*)0}, {&l_235, (void*)0, &l_697, (void*)0, &l_235, (void*)0, &l_697, (void*)0}, {&l_235, (void*)0, &l_697, (void*)0, &l_235, (void*)0, &l_697, (void*)0}, {&l_235, (void*)0, &l_697, (void*)0, &l_235, (void*)0, &l_697, (void*)0}, {&l_235, (void*)0, &l_697, (void*)0, &l_235, (void*)0, &l_697, (void*)0}}, {{&l_235, (void*)0, &l_697, (void*)0, &l_235, (void*)0, &l_697, (void*)0}, {&l_235, (void*)0, &l_697, (void*)0, &l_235, (void*)0, &l_697, (void*)0}, {&l_235, (void*)0, &l_697, (void*)0, &l_235, (void*)0, &l_697, (void*)0}, {&l_235, (void*)0, &l_697, (void*)0, &l_235, (void*)0, &l_697, (void*)0}, {&l_235, (void*)0, &l_697, (void*)0, &l_235, (void*)0, &l_697, (void*)0}}, {{&l_235, (void*)0, &l_697, (void*)0, &l_235, (void*)0, &l_697, (void*)0}, {&l_235, (void*)0, &l_697, (void*)0, &l_235, (void*)0, &l_697, (void*)0}, {&l_235, (void*)0, &l_697, (void*)0, &l_235, (void*)0, &l_697, (void*)0}, {&l_235, (void*)0, &l_697, (void*)0, &l_235, (void*)0, &l_697, (void*)0}, {&l_235, (void*)0, &l_697, (void*)0, &l_235, (void*)0, &l_697, (void*)0}}, {{&l_235, (void*)0, &l_697, (void*)0, &l_235, (void*)0, &l_697, (void*)0}, {&l_235, (void*)0, &l_697, (void*)0, &l_235, (void*)0, &l_697, (void*)0}, {&l_235, (void*)0, &l_697, (void*)0, &l_235, (void*)0, &l_697, (void*)0}, {&l_235, (void*)0, &l_697, (void*)0, &l_235, (void*)0, &l_697, (void*)0}, {&l_235, (void*)0, &l_697, (void*)0, &l_235, (void*)0, &l_697, (void*)0}}, {{&l_235, (void*)0, &l_697, (void*)0, &l_235, (void*)0, &l_697, (void*)0}, {&l_235, (void*)0, &l_697, (void*)0, &l_235, (void*)0, &l_697, (void*)0}, {&l_235, (void*)0, &l_697, (void*)0, &l_235, (void*)0, &l_697, (void*)0}, {&l_235, (void*)0, &l_697, (void*)0, &l_235, (void*)0, &l_697, (void*)0}, {&l_235, (void*)0, &l_697, (void*)0, &l_235, (void*)0, &l_697, (void*)0}}, {{&l_235, (void*)0, &l_697, (void*)0, &l_235, (void*)0, &l_697, (void*)0}, {&l_235, (void*)0, &l_697, (void*)0, &l_235, (void*)0, &l_697, (void*)0}, {&l_235, (void*)0, &l_697, (void*)0, &l_235, (void*)0, &l_697, (void*)0}, {&l_235, (void*)0, &l_697, (void*)0, &l_235, (void*)0, &l_697, (void*)0}, {&l_235, (void*)0, &l_697, (void*)0, &l_235, (void*)0, &l_697, (void*)0}}};
            uint8_t *l_904 = (void*)0;
            uint8_t *l_905 = &l_279;
            uint8_t *l_908 = &l_802;
            int8_t l_916 = 0x7DL;
            uint16_t l_917 = 0x6AEFL;
            int i, j, k;
            l_235 = (func_47(p_39.f0) , (l_697 = l_892));
            l_626 = (safe_rshift_func_uint16_t_u_u((safe_sub_func_uint64_t_u_u((safe_unary_minus_func_int8_t_s((safe_div_func_uint32_t_u_u(g_56, (l_901 & (p_39.f0 && (l_628[0] = (((safe_mul_func_uint8_t_u_u((+((*l_908) ^= ((*l_905)++))), (safe_sub_func_uint64_t_u_u(((**g_191) = (((p_39.f0 == (g_415.f4 != (safe_lshift_func_int16_t_s_s(p_39.f0, p_39.f0)))) >= func_52((p_39.f0 < (0x29D8L < 0xCDB7L)))) , p_38)), p_38)))) , l_913) <= 0L)))))))), 4L)), p_39.f0));
            ++l_917;
        }
        l_178 = (((&g_117 == ((*l_921) = p_41)) , g_415.f1) || ((g_931[0] = &g_806[0]) != (void*)0));
        if ((safe_mul_func_int16_t_s_s((((safe_lshift_func_uint16_t_u_s((*l_612), 4)) > ((safe_add_func_int32_t_s_s(((((((((**l_921) = (safe_rshift_func_uint16_t_u_u((safe_lshift_func_int8_t_s_u((safe_mod_func_int8_t_s_s(((*l_944) = g_13.f0), (safe_sub_func_uint64_t_u_u((**g_191), ((l_598 = ((*l_131) = p_39)) , (g_58 = (*l_612))))))), 3)), 6))) , 0xD179L) || (safe_div_func_uint32_t_u_u(((*l_922) ^= g_103), p_39.f0))) >= (safe_add_func_uint16_t_u_u(65529UL, 0UL))) || 0x04EB1BF3L) ^ g_117) , 0L), g_60[2][0])) <= p_38)) & p_38), 0UL)))
        { /* block id: 507 */
            return (*l_687);
        }
        else
        { /* block id: 509 */
            int32_t *l_951[4][1][4] = {{{&l_178, &g_631, &l_182[5], &g_631}}, {{&l_178, &g_631, &l_182[5], &g_631}}, {{&l_178, &g_631, &l_182[5], &g_631}}, {{&l_178, &g_631, &l_182[5], &g_631}}};
            uint32_t l_960 = 0xF66E5C2DL;
            int i, j, k;
            l_952++;
            l_955++;
            for (g_642 = 0; (g_642 > 37); g_642 = safe_add_func_uint8_t_u_u(g_642, 3))
            { /* block id: 514 */
                l_960++;
                for (l_952 = 0; l_952 < 5; l_952 += 1)
                {
                    g_931[l_952] = (void*)0;
                }
            }
        }
    }
    return &g_65;
}


/* ------------------------------------------ */
/* 
 * reads : g_56 g_60 g_64 g_75 g_76.f1.f4 g_76.f3 g_96 g_76.f2
 * writes: g_56 g_58 g_60 g_64 g_75 g_96 g_76.f2
 */
static const uint8_t  func_47(int32_t  p_48)
{ /* block id: 2 */
    uint16_t l_51 = 65532UL;
    struct S1 l_54 = {0x6BL};
    int64_t *l_55 = &g_56;
    int64_t *l_57 = &g_58;
    uint64_t l_71 = 1UL;
    const int8_t l_79 = 1L;
    if ((safe_mod_func_int16_t_s_s(l_51, func_52(((*l_57) = ((*l_55) |= (l_54 , p_48)))))))
    { /* block id: 14 */
        int32_t *l_62 = &g_60[0][8];
        int32_t **l_63 = &l_62;
        uint32_t **l_66 = &g_64;
        int16_t l_74[5][6][8] = {{{0x6F63L, (-1L), 0xEA20L, (-8L), (-5L), 7L, 0x5C99L, (-5L)}, {0x6F63L, (-1L), 0xEA20L, (-8L), (-5L), 7L, 0x5C99L, (-5L)}, {0x6F63L, (-1L), 0xEA20L, (-8L), (-5L), 7L, 0x5C99L, (-5L)}, {0x6F63L, (-1L), 0xEA20L, (-8L), (-5L), 7L, 0x5C99L, (-5L)}, {0x6F63L, (-1L), 0xEA20L, (-8L), (-5L), 7L, 0x5C99L, (-5L)}, {0x6F63L, (-1L), 0xEA20L, (-8L), (-5L), 7L, 0x5C99L, (-5L)}}, {{0x6F63L, (-1L), 0xEA20L, (-8L), (-5L), 7L, 0x5C99L, (-5L)}, {0x6F63L, (-1L), 0xEA20L, (-8L), (-5L), 7L, 0x5C99L, (-5L)}, {0x6F63L, (-1L), 0xEA20L, (-8L), (-5L), 7L, 0x5C99L, (-5L)}, {0x6F63L, (-1L), 0xEA20L, (-8L), (-5L), 7L, 0x5C99L, (-5L)}, {0x6F63L, (-1L), 0xEA20L, (-8L), (-5L), 7L, 0x5C99L, (-5L)}, {0x6F63L, (-1L), 0xEA20L, (-8L), (-5L), 7L, 0x5C99L, (-5L)}}, {{0x6F63L, (-1L), 0xEA20L, (-8L), (-5L), 7L, 0x5C99L, (-5L)}, {0x6F63L, (-1L), 0xEA20L, (-8L), (-5L), 7L, 0x5C99L, (-5L)}, {0x6F63L, (-1L), 0xEA20L, (-8L), (-5L), 7L, 0x5C99L, (-5L)}, {0x6F63L, (-1L), 0xEA20L, (-8L), (-5L), 7L, 0x5C99L, (-5L)}, {0x6F63L, (-1L), 0xEA20L, (-8L), (-5L), 7L, 0x5C99L, (-5L)}, {0x6F63L, (-1L), 0xEA20L, (-8L), (-5L), 7L, 0x5C99L, (-5L)}}, {{0x6F63L, (-1L), 0xEA20L, (-8L), (-5L), 7L, 0x5C99L, (-5L)}, {0x6F63L, (-1L), 0xEA20L, (-8L), (-5L), 7L, 0x5C99L, (-5L)}, {0x6F63L, (-1L), 0xEA20L, (-8L), (-5L), 7L, 0x5C99L, (-5L)}, {0x6F63L, (-1L), 0xEA20L, (-8L), (-5L), 7L, 0x5C99L, (-5L)}, {0x6F63L, (-1L), 0xEA20L, (-8L), (-5L), 7L, 0x5C99L, (-5L)}, {0x6F63L, (-1L), 0xEA20L, (-8L), (-5L), 7L, 0x5C99L, (-5L)}}, {{0x6F63L, (-1L), 0xEA20L, (-8L), (-5L), 7L, 0x5C99L, (-5L)}, {0x6F63L, (-1L), 0xEA20L, (-8L), (-5L), 7L, 0x5C99L, (-5L)}, {0x6F63L, (-1L), 0xEA20L, (-8L), (-5L), 7L, 0x5C99L, (-5L)}, {0x6F63L, (-1L), 0xEA20L, (-8L), (-5L), 7L, 0x5C99L, (-5L)}, {0x6F63L, (-1L), 0xEA20L, (-8L), (-5L), 7L, 0x5C99L, (-5L)}, {0x6F63L, (-1L), 0xEA20L, (-8L), (-5L), 7L, 0x5C99L, (-5L)}}};
        struct S3 *l_77 = (void*)0;
        struct S3 **l_78 = &g_75;
        int i, j, k;
        (*l_63) = l_62;
        l_71 |= func_52(((*l_57) = ((((*l_66) = g_64) != (void*)0) && (safe_div_func_uint8_t_u_u(p_48, (safe_mul_func_int16_t_s_s((**l_63), (**l_63))))))));
        g_60[0][8] = (safe_lshift_func_uint16_t_u_u(l_74[1][4][7], g_60[0][8]));
        (*l_78) = (l_77 = g_75);
    }
    else
    { /* block id: 22 */
        return l_79;
    }
    for (l_54.f0 = 12; (l_54.f0 >= 11); --l_54.f0)
    { /* block id: 27 */
        uint32_t l_88 = 18446744073709551606UL;
        struct S3 **l_94 = (void*)0;
        int8_t *l_95 = &g_96;
        int32_t *l_97 = &g_76.f2;
        (*l_97) |= (g_76.f1.f4 , (safe_div_func_uint8_t_u_u((safe_add_func_uint64_t_u_u(1UL, (l_51 | (safe_sub_func_int64_t_s_s(l_88, (((safe_div_func_uint64_t_u_u(((p_48 <= (0x3E7469BAL && g_76.f3)) <= (safe_unary_minus_func_uint8_t_u((((*l_95) &= (safe_lshift_func_uint8_t_u_s(((((void*)0 == l_94) , 251UL) <= g_76.f1.f4), 3))) ^ l_88)))), l_88)) > g_76.f1.f4) != g_76.f1.f4)))))), l_88)));
        return l_79;
    }
    return g_60[0][8];
}


/* ------------------------------------------ */
/* 
 * reads : g_60
 * writes: g_60
 */
static const int16_t  func_52(int64_t  p_53)
{ /* block id: 5 */
    int32_t *l_59 = &g_60[0][8];
    int32_t **l_61 = &l_59;
    (*l_61) = l_59;
    if ((!((0x0ED6D23037D9CC13LL || 0x6C21779538E5EFB4LL) < p_53)))
    { /* block id: 7 */
        return g_60[5][7];
    }
    else
    { /* block id: 9 */
        (*l_61) = (*l_61);
    }
    (**l_61) &= p_53;
    return p_53;
}




/* ---------------------------------------- */
int main (void)
{
    int i, j, k;
    int print_hash_value = 0;
    platform_main_begin();
    crc32_gentab();
    func_1();
    transparent_crc(g_13.f0, "g_13.f0", print_hash_value);
    transparent_crc(g_28, "g_28", print_hash_value);
    transparent_crc(g_56, "g_56", print_hash_value);
    transparent_crc(g_58, "g_58", print_hash_value);
    for (i = 0; i < 7; i++)
    {
        for (j = 0; j < 10; j++)
        {
            transparent_crc(g_60[i][j], "g_60[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_65, "g_65", print_hash_value);
    transparent_crc(g_76.f0, "g_76.f0", print_hash_value);
    transparent_crc(g_76.f1.f0, "g_76.f1.f0", print_hash_value);
    transparent_crc(g_76.f1.f1, "g_76.f1.f1", print_hash_value);
    transparent_crc(g_76.f1.f2, "g_76.f1.f2", print_hash_value);
    transparent_crc(g_76.f1.f3, "g_76.f1.f3", print_hash_value);
    transparent_crc(g_76.f1.f4, "g_76.f1.f4", print_hash_value);
    transparent_crc(g_76.f2, "g_76.f2", print_hash_value);
    transparent_crc(g_76.f3, "g_76.f3", print_hash_value);
    transparent_crc(g_96, "g_96", print_hash_value);
    transparent_crc(g_103, "g_103", print_hash_value);
    for (i = 0; i < 5; i++)
    {
        for (j = 0; j < 2; j++)
        {
            transparent_crc(g_113[i][j], "g_113[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    transparent_crc(g_115, "g_115", print_hash_value);
    transparent_crc(g_116, "g_116", print_hash_value);
    transparent_crc(g_117, "g_117", print_hash_value);
    transparent_crc(g_119.f0, "g_119.f0", print_hash_value);
    transparent_crc(g_125, "g_125", print_hash_value);
    transparent_crc(g_228.f0, "g_228.f0", print_hash_value);
    transparent_crc(g_261, "g_261", print_hash_value);
    transparent_crc(g_287.f0, "g_287.f0", print_hash_value);
    transparent_crc(g_288, "g_288", print_hash_value);
    transparent_crc(g_415.f0, "g_415.f0", print_hash_value);
    transparent_crc(g_415.f1, "g_415.f1", print_hash_value);
    transparent_crc(g_415.f2, "g_415.f2", print_hash_value);
    transparent_crc(g_415.f3, "g_415.f3", print_hash_value);
    transparent_crc(g_415.f4, "g_415.f4", print_hash_value);
    transparent_crc(g_482, "g_482", print_hash_value);
    transparent_crc(g_575, "g_575", print_hash_value);
    transparent_crc(g_631, "g_631", print_hash_value);
    transparent_crc(g_642, "g_642", print_hash_value);
    transparent_crc(g_965, "g_965", print_hash_value);
    transparent_crc(g_967, "g_967", print_hash_value);
    transparent_crc(g_1068, "g_1068", print_hash_value);
    transparent_crc(g_1075.f0, "g_1075.f0", print_hash_value);
    transparent_crc(g_1095, "g_1095", print_hash_value);
    for (i = 0; i < 4; i++)
    {
        transparent_crc(g_1210[i], "g_1210[i]", print_hash_value);
        if (print_hash_value) printf("index = [%d]\n", i);

    }
    for (i = 0; i < 1; i++)
    {
        for (j = 0; j < 3; j++)
        {
            transparent_crc(g_1217[i][j], "g_1217[i][j]", print_hash_value);
            if (print_hash_value) printf("index = [%d][%d]\n", i, j);

        }
    }
    platform_main_end(crc32_context ^ 0xFFFFFFFFUL, print_hash_value);
    return 0;
}

/************************ statistics *************************
XXX max struct depth: 2
breakdown:
   depth: 0, occurrence: 312
   depth: 1, occurrence: 19
   depth: 2, occurrence: 5
XXX total union variables: 0

XXX non-zero bitfields defined in structs: 6
XXX zero bitfields defined in structs: 1
XXX const bitfields defined in structs: 1
XXX volatile bitfields defined in structs: 0
XXX structs with bitfields in the program: 22
breakdown:
   indirect level: 0, occurrence: 13
   indirect level: 1, occurrence: 5
   indirect level: 2, occurrence: 4
XXX full-bitfields structs in the program: 8
breakdown:
   indirect level: 0, occurrence: 8
XXX times a bitfields struct's address is taken: 3
XXX times a bitfields struct on LHS: 0
XXX times a bitfields struct on RHS: 23
XXX times a single bitfield on LHS: 0
XXX times a single bitfield on RHS: 59

XXX max expression depth: 51
breakdown:
   depth: 1, occurrence: 334
   depth: 2, occurrence: 67
   depth: 3, occurrence: 7
   depth: 4, occurrence: 3
   depth: 5, occurrence: 4
   depth: 6, occurrence: 6
   depth: 7, occurrence: 4
   depth: 8, occurrence: 3
   depth: 10, occurrence: 2
   depth: 11, occurrence: 2
   depth: 12, occurrence: 2
   depth: 13, occurrence: 7
   depth: 14, occurrence: 2
   depth: 16, occurrence: 3
   depth: 17, occurrence: 4
   depth: 18, occurrence: 6
   depth: 19, occurrence: 2
   depth: 20, occurrence: 2
   depth: 21, occurrence: 3
   depth: 22, occurrence: 2
   depth: 23, occurrence: 1
   depth: 24, occurrence: 1
   depth: 26, occurrence: 1
   depth: 31, occurrence: 1
   depth: 32, occurrence: 1
   depth: 51, occurrence: 1

XXX total number of pointers: 345

XXX times a variable address is taken: 94
XXX times a pointer is dereferenced on RHS: 89
breakdown:
   depth: 1, occurrence: 73
   depth: 2, occurrence: 16
XXX times a pointer is dereferenced on LHS: 158
breakdown:
   depth: 1, occurrence: 143
   depth: 2, occurrence: 15
XXX times a pointer is compared with null: 11
XXX times a pointer is compared with address of another variable: 4
XXX times a pointer is compared with another pointer: 5
XXX times a pointer is qualified to be dereferenced: 5383

XXX max dereference level: 4
breakdown:
   level: 0, occurrence: 0
   level: 1, occurrence: 868
   level: 2, occurrence: 148
   level: 3, occurrence: 15
   level: 4, occurrence: 3
XXX number of pointers point to pointers: 120
XXX number of pointers point to scalars: 207
XXX number of pointers point to structs: 18
XXX percent of pointers has null in alias set: 33.6
XXX average alias set size: 1.56

XXX times a non-volatile is read: 825
XXX times a non-volatile is write: 533
XXX times a volatile is read: 0
XXX    times read thru a pointer: 0
XXX times a volatile is write: 0
XXX    times written thru a pointer: 0
XXX times a volatile is available for access: 0
XXX percentage of non-volatile access: 100

XXX forward jumps: 0
XXX backward jumps: 3

XXX stmts: 302
XXX max block depth: 5
breakdown:
   depth: 0, occurrence: 29
   depth: 1, occurrence: 31
   depth: 2, occurrence: 36
   depth: 3, occurrence: 54
   depth: 4, occurrence: 58
   depth: 5, occurrence: 94

XXX percentage a fresh-made variable is used: 17.3
XXX percentage an existing variable is used: 82.7
FYI: the random generator makes assumptions about the integer size. See platform.info for more details.
********************* end of statistics **********************/

