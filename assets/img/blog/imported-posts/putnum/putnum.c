/* frama-c -cpp-extra-args="-I`frama-c -print-share-path`/libc -nostdinc" `frama-c -print-share-path`/libc/fc_runtime.c -unspecified-access -val junk.c */

int putchar(int);

void print(const char *ptr)
{
  while (*ptr)
    putchar(*ptr++);
}

void putnum(unsigned long n)
{
  char buf[10], *ptr = buf + sizeof(buf);
  *--ptr = 0;
  do {
    *--ptr = "0123456789ABCDEF"[n&15];
    n >>= 4;
  } while(n);
  print(ptr);
}

unsigned long unknown();

int main(void)
{
  putnum(unknown());
  putchar('\n');
  return 0;
}
