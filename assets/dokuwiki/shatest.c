/*
 *  shatest.c
 *
 *  Copyright (C) 1998, 2009
 *  Paul E. Jones <paulej@packetizer.com>
 *  All Rights Reserved
 *
 *  File modified by CEA LIST for being used by Frama-C:
 *    - V. Prevosto <virgile.prevosto@cea.fr>
 *    - J. Signoles <julien.signoles@cea.fr>
 *
 *****************************************************************************
 *  $Id: shatest.c 12 2009-06-22 19:34:25Z paulej $
 *****************************************************************************
 *
 *  Description:
 *      This file will exercise the SHA1 class and perform the three
 *      tests documented in FIPS PUB 180-1.
 *
 *  Portability Issues:
 *      None.
 *
 */

#include "sha1.h"

/*
 *  Define patterns for testing
 */
#define TESTA   "abc"
#define TESTB_1 "abcdbcdecdefdefgefghfghighij"
#define TESTB_2 "hijkijkljklmklmnlmnomnopnopq"
#define TESTB   TESTB_1 TESTB_2
#define TESTC   "a"

int strlen(const unsigned char * a) {
  int i =0;
  while (*a++) i++;
  return i;
}

int main()
{
    SHA1Context sha;
    int i;

    SHA1Reset(&sha);
    SHA1Input(&sha, (const unsigned char *) TESTA, strlen(TESTA));

    if (!SHA1Result(&sha))
    {
      Frama_C_show_each_ERROR("could not compute message digest\n");
    }
    else
    {
      unsigned int answer[] = { 
        0xA9993E36,
        0x4706816A,
        0xBA3E2571,
        0x7850C26C,
        0x9CD0D89D 
      };
        for(i = 0; i < 5 ; i++)
        {
          Frama_C_show_each_A (i,sha.Message_Digest[i], answer[i]);
        }
    }

#if 0
    /*
     *  Perform test B
     */

    SHA1Reset(&sha);
    SHA1Input(&sha, (const unsigned char *) TESTB, strlen(TESTB));

    if (!SHA1Result(&sha))
    {
      Frama_C_show_each_ERROR("could not compute message digest\n");
    }
    else
    {
      unsigned int answer[] = {
        0x84983E44,
        0x1C3BD26E,
        0xBAAE4AA1,
        0xF95129E5,
        0xE54670F1
      };
        for(i = 0; i < 5 ; i++)
        {
          Frama_C_show_each_B(i,sha.Message_Digest[i],answer[i]);
        }
    }
#endif

    return 0;
}
