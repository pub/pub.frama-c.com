---
plugins: [ "sante" ]
authors: ["Omar Chebaro", "Nikolaï Kosmatov", "Alain Giorgetti", "Jacques Julliand"]
title: "Combining static analysis and test generation for C program debugging"
book: "Proceedings of the 4th International Conference on Tests & Proofs (TAP)"
link: "https://hal.archives-ouvertes.fr/hal-00563308/en"
doi: "10.1007/978-3-642-13977-2_9"
year: 2010
category: foundational
---

This paper presents our ongoing work on a tool prototype called
SANTE (StaticANalysis and TEsting), implementing a combination of
static analysis and structural program testing for detection of
run-time errors in C programs. First, a static analysis tool
(Frama-C) is called to generate alarms when it cannot ensure the
absence of run-time errors. Second, these alarms guide a structural
test generation tool (PathCrawler) trying to confirm alarms by
activating bugs on sometest cases.Our experiments on real-life
software showthat this combination can outperform the use of each
technique independently.
