---
plugins: [ "sante" ]
authors: ["Omar Chebaro"]
title: "Outil SANTE : Détection d’erreurs par analyse statique et test structurel des programmes C"
book: "Proceedings of 10iemes Journées Francophones Internationales sur les Approches Formelles dans l'Assistance au Développement de Logiciels (AFADL)"
year: 2010
category: foundational
short: In French.
---

Ce papier présente un prototype appelé SANTE (Static ANalysis and TEsting), qui implémente une nouvelle méthode combinant l’analyse statique et le test structurel pour la détection des erreurs à l’exécution dans les programmes  C. Tout d’abord, un outil d’analyse statique (Frama-C) est appelé pour générer des alarmes lorsqu’il ne peut pas garantir l’absence d’erreurs à l’exécution. Ensuite ces alarmes guident la génération de tests structurels dans PathCrawler qui va essayer de confirmer les alarmes en activant des bugs
sur certains cas de test.
