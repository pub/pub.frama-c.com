---
plugins: [ "sante" ]
authors: ["Omar Chebaro", "Nikolaï Kosmatov", "Alain Giorgetti", "Jacques Julliand"]
title: "The SANTE Tool: Value Analysis, Program Slicing and Test Generation for C Program Debugging"
book: "Proceedings of the 5th International Conference on Tests & Proofs (TAP)"
link: "https://hal.inria.fr/inria-00622904/en"
doi: "10.1007/978-3-642-21768-5_7"
year: 2011
category: foundational
---

This short paper presents a prototype tool called SANTE (Static
ANalysis and TEsting) implementing an original method combining
value analysis, program slicing and structural test generation for
verification of C programs. First, value analysis is called to
generate alarms when it can not guarantee the absence of errors.
Then the program is reduced by program slicing. Alarm-guided test
generation is then used to analyze the simplified program(s) in
order to confirm or reject alarms.
