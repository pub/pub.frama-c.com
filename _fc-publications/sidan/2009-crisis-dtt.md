---
plugins: [ "sidan" ]
authors: ["Jonathan-Christopher Demay", "Éric Totel", "Frédéric Tronel"]
title: "SIDAN: a tool dedicated to Software Instrumentation for Detecting Attacks on Non-control-data"
book: "4th International Conference on Risks and Security of Internet and Systems (CRISIS)"
link: "https://hal.archives-ouvertes.fr/hal-00424574/en"
doi: "10.1109/CRISIS.2009.5411977"
year: 2009
category: foundational
---

Anomaly based intrusion detection systems rely on the build of a normal behavior model. When a deviation from this normal behavior is detected, an alert is raised. This anomaly approach, unlike the misuse approach, is able to detect unknown attacks. A basic technique to build such a model for a program is to use the system call sequences of the process. To improve the accuracy and completeness of this detection model, we can add information related to the system call, such as its arguments or its execution context. But even then, attacks that target non-control-data may be missed and attacks on control-data may be adapted to bypass the detection mechanism using evasion techniques. We propose in this article an approach that focuses on the detection of non-control-data attacks. Our approach aims at exploiting the internal state of a program to detect a memory corruption on non-control-data that could lead to an illegal system call. To achieve this, we propose to build a data-oriented detection model by statically analyzing a program source code. This model is used to instrument the program by adding reasonableness checks that verify the consistent state of the data items the system calls depend on. We thus argue that it is possible to detect a program misuse issued by a non-control-data attack inside the program during its execution. While keeping a low overhead, this approach allows to detect non-control-data attacks.
