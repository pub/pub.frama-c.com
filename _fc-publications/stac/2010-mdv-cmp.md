---
plugins: [ "stac" ]
authors: ["Dumitru Ceara", "Laurent Mounier", "Marie-Laure Potet"]
title: "Taint Dependency Sequences: a characterization of insecure execution paths based on input-sensitive cause sequences"
book: "Modeling and Detecting Vulnerabilities workshop (MDV)"
link: "http://www-verimag.imag.fr/PEOPLE/mounier/Papers/mdv10.pdf"
doi: "10.1109/ICSTW.2010.28"
year: 2010
category: foundational
---

Numerous software vulnerabilities can be activated only with dedicated user inputs. Taint analysis is a security check which consists in looking for possible dependency chains between user inputs and vulnerable statements (like array accesses). Most of the existing static taint analysis tools produce some warnings on potentially vulnerable program locations. It is then up to the developer to analyze these results by scanning the possible execution paths that may lead to these locations with unsecured user inputs. We present a Taint Dependency Sequences Calculus, based on a fine-grain data and control taint analysis, that aims to help the developer in this task by providing some information on the set of paths that need to be analyzed. Following some ideas introduced in, we also propose some metrics to characterize these paths in term of "dangerousness". This approach is illustrated with the help of the Verisec Suite and by describing a prototype, called STAC.
