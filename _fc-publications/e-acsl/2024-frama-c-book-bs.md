---
plugins: [ "e-acsl" ]
authors: [ Thibaut Benjamin, Julien Signoles ]
title: "Runtime Annotation Checking with Frama-C: The E-ACSL Plug-in"
book: "Guide to Software Verification with Frama-C"
link: "https://link.springer.com/chapter/10.1007/978-3-031-55608-1_5"
doi: "10.1007/978-3-031-55608-1_5"
year: 2024
category: foundational
---
Runtime Annotation Checking (RAC) is a lightweight formal method consisting in
checking code annotations written in the source code during the program
execution. While static formal methods aim for guarantees that hold for any
execution of the analyzed program, RAC only provides guarantees about the
particular execution it monitors. This allows RAC-based tools to be used to
check a wide range of properties with minimum intervention from the user.
Frama-C can perform RAC on C programs with the plug-in E-ACSL. This chapter
presents RAC through practical use with E-ACSL, shows advanced uses of E-ACSL
leveraging the collaboration with other plug-ins, and sheds some light on the
internals of E-ACSL and the technical difficulties of implementing RAC.
