---
plugins: [ "e-acsl" ]
authors: ["Dara Ly"]
title: "Formalisation d'un vérificateur dynamique de propriétés mémoire pour programmes C"
school: "University of Orléans"
link: "https://www.theses.fr/s265339"
year: 2022
category: phdthesis
type: "PhD Thesis"
---

Runtime Assertion Checking is a program verification technique allowing to
monitor programs during their execution, in order to check their validity with
respect to some specification expressed as assertions, that is, formal
annotations inserted in the source code. Assertions are translated into
executable code through a process called instrumentation, thus implementing an
inline monitor for the program. During execution, the monitor checks whether the
program complies with its assertions, and aborts the execution in case of
violation of an assertion. If no such violation occurs, the program's functional
behavior is unchanged. Instrumenting a program is a process whose implementation
complexity is strongly related to the expressiveness of the annotation
language. For C programs, the E-ACSL plugin of Frama-C, an open-source analysis
platform for C programs, can express properties related to the memory state of
the program, at the cost of a complex instrumentation. In this thesis, we
present a formalization of the instrumentation process, that is, a description
accurate enough to enable formal reasoning about the semantic properties of the
instrumentation. The problem is modeled as a translation, from a source language
with logical assertions, to a target language. The latter has no logical
assertions, instead featuring a data-structure we call observation memory, which
is designed to keep track of memory properties. We present an axiomatic
characterization of observation memory, and use it to define the translation's
target language semantics. The translation is then proved correct with regards
to the respective semantics of source and target languages. In addition, we
study an optimization of the instrumentation through data-flow analysis. This
optimization is implemented in E-ACSL in order to mitigate the performance costs
of the instrumentation. The analysis aims at identifying a minimal subset of
memory locations, whose instrumentation suffices for the monitor to evaluate the
program's assertions soundly. We define such an analysis and prove its
soundness, meaning that restricting the instrumentation to memory locations
computed by the analysis does not threaten the validity of the monitor's
verdicts.
