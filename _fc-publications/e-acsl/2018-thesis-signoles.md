---
plugins: [ "e-acsl" ]
authors: ["Julien Signoles"]
title: "From Static Analysis to Runtime Verification with Frama-C and E-ACSL"
school: "University of Paris Sud"
year: "2018"
link: "https://julien-signoles.fr/publis/hdr.pdf"
category: phdthesis
type: "Habilitation Thesis"
---
