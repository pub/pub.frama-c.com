---
plugins: [ "e-acsl" ]
authors: ["Julien Signoles"]
title: "The E-ACSL Perspective on Runtime Assertion Checking"
book: "International Workshop on Verification and mOnitoring at Runtime EXecution (VORTEX)"
link: "https://julien-signoles.fr/publis/2021_vortex.pdf"
doi: "10.1145/3464974.3468451"
year: 2021
category: other
---

Runtime Assertion Checking (RAC) is the discipline of verifying program
assertions at runtime, i.e. when executing the code. Nowadays, RAC usually
relies on Behavioral Interface Specification Languages (BISL) à la Eiffel for
writing powerful code specifications. Since now more than 20 years, several
works have studied RAC. Most of them have focused on BISL. Some others have also
considered combinations of RAC with others techniques, e.g. deductive
verification (DV). Very few tackle RAC as a verification technique that soundly
generates efficient code from formal annotations. Here, we revisit these three
RAC's research areas by emphasizing the works done in E-ACSL, which is both a
BISL and a RAC tool for C code. We also compare it to others languages and
tools.
