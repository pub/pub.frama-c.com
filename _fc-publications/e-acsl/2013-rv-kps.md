---
plugins: [ "e-acsl" ]
authors: ["Nikolai Kosmatov", "Guillaume Petiot", "Julien Signoles"]
title: "An Optimized Memory Monitoring for Runtime Assertion Checking of C Programs"
book: "Proceedings of Runtime Verification (RV)"
link: "https://hal.archives-ouvertes.fr/cea-01834990/en"
doi: "10.1007/978-3-642-40787-1_10"
year: 2013
category: other
---

Runtime assertion checking provides a powerful, highly automatizable technique to detect violations of specified program properties. However, monitoring of annotations for pointers and memory locations (such as being valid, initialized, in a particular block, with a particular offset, etc.) is not straightforward and requires systematic instrumentation and monitoring of memory-related operations. This paper describes the runtime memory monitoring library we developed for execution support of E-ACSL, executable specification language for C programs offered by the Frama-C platform for analysis of C code. We present the global architecture of our solution as well as various optimizations we realized to make memory monitoring more efficient. Our experiments confirm the benefits of these optimizations and illustrate the bug detection potential of runtime assertion checking with E-ACSL.
