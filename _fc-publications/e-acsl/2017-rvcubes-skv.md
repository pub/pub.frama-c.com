---
plugins: [ "e-acsl" ]
authors: ["Julien Signoles", "Nikolai Kosmatov", "Kostyantyn Vorobyov"]
title: "E-ACSL, a Runtime Verification Tool for Safety and Security of C Programs. Tool Paper."
book: "International Workshop on Competitions, Usability, Benchmarks, Evaluation, and Standardisation for Runtime Verification Tools (RV-CuBES)"
link: "https://julien-signoles.fr/publis/2017_rvcubes_tool.pdf"
doi: "10.29007/fpdh"
year: 2017
category: foundational
short: "An overview of the E-ACSL plug-in."
---

This tool paper presents E-ACSL, a runtime verification tool for C programs capable of checking a broad range of safety and security properties expressed using a formal specification language. E-ACSL consumes a C program annotated with formal specifications and generates a new C program that behaves similarly to the original if the formal properties are satisfied, or aborts its execution whenever a property does not hold. This paper presents an overview of E-ACSL and its specification language.
