---
plugins: [ "e-acsl" ]
authors: ["Dara Ly", "Nikolai Kosmatov", "Julien Signoles", "Frédéric Loulergue"]
title: "Soundness of a Dataflow Analysis for Memory Monitoring"
book: "Workshop on Languages and Tools for Ensuring Cyber-Resilience in Critical Software-Intensive Systems (HILT)"
link: "https://hal.archives-ouvertes.fr/cea-02283406/en"
doi: "10.1145/3375408.3375416"
year: 2018
category: other
---

An important concern addressed by runtime verification tools for C code is related to detecting memory errors. It requires to monitor some properties of memory locations (e.g., their validity and initialization) along the whole program execution. Static analysis based optimizations have been shown to significantly improve the performances of such tools by reducing the monitoring of irrelevant locations. However, soundness of the verdict of the whole tool strongly depends on the soundness of the underlying static analysis technique. This paper tackles this issue for the dataflow analysis used to optimize the E-ACSL runtime assertion checking tool. We formally define the core dataflow analysis used by E-ACSL and prove its soundness.
