---
plugins: [ "e-acsl" ]
authors: ["Dara Ly", "Nikolai Kosmatov", "Frédéric Loulergue", "Julien Signoles"]
title: "Verified Runtime Assertion Checking for Memory Properties"
book: "International Conference on Tests and Proofs (TAP)"
link: "https://hal-cea.archives-ouvertes.fr/cea-02879211/en"
doi: "10.1007/978-3-030-50995-8_6"
year: 2020
category: other
---

Runtime Assertion Checking (RAC) for expressive specification languages is a non-trivial verification task, that becomes even more complex for memory-related properties of imperative languages with dynamic memory allocation. It is important to ensure the soundness of RAC verdicts, in particular when RAC reports the absence of failures for execution traces. This paper presents a formalization of a program transformation technique for RAC of memory properties for a representative language with memory operations. It includes an observation memory model that is essential to record and monitor memory-related properties. We prove the soundness of RAC verdicts with regard to the semantics of this language.
