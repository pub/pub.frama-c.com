---
plugins: [ "e-acsl" ]
authors: ["Kostyantyn Vorobyov", "Julien Signoles", "Nikolai Kosmatov"]
title: "Shadow state encoding for efficient monitoring of block-level properties"
book: "International Symposium on Memory Management (ISMM)"
link: "https://julien-signoles.fr/publis/2017_ismm.pdf"
doi: "10.1145/3092255.3092269"
year: 2017
category: foundational
short: "Presentation of the shadow memory technique used by E-ACSL to monitor memory properties."
---

Memory shadowing associates addresses from an application's memory to values stored in a disjoint memory space called shadow memory. At runtime shadow values store metadata about application memory locations they are mapped to. Shadow state encodings -- the structure of shadow values and their interpretation -- vary across different tools. Encodings used by the state-of-the-art monitoring tools have been proven useful for tracking memory at a byte-level, but cannot address properties related to memory block boundaries. Tracking block boundaries is however crucial for spatial memory safety analysis, where a spatial violation such as out-of-bounds access, may dereference an allocated location belonging to an adjacent block or a different struct member.

This paper describes two novel shadow state encodings which capture block-boundary-related properties. These encodings have been implemented in E-ACSL - a runtime verification tool for C programs. Initial experiments involving checking validity of pointer and array accesses in computationally intensive runs of programs selected from SPEC CPU benchmarks demonstrate runtime and memory overheads comparable to state-of-the-art memory debuggers.
