---
plugins: [ "e-acsl" ]
authors: ["Kostyantyn Vorobyov", "Nikolai Kosmatov", "Julien Signoles"]
title: "Detection of Security Vulnerabilities in C Code using Runtime Verification"
book: "International Conference on Tests and Proofs (TAP)"
link: "https://julien-signoles.fr/publis/2018_tap_vorobyov.pdf"
doi: "10.1007/978-3-319-92994-1_8"
year: 2018
category: other
short: "Explore how some runtime verification tools (including E-ACSL) may detect security vulnerabilities."
---

Despite significant progress made by runtime verification tools in recent years, memory errors remain one of the primary threats to software security. The present work is aimed at providing an objective up-to-date experience study on the capacity of modern online runtime verification tools to automatically detect security flaws in C programs. The reported experiments are performed using three advanced runtime verification tools (E-ACSL, Google Sanitizer and RV-Match) over 700 test cases belonging to SARD-100 test suite of the SAMATE project and Toyota ITC Benchmark, a publicly available benchmarking suite developed at the Toyota InfoTechnology Center. SARD-100 specifically targets security flaws identified by the Common Weakness Enumeration (CWE) taxonomy, while Toyota ITC Benchmark addresses more general memory defects, as well as numerical and concurrency issues. We compare tools based on different approaches – a formal semantic based tool, a formal specification verifier and a memory debugger – and evaluate their cumulative detection capacity.
