---
plugins: [ "deadlockf" ]
authors: ["Tomáš Dacík", "Tomáš Vojnar"]
title: "Static Analysis in the Frama-C Environment Focused on Deadlock Detection"
school: "Brno University of Technology, Faculty of Information Technology"
link: "https://www.fit.vut.cz/study/thesis/22928/.en"
year: 2020
category: "other"
short: "Bachelor thesis describing principles and implementation of the DeadlockF analyser."
---
This thesis presents a design of a new static analyser focused on deadlock
detection, implemented as a plugin of the Frama-C platform. Together with the
core algorithm of deadlock detection, we also present a light-weight method that
allows one to analyse (not only for the purposes of deadlock detection)
multithreaded programs using sequential analysers of Frama-C. Results of
experiments show that our tool is able to handle real-world C code with high
precision. Moreover, we demonstrate its extensibility by so-far experimental
implementation of data race detection.
