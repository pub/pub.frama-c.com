---
plugins: [ "pathcrawler" ]
authors: ["Patricia Mouy", "Bruno Marre", "Nicky Williams", "Pascale Le Gall"]
title: "Generation of all-paths unit test with function calls"
book: "International Conference on Software Testing, Verification, and Validation (ICST)"
link: "https://hal.archives-ouvertes.fr/hal-01810199/en"
doi: "10.1109/ICST.2008.35"
year: 2008
category: other
---

Structural testing is usually restricted to unit tests and based on some clear definition of source code coverage. In particular, the all-paths criterion, which requires at least one test-case per feasible path of the function under test, is recognised as offering a high level of software reliability. This paper deals with the difficulties of using structural unit testing to test functions which call other functions. To limit the resulting combinatorial explosion in the number of paths, we choose to abstract the called functions by their specification. We incorporate the functional information on the called functions within the structural information on the function under test, given as a control flow graph (CFG). This representation combining functional and structural descriptions may be viewed as an extension of the classic CFG and allows us to characterise test selection criteria ensuring the coverage of the source code of the function under test. Two new criteria will be proposed. The first criterion corresponds to the coverage of all the paths of this new representation, including all the paths arising from the functional description of the called functions. The second criterion covers all the feasible paths of the function under test only. We describe how we automate test-data generation with respect to such grey-box (combinations of black- box and white-box) test selection strategies, and we apply the resulting extension of our PathCrawler tool to examples coded in the C language.
