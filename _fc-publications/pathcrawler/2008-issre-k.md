---
plugins: [ "pathcrawler" ]
authors: ["Nikolai Kosmatov"]
title: "All-paths test generation for programs with internal aliases"
book: "International Symposium on Software Reliability Engineering (ISSRE)"
link: "http://nikolai.kosmatov.free.fr/publications/kosmatov_issre_2008.pdf"
doi: "10.1109/ISSRE.2008.25"
year: 2008
category: other
---

In structural testing of programs, the all-paths coverage criterion requires to generate a set of test cases such that every possible execution path of the program under test is executed by one test case. This task becomes very complex in presence of aliases, i.e. different ways to address the same memory location. In practice, the presence of aliases may result in enumeration of possible inputs, generation of several test cases for the same path and/or a failure to generate a test case for some feasible path. This article presents the problem of aliases in the context of classical depth-first test generation method. We classify aliases into two groups: external aliases, existing already at the entry point of the function under test (due to pointer inputs), and internal ones, created during its symbolic execution. This paper focuses on internal aliases.We propose an original extension of the depth-first test generation method for C programs with internal aliases. It limits the enumeration of inputs and the generation of superfluous test cases. Initial experiments show that our method canconsiderably improve the performances of the existing tools on programs with aliases.
