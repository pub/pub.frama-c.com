---
plugins: [ "pathcrawler" ]
authors: ["Nicky Williams", "Nikolai Kosmatov"]
title: "Structural Testing with PathCrawler. Tutorial Synopsis"
book: "International Conference on Quality Software (QSIC)"
link: "https://hal.archives-ouvertes.fr/hal-01810295/"
doi: "10.1109/QSIC.2012.24"
year: 2012
category: tutorial
---

Automatic testing tools allow huge savings but they do not exonerate the user from thinking carefully about what they want testing to achieve. To successfully use the Path Crawler-online structural testing tool, the user must provide not only the full source code, but also must set the test parameters and program the oracle. This demands a different"mindset" from that used for informal functional-style manual testing, as we explain with the help of several examples.
