---
plugins: [ "pathcrawler" ]
authors: ["Nicky Williams", "Bruno Marre", "Patricia Mouy"]
title: "On-the-fly generation of k-paths tests for C functions: towards the automation of grey-box testing"
book: "International Conference on Automated Software Engineering (ASE)"
link: "https://hal.archives-ouvertes.fr/hal-01810203/en"
year: 2004
category: foundational
---

We present a method for the automatic generation of tests satisfying the all-paths
criterion, with a user-defined limit, k, on the number of loop  iterations in the
covered paths. We have implemented a prototype for C code. We illustrate our approach
on a representative example of a C function containing data-structures of variable
dimensions, loops with variable numbers of iterations and many infeasible paths.

We explain why our method is efficient enough to scale up to the unit testing of
ealistic programs. It is flexible enough to take into account certain specifications
of the tested code. This is why we believe that it could become the cornerstone of a
fully automatic grey-box testing process, based on the novel combination of code
instrumentation and constraint solving described here.
