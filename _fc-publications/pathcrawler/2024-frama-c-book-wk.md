---
plugins: [ "pathcrawler" ]
authors: [ Nicky Williams, Nikolai Kosmatov ]
title: "Test Generation with PathCrawler"
book: "Guide to Software Verification with Frama-C"
link: "https://link.springer.com/chapter/10.1007/978-3-031-55608-1_6"
doi: "10.1007/978-3-031-55608-1_6"
year: 2024
category: foundational
---
Structural testing allows validation engineers to ensure that all parts of the
program source code are activated (or covered) by the executed tests. The parts
of code to be covered are determined by the choice of a coverage criterion.
Automated test generation tools can be used effectively to generate test inputs
satisfying a selected coverage criterion. This chapter presents PathCrawler, an
automatic test generation tool for structural testing of C code, available as a
plug-in of the Frama-C verification platform. We present the structural testing
approach and describe the method implemented in PathCrawler and its usage in
practice. The freely available online test generation service PathCrawler-online
is presented as well. Finally, we discuss the place of testing in software
verification and validation with respect to other Frama-C plug-ins and give an
overview of various research, industrial and teaching activities using
PathCrawler.
