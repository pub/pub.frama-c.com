---
plugins: [ "pathcrawler" ]
authors: ["Nicky Williams"]
title: "WCET measurement using modified path testing"
book: "International Workshop on Worst-Case Execution Time Analysis (WCET)"
link: "https://hal.archives-ouvertes.fr/hal-01810200/en"
doi: "10.4230/OASIcs.WCET.2005.809"
year: 2005
category: other
---

Prediction of Worst Case Execution Time (WCET) is made increasingly difficult by the recent developments in micro-processor architectures. Instead of predicting the WCET using techniques such as static analysis, the effective execution time can be measured when the program is run on the target architecture ir a cycle-accurate simulator. However, exhaustive measurments on all possible input values are usually prohibited by the numer of possible input values. As a first step towardsa solution, we propose path testing using the PathCrawler tool to automatically generate test inputs for all feasible execution paths in C source code. For programs containing too many execution paths for this approach to be feasible, we propose to modify PathCrawler's strategy in order to cut down on the number of generated tests while still ensuring measurment of the path with the longest execution time.
