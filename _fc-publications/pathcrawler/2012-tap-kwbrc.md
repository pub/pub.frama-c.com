---
plugins: [ "pathcrawler" ]
authors: ["Nikolai Kosmatov", "Nicky Williams", "Bernard Botella", "Muriel Roger", "Omar Chebaro"]
title: "A Lesson on Structural Testing with PathCrawler-online.com"
book: "International Conference on Tests and Proofs (TAP)"
link: "https://hal.archives-ouvertes.fr/hal-01810295/en"
doi: "10.1007/978-3-642-30473-6_15"
year: 2012
category: tutorial
---

PathCrawler is a test generation tool developed at CEA LIST for structural testing of C programs. The new version of PathCrawler is developed in an entirely novel form: that of a test-case generation web service which is freely accessible at PathCrawler-online.com. This service allows many test-case generation sessions to be run in parallel in a completely robust and secure way. This tool demo and teaching experience paper presents PathCrawler-online.com in the form of a lesson on structural software testing, showing its benefits, limitations and illustrating the usage of the tool on simple examples.
