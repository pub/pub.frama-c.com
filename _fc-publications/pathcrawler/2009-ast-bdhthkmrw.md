---
plugins: [ "pathcrawler" ]
authors: ["Bernard Botella", "Mickaël Delahaye", "Stéphane Hong-Tuan-Ha", "Nikolai Kosmatov", "Patricia Mouy", "Muriel Roger", "Nicky Williams"]
title: "Automating Structural Testing of C Programs: Experience with PathCrawler"
book: "International Workshop on Automation of Software Test (AST)"
link: "https://ieeexplore.ieee.org/document/5069043"
doi: "10.1109/IWAST.2009.5069043"
year: 2009
category: other
---

Structural testing is widely used in industrial verification processes of critical software. This report presents PathCrawler, a structural test generation tool that may be used to automate this activity, and several evaluation criteria of automatic test generation tools for C programs. These criteria correspond to the issues identified during our ongoing experience in the development of PathCrawler and its application to industrial software. They include issues arising for some specific types of software. Some of them are still difficult open problems. Others are (partially) solved, and the solution adopted in PathCrawler is discussed. We believe that these criteria must be satisfied in order for the automation of structural testing to become an industrial reality.
