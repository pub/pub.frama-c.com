---
plugins: [ "pathcrawler" ]
authors: ["Nikolai Kosmatov", "Bernard Botella", "Muriel Roger", "Nicky Williams"]
title: "Online Test Generation with PathCrawler. Tool demo."
book: "International Workshop on Constraints in Software Testing, Verification, and Analysis (CSTVA)"
link: "https://nikolai-kosmatov.eu/publications/kosmatov_brw_cstva_2011.pdf"
doi: "10.1109/ICSTW.2011.85"
year: 2011
category: other
short: "Best demo award"
---

This demonstration presents a new version of Path Crawler developed in an entirely novel form: that of a test-case server which is freely accessible online.
