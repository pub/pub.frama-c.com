---
plugins: [ "pathcrawler" ]
authors: ["Nicky Williams", "Muriel Roger"]
title: "Test Generation Strategies to Measure Worst-Case Execution Time"
book: "International Workshop on Automation of Software Test (AST)"
link: "https://ieeexplore.ieee.org/document/5069045"
doi: "10.1109/IWAST.2009.5069045"
year: 2009
category: other
---

Under certain conditions, the worst-case execution time (WCET) of a function can be found by measuring the effective execution time for each feasible execution path. Automatic generation of test inputs can help make this approach more feasible. To reduce the number of tests, we define two partial orders on the execution paths of the program under test. Under further conditions, these partial orders represent the relation between the execution times of the paths. We explain how we modified the strategy of the PathCrawler structural test-case generation tool to generate as few tests as possible for paths which are not maximal in these partial orders, whilst ensuring that the WCET is exhibited by at least one case in the set. The techniques used could also serve in the implementation of other test generation strategies which have nothing to do with WCET.
