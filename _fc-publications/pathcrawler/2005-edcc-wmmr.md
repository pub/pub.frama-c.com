---
plugins: [ "pathcrawler" ]
authors: ["Nicky Williams", "Bruno Marre", "Patricia Mouy", "Muriel Roger"]
title: "PathCrawler: automatic generation of path tests by combining static and dynamic analysis"
book: "European Dependable Computing Conference (EDCC)"
link: "https://hal.archives-ouvertes.fr/hal-01810201/en"
doi: "10.1007/11408901_21"
year: 2005
category: foundational
---

We present the PathCrawler prototype tool for the automatic generation of test-cases satisfying the rigorous all-paths criterion, with a user-defined limit on the number of loop iterations in the covered paths. The prototype treats C code and we illustrate the test-case generation process on a representative example of a C function containing data-structures of variable dimensions, loops with variable numbers of iterations and many infeasible paths. PathCrawler is based on a novel combination of code instrumentation and constraint solving which makes it both efficient and open to extension. It suffers neither from the approximations and complexity of static analysis, nor from the number of executions demanded by the use of heuristic algorithms in function minimisation and the possibility that they fail to find a solution. We believe that it demonstrates the feasibility of rigorous and systematic testing of sequential programs coded in imperative languages.
