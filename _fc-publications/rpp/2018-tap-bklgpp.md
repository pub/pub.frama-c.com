---
plugins: [ "rpp" ]
authors: ["Lionel Blatter", "Nikolai Kosmatov", "Pascale Le Gall", "Virgile Prevosto", "Guillaume Petiot"]
title: "Static and Dynamic Verification of Relational Properties on Self-composed C Code"
book: "In Tests and Proofs - 12th International Conference (TAP)"
link: "https://hal-cea.archives-ouvertes.fr/cea-01835470/en"
doi: "10.1007/978-3-319-92994-1_3"
year: 2018
category: foundational
---

Function contracts are a well-established way of formally specifying the intended behavior of a function. However, they usually only describe what should happen during a single call. Relational properties, on the other hand, link several function calls. They include such properties as non-interference, continuity and monotonicity. Other examples relate sequences of function calls, for instance, to show that decrypting an encrypted message with the appropriate key gives back the original message. Such properties cannot be expressed directly in the traditional setting of modular deductive verification, but are amenable to verification through self-composition. This paper presents a verification technique dedicated to relational properties in C programs and its implementation in the form of a FRAMA-C plugin called RPP and based on self-composition. It supports functions with side effects and recursive functions. The proposed approach makes it possible to prove a relational property, to check it at runtime, to generate a counterexample using testing and to use it as a hypothesis in the subsequent verification. Our initial experiments on existing benchmarks confirm that the proposed technique is helpful for static and dynamic analysis of relational properties.
