---
plugins: [ "rpp" ]
authors: ["Lionel Blatter"]
title: "Relational properties for specification and verification of C programs in Frama-C"
school: "University of Paris Saclay"
year: "2019"
link: "http://www.theses.fr/2019SACLC065"
category: phdthesis
type: "PhD Thesis"
---
