---
plugins: [ "rpp" ]
authors: ["Lionel Blatter", "Nikolai Kosmatov", "Pascale Le Gall", "Virgile Prevosto"]
title: "RPP: Automatic Proof of Relational Properties by Self-composition"
book: "Tools and Algorithms for the Construction and Analysis of Systems23rd International Conference, TACAS"
link: "https://hal-cea.archives-ouvertes.fr/cea-01808885/en"
doi: "10.1007/978-3-662-54577-5_22"
year: 2017
category: foundational
---

Self-composition provides a powerful theoretical approach to prove relational properties, i.e. properties relating several program executions, that has been applied to compare two runs of one or similar programs (in secure dataflow properties, code transformations, etc.). This tool demo paper presents RPP, an original implementation of self-composition for specification and verification of relational properties in C programs in the FRAMA-C platform. We consider a very general notion of relational properties invoking any finite number of function calls of possibly dissimilar functions with possible nested calls. The new tool allows the user to specify a relational property, to prove it in a completely automatic way using classic deductive verification, and to use it as a hypothesis in the proof of other properties that may rely on it.
