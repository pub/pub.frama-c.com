---
plugins: [ "pilat" ]
authors: ["Steven de Oliveira", "Saddek Bensalem", "Virgile Prevosto"]
title: "Synthesizing invariants by solving solvable loops"
book: "Automated Technology for Verification and Analysis (ATVA)"
link: "https://arxiv.org/abs/1611.07753"
doi: "10.1007/978-3-319-68167-2_22"
year: 2017
category: foundational
short: "Extension of the PILAT technique to polynomial inequalities"
---

Formal program verification faces two problems. The first problem is related to
the necessity of having automated solvers that are powerful enough to decide
whether a formula holds for a set of proof obligations as large as possible,
whereas the second manifests in the need of finding sufficiently strong
invariants to obtain correct proof obligations. This paper focuses on the second
problem and describes a new method for the automatic generation of loop
invariants that handles polynomial and non deterministic assignments. This
technique is based on the eigenvector generation for a given linear
transformation and on the polynomial optimization problem, which we implemented
on top of the open-source tool Pilat.
