---
plugins: [ "pilat" ]
authors: ["Steven de Oliveira", "Virgile Prevosto", "Peter Habermehl", "Saddek Bensalem"]
title: "Left-Eigenvectors Are Certificates of the Orbit Problem."
book: "Reachability Problems (RP)"
link: "https://arxiv.org/abs/1803.09511"
doi: "10.1007/978-3-030-00250-3_3"
year: 2018
category: foundational
short: ""
---

 This paper investigates the connexion between the Kannan-Lipton Orbit Problem
 and the polynomial invariant generator algorithm PILA based on eigenvectors
 computation. Namely, we reduce the problem of generating linear and polynomial
 certificates of non-reachability for the Orbit Problem for linear
 transformations with rational coefficients to the generalized eigenvector
 problem. Also, we prove the existence of such certificates for any
 transformation with integer coefficients, which is not the case with rational
 coefficients.
