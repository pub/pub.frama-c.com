---
plugins: [ "pilat" ]
authors: ["Steven de Oliveira", "Saddek Bensalem", "Virgile Prevosto"]
title: "Polynomial Invariants by Linear Algebra"
book: "Automated Technology for Verification and Analysis (ATVA)"
link: "https://arxiv.org/abs/1611.07726"
doi: "10.1007/978-3-319-46520-3_30"
year: 2016
category: foundational
short: "Presentation of a loop invariant generation technique to discover polynomial relations between numerical variables"
---

We present in this paper a new technique for generating polynomial invariants,
divided in two independent parts: a procedure that reduces polynomial
assignments composed loops analysis to linear loops under certain hypotheses and
a procedure for generating inductive invariants for linear loops. Both of these
techniques have a polynomial complexity for a bounded number of variables and we
guarantee the completeness of the technique for a bounded degree which we
successfully implemented for C programs verification.
