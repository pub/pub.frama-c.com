---
plugins: [ "fanc" ]
authors: ["Pascal Cuoq", "David Delmas", "Stéphane Duprat", "Victoria Moya Lamiel"]
title: "Fan-C, a Frama-C plug-in for data flow verification"
book: "Proceedings of Embedded Real Time Software and Systems (ERTS²)"
link: "https://hal.archives-ouvertes.fr/hal-02263407/en"
year: 2012
category: foundational
---

DO-178B compliant avionics development processes must both define the data and control flows of embedded software at design level, and verify flows are faithfully implemented in the source code. This verification is traditionally performed during dedicated code reviews, but such intellectual activities are costly and error-prone, especially for large and complex software. In this paper, we present the Fan-C plug-in, developed by Airbus on top of the abstract-interpretation-based value and dataflow analyses of the Frama-C platform, in order to automate this verification activity for C avionics software. We therefore describe the Airbus context, the Frama-C platform, its value analysis and related plug-ins, the Fan-C plug-in, and discuss analysis results and ongoing industrial deployment and qualification activities.
