---
plugins: [ "eva" ]
authors: ["David Bühler", "Pascal Cuoq", "Boris Yakobowski"]
title: "Eva - The Evolved Value Analysis plug-in"
link: "http://frama-c.com/download/frama-c-eva-manual.pdf"
category: manual
---
