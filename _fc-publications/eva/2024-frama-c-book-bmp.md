---
plugins: [ "eva" ]
authors: [ David Bühler, André Maroneze, Valentin Perrelle ]
title: "Abstract Interpretation with the Eva Plug-in"
book: "Guide to Software Verification with Frama-C"
link: "https://link.springer.com/chapter/10.1007/978-3-031-55608-1_3"
doi: "10.1007/978-3-031-55608-1_3"
year: 2024
category: foundational
---
This chapter provides an overview of the Eva plug-in of Frama-C, a static
analyzer based on abstract interpretation, intended to automatically prove the
absence of runtime errors in critical software. It aims at giving users a good
understanding of how Eva works, by describing the theoretic principles
underlying its analysis and by detailing its most important features. More
practically, it also explains how to use Eva, set up an analysis and exploit its
results.
