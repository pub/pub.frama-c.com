---
plugins: [ "eva" ]
authors: ["Sandrine Blazy", "David Bühler", "Boris Yakobowski"]
title: "Structuring Abstract Interpreters Through State and Value Abstractions"
book: "Verification, Model Checking, and Abstract Interpretation (VMCAI)"
link: "https://hal-cea.archives-ouvertes.fr/cea-01808886/en"
doi: "10.1007/978-3-319-52234-0_7"
year: 2017
category: foundational
short: "Formalization of the communication mechanism between abstractions in EVA."
---

We present a new modular way to structure abstract interpreters. Modular means that new analysis domains may be plugged-in. These abstract domains can communicate through different means to achieve maximal precision. First, all abstractions work cooperatively to emit alarms that exclude the undesirable behaviors of the program. Second, the state abstract domains may exchange information through abstractions of the possible value for expressions. Those value abstractions are themselves extensible, should two domains require a novel form of cooperation. We used this approach to design EVA, an abstract interpreter for C implemented within the Frama-C framework. We present the domains that are available so far within EVA, and show that this communication mechanism is able to handle them seamlessly.
