---
plugins: [ "eva" ]
authors: ["David Bühler"]
title: "EVA, an Evolved Value Analysis for Frama-C: structuring an abstract interpreter through value and state abstractions"
school: "University of Rennes 1"
year: "2017"
link: "http://www.theses.fr/2017REN1S016"
category: phdthesis
type: "PhD Thesis"
---
