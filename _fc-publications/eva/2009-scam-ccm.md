---
plugins: [ "eva" ]
authors: ["Géraud Canet", "Pascal Cuoq", "Benjamin Monate"]
title: "A Value Analysis for C Programs"
book: "Proceedings of the Ninth IEEE International Working Conference on Source Code Analysis and Manipulation (SCAM)"
doi: "10.1109/SCAM.2009.22"
year: 2009
category: other
short: "Presentation of a former version of Eva."
---

We demonstrate the value analysis of Frama-C. Frama-C is an Open Source static analysis framework for the C language. In Frama-C, each static analysis technique, approach or idea can be implemented as a new plug-in, with the opportunity to obtain information from other plug-ins, and to leave the verification of difficult properties to yet other plug-ins. The new analysis may in turn provide access to the data it has computed. The value analysis of Frama-C is a plug-in based on abstract interpretation. It computes and stores supersets of possible values for all the variables at each statement of the analyzed program. It handles pointers, arrays, structs, and heterogeneous pointer casts. Besides producing supersets of possible values for the variables at each point of the execution, the value analysis produces run-time-error alarms. An alarm is emitted for each operation in the analyzed program where the value analysis cannot guarantee that there will not be a run-time error.
