---
plugins: [ "eva" ]
authors: ["Pascal Cuoq", "Damien Doligez"]
title: "Hashconsing in an incrementally garbage-collected system: a story of weak pointers and hashconsing in OCaml 3.10.2"
book: "Workshop on ML"
doi: "10.1145/1411304.1411308"
year: 2008
category: other
short: "This article describes weak pointers, weak hashtables and hashconsing as implemented in OCaml and used in a former version of Eva."
---

This article describes the implementations of weak pointers, weak hashtables and hashconsing in version 3.10.2 of the Objective Caml system, with focus on several performance pitfalls and their solutions.
