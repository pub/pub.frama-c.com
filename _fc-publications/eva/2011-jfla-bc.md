---
plugins: [ "eva" ]
authors: ["Richard Bonichon", "Pascal Cuoq"]
title: "A Mergeable Interval Map"
book: "Journées Francophones des Langages Applicatifs (JFLA)"
link: "https://pdfs.semanticscholar.org/5972/1d7cac5cf4fdc0cc3947432c6472f1da0f82.pdf"
year: 2011
category: foundational
---

This article describes an efficient persistent mergeable data structure for mapping intervals to values. We call this data structure rangemap. We provide an example of application where the need for such a data structure arises (abstract interpretation of programs with pointer casts). We detail different solutions we have considered and dismissed before reaching the solution of rangemaps. We show how they solve the initial problem and eventually describe their implementation.
