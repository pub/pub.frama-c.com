---
plugins: [ "slicing" ]
authors: ["Benjamin Monate", "Julien Signoles"]
title: "Slicing for Security of Code"
book: "Proceedings of the 1st international conference on Trusted Computing and Trust in Information Technologies (TRUST)"
link: "https://julien-signoles.fr/publis/2008_trust.pdf"
doi: "10.1007/978-3-540-68979-9_10"
year: 2008
category: foundational
---

Bugs in programs implementing security features can be catastrophic: for example they may be exploited by malign users to gain access to sensitive data. These exploits break the confidentiality of information. All security analyses assume that softwares implementing security features correctly implement the security policy, i.e. are security bug-free. This assumption is almost always wrong and IT security administrators consider that any software that has no security patches on a regular basis should be replaced as soon as possible. As programs implementing security features are usually large, manual auditing is very error prone and testing techniques are very expensive. This article proposes to reduce the code that has to be audited by applying a program reduction technique called slicing . Slicing transforms a source code into an equivalent one according to a set of criteria. We show that existing slicing criteria do not preserve the confidentiality of information. We introduce a new automatic and correct source-to-source method properly preserving the confidentiality of information i.e. confidentiality is guaranteed to be exactly the same in the original program and in the sliced program.
