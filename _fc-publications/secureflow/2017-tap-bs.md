---
plugins: [ "secureflow" ]
authors: ["Gergö Barany", "Julien Signoles"]
title: "Hybrid Information Flow Analysis for Real-World C Code"
book: "International Conference on Tests and Proofs (TAP)"
link: "https://julien-signoles.fr/publis/2017_tap.pdf"
doi: "10.1007/978-3-319-61467-0_2"
year: 2017
category: foundational
---

Information flow analysis models the propagation of data through a software system and identifies unintended information leaks. There is a wide range of such analyses, tracking flows statically, dynamically, or in a hybrid way combining both static and dynamic approaches.

We present a hybrid information flow analysis for a large subset of the C programming language. Extending previous work that handled a few difficult features of C, our analysis can now deal with arrays, pointers with pointer arithmetic, structures, dynamic memory allocation, complex control flow, and statically resolvable indirect function calls. The analysis is implemented as a plugin to the Frama-C framework.

We demonstrate the applicability and precision of our analyzer by applying it to an open-source cryptographic library. By combining abstract interpretation and monitoring techniques, we verify an information flow policy that proves the absence of control-flow based timing attacks against the implementations of many common cryptographic algorithms. Conversely, we demonstrate that our analysis is able to detect a known instance of this kind of vulnerability in another cryptographic primitive.
