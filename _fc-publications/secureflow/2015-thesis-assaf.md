---
plugins: [ "secureflow" ]
authors: ["Mounir Assaf"]
title: "From qualitative to quantitative program analysis: permissive enforcement of secure information flow"
school: "University of Rennes 1"
year: "2015"
link: "http://www.theses.fr/2015REN1S003"
category: phdthesis
type: "PhD Thesis"
---
