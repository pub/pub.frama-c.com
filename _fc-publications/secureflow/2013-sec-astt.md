---
plugins: [ "secureflow" ]
authors: ["Mounir Assaf", "Julien Signoles", "Éric Totel", "Frédéric Tronel"]
title: "Program transformation for non-interference verification on programs with pointers"
book: "International Information Security and Privacy Conference (SEC)"
link: "https://julien-signoles.fr/publis/2013_sec.pdf"
doi: "10.1007/978-3-642-39218-4_18"
year: 2013
category: foundational
---

Novel approaches for dynamic information flow monitoring are promising since they enable permissive (accepting a large subset of executions) yet sound (rejecting all unsecure executions) enforcement of non-interference. In this paper, we present a dynamic information flow monitor for a language supporting pointers. Our flow-sensitive monitor relies on prior static analysis in order to soundly enforce non-interference. We also propose a program transformation that preserves the behavior of initial programs and soundly inlines our security monitor. This program transformation enables both dynamic and static verification of non-interference.
