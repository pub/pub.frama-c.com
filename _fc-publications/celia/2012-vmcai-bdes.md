---
plugins: [ "celia" ]
authors: ["Ahmed Bouajjani", "Cezara Drăgoi", "Constantin Enea", "Mihaela Sighireanu"]
title: "Abstract Domains for Automated Reasoning about List-Manipulating Programs with Infinite Data"
book: "International Workshop on Verification, Model Checking, and Abstract Interpretation (VMCAI)"
link: "https://www.irif.fr/~cenea/papers/vmcai2012-lists.pdf"
doi: "10.1007/978-3-642-27940-9_1"
year: 2012
category: foundational
---

We describe a framework for reasoning about programs with lists carrying integer numerical data. We use abstract domains to describe and manipulate complex constraints on configurations of these programs mixing constraints on the shape of the heap, sizes of the lists, on the multisets of data stored in these lists, and on the data at their different positions. Moreover, we provide powerful techniques for automatic validation of Hoare-triples and invariant checking, as well as for automatic synthesis of invariants and procedure summaries using modular inter-procedural analysis. The approach has been implemented in a tool called Celia and experimented successfully on a large benchmark of programs.
