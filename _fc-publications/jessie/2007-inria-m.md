---
plugins: [ "jessie" ]
authors: ["Yannick Moy"]
title: "Checking C Pointer Programs for Memory Safety"
book: "INRIA Research Report n°6334"
link: "https://hal.inria.fr/inria-00181950/en"
year: 2007
category: foundational
---

We propose an original approach for checking memory safety of C
pointer programs that do not include casts, structures, double
indirection and memory deallocation. This involves first identifying
aliasing and strings, which we do in a local setting rather than
through a global analysis as it is done usually. Our separation
analysis in particular is a totally new treatment of non-aliasing.
We present for the first time two abstract lattices to deal with
local pointer aliasing and local pointer non-aliasing in an abstract
interpretation framework. The key feature of our work is to combine
abstract interpretation techniques and deductive verification. The
approach is modular and contextual, thanks to the use of Hoare-style
annotations (pre- and postconditions), allowing to verify each C
function independently. Abstract interpretation techniques are used
to automatically generate such annotations, in an idiomatic way:
standard practice of C programming is identified and incorporated as
heuristics. Abstract interpretation and deductive verification are
both used to check these annotations in a sound way. Our first
contribution is the design of an abstract domain for implications,
which makes it possible to build efficient contextual analyses. Our
second contribution is an efficient back-and-forth propagation
method to generate contextual annotations in a modular way, in the
framework of abstract interpretation. It considers the safety
conditions to prove as a starting point, which focuses the analysis
on the paths of interest. Thanks to previously unknown loop
refinement operators, this propagation method does not require
iterating around loops. We implemented our method in Caduceus, a
tool for the verification of C programs, and successfully verified
automatically the C standard string library functions.
