---
plugins: [ "jessie" ]
authors: ["Nikolai Kosmatov", "Virgile Prevosto", "Julien Signoles"]
title: "Specification and Proof of Programs with Frama-C"
book: "28th Symposium on Applied Computing (SAC)"
link: "http://www.sigapp.org/sac/sac2013/T4.pdf"
year: 2013
category: tutorial
---
