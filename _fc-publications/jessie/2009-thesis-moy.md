---
plugins: [ "jessie" ]
authors: ["Yannick Moy"]
title: "Automatic Modular Static Safety Checking for C Programs"
school: "University of Paris Sud"
year: "2009"
link: "http://www.lri.fr/~marche/moy09phd.pdf"
category: phdthesis
type: "PhD Thesis"
---
