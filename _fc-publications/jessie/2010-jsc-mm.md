---
plugins: [ "jessie" ]
authors: ["Yannick Moy", "Claude Marché"]
title: "Modular inference of subprogram contracts for safety checking"
book: "Journal of Symbolic Computation"
link: "http://hal.inria.fr/inria-00534331/en"
doi: "10.1016/j.jsc.2010.06.004"
year: 2010
category: foundational
---

Contracts expressed by logic formulas allow one to formally specify expected behavior of programs. But writing such specifications manually takes a significant amount of work, in particular for uninteresting contracts which only aim at avoiding run-time errors during the execution. Thus, for programs of large size, it is desirable to at least partially infer such contracts. We propose a method to infer contracts expressed as boolean combinations of linear equalities and inequalities by combining different kinds of static analyses: abstract interpretation, weakest precondition computation and quantifier elimination. An important originality of our approach is to proceed modularly, considering subprograms independently. The practical applicability of our approach is demonstrated on experiments performed on a library and two benchmarks of vulnerabilities of C code.
