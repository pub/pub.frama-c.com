---
plugins: [ "jessie" ]
authors: ["Romain Bardou"]
title: "Verification of Pointer Programs Using Regions and Permissions."
school: "University of Paris Sud"
year: "2011"
link: "https://theses.fr/2011PA112220"
category: phdthesis
type: "PhD Thesis"
---
