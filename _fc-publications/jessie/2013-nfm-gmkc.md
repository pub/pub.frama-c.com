---
plugins: [ "jessie" ]
authors: ["Alwyn Goodloe", "César A. Muñoz", "Florent Kirchner", "Loïc Correnson"]
title: "Verification of Numerical Programs: From Real Numbers to Floating Point Numbers"
book: "NASA Formal Methods (NFM)"
link: "https://shemesh.larc.nasa.gov/fm/papers/nfm2013-draft.pdf"
doi: "10.1007/978-3-642-38088-4_31"
year: 2013
category: other
---

Usually, proofs involving numerical computations are conducted in the infinitely precise realm of the field of real numbers. However, numerical computations in these algorithms are often implemented using floating point numbers. The use of a finite representation of real numbers introduces uncertainties as to whether the properties verified in the theoretical setting hold in practice. This short paper describes work in progress aimed at addressing these concerns. Given a formally proven algorithm, written in the Program Verification System(PVS), the Frama-C suite of tools is used to identify sufficient conditions and verify that under such conditions the rounding errors arising in a C implementation of the algorithm do not affect its correctness. The technique is illustrated using an algorithm for detecting loss of separation among aircraft.
