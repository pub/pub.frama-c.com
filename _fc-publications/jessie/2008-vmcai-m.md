---
plugins: [ "jessie" ]
authors: ["Yannick Moy"]
title: "Sufficient Preconditions for Modular Assertion Checking"
book: "Proceedings of the 9th International Conference on Verification, Model Checking, and Abstract Interpretation (VMCAI)"
link: "https://link.springer.com/chapter/10.1007/978-3-540-78163-9_18"
doi: "10.1007/978-3-540-78163-9_18"
year: 2008
category: foundational
---

Assertion checking is the restriction of program verification to
validity of program assertions. It encompasses safety checking,
which is program verification of safety properties, like memory
safety or absence of overflows. In this paper, we consider assertion
checking of program parts instead of whole programs, which we call
modular assertion checking. Classically, modular assertion checking
is possible only if the context in which a program part is executed
is known. By default, the worst-case context must be assumed, which
may impair the verification task. It usually takes user effort to
detail enough the execution context for the verification task to
succeed, by providing strong enough preconditions. We propose a
method to automatically infer sufficient preconditions in the
context of modular assertion checking of imperative pointer
programs. It combines abstract interpretation, weakest precondition
calculus and quantifier elimination. We instantiate this method to
prove memory safety for C and Java programs, under some memory
separation conditions.
