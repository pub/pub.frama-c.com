---
plugins: [ "jessie" ]
authors: ["François Bobot"]
title: "Logique de séparation et vérification déductive"
school: "University of Paris Sud"
year: "2011"
link: "https://theses.fr/2011PA112332"
category: phdthesis
type: "PhD Thesis"
---
