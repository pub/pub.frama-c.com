---
plugins: [ "jessie" ]
authors: ["Jochen Burghardt", "Jens Gerlach", "Hans Pohl", "Juan Soto"]
title: "An Experience Report on the Verification of Algorithms in the C++ Standard Library using Frama-C"
book: "First Proceedings of International Conference of Formal Verification of Object-Oriented Software (FoVeOOS)"
year: 2010
category: other
---

Over the past few years, we have been conducting assessment studies
to determine the utility of the Frama-C/Jessie platform of software
analyzers (in conjunction with automatic theorem provers) for the
formal verification of software. In this experience report, we
discuss experiments in the verification of algorithms in the C++
Standard Library based on tool-supported Hoare-style weakest
precondition computations to formally prove ACSL (ANSI/ISO C
Specification Language) properties. Often automated provers are
unable to perform inductive proofs. Hence, we introduce an approach
to guide automated provers to find an inductive proof using
auxiliary C-code corresponding to the proof structure. We also
present a method to verify that a function only permutes the
contents of an array, and obtain the relation between the pre- and
post-index for each array element for use in later specification
properties. Furthermore, we describe an approach to prove the
essential properties of a function independent of each other,
supplying for each task only the assumptions actually needed, i.e.,
related to the current goal. This approach reduces the proof search
space and leads to higher verification rates for automatic provers.
However, additional methods and tool support are desired to overcome
drawbacks from a software engineering point of view. Finally, we
sketch some ideas for an extension of ACSL for C++.
