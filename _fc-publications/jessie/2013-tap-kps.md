---
plugins: [ "jessie" ]
authors: ["Nikolai Kosmatov", "Virgile Prevosto", "Julien Signoles"]
title: "A Lesson on Proof of Programs with Frama-C"
book: "International Conference on Runtime Verification (RV)"
link: "https://frama-c.com/download/publications/kosmatov_ps_tap_2013.pdf"
doi: "10.1007/978-3-642-38916-0_10"
year: 2013
category: tutorial
---

Recent advances on proof of programs based on deductive methods allow verification tools to be successfully integrated into industrial verification processes. However, their usage remains mostly confined to the verification of the most critical software. One of the obstacles to their deeper penetration into industry is the lack of engineers properly trained in formal methods. A wider use of formal verification methods and tools in industrial verification requires their wider teaching and practical training for software engineering students as well as professionals.

This tutorial paper presents a lesson on proof of programs in the form of several exercises followed by their solutions. It is based on our experience in teaching at several French universities over the last four years. This experience shows that, for the majority of students, theoretical courses (like lectures on Hoare logic and weakest precondition calculus) are insufficient to learn proof of programs. We discuss the difficulties of the lesson for a student, necessary background, most frequent mistakes, and emphasize some points that often remain misunderstood. This lesson assumes that students have learned the basics of formal specification such as precondition, postcondition, invariant, variant, assertion.
