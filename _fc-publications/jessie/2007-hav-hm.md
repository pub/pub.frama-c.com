---
plugins: [ "jessie" ]
authors: ["Thierry Hubert", "Claude Marché"]
title: "Separation analysis for deductive verification"
book: "Proceedings of Heap Analysis and Verification (HAV)"
link: "http://www.lri.fr/~marche/hubert07hav.pdf"
year: 2007
category: foundational
---
