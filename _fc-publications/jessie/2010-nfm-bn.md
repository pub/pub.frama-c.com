---
plugins: [ "jessie" ]
authors: ["Sylvie Boldo", "Thi Minh Tuyen Nguyen"]
title: "Proofs of numerical programs when the compiler optimizes"
book: "Innovations in Systems and Software Engineering"
link: "https://hal.inria.fr/hal-00777639/document"
doi: "10.1007/s11334-011-0151-6"
year: 2011
category: foundational
---

On certain recently developed architectures, a numerical program may give different answers depending on the execution hardware and the compilation. Our goal is to formally prove properties about numerical programs that are true for multiple architectures and compilers. We propose an approach that states the rounding error of each floating-point computation whatever the environment and the compiler choices. This approach is implemented in the Frama-C platform for static analysis of C code. Small case studies using this approach are entirely and automatically proved.
