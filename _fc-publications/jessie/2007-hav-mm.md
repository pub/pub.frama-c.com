---
plugins: [ "jessie" ]
authors: ["Yannick Moy", "Claude Marché"]
title: "Inferring local (non-)aliasing and strings for memory safety"
book: "Proceedings of Heap Analysis and Verification (HAV)"
link: "https://www.researchgate.net/publication/250763933_Inferring_Local_NonAliasing_and_Strings_for_Memory_Safety_1"
year: 2007
category: foundational
---

We propose an original approach for checking memory safety of C
pointer programs, by combining deductive verification and abstract
interpretation techniques. The approach is modular and contextual,
thanks to the use of Hoare-style annotations (pre- and
postconditions), allowing us to verify each C function
independently. Deductive verification is used to check these
annotations in a sound way. Abstract interpretation techniques are
used to automatically generate such annotations, in an idiomatic
way: standard practice of C programming is identified and
incorporated as heuristics. Our first contribution is a set of
techniques for identifying aliasing and strings, which we do in a
local setting rather than through a global analysis as it is done
usually. Our separation analysis in particular is a totally new
treatment of non-aliasing. We present for the first time two
abstract lattices to deal with local pointer aliasing and local
pointer non-aliasing in an abstract interpretation framework. Our
second contribution is the design of an abstract domain for
implications, which makes it possible to build efficient contextual
analyses. Our last contribution is an efficient back-and-forth
propagation method to generate contextual annotations in a modular
way, in the framework of abstract interpretation. We implemented our
method in Caduceus, a tool for the verification of C programs, and
successfully generated appropriate annotations for the C standard
string library functions.
