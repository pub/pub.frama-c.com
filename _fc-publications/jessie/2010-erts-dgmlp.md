---
plugins: [ "jessie" ]
authors: ["Stéphane Duprat", "Pierre Gaufillet", "Victoria Moya Lamiel", "Frédéric Passarello"]
title: "Formal verification of SAM state machine implementation"
book: "Proceedings of Embedded Real Time Software and Systems (ERTS²)"
link: "https://www.researchgate.net/publication/265141424_Formal_verification_of_SAM_state_machine_implementation"
year: 2010
category: other
---

This paper reports the results of an experiment about the formal
verification of source code made according to an EMF model. Models
define the semantics of a system whereas the source code defines its
implementation. We applied this solution to a model of Automaton in
SAM language and its C language implementation. The technical
environment is close to an industrial operational context and all
the tools are available. The experimentation has succeeded and has
to be consolidated with bigger cases before an introduction in the
operational development process. More generally, this solution must
be extended to other model languages.
