---
plugins: [ "ltest" ]
authors: ["Thibault Martin", "Nikolai Kosmatov", "Virgile Prevosto", "Matthieu Lemerre"]
title: "Detection of Polluting Test Objectives for Dataflow Criteria"
book: "International Conference on Integrated Formal Methods (IFM)"
link: "https://hal-cea.archives-ouvertes.fr/cea-02974228"
doi: "10.1007/978-3-030-63461-2_18"
year: 2020
category: foundational
---

Dataflow test coverage criteria, such as all-defs and all-uses, belong to the most advanced coverage criteria. These criteria are defined by complex artifacts combining variable definitions, uses and program paths. Detection of polluting (i.e. inapplicable, infeasible and equivalent) test objectives for such criteria is a particularly challenging task. This short paper evaluates three detection approaches involving dataflow analysis, value analysis and weakest precondition calculus. We implement and compare these approaches, analyze their detection capacities and propose a methodology for their efficient combination. Initial experiments illustrate the benefits of the proposed approach.
