---
plugins: [ "ltest" ]
authors: ["Michaël Marcozzi", "Sébastien Bardin", "Mickaël Delahaye", "Nikolai Kosmatov", "Virgile Prevosto"]
title: "Taming Coverage Criteria Heterogeneity with LTest"
book: "International Conference on Software Testing, Verification and Validation (ICST)"
link: "https://hal-cea.archives-ouvertes.fr/cea-01808788v1"
doi: "10.1109/ICST.2017.57"
year: 2017
category: foundational
---

Automated white-box testing is a major issue in software engineering. In previous work, we introduced LTest, a generic and integrated toolkit for automated white-box testing of C programs. LTest supports a broad class of coverage criteria in a unified way (through the label specification mechanism) and covers most major parts of the testing process - including coverage measurement, test generation and detection of infeasible test objectives. However, the original version of LTest was unable to handle several major classes of coverage criteria, such as MCDC or dataflow criteria. Moreover, its practical applicability remained barely assessed. In this work, we present a significantly extended version of LTest that supports almost all existing testing criteria, including MCDC and some software security properties, through a native support of recently proposed hyperlabels. We also provide a more realistic view on the practical applicability of the extended tool, with experiments assessing its efficiency and scalability on real-world programs.
