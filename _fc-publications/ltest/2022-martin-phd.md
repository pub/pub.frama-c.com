---
plugins: [ "ltest" ]
authors: ["Thibault Martin"]
title: "Techniques de test pour des critères de couverture avancés"
school: "Université Paris-Saclay"
link: "https://www.theses.fr/2022UPASG077"
year: 2022
category: phdthesis
type: "PhD Thesis"
---

At a time where software are omnipresent in our daily life, verifying their
safety and security present an important challenge for the industry. The most
used technique to ensure that a software meets certain requirements is testing,
where we run the program with some controlled input sets. A major issue is then
to ensure that these input sets cover enough diﬀerent situations. Many proposals
have been made, called coverage criteria, which deﬁne test objectives through
which at least one execution must pass during tests. In this thesis, we examine
diﬀerent possibilities to combine formal methods (mathematical techniques) and
testing in order to improve the eﬃciency of the testing process. We focused our
eﬀorts on dataﬂow criteria, which observe the behavior of deﬁnitions and uses of
each variables in the program, and the detection of polluting objectives for
these criteria.We proposed several techniques, with their implementations and
evaluations on real C programs. We then studied the impact of these dataﬂow
criteria on test generation. Finally, we conclude by designing a method, using
polluting objectives detection, to verify the eﬀectiveness of countermeasures
against fault injection attacks, and thus ﬁnd errors in their
implementation. This method was applied to the bootloader of WooKey, an
encrypted storage device.
