---
plugins: [ "ltest" ]
authors: ["Sébastien Bardin", "Omar Chebaro", "Mickaël Delahaye", "Nikolai Kosmatov"]
title: "An All-in-One Toolkit for Automated White-Box Testing"
book: "International Conference on Tests and Proofs (TAP)"
link: "https://hal-cea.archives-ouvertes.fr/cea-01834983v1"
doi: "10.1007/978-3-319-09099-3_4"
year: 2014
category: foundational
---

Automated white-box testing is a major issue in software engineering. Over the years, several tools have been proposed for supporting distinct parts of the testing process. Yet, these tools are mostly separated and most of them support only a fixed and restricted subset of testing criteria. We describe in this paper Frama-C/LTest, a generic and integrated toolkit for automated white-box testing of C programs. LTest provides a unified support of many different testing criteria as well as an easy integration of new criteria. Moreover, it is designed around three basic services (test coverage estimation, automatic test generation, detection of uncoverable objectives) covering most major aspects of white-box testing and taking benefit from a combination of static and dynamic analyses. Services can cooperate through a shared coverage database. Preliminary experiments demonstrate the possibilities and advantages of such cooperations.
