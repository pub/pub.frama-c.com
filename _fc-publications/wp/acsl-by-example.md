---
plugins: [ "wp" ]
authors: ["Jens Gerlach", "Denis Efremov", "Tim Sikatzki"]
year: 2020
title: "ACSL by Example"
link: "https://github.com/fraunhoferfokus/acsl-by-example/blob/master/ACSL-by-Example.pdf"
category: tutorial
short: "A fairly complete tour of ACSL and WP features through various functions inspired from C++ STL."
---

[GitHub repository](https://github.com/fraunhoferfokus/acsl-by-example/)
