---
plugins: [ "wp" ]
authors: ["Patrick Baudin", "François Bobot", "Loïc Correnson", "Zaynah Dargaye", "Allan Blanchard"]
title: "WP Plug-in Manual"
link: "http://frama-c.com/download/frama-c-wp-manual.pdf"
category: manual
short: "The official manual for the WP plug-in."
---
