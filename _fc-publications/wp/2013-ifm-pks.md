---
plugins: [ "wp" ]
authors: ["Virgile Prevosto", "Nikolay Kosmatov", "Julien Signoles"]
title: "Specification and Proof of Programs with Frama-C"
book: "Integrated Formal Methods (iFM)"
link: "/download/publications/2013-ifm-pks/slides.pdf"
year: 2013
category: tutorial
short: "A tutorial about the Frama-C plug-in WP."
---

Despite the spectacular progress made by the developers of formal verification tools, their usage remains basically reserved for the most critical software. More and more engineers and researchers today are interested in such tools in order to integrate them into their everyday work. This half-day tutorial proposes a practical introduction during which the participants will write C program specifications, observe the proof results, analyze proof failures and fix them. Participants will be taught how to write a specification for a C program, in the form of function contracts, and easily prove it with an automatic tool in Frama-C, a freely available software verification toolset. Those who will have Frama-C installed (e.g. from ready-to-install packages frama-c, why, alt-ergo under Linux) will also run automatic proof of programs on their computer. Program specifications will be written in the specification language ACSL similar to other specification languages like JML that some participants may know. ACSL-syntax is intentionally close to C and can be easily learned on-the-fly.

[Annotated files](/download/publications/2013-ifm-pks/frama_c_tutorial_examples.tar.gz)
