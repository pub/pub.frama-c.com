---
plugins: [ "wp" ]
authors: ["Loïc Correnson"]
title: "Qed. Computing What Remains to Be Proved."
book: "NASA Formal Methods (NFM)"
link: "http://dx.doi.org/10.1007/978-3-319-06200-6_17"
year: 2012
category: foundational
short: "Presentation of Qed, a core library of WP that simplifies proof obligations before sending them to provers."
---

We propose a framework for manipulating in a efficient way terms and formulæ in classical logic modulo theories. Qed was initially designed for the generation of proof obligations of a weakest-precondition engine for C programs inside the Frama-C framework, but it has been implemented as an independent library. Key features of Qed include on-the-fly strong normalization with various theories and maximal sharing of terms in memory. Qed is also equipped with an extensible simplification engine. We illustrate the power of our framework by the implementation of non-trivial simplifications inside the Wp plug-in of Frama-C. These optimizations have been used to prove industrial, critical embedded softwares.
