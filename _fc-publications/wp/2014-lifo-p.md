---
plugins: [ "wp" ]
authors: ["Virgile Prevosto"]
title: "Frama-C WP tutorial"
book: "Laboratoire d'Informatique Fondamentale d'Orléans"
link: "/download/publications/2014-lifo-p/slides.pdf"
year: 2014
category: tutorial
short: "A tutorial about the Frama-C plug-in WP."
---

[LIFO's website](https://www.univ-orleans.fr/lifo/)

[Annotated files](/download/publications/2014-lifo-p/solutions.tar.gz)
