---
plugins: [ "wp" ]
authors: ["Pascal Cuoq", "Benjamin Monate", "Anne Pacalet", "Virgile Prevosto"]
title: "Functional Dependencies of C Functions via Weakest Pre-Conditions"
book: "International Journal on Software Tools for Technology Transfer (STTT)"
link: "https://link.springer.com/article/10.1007/s10009-011-0192-z"
doi: "10.1007/s10009-011-0192-z"
year: 2011
category: other
---

We present functional dependencies, a convenient, formal, but high-level, specification format for a piece of procedural software (function). Functional dependencies specify the set of memory locations, which may be modified by the function, and for each modified location, the set of memory locations that influence its final value. Verifying that a function respects pre-defined functional dependencies can be tricky: the embedded world uses C and Ada, which have arrays and pointers. Existing systems we know of that manipulate functional dependencies, Caveat and SPARK, are restricted to pointer-free subsets of these languages. This article deals with the functional dependencies in a programming language with full aliasing. We show how to use a weakest pre-condition calculus to generate a verification condition for pre-existing functional dependencies requirements. This verification condition can then be checked using automated theorem provers or proof assistants. With our approach, it is possible to verify the specification as it was written beforehand. We assume little about the implementation of the verification condition generator itself. Our study takes place inside the C analysis framework Frama-C, where an experimental implementation of the technique described here has been implemented on top of the WP plug-in in the development version of the tool.
