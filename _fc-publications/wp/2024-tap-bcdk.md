---
plugins: [ "wp" ]
authors: [ Allan Blanchard, Loïc Correnson, Adel Djoudi, Nikolai Kosmatov ]
title: "No Smoke without Fire: Detecting Specification Inconsistencies with Frama-C/WP"
book: "Proceedings of the 18th International Conference on Tests & Proofs (TAP 2024)"
link: "https://allan-blanchard.fr/publis/bcdk_tap_2024.pdf"
doi: "10.1007/978-3-031-72044-4_4"
year: 2024
category: other
---
Deductive verification provides a proof that, under the provided pre-conditions,
each terminating execution of a given function satisfies the stated
post-conditions. In general, pre- and post-conditions are expressed in a logical
specification language and typically rely on theories including abstract
definitions, axioms and lemmas. As they are written by humans, errors may be
introduced into specifications. Some errors can be detected when the proof
fails, but sometimes, they remain unnoticed due to misleading proofs: most of
the program may become dead code under the provided pre-conditions, or the proof
may succeed because of inconsistencies in hypotheses and axioms. In this tool
paper, we explore how to detect such unwanted situations by using deductive
verification techniques and describe the smoke test mechanism in Frama-C/WP, a
popular deductive verifier for C code. We show that, while the intuitive idea is
simple, making it practical requires optimizations to scale up, and report on
experiments with critical industrial code. Although our method is based on proof
techniques, it is not complete and is similar to testing. In the end, can we
ever be sure that our programs are proved well enough?
