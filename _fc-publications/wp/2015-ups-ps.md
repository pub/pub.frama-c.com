---
plugins: [ "wp" ]
authors: ["Andrei Paskevich", "Julien Signoles"]
title: "Course on Deductive Verification"
school: "University Paris Saclay"
year: 2015
category: teaching
short: In French.
---

Course on Deductive Verification in [Master MPRI](https://www.universite-paris-saclay.fr/formation/master/informatique/m1-master-parisien-de-recherche-en-informatique-mpri#programme) (1st year) at [University Paris-Saclay](https://www.universite-paris-saclay.fr/en) (France), given by [Andrei Paskevich](http://tertium.org/) (Why3) and [Julien Signoles](https://julien-signoles.fr/index.en.html) (Frama-C), since 2015.

Our exercises use both Frama-C/WP and [Why3](https://www.why3.org//) ([in a browser](https://www.why3.org//try/)). [A tarball](/download/publications/2015-ups-ps/wp-ups.tar.gz) contains most of our material related to Frama-C/WP (in French, still evolving each year). The training exercises are embedded in slides, the ones for the final practical session are directly provided in .c files.
