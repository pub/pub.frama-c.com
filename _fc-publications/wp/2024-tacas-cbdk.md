---
plugins: [ "wp" ]
authors: [ Loïc Correnson, Allan Blanchard, Adel Djoudi, Nikolai Kosmatov ]
title: "Automate where Automation Fails: Proof Strategies for Frama-C/WP"
book: "Tools and Algorithms for the Construction and Analysis of Systems. TACAS 2024"
link: "https://allan-blanchard.fr/publis/cbdk_tacas_2024.pdf"
doi: "10.1007/978-3-031-57246-3_18"
year: 2024
category: other
---
Modern deductive verification tools succeed in automatically proving the great
majority of program annotations thanks in particular to constantly evolving SMT
solvers they rely on. The remaining proof goals still require interactively
created proof scripts. This tool demo paper presents a new solution for an
automatic creation of proof scripts in Frama-C/WP, a popular deductive verifier
for C programs. The verification engineer defines a proof strategy describing
several initial proof steps, from which proof scripts are automatically
generated and applied. Our experiments on a large real-life industrial project
confirm that the new proof strategy engine strongly facilitates the verification
process by automating the creation of proof scripts, thus increasing the
potential of industrial applications of deductive verification on large code
bases.
