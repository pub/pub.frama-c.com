---
plugins: [ "wp" ]
authors: [ Allan Blanchard, François Bobot, Patrick Baudin, Loïc Correnson ]
title: "Formally Verifying that a Program Does What It Should: The Wp Plug-in"
book: "Guide to Software Verification with Frama-C"
link: "https://link.springer.com/chapter/10.1007/978-3-031-55608-1_4"
doi: "10.1007/978-3-031-55608-1_4"
year: 2024
category: foundational
---
This chapter presents how to prove ACSL properties of C programs with the Wp
plug-in of Frama-C using deductive verification and SMT solvers or Proof
Assistants. Specifically, this chapter explores the internals of the Wp plug-in,
with a specific focus on how ACSL and C are encoded into classical first-order
logic, including its various memory models. Then, the internal proof strategy of
Wp is described, which leads to a discussion on specification methodology and
proof engineering, and how to interact with the Wp plug-in. Finally, we compare
Wp with a selection of other amazing systems and logics for the deductive
verification of C programs.
