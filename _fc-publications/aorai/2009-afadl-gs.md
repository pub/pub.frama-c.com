---
plugins: [ "aorai" ]
authors: ["Julien Groslambert", "Nicolas Stouls"]
title: "Vérification de propriétés LTL sur des programmes C par génération d'annotations"
book: "Proceedings of Approches Formelles dans l'Assistance au Développement de Logiciels (AFADL)"
link: "http://hal.archives-ouvertes.fr/inria-00568947"
year: 2009
category: foundational
short: "In French."
---

Ce travail propose une méthode de vérification de propriétés temporelles basée sur la génération automatique d'annotations de programmes. Les annotations générées sont ensuite vérifiées par des prouveurs automatiques via un calcul de plus faible précondition. Notre contribution vise à optimiser une technique existante de génération d'annotations, afin d'améliorer l'automatisation de la vérification des obligations de preuve produites. Cette approche a été outillée sous la forme d'un plugin au sein de l'outil Frama-C pour la vérification de programmes C.
