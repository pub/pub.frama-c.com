---
plugins: [ "aorai" ]
authors: ["Nicolas Stouls"]
title: "Aoraï Plug-in Tutorial"
link: "http://frama-c.com/download/aorai/aorai-manual.pdf"
category: manual
short: "The official manual of the Frama-C plug-in Aoraï."
---
