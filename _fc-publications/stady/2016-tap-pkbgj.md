---
plugins: [ "stady" ]
authors: ["Guillaume Petiot", "Nikolai Kosmatov", "Bernard Botella", "Alain Giorgetti", "Jacques Julliand"]
title: "Your Proof Fails? Testing Helps to Find the Reason"
book: "International Conference on Tests and Proofs (TAP)"
link: "https://arxiv.org/abs/1508.01691"
doi: "10.1007/978-3-319-41135-4_8"
year: 2016
category: foundational
---

Applying deductive verification to formally prove that a program respects its formal specification is a very complex and time-consuming task due in particular to the lack of feedback in case of proof failures. Along with a non-compliance between the code and its specification (due to an error in at least one of them), possible reasons of a proof failure include a missing or too weak specification for a called function or a loop, and lack of time or simply incapacity of the prover to finish a particular proof. This work proposes a complete methodology where test generation helps to identify the reason of a proof failure and to exhibit a counterexample clearly illustrating the issue. We define the categories of proof failures, introduce two subcategories of contract weaknesses (single and global ones), and examine their properties. We describe how to transform a formally specified C program into C code suitable for testing, and illustrate the benefits of the method on comprehensive examples. The method has been implemented in StaDy, a plugin of the software analysis platform Frama-C. Initial experiments show that detecting non-compliances and contract weaknesses allows to precisely diagnose most proof failures.
