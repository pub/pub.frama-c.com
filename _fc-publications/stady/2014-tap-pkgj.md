---
plugins: [ "stady" ]
authors: ["Guillaume Petiot", "Nikolai Kosmatov", "Alain Giorgetti", "Jacques Julliand"]
title: "How Test Generation Helps Software Specification and Deductive Verification in Frama-C"
book: "International Conference on Tests and Proofs (TAP)"
link: "https://hal.inria.fr/hal-01108553/en"
doi: "10.1007/978-3-319-09099-3_16"
year: 2014
category: foundational
---

This paper describes an incremental methodology of deductive verification assisted by test generation and illustrates its benefits by a set of frequent verification scenarios. We present StaDy, a new integration of the concolic test generator PathCrawler within the software analysis platform Frama-C . This new plugin treats a complete formal specification of a C program during test generation and provides the validation engineer with a helpful feedback at all stages of the specification and verification tasks.
