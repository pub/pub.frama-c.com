---
plugins: [ "stady" ]
authors: ["Guillaume Petiot", "Nikolai Kosmatov", "Bernard Botella", "Alain Giorgetti", "Jacques Julliand"]
title: "Instrumentation of Annotated C Programs for Test Generation"
book: "14th International Working Conference on Source Code Analysis and Manipulation (SCAM)"
link: "https://hal.archives-ouvertes.fr/cea-01836306/en"
doi: "10.1109/SCAM.2014.19"
year: 2014
category: foundational
---

Software verification and validation often rely on formal specifications that encode desired program properties. Recent research proposed a combined verification approach in which a program can be incrementally verified using alternatively deductive verification and testing. Both techniques should use the same specification expressed in a unique specification language. This paper addresses this problem within the Frama-C framework for analysis of C programs, that offers ACSL as a common specification language. We provide a formal description of an automatic translation of ACSL annotations into C code that can be used by a test generation tool either to trigger and detect specification failures, or to gain confidence, or, under some assumptions, even to confirm that the code is in conformity with respect to the annotations. We implement the proposed specification translation in a combined verification tool Study. Our initial experiments suggest that the proposed support for a common specification language can be very helpful for combined static-dynamic analyses.
