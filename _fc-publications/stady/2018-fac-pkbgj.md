---
plugins: [ "stady" ]
authors: ["Guillaume Petiot", "Nikolai Kosmatov", "Bernard Botella", "Alain Giorgetti", "Jacques Julliand"]
title: "How testing helps to diagnose proof failures"
book: "Formal Aspects of Computing, vol. 30 issue 6"
link: "https://hal.archives-ouvertes.fr/hal-01948799/en"
doi: "10.1007/s00165-018-0456-4"
year: 2018
category: foundational
short: "Extended version of \"Your Proof Fails? Testing Helps to Find the Reason\"."
---

Applying deductive verification to formally prove that a program respects its formal specification is a very complex and time-consuming task due in particular to the lack of feedback in case of proof failures. Along with a non-compliance between the code and its specification (due to an error in at least one of them), possible reasons of a proof failure include a missing or too weak specification for a called function or a loop, and lack of time or simply incapacity of the prover to finish a particular proof. This work proposes a methodology where test generation helps to identify the reason of a proof failure and to exhibit a counterexample clearly illustrating the issue. We define the categories of proof failures, introduce two subcategories of contract weaknesses (single and global ones), and examine their properties. We describe how to transform a C program formally specified in an executable specification language into C code suitable for testing, and illustrate the benefits of the method on comprehensive examples. The method has been implemented in StaDy, a plugin of the software analysis platform Frama-C. Initial experiments show that detecting non-compliances and contract weaknesses allows to precisely diagnose most proof failures.
