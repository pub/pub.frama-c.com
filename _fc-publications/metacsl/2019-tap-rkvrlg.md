---
plugins: [ "metacsl" ]
authors: ["Virgile Robles", "Nikolai Kosmatov", "Virgile Prevosto", "Louis Rilling", "Pascale Le Gall"]
title: "Tame Your Annotations with MetAcsl: Specifying, Testing and Proving High-Level Properties"
book: "Tests and Proofs (TAP)"
link: "https://hal.archives-ouvertes.fr/cea-02301892/en"
doi: "10.1007/978-3-030-31157-5_11"
year: 2019
category: foundational
---

A common way to specify software properties is to associate a contract to each function, allowing the use of various techniques to assess (e.g. to prove or to test) that the implementation is valid with respect to these contracts. However, in practice, high-level properties are not always easily expressible through function contracts. Furthermore, such properties may span across multiple functions, making the specification task tedious, and its assessment difficult and error-prone, especially on large code bases. To address these issues, we propose a new specification mechanism called meta-properties. Meta-properties are enhanced global invariants specified for a set of functions, capable of expressing predicates on values of variables as well as memory related conditions (such as separation) and read or write access constraints. This paper gives a detailed presentation of meta-properties and their support in a dedicated Frama-C plugin MetAcsl, and shows that they are automatically amenable to both deductive verification and testing. This is demonstrated by applying these techniques on two illustrative case studies.
