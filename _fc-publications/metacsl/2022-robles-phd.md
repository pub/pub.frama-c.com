---
plugins: [ "metacsl" ]
authors: ["Virgile Robles"]
title: "Specifying and verifying high-level requirements on large programs : application to security of C programs"
school: "Université Paris-Saclay"
link: "https://www.theses.fr/2022UPAST001"
year: 2022
category: phdthesis
type: "PhD Thesis"
---

Specification and verification of highlevel requirements (such as security
properties like data integrity or confidentiality) remains an important
challenge for the industrial practice, despite being a major part of functional
specifications. This thesis presents a formal framework for their expression
called meta-properties, supported by a description on an abstract programming
language, focusing on properties related to memory and global invariants. This
framework is then applied to the C programming language, introducing the HILARE
extension to ACSL, to allow easy specification of these requirements on large C
programs. Verification techniques for HILARE, based on local assertion
generation and reuse of the existing Frama-C analyzers, are presented and
implemented into the MetAcsl plugin for Frama-C. A complete methodology for
assessing large programs is laid out, articulating meta-properties, verification
techniques and quirks specific to the C programming language. This methodology
is illustrated to a complex case study involving the bootloader of WooKey, a
secure USB storage device. Finally, we explore another way to verify a
high-level requirement deducing it from others, through a formal system proven
in Why3 and integrated in MetAcsl.
