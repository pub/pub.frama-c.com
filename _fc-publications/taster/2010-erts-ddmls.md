---
plugins: [ "taster" ]
authors: ["David Delmas", "Stéphane Duprat", "Victoria Moya Lamiel", "Julien Signoles"]
title: "Taster, a Frama-C plug-in to enforce Coding Standards"
book: "Proceedings of Embedded Real Time Software and Systems (ERTS²)"
link: "https://www.di.ens.fr/~delmas/papers/erts10.pdf"
year: 2010
category: foundational
---

Enforcing Coding Standards is part of the traditional concerns of industrial software developments. In this paper, we present a framework based on the open source Frama-C platform for easily developing syntactic, typing (and even some semantic) analyses of C source code, among which conformance to Coding Standards. We report on our successful attempt to develop a Frama-C plug-in named Taster, in order to replace a commercial, off-the-shelf, legacy tool in the verification process of several Airbus avionics software products. We therefore present the types of coding rules to be verified, the Frama-C platform and the Taster plug-in. We also discuss ongoing industrial deployment and qualification activities.
