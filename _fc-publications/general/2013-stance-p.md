---
plugins: [ "general" ]
authors: ["Virgile Prevosto"]
title: "Frama-C tutorial"
book: "STANCE Project"
year: 2013
category: tutorial
---

- Frama-C Overview: [Slides](/download/publications/2013-stance-p/frama_c_overview.pdf), [Simple plugin](/download/publications/2013-stance-p/simple_metrics.ml)
- Value Analysis: [Slides](/download/publications/2013-stance-p/value_tutorial.pdf) and [Examples](/download/publications/2013-stance-p/value_examples.tar.gz)
- WP: [Slides](/download/publications/2013-stance-p/wp_advanced_slides.pdf) and [Examples](/download/publications/2013-stance-p/wp_examples.tar.gz)
