---
plugins: [ "general" ]
authors: ["Loïc Correnson"]
title: "Ivette: A Modern GUI for Frama-C"
book: "Formal Integrated Development Environment (F-IDE)"
link: "/download/publications/correnson_fide_2022.pdf"
doi: "10.1007/978-3-031-26236-4_10"
year: 2022
category: uiux
short: "This papers presents the Frama-C Ivette GUI and discusses its design choices."
---

Using a static analyzer such as Frama-C is known to be difficult, even for
experienced users. Building a comfortable user interface to alleviate those
difficulties is however a complex task that requires many technical issues to be
handled that are outside the scope of static analyzers techniques. In this
paper, we present the design directions that we have chosen for completely
refactoring the old Graphical User Interface of Frama-C within the ReactJS
framework. In particular, we discuss middleware and language issues,
multithreaded client vs batch analyzer design, synchronization issues, multiple
protocol support, plug-in integration, graphical and user-interaction techniques
and how various programming language traits scale (or not) for such a
development project.
