---
plugins: [ "general" ]
authors: ["Florent Kirchner", "Nikolai Kosmatov", "Virgile Prevosto", "Julien Signoles", "Boris Yakobowski"]
title: "Frama-C, A Software Analysis Perspective"
book: "Formal Aspects of Computing, vol. 27 issue 3"
link: "https://julien-signoles.fr/publis/2015_fac.pdf"
doi: "10.1007/s00165-014-0326-7"
year: 2015
category: foundational
---

Frama-C is a source code analysis platform that aims at conducting verification of industrial-size C programs. It provides its users with a collection of plug-ins that perform static analysis, deductive verification, and testing, for safety- and security-critical software. Collaborative verification across cooperating plug-ins is enabled by their integration on top of a shared kernel and datastructures, and their compliance to a common specification language. This foundational article presents a consolidated view of the platform, its main and composite analyses, and some of its industrial achievements.
