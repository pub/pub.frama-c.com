---
plugins: [ "general" ]
authors: [ André Maroneze, Virgile Prevosto, Julien Signoles ]
title: "The Heart of Frama-C: The Frama-C Kernel"
book: "Guide to Software Verification with Frama-C"
link: "https://link.springer.com/chapter/10.1007/978-3-031-55608-1_2"
doi: "10.1007/978-3-031-55608-1_2"
year: 2024
category: kernel
---

This chapter provides an overview of the Frama-C distribution, including its
main plugins that are covered in depth by other chapters. It mainly focuses on
the Frama-C kernel and the main services that it offers to the user. This
includes notably passing proper arguments to launch Frama-C and drive an
analysis, controlling the parsing and code normalization phases of the analysis,
and visualizing results.
