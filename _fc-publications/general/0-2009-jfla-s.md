---
plugins: [ "general" ]
authors: ["Julien Signoles"]
title: " Foncteurs impératifs et composés: la notion de projets dans Frama-C"
book: "Journées Francophones des Langages Applicatifs (JFLA)"
year: 2009
category: kernel
short: "In French. Presentation of the Frama-C library providing its notion of projects."
---

This article describes the library of projects embedded in Frama-C, which is an extensible platform dedicated to development of source-code analysis of C software. Through this presentation, we detail an original aspect of ML functors which uses their imperative and compositional parts. That is the only well-typed way to implement the required functionality. Furthermore we show a singular example of a hugely-applied functor. This article also introduces the Frama-C platform through one of its main features, the notion of project.
