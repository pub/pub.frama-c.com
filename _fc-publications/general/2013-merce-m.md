---
plugins: [ "general" ]
authors: ["David Mentré"]
title: "Practical introduction to Frama-C"
book: "Mitsubishi Electric R&D Centre Europe"
link: "/download/publications/2013-merce-m/introduction-to-frama-c_v2.pdf"
year: 2013
category: tutorial
---

[Examples](/download/publications/2013-merce-m/introduction-slides-examples.tar.gz)
