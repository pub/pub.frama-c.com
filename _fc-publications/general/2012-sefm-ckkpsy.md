---
plugins: [ "general" ]
authors: ["Pascal Cuoq", "Florent Kirchner", "Nikolai Kosmatov", "Virgile Prevosto", "Julien Signoles", "Boris Yakobowski"]
title: "Frama-C, A Software Analysis Perspective"
book: "Proceedings of International Conference on Software Engineering and Formal Methods 2012 (SEFM)"
doi: "10.1007/978-3-642-33826-7_16"
year: 2012
category: foundational
short: "This paper presents a synthetic view of Frama-C, its main and composite analyses, and some of its industrial achievements."
---

Frama-C is a source code analysis platform that aims at conducting verification of industrial-size C programs. It provides its users with a collection of plug-ins that perform static analysis, deductive verification, and testing, for safety- and security-critical software. Collaborative verification across cooperating plug-ins is enabled by their integration on top of a shared kernel and datastructures, and their compliance to a common specification language. This foundational article presents a consolidated view of the platform, its main and composite analyses, and some of its industrial achievements.
