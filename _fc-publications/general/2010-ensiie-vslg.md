---
plugins: [ "general" ]
authors: ["Virgile Prevosto", "Julien Signoles", "Tristan Le Gall"]
title: "Static Analysis Course"
school: "ENSIIE Evry"
year: 2010-2018
category: teaching
short: In French.
---

Below is the list of exercices done with Frama-C during the Static
Analysis Course at ENSIIE:

  * [2010-2011](/html/publications/2010-ensiie-vslg/ensiie2010-2011-ias-tp.html)
  * [2011-2012](/html/publications/2010-ensiie-vslg/ensiie2011-2012-ias-tp.html)
  * [2012-2013](/html/publications/2010-ensiie-vslg/ensiie2012-2013-ias-tp.html)
  * [2013-2014](/html/publications/2010-ensiie-vslg/ensiie2013-2014-ias-tp.html)
  * [2014-2015](/html/publications/2010-ensiie-vslg/ensiie2014-2015-ias-tp.html)
  * [2015-2016](/html/publications/2010-ensiie-vslg/ensiie2015-2016-ias-tp.html)
  * [2016-2017](/html/publications/2010-ensiie-vslg/ensiie2016-2017-ias-tp.html)
  * [2017-2018](/html/publications/2010-ensiie-vslg/ensiie2017-2018-ias-tp.html)
