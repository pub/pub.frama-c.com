---
plugins: [ "general" ]
authors: ["Loïc Correnson", "Julien Signoles"]
title: "Combining Analyses for C Program Verification"
book: "Proceedings of International Workshop on Formal Methods for Industrial Critical Systems (FMICS)"
link: "https://julien-signoles.fr/publis/2012_fmics.pdf"
doi: "10.1007/978-3-642-32469-7_8"
year: 2012
category: kernel
short: "This paper explains how Frama-C combines several partial results coming from its plug-ins into a fully consolidated validity status for each program property."
---

Static analyzers usually return partial results. They can assert that some properties are valid during all possible executions of a program, but gener-ally leave some other properties to be verified by other means. In practice, it is common to combine results from several methods manually to achieve the full verification of a program. In this context, Frama-C is a platform for analyzing C source programs with multiple analyzers. Hence, one analyzer might conclude about properties assumed by another one, in the same environment. We present here the semantical foundations of validity of program properties in such a con-text. We propose a correct and complete algorithm for combining several partial results into a fully consolidated validity status for each program property. We illustrate how such a framework provides meaningful feedback on partial results.
