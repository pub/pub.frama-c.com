---
plugins: [ "general" ]
authors: ["Eugene Kornykhin"]
title: "Formal Specification and Verification"
school: "Moscow State University"
year: 2012-2013
link: "https://sed.ispras.ru/fmprac"
category: teaching
short: In Russian.
---
