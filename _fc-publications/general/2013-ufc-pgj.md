---
plugins: [ "general" ]
authors: ["Guillaume Petiot", "Alain Giorgetti", "Jacques Julliand"]
title: "Course on Frama-C"
school: "Université de Franche-Comté"
link: "/download/publications/2013-ufc-pgj/tp1.pdf"
year: 2013-2014
category: teaching
short: In French.
---
