---
plugins: [ "general" ]
authors: ["Pascal Cuoq", "Damien Doligez", "Julien Signoles"]
title: "Lightweight Typed Customizable Unmarshaling"
book: "Workshop on ML"
link: "http://blog.frama-c.com/public/unmarshal.pdf"
year: 2011
category: kernel
short: "This short paper presents how Frama-C unmarshal its data."
---

The contribution of this work is threefold. First, we offer an OCaml unmarshaling algorithm that uses a lightweight type-directed description of the expected structure of data to make consistency checks. The second contribution is the opportunity to specify functions to be systematically applied on values as they are being unmarshaled. Our third contribution is a type-safe layer for  these functions and for the unmarshaling algorithm itself.
