---
plugins: [ "general" ]
authors: ["Allan Blanchard", "Nikolai Kosmatov", "Frédéric Loulergue"]
title: "A Lesson on Verification of IoT Software with Frama-C"
book: "International Conference on High Performance Computing & Simulation (HPCS)"
link: "https://hal.inria.fr/hal-02317078/en"
doi: "10.1109/HPCS.2018.00018"
year: 2019
category: tutorial
---

This paper is a tutorial introduction to Frama-C, a framework for the analysis and verification of sequential C programs, and in particular its EVA, WP, and E-ACSL plugins. The examples are drawn from Contiki, a lightweight operating system for the Internet of Things.
