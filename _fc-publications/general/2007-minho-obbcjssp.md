---
plugins: [ "general" ]
authors: ["José N. Oliveira", "Luís S. Barbosa", "José B. Barros", "Alcino Cunha", "Maria João Frade", "Jorge Sousa Pinto"]
title: "Formal Methods in Software Engineering"
school: "University of Minho"
link: "https://mei.di.uminho.pt/?q=en/1112/mfes-uk"
year: 2007-2012
category: teaching
---
