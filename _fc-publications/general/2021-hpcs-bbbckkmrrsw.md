---
plugins: [ "general" ]
authors: ["Patrick Baudin", "François Bobot", "David Bühler", "Loïc Correnson", "Florent Kirchner", "Nikolai Kosmatov", "André Maroneze", "Valentin Perrelle", "Virgile Prevosto", "Julien Signoles", "Nicky Williams "]
title: "The Dogged Pursuit of Bug-Free C Programs: The Frama-C Software Analysis Platform"
book: "Communications of the ACM, Vol. 64, No. 8"
link: "https://cacm.acm.org/magazines/2021/8/254311-the-dogged-pursuit-of-bug-free-c-programs/fulltext"
doi: "10.1145/3470569"
year: 2021
category: foundational
---

<p>
The C programming language is a cornerstone of computer science. Designed by
Dennis Ritchie and Ken Thompson at Bell Labs as a key element of Unix
engineering, it was rapidly adopted by system-level programmers for its
portability, efficiency, and relative ease of use compared to assembly
languages. Nearly 50 years after its creation, it is still widely used in
software engineering.
</p>
<div class="media">
  <figure>
    <iframe
      src="https://player.vimeo.com/video/566198006?h=0b381aa7ea"
      frameborder="0"
      allowfullscreen
    ></iframe>
  </figure>
</div>
