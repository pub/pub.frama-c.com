---
plugins: [ "general" ]
authors: ["Julien Signoles", "Thibaud Antignac", "Loïc Correnson", "Matthieu Lemerre", "Virgile Prevosto"]
title: "Frama-C Plug-in Development Guide"
link: "http://frama-c.com/download/frama-c-plugin-development-guide.pdf"
category: manual
short: "The manual for developing a Frama-C plug-in."
---
