---
plugins: [ "general" ]
authors: ["Pascal Cuoq", "Julien Signoles", "Patrick Baudin", "Richard Bonichon", "Géraud Canet", "Loïc Correnson", "Benjamin Monate", "Virgile Prevosto", "Armand Puccetti"]
title: "Experience Report: OCaml for an Industrial-Strength Static Analysis Framework"
book: "International Conference of Functional Programming (ICFP)"
link: "https://julien-signoles.fr/publis/2009_icfp.pdf"
doi: "10.1145/1596550.1596591"
year: 2009
category: kernel
short: "This experience report describes the choice of OCaml as the implementation language for Frama-C."
---

This experience report describes the choice of OCaml as the implementation language for Frama-C, a framework for the static analysis of C programs. OCaml became the implementation language for Frama-C because it is expressive. Most of the reasons listed in the remaining of this article are secondary reasons, features which are not specific to OCaml (modularity, availability of a C parser, control over the use of resources...) but could have prevented the use of OCaml for this project if they had been missing.
