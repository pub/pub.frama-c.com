---
plugins: [ "c2s" ]
authors: ["Allan Blanchard", "Nikolai Kosmatov", "Frédéric Loulergue"]
title: "From Concurrent Programs to Simulating Sequential Programs: Correctness of a Transformation"
book: "Fifth International Workshop on Verification and Program Transformation (VPT)"
link: "https://hal.archives-ouvertes.fr/hal-01495454/en"
doi: "10.4204/EPTCS.253.9"
year: 2017
category: foundational
---

Frama-C is a software analysis framework that provides a common infrastructure and a common behavioral specification language to plugins that implement various static and dynamic analyses of C programs. Most plugins do not support concurrency. We have proposed conc2seq, a Frama-C plugin based on program transformation, capable to leverage the existing huge code base of plugins and to handle concurrent C programs. In this paper we formalize and sketch the proof of correctness of the program transformation principle behind conc2seq, and present an effort towards the full mechanization of both the for- malization and proofs with the proof assistant Coq.
