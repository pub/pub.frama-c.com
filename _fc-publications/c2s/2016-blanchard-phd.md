---
plugins: [ "c2s" ]
authors: ["Allan Blanchard"]
title: "Aide à la vérification de programmes concurrents par transformation de code et de spécifications"
school: "University of Orléans"
link: "https://www.theses.fr/2016ORLE2073"
year: 2016
category: phdthesis
type: "PhD Thesis"
---

Formal verification of concurrent programs is a hard task. There exists
different methods to perform such a task, but very few are applied to the
verification of programs written using real life programming languages. On the
other side, formal verification of sequential programs is successfully applied
for many years, and allows to get high confidence in our systems. As an
alternative to dedicated concurrent program analyses, we propose a method to
transform concurrent programs into sequential ones to make them analyzable by
tools dedicated to sequential programs. This work takes place within the
analysis framework FRAMA-C, dedicated to the analysis of C code specified with
ACSL. The different analyses provided by FRAMA-C are plugins to the framework,
which are currently mostly dedicated to sequential programs. We apply this
method to the verification of a concurrent code taken from an hypervisor. We
describe the automation of the method implemented by a new plugin to FRAMAC that
allow to produce, from a specified concurrent program, an equivalent specified
sequential program. We present the basis of a formalization of the method with
the objective to prove its validity. This validity is admissible only for the
class of sequentially consistent programs. So, we finally propose a prototype of
constraint solver for weak memory models, which is able to determine whether a
program is in this class or not, depending on the targeted hardware.
