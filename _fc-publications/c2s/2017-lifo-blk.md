---
plugins: [ "c2s" ]
authors: ["Allan Blanchard", "Nikolai Kosmatov", "Frédéric Loulergue"]
title: "Concurrent Program Verification by Code Transformation: Correctness"
book: "LIFO Research Report RR-2017-03"
link: "https://www.univ-orleans.fr/lifo/prodsci/rapports/RR/RR2017/RR-2017-03.pdf"
year: 2017
category: other
short: "Complete version of \"From Concurrent Programs to Simulating Sequential Programs: Correctness of a Transformation\""
---

Frama-C is a software analysis framework that provides a common infrastructure and a common behavioral specification language to plugins that implement various static and dynamic analyses of C programs. Most plugins do not support concurrency. We have proposed Conc2Seq, a Frama-C plugin based on program transformation, capable to leverage the existing huge code base of plugins and to handle concurrent C programs. In this paper we formalize and sketch the proof of correctness of the program transformation principle behind Conc2Seq, and present an effort towards the full mechanization of both the formalization and proofs with the proof assistant Coq.
