---
layout: post
author: André Maroneze
date: 2023-03-27 12:00 +0100
categories: event cybersecurity collaboration
title: Cyberhackathon - Frama-C + Binsec - 28/04/23
image: /assets/img/blog/cyber-hackathon-2023-04-28/banner.png
---

(*This is an announcement for an event near Paris; first follows
the French version, then an [English version](#english).*)

Si vous êtes près de Paris, venez au Cyber-hackathon **Frama-C +
[Binsec](https://binsec.github.io)**, le **28/04** de **9h à 17h**,
au [CEA List](https://list.cea.fr), dans le campus Paris-Saclay
([Nano-Innov, 2 bd Thomas Gobert, 91120 Palaiseau](https://www.openstreetmap.org/?mlat=48.71264&mlon=2.19218#map=17/48.71264/2.19218&layers=H)) !

Cette journée sera consacrée aux méthodes formelles, à l'analyse de code,
à la cybersécurité et au reverse engineering, à l'aide des plateformes
Frama-C et Binsec.


Lors de cette journée, vous pourrez :

- Amener votre propre code C pour le faire analyser par l'équipe Frama-C ;

- Participer aux tutoriels et challenges d'analyse de code avec Frama-C ;

- Participer aux challenges crackme et reverse engineering avec la plateforme Binsec ;

- Apprendre à utiliser Frama-C / Binsec, ou approfondir vos connaissances.


Si vous êtes fan d'OCaml (langage de développement majoritaire pour ces deux
plateformes), participez au développement de ces plateformes open-source.

Les équipes Frama-C et Binsec recrutent ! Stagiaires, CDDs, postdocs,
ingénieur·e·s-chercheu·r·se·s... n'hésitez pas à passer nous voir !


**Pour vous inscrire (date limite: 24/04): [cliquez ici](https://framaforms.org/inscription-cyber-hackathon-frama-c-binsec-1678815370)
**

Nous vous attendons nombreux·ses !


## English

If you are near Paris, come to the Cyber-hackathon **Frama-C +
[Binsec](https://binsec.github.io)**, on **28/04** from **9h to 17h**,
at [CEA List](https://list.cea.fr), in the Paris-Saclay campus
([Nano-Innov, 2 bd Thomas Gobert, 91120 Palaiseau](https://www.openstreetmap.org/?mlat=48.71264&mlon=2.19218#map=17/48.71264/2.19218&layers=H))!

This is an event dedicated to formal methods, code analysis, cybersecurity
and reverse engineering, with the help of the Frama-C and Binsec frameworks.

During this day, you will be able to:

- Bring your own C code to have it analyzed by the Frama-C team;
- Participate in the tutorials and code analysis challenges with Frama-C;
- Participate in the crackme and reverse challenges with the Binsec tool;
- Learn how to use Frama-C/Binsec, or become proficient with them;
- Participate in the development of these open-source platforms, developed mainly in OCaml.

**[Please click here to register (until 24/04)](https://framaforms.org/inscription-cyber-hackathon-frama-c-binsec-1678815370)**
(the form is in French, but feel free to contact us directly in English if you prefer).

By the way, the Frama-C and Binsec teams are hiring! Interns, PhDs, postdocs,
temporary- and fixed-term researchers… drop by if you are interested!
