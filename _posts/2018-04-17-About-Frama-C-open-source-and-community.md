---
layout: post
author: André Maroneze
date: 2018-04-17 18:54 +0200
categories: collaboration
image:
title: "About Frama-C, open source, and community"
published: false
---

The Frama-C platform being an open-source software project, it has significant
differences with respect to commercial tools, which go beyond questions of
cost of acquisition and source code availability. In this post, we list
the various communication channels available to users, why asking questions
is important, and the benefits of the *community* aspect of Frama-C.

## Free in dollars, but not free in time

Frama-C is released (about twice a year) "for free": the source code is
made available, so that users (from industry and academia alike) may access
it for free, without having to buy any licenses. However, using Frama-C has
a real, tangible cost: time. Time to download and install the software
(in the best case, simply `opam install frama-c`), time to learn how to use it
(read manuals, tutorials, learn some ACSL, develop a plug-in, ask questions in
Stack Overflow), and time to run the analyses and compile results.

Frama-C is very mindful of this and its developers strive to make the user
experience as straightforward as possible, with several communication channels
available for discussion, including:

- a mailing list ([frama-c-discuss](https://groupes.renater.fr/sympa/subscribe/frama-c-discuss));
- a [Stack Overflow tag](http://stackoverflow.com/tags/frama-c/) which is
  regularly monitored by Frama-C developers and expert users;
- a [bug tracking system](http://bts.frama-c.com/) for "old-style" developers;
- a [Github group](https://github.com/Frama-C/Frama-C-snapshot) for "new-style"
  developers, including:
  - another [Github group for ACSL](https://github.com/acsl-language/acsl) itself
    (which is related, but independent of Frama-C);
  - a repository with [Frama-C snapshots](https://github.com/Frama-C/Frama-C-snapshot),
    which also serves to post issues (for instance, the bug tracking system
    does not support Markdown syntax);
  - a repository with some [concrete examples](https://github.com/Frama-C/open-source-case-studies)
    on how Frama-C is used in practice, particularly the *Eva* plug-in;
- this very [blog](http://blog.frama-c.com/) itself;
- and a [@frama_c twitter account](https://twitter.com/frama_c).

All of these channels can help users get going and be productive with Frama-C.
Also, don't forget that the PDF manuals (available at the
[Frama-C download page](http://frama-c.com/download.html)) are more detailed
than Frama-C's help messages and `man` page.

> Note that Frama-C developers and users monitor several channels at once,
> so if you ask a question in one of them, it's not worth repeating it on the
> others; if there hasn't been an answer after a few days, it's possible that
> the question is technical and requires a specific person to reply to it.
> Also, some questions are just too vague, imprecise or unclear to be able to
> be answered. [Stack Overflow](https://stackoverflow.com/help/how-to-ask) has
> useful tips on this subject.

## Don't be afraid to ask

Frama-C is not a product: it is not (yet?) an off-the-shelf tool that you can
just expect to install and readily apply to any code base you have
(this is still one of our goals; our base of
[open source case studies](https://github.com/Frama-C/open-source-case-studies)
serves to remind us of how much we still need to advance, but also allows
us to see its evolution: less parametrization and more automation is still
happening at each release).

Still, we are fully aware that Frama-C's features evolve faster than the time
dedicated to fully explain and make them as intuitive as possible. Expert
industrial partners and advanced academic researchers dictate much of the
pacing of the platform. Frama-C's strengths do not lie in syntactic bug
detection, but in sophisticated semantic analyses that impose some barriers
to entry. Trying to minimize these barriers is part of our roadmap, but they
also require some commitment or, at the very least, awareness of one's own
limitations
([Dunning-Kruger](https://en.wikipedia.org/wiki/Dunning%E2%80%93Kruger_effect)
plays a role here). Knowing that you *can* ask the community for help is
important. It is equally important to realize that, if things seem too hard
or convoluted, there's probably a better approach. Consider asking about it:
in the best case, someone will suggest an alternative. In the worst case,
it helps knowing that you're not alone (and that the problem is likely more
complicated than anticipated).

## A community is not a product

Frama-C enjoys a community which feeds back lots of very useful and helpful
comments: users reporting issues, but also success stories; researchers
mentioning how Frama-C helped them obtain results, or comparing them to their
research prototypes; limitations found in the tool, and directions where
evolution is needed. Industrial partners commending on the possibilities given
by the tool, but also criticizing when it failed to meet expectations.

All of this is very welcome, but you are also free not to give back any
feedback at all! Frama-C does not track usage statistics, installations,
and does not restrict users from using the tool in any way they like.

More importantly, Frama-C imposes no restrictions on its usage and on the
diffusion of its results. No NDAs, no requirements about publishing results
whatsoever. This puts us in a fragile position, since we are under scrutiny
but cannot always extend the same level of scrutiny towards others;
however, the benefits we obtain from the open source community are worth it:
not only we get users reporting feedback and providing interesting ideas,
but in some cases they even spend their precious time towards *improving*
the platform, not only for themselves, but for everyone.

In exchange, we kindly request users that, if they are able to provide
feedback, they should certainly do so. We value fair criticism, especially
when given right of reply: Frama-C does not advertise its capabilities
in the same way as consumer-ready products, so being able to reply with
the point of view of an insider about technical capabilities and limitations
is essential to ensure a proper assessment of the situation.

For instance, something as short as a tweet to mention something like
"*I tried Frama-C, had this issue during installation, then that issue when
running plug-in X, but overall I managed to do what I intended*", while not a
request in itself, is certainly useful to remind us of Frama-C's pain points
and take them into account in the future.

Finally, there is the possibility of engaging in collaborations for the
development of new features, either via contracts or research projects and
partnerships. Sophisticated plug-ins and analyses might require substantial
work, but don't be afraid to expose your needs and ideas. Maybe there's already
someone doing similar things, or other people with the same needs. Knowing
about them may help form the critical mass to make it happen.

## Conclusion

Our open-source philosophy is not simply "software whose sources are available
for everyone to see", but that of a community structured around a platform for
the development and promotion of formal methods around this unique programming
language that is C. This includes both academic and industrial research,
as well as occasional usage by lone [hackers](https://monocypher.org/)
trying different things.

If you are new to the platform, do not hesitate to contact the community for
guidance and feedback. We are always interested in hearing from users of
Frama-C, whether the attempted usage did or did not work.
