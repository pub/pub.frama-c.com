---
layout: post
author: Pascal Cuoq
date: 2011-06-06 12:12 +0200
categories: derived-analysis floating-point icpc2011 value value-builtins
format: xhtml
title: "Fixing robots, part 1"
summary:
---
{% raw %}
<p>This blog post is a revised version of part of my submission to the <a href="http://icpc2011.cs.usask.ca/conf_site/IndustrialTrack.html">ICPC 2011 Industry Challenge</a>. Please  go ahead and read the challenge description. I could only paraphrase it without adding anything to it  and so I won't.</p>
<p>The study was made with the April development version of Frama-C  which
differs from the last released version in a number of bugfixes (<a href="http://bts.frama-c.com/" hreflang="en">list of reported issues</a>) and minor new
features. Specifically  it uses the value analysis and dependencies
computations (documented in <a href="http://frama-c.com/download/frama-c-value-analysis.pdf" hreflang="en">this manual</a>) and
<a href="http://frama-c.com/scope.html" hreflang="en">code navigation features</a>. The latter reuse
building blocks that were implemented for <a href="http://frama-c.com/slicing.html" hreflang="en">slicing</a>.</p>
<h2>The ever important first step: identify missing library functions</h2>
<p>The first step  as the faithful reader of this blog would now know  before using the value analysis for anything is to
identify library functions the program depends on:</p>
<pre>frama-c impls.c main.c roco.c sim.c main_testcase_1.c -metrics -cpp-command "gcc -DTESTCASE=1 -C -E"
...
Undefined functions (10):
 __builtin_inf  (15 calls); __builtin_inff  (15 calls); fprintf  (2 calls);
 __swbuf  (10 calls); __builtin_fabsf  (15 calls); exp  (1 call);
 __builtin_infl  (15 calls); __builtin_fabsl  (15 calls);
 __builtin_fabs  (15 calls); fabs  (9 calls);
...
</pre>
<p>The host computer's standard headers have been used  hence the
confusing <code>__builtin_</code> prefixes. Better not rely on them: the robot's
operating system probably bears little resemblance to the host's.
Placeholder headers provided with Frama-C can be used instead:</p>
<pre>frama-c impls.c main.c roco.c sim.c main_testcase_1.c -metrics -cpp-command "gcc -DTESTCASE=1 -C -E -nostdinc -I. -I/usr/local/share/frama-c/libc"
...
Undefined functions (3):
 exp  (1 call); fabs  (9 calls); fprintf  (2 calls);
...
</pre>
<p>Standard functions <code>exp()</code> and <code>fabs()</code> are missing from the Carbon release  but
have been added to the development version. With the development version  one
simply needs to list <code>/usr/local/share/frama-c/libc/math.c</code> as a file of
the analysis project. Function <code>exp()</code> is implemented as a value analysis
builtin  taking advantage of the host's own <code>exp()</code> function.</p>
<p>If you wish to reproduce at home  you have to provide
your own implementation for <code>exp()</code>. It may not need to be very accurate
but it needs to be analyzable with Carbon's value analysis. A good way would
be to clean up the implementation from Sun that
everyone is using.
As I already said in <a href="/2011/02/25/Numerical-functions-are-not-merely-twisted.html">previous posts</a>  accessing the bits of a floating-point
representation directly is <em>so</em> 1990s. If someone removed these ugly and
obsolete manipulations  we would end up with a better implementation for
<code>exp()</code> (and it would be analyzable with the value analysis from Carbon).</p>
<p>For <code>fabs()</code>
you can use the function below. An implementation from a library would
probably unset the sign bit in the IEEE 754 representation but  again
Carbon's value analysis doesn't handle this precisely yet.
The implementation below is handled
precisely in the sense that a singleton for the input <code>x</code> results in
a singleton for the returned value.</p>
<pre>double fabs(double x){
  if (x==0.0) return 0.0;
  if (x&gt;0.0) return x;
  return -x;
}
</pre>
<p>Finally  calls to <code>fprintf()</code>  that do not have any
repercussions on the continuing execution  can be replaced with
calls to the <code>Frama_C_show_each()</code> built-in. We can let the
pre-processor to do this for us
passing the option <code>-Dfprintf=Frama_C_show_each</code> to GCC when pre-processing.</p>
<h2>Looking for unspecified/undefined behaviors</h2>
<p>We are now ready to launch the value analysis.
Since we anticipate testcases 2 and 3 to be very similar  we write a short
script:</p>
<pre>#!/bin/bash
export CPP="gcc -Dfprintf=Frama_C_show_each -DTESTCASE=$1 -C -E -nostdinc -I. -I/usr/local/share/frama-c/libc"
FILES="/usr/local/share/frama-c/libc/math.c impls.c main.c roco.c sim.c"
TESTCASE="main_testcase_$1.c"
exec frama-c ${FILES} ${TESTCASE} -val -slevel 50000 -unspecified-access -val-signed-overflow-alarms -calldeps -save state$1
</pre>
<p>About ten minutes after running <code>script.sh 1 &gt; log1</code>
we obtain a log (long:~70MiB) and a state (~1MiB).
The log is long because it contains progression messages that are only intended to help identify
analysis issues  and can be ignored most of the time. The state contains all the information
that has been computed about the program  including values of variables
in a very compact format.</p>
<p>The analysis is completely unrolled (because of option <code>-slevel 50000</code>)
and precise until the end. This means that the value analysis has in
effect simulated the execution of the program with the inputs
provided in <code>main_testcase_1.c</code>.</p>
<p>The log  despite its size  <strong>does not warn about any of the undefined or unspecified behaviors</strong> that the
value analysis is guaranteed to
identify (uninitialized access  use of a dangling pointer
overflows in signed integer arithmetics  invalid memory access  invalid
comparison of pointers  division by zero
undefined logical shift  overflows in conversions from
floating-point to integer  infinite or NaN resulting from a
floating-point operation  undefined side-effects in expressions).
This is very important. It means that we can rest assured
that the strange dynamic behavior we are going to investigate
is not caused by the misuse of one of C's dangerous
constructs. Nothing would be more frustating than having to track the value
of a variable which  according to the source code  is not supposed to
change  but is modified through a buffer overflow.  The value analysis
guarantees we won't have to do that for this execution.</p>
<blockquote><p>What would of course be better would be to verify that there can be none of the above undefined behaviors for <strong>any</strong> command sequence. This would be a much stronger result  but would also require a lot more work. When simulating a single execution inside the analyzer  we only check that that particular execution is free of undefined behavior  but it does not require any work from us (only <a href="/2011/03/01/Analyzing-unit-tests-and-interpretation-speed.html">a few minutes of work from the computer</a>).</p>
</blockquote>
<h2>Exploiting the value analysis' results for program comprehension</h2>
<p>The values computed and stored in <code>state1</code> can be observed in
Frama-C's GUI  using the command-line <code>frama-c-gui -load state1</code>.
The GUI can also be used to identify the definition
site(s) of a variable's value: select the variable at the program point you are interested in
and in the contextual menu  invoke Dependencies → Show defs.</p>
<p>Here  it is the value of variable <code>RoCo_engineVoltage</code>
as displayed by the call to <code>fprintf()</code> (that we transformed into a call
to <code>Frama_C_show_each()</code>) that is wrong  so we request the definition site(s)
of that value:</p>
<p><a href="/assets/img/blog/imported-posts/ICPC11/s1.png" title="s1.png"><img src="/assets/img/blog/imported-posts/ICPC11/.s1_m.jpg" alt="s1.png" style="display:block; margin:0 auto;" title="s1.png  avr. 2011" /></a></p>
<p>The GUI has pointed us to a call to function <code>RoCo_Process()</code> (using the yellow mark)
so we now request the definition
sites of <code>RoCo_engineVoltage</code> by the <code>return;</code> statement of that function.
We obtain the two sites identified below:</p>
<p><a href="/assets/img/blog/imported-posts/ICPC11/s2.png" title="s2.png"><img src="/assets/img/blog/imported-posts/ICPC11/.s2_m.jpg" alt="s2.png" style="display:block; margin:0 auto;" title="s2.png  avr. 2011" /></a></p>
<p>The condition that decides which branch is executing is the one shown in the screenshot
below.</p>
<p><a href="/assets/img/blog/imported-posts/ICPC11/s3.png" title="s3.png"><img src="/assets/img/blog/imported-posts/ICPC11/.s3_m.jpg" alt="s3.png" style="display:block; margin:0 auto;" title="s3.png  avr. 2011" /></a></p>
<p>The value analysis tell us the value of <code>RoCo_isActive</code> can be either <code>0</code> or <code>1</code>
at this point during execution  but this variable is one of the variables whose value is printed in
the analysis log  and its value was <code>1</code> at the instant we are interested in. We therefore focus
on the definition site where the value assigned to <code>RoCo_engineVoltage</code> is computed
in a call to <code>PT1_Filter()</code>.</p>
<p>The dependencies of the particular call to <code>PT1_Filter()</code> we are
interested in were computed by option -calldeps and can be found in the log.
The call we are interested in is at statement 433. The log contains:</p>
<pre>call PT1_Filter at statement 433:
 voltageFilter FROM state; x; t1; dt; voltageFilter
 \esult FROM state; x; t1; dt; voltageFilter
</pre>
<p>Apart from reading its arguments <code>state</code>  <code>x</code>  <code>t1</code>  and <code>dt</code>  the call accesses
a static variable <code>voltageFilter</code>. The address of <code>voltageFilter</code> is taken
so we have to be careful: this variable could be modified erroneously
through a pointer (although the address-taking appears to be only to
pass it to <code>PT1_Filter()</code>  which is innocent enough).</p>
<p>In fact  at this point  we have no idea which of the variables involved
in the computation of the result of this call to <code>PT1_Filter()</code> is wrong.
Clicking on a variable in the GUI provides the set of values for this
variable at this program point  but this is still too imprecise here
since it mixes all 10000 or so passages through the statement.</p>
<p>Let us take advantage of the "blackboard" structure of the analyzed
program and dump the entire program state at this statement
by inserting a call to <code>Frama_C_dump_each()</code>.
See <a href="/2011/04/21/List-of-the-ways-Frama_C_dump_each-is-better-than-printf.html">this previous post</a> for
a list of advantages of this built-in function over <code>printf()</code> or a
debugger.</p>
<pre>--- roco.c	(revision 12956)
+++ roco.c	(working copy)
@@ -293 6 +293 7 @@
                 desiredEngineVoltage  Engine_maxVoltage_PARAM);
         limitationActive = (Engine_maxVoltage_PARAM == desiredEngineVoltage) ||
                              (Engine_minVoltage_PARAM == desiredEngineVoltage);
+	Frama_C_dump_each();
         RoCo_engineVoltage = PT1_Filter (&amp;voltageFilter  desiredEngineVoltage
                 t13  dT);
         wasInit = init;
</pre>
<p>We need to launch the analysis again and find something to do for 10 minutes.
This is a good time to start looking at bugs 2 and 3.</p>
<p>The log now contains state dumps for each passage through the statement
where <code>RoCo_engineVoltage</code> is computed.</p>
<p>According to the <code>ReadMe.txt</code> in the challenge package
an order is given at t=50s. The log shows that this order
fails to be executed speedily.
The state dump at which <code>lastTime</code> contains 50000
and the next few ones show that of <code>RoCo_engineVoltage</code>'s dependencies
variable <code>desiredEngineVoltage</code> is the one with the suspicious value: it is only
<code>-0.8</code>  whereas parameters in file <code>roco_config_testcase_1.h</code> and values of the variable elsewhere in our
log show that this voltage can go much higher. We are therefore left with the sub-problem of identifying why
this variable has this value at this program point.</p>
<p>We use the same tools we have already
used for <code>RoCo_engineVoltage</code>  this time applied to variable <code>desiredEngineVoltage</code>
and this program point. The screenshot below shows the definitions sites for that value.</p>
<p><a href="/assets/img/blog/imported-posts/ICPC11/s4.png" title="s4.png"><img src="/assets/img/blog/imported-posts/ICPC11/.s4_m.jpg" alt="s4.png" style="display:block; margin:0 auto;" title="s4.png  avr. 2011" /></a></p>
<p>The value of variable <code>desiredEngineVoltage</code> is defined by the call to function <code>Limiter_Out()</code>
whose argument is in turn defined by the call to <code>Interpolate_from_curve()</code> above.</p>
<p>Option -calldeps computed the implicit inputs of this call  which can be found in the log:</p>
<pre>call Interpolate_from_curve at statement 423:
 \esult FROM curve; x;
              EngineSpeedToVoltage_CURVE{.numPoints; .x[0..4]; .y[1..3]; }
</pre>
<p>The state dump in which <code>lastTime==50000</code> shows that a low value for
<code>angleDiffRequest</code> is the cause for the low value of <code>desiredEngineVoltage</code>.</p>
<p>The "Show defs" action in the GUI finds three possible definition sites for
this value of <code>angleDiffRequest</code>  shown in the screenshots below.</p>
<p><a href="/assets/img/blog/imported-posts/ICPC11/s5.png" title="s5.png"><img src="/assets/img/blog/imported-posts/ICPC11/.s5_m.jpg" alt="s5.png" style="display:block; margin:0 auto;" title="s5.png  avr. 2011" /></a></p>
<p><a href="/assets/img/blog/imported-posts/ICPC11/s6.png" title="s6.png"><img src="/assets/img/blog/imported-posts/ICPC11/.s6_m.jpg" alt="s6.png" style="display:block; margin:0 auto;" title="s6.png  avr. 2011" /></a></p>
<p><a href="/assets/img/blog/imported-posts/ICPC11/s7.png" title="s7.png"><img src="/assets/img/blog/imported-posts/ICPC11/.s7_m.jpg" alt="s7.png" style="display:block; margin:0 auto;" title="s7.png  avr. 2011" /></a></p>
<p>We find in the log that variable <code>rampValue</code> remains at <code>0</code> in the cycles
that follow instant <code>50000</code>. The value we observe for <code>angleDiffRequest</code>
is compatible with the algorithm and values of variables at lines
264-277 of file roco.c. So it looks like the cause of the issue
is the value of variable <code>rampValue</code>. Action "Show defs" in the GUI
indicates that this value is computed by the call to <code>Ramp_out()</code> at line 240.
The value of <code>rampTarget</code> is computed as <code>0.0</code> or <code>1.0</code> from a number of
variables  and of these variables  <code>RoCo_legAngleValid</code> was always 1
and <code>direction</code> was always <code>0</code> or <code>1</code>. The latter is suspicious  since
in this execution  orders are given to move in both directions:</p>
<p><a href="/assets/img/blog/imported-posts/ICPC11/s8.png" title="s8.png"><img src="/assets/img/blog/imported-posts/ICPC11/.s8_m.jpg" alt="s8.png" style="display:block; margin:0 auto;" title="s8.png  avr. 2011" /></a></p>
<p>The command "Show defs" applied to variable <code>direction</code> shows that indeed
the variable may have been set to <code>0</code> or <code>1</code> in three different sites.</p>
<p><a href="/assets/img/blog/imported-posts/ICPC11/s9.png" title="s9.png"><img src="/assets/img/blog/imported-posts/ICPC11/.s9_m.jpg" alt="s9.png" style="display:block; margin:0 auto;" title="s9.png  avr. 2011" /></a></p>
<p>The site that corresponds to the MoveByAngle command  the middle one in the screenshot above
is suspicious: in the file main_testcase_1.c  the angle passed to this command is negative.
This is not just a strange convention  because the computation below for the third
definition site determined the direction for the other commands  that are all in
the opposite direction  and variable <code>direction</code> was assigned <code>1</code> again there.</p>
<p>This suggests the fix below.</p>
<pre>--- roco.c	(revision 13019)
+++ roco.c	(working copy)
@@ -211 7 +211 7 @@
                     direction = 0;
                 }
                 else {
-                    direction = (RoCo_desiredDeltaAngle &gt; 0.0) ? -1 : 1;
+                    direction = (RoCo_desiredDeltaAngle &gt; 0.0) ? 1 : -1;
                 }
                 RoCo_commandMoveByAngle = FALSE;
             }
</pre>
<p>Looking at the source code for related issues  one may notice that the value given to <code>direction</code>
is also affected by the piece of code below. It would be worth the time testing different values of
<code>t9</code> and <code>RoCo_hasMinMaxAngles_PARAM</code>  although that complex computation is
only active when using commands other than MoveByAngle.</p>
<pre>            if ((fabs(t9) &gt; t11) &amp;&amp; (direction == 0)) {
                direction = ((((!RoCo_hasMinMaxAngles_PARAM) ||
                               (fabs(t9) &gt;= 180.0))
                              ? t9 : -t9) &gt; 0.0) ? 1 : -1;
            }
</pre>
<h2>Thanks  and Next</h2>
<p>My colleagues Benjamin Monate and Virgile Prevosto found the challenge and set up the files
in the development repository  so that the only thing that was left to do was to identify the
bugs  and started looking at them with me.
Anne Pacalet and David Mentré have provided feedback for transforming this
submission into a blog post. Anne Pacalet also implemented of all the code navigation
features used in this study. The organizers Jochen Quante and Andrew Begel have done
an impressive job of making this challenge interesting  and indeed  challenging.</p>
<p>I have also submitted my explanation of tasks 2 and 3 in the challenge.
However  I thought someone might want
to look at these themselves now that I have shown how easy it is on task 1.
The only difficult part is finding an <code>exp()</code> function.</p>
<p>To be continued...</p>
{% endraw %}
