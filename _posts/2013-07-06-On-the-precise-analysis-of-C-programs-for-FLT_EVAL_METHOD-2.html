---
layout: post
author: Pascal Cuoq
date: 2013-07-06 21:07 +0200
categories: FLT_EVAL_METHOD facetious-colleagues floating-point
format: xhtml
title: "On the precise analysis of C programs for FLT_EVAL_METHOD==2"
summary: 
---
{% raw %}
<p>There has been talk recently amongst my colleagues of Frama-C-wide support for compilation platforms that define <code>FLT_EVAL_METHOD</code> as 2. Remember that this compiler-set value, introduced in C99, means that all floating-point computations in the C program are made with <code>long double</code> precision, even if the type of the expressions they correspond to is <code>float</code> or <code>double</code>. This post is a reminder, to the attention of these colleagues and myself, of pitfalls to be anticipated in this endeavor.</p> 
<p>We are talking of C programs like the one below.</p> 
<pre>#include &lt;stdio.h&gt; 
int r1; 
double ten = 10.0; 
int main(int c, char **v) 
{ 
  r1 = 0.1 == (1.0 / ten); 
  printf(1=%d"  r1); 
} 
</pre> 
<p>With a C99 compilation platform that defines <code>FLT_EVAL_METHOD</code> as 0  this program prints "r1=1"  but with a compilation platform that sets  <code>FLT_EVAL_METHOD</code> to 2  it prints “r1=0”.</p> 
<p>Although we are discussing non-strictly-IEEE 754 compilers  we are assuming IEEE 754-like floating-point: we're not in 1980 any more. 
Also  we are assuming that <code>long double</code> has more precision than <code>double</code>  because the opposite situation would make any discussion specifically about <code>FLT_EVAL_METHOD == 2</code> quite moot. In fact  we are precisely thinking of compilation platforms where <code>float</code> is IEEE 754 single-precision (now called binary32)  <code>double</code> is IEEE 754 double-precision (binary64)  and <code>long double</code> is the 8087 80-bit double-extended format.</p> 
<p>Let us find ourselves a compiler with the right properties :</p> 
<pre>$ gcc -v 
Using built-in specs. 
Target: x86_64-linux-gnu 
… 
gcc version 4.4.3 (Ubuntu 4.4.3-4ubuntu5.1)  
$ gcc -mfpmath=387 -std=c99 t.c &amp;&amp; ./a.out  
r1=0 
</pre> 
<p>Good! (it seems)</p> 
<blockquote><p>The test program sets <code>r1</code> to 0 because the left-hand side <code>0.1</code> of the equality test is the double-precision constant 0.1  whereas the right-hand side is the double-extended precision result of the division of 1 by 10. The two differ because 0.1 cannot be represented exactly in binary floating-point  so the <code>long double</code> representation is closer to the mathematical value and thus different from the <code>double</code> representation. We can make sure this is the right explanation by changing the expression for <code>r1</code> to <code>0.1L == (1.0 / ten)</code>  in which the division is typed as <code>double</code> but computed as <code>long double</code>  then promoted to <code>long double</code> in order to be compared to <code>0.1L</code>  the <code>long double</code> representation of the mathematical constant <code>0.1</code>. This change causes <code>r1</code> to receive the value 1 with our test compiler  whereas the change would make <code>r1</code> receive 0 if the program was compiled with a strict IEEE 754 C compiler.</p> 
</blockquote> 
<h2>Pitfall 1: Constant expressions</h2> 
<p>Let us test the augmented program below:</p> 
<pre>#include &lt;stdio.h&gt; 
int r1  r2; 
double ten = 10.0; 
int main(int c  char **v) 
{ 
  r1 = 0.1 == (1.0 / ten); 
  r2 = 0.1 == (1.0 / 10.0); 
  printf("r1=%d r2=%d"  r1  r2); 
} 
</pre> 
<p>In our first setback  the program prints “r1=0 r2=1”. The assignment to <code>r2</code> has been compiled into a straight constant-to-register move  based on a constant evaluation algorithm that does not obey the same rules that execution does. If we are to write a <strong>precise</strong> static analyzer that corresponds to this GCC-4.4.3  this issue is going to seriously complicate our task. We will have to delineate a notion of “constant expressions” that the analyzer with evaluate with the same rules as GCC's rules for evaluating constant expressions  and then implement GCC's semantics for run-time evaluation of floating-point expressions for non-constant expressions. And our notion of “constant expression” will have to exactly match GCC's notion of “constant expression”  lest our analyzer be unsound.</p> 
<h2>Clarification: What is a “precise” static analyzer?</h2> 
<p>This is as good a time as any to point out that Frama-C's value analysis plug-in  for instance  is already able to analyze programs destined to be compiled with <code>FLT_EVAL_METHOD</code> as 2. By default  the value analysis plug-in assumes IEEE 754 and <code>FLT_EVAL_METHOD == 0</code>:</p> 
<pre>$ frama-c -val t.c 
… 
t.c:9:[kernel] warning: Floating-point constant 0.1 is not represented exactly. 
 Will use 0x1.999999999999ap-4. 
 See documentation for option -warn-decimal-float 
… 
[value] Values at end of function main: 
          r1 ∈ {1} 
          r2 ∈ {1} 
</pre> 
<p>The possibility of <code>FLT_EVAL_METHOD</code> being set to 2 is captured by the option <code>-all-rounding-modes</code>:</p> 
<pre>$ frama-c -val -all-rounding-modes t.c 
… 
t.c:9:[kernel] warning: Floating-point constant 0.1 is not represented exactly. 
 Will use 0x1.999999999999ap-4. 
 See documentation for option -warn-decimal-float 
… 
[value] Values at end of function main: 
          r1 ∈ {0; 1} 
          r2 ∈ {0; 1} 
</pre> 
<p>The sets of values predicted for variables <code>r1</code> and <code>r2</code> at the end of <code>main()</code> each contain the value given by the program as compiled by GCC-4.4.3  but these sets are not precise. If the program then went on to divide <code>r1</code> by <code>r2</code>  Frama-C's value analysis would warn about    a possible division by zero  whereas we know that with our compiler  the division is safe. The warning would be a false positive.</p> 
<p>We are talking here about making a static analyzer with the ability to conclude that <code>r1</code> is 0 and <code>r2</code> is 1 because we told it that we are targeting a compiler that makes it so.</p> 
<blockquote><p>The above example command-lines are for Frama-C's value analysis  but during her PhD  Thi Minh Tuyen Nguyen has shown that the same kind of approach could be applied to source-level Hoare-style verification of floating-point C programs. The relevant articles can be found in the <a href="http://hisseo.saclay.inria.fr/documents.html">results of the Hisseo project</a>.</p> 
</blockquote> 
<h2>To be continued</h2> 
<p>In the next post  we will find more pitfalls  revisit a post by Joseph S. Myers in the GCC mailing list  and conclude that implementing a precise static analyzer for this sort of compilation platform is a lot of work.</p>
{% endraw %}
