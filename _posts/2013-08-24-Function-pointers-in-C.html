---
layout: post
author: Pascal Cuoq
date: 2013-08-24 08:31 +0200
categories: conversions-and-promotions function-pointers link
format: xhtml
title: "Function pointers in C"
summary: 
---
{% raw %}
<p>This post contains a complete list of everything a C program can do with a function pointer, for a rather reasonable definition of “do”. Examples of things not to do with a function pointer are also provided. That list, in contrast, is in no way exhaustive.</p> 
<h2>What a C program can do with a function pointer</h2> 
<h3>Convert it to a different <em>function</em> pointer type</h3> 
<p>A function pointer can be converted to a different function pointer type. The C99 standard's clause 6.3.2.3:8 starts:</p> 
<p>“A pointer to a function of one type may be converted to a pointer to a function of another type and back again; the result shall compare equal to the original pointer.”</p> 
<h3>Call the pointed function <em>with the original type</em></h3> 
<p>Clause 6.3.2.3:8 continues:</p> 
<p>“If a converted pointer is used to call a function whose type is not compatible with the pointed-to type, the behavior is undefined.”</p> 
<p>Alright, so the above title is slightly sensationalistic: the pointed function can be called with a <em>compatible</em> type. After <code>typedef int t;</code>, the types <code>t</code> and <code>int</code> are compatible, and so are <code>t (*)(t)</code> and <code>int (*)(int)</code>, the types of functions taking a <code>t</code> and returning a <code>t</code> and of functions taking an <code>int</code> and returning an <code>int</code>, respectively.</p> 
<h3>There is no third thing a C program can do with a function pointer</h3> 
<p>Seriously. The C99 standard has <code>uintptr_t</code>, a recommended integer type to convert data pointers to, but there is not even an equivalent integer type to store function pointers.</p> 
<h2>What a C program cannot do with a function pointer</h2> 
<h3>Convert it to an ordinary pointer</h3> 
<p>Function pointers should not be converted to <code>char *</code> or <code>void *</code>, both of which are intended for pointers to data (“objects” in the vocabulary of the C standard). Historically, there has been plenty of reasons why pointers to functions and pointers to data might not have the same representation. With 64-bit architectures, the same reasons continue to apply nowadays.</p> 
<h3>Call the pointed function with an incompatible type</h3> 
<p>Even if you know that type <code>float</code> is 32-bit, the same as <code>int</code> on your platform, the following is undefined:</p> 
<pre>void f(int x); 
int main(){ 
  void (*p)(float) = f; 
  (*p)(3); 
} 
</pre> 
<p>The line <code>void (*p)(float) = f;</code>, which defines a variable <code>p</code> of type “pointer to function that takes a float”, and initializes it with the conversion of <code>f</code>, is legal as per 6.3.2.3:8. However, the following statement, <code>(*p)(3);</code> is actually equivalent to <code>(*p)((float)3);</code>, because the type of <code>p</code> is used to decide how to convert the argument prior to the call, and it is undefined because <code>p</code> points to a function that requires an <code>int</code> as argument.</p> 
<p>Even if you know that the types <code>int</code> and <code>long</code> are both 32-bit and virtually indistinguishable on your platform (you may be using an ILP32 or an IL32P64 platform), the types <code>int</code> and <code>long</code> are not compatible. Josh Haberman has written <a href="https://blog.reverberate.org/2013/03/cc-gripe-1-integer-types.html">a nice essay on this precise topic</a>.</p> 
<p>Consider the program:</p> 
<pre>void f(int x); 
int main(){ 
  void (*p)(long) = f; 
  (*p)(3); 
} 
</pre> 
<p>This time the statement is equivalent to <code>(*p)((long)3);</code>  and it is undefined  even if <code>long</code> and <code>int</code> are both 32-bit (substitute <code>long</code> and <code>long long</code> if you have a typical I32LP64 platform).</p> 
<p>Lastly  the example that prompted this post was  in a bit of Open-Source code  the creation of a new execution thread. The example can be simplified into:</p> 
<pre>void apply(void (*f)(void*)  void *arg) 
{ 
  f(arg); 
} 
void fun(int *x){ 
  // work work 
  *x = 1; 
} 
int data; 
int main(){ 
  apply(fun  &amp;data); 
} 
</pre> 
<p>The undefined behavior is not visible: it takes place inside function <code>apply()</code>  which is a standard library function (it was <code>pthread_create()</code> in the original example). But it is there: the function <code>apply()</code> expects a pointer to function that takes a <code>void*</code> and applies it as such. The types <code>int *</code> and <code>void *</code> are not compatible  and neither are the types of functions that take these arguments.</p> 
<p>Note that <code>gcc -Wall</code> warns about the conversion when passing <code>fun</code> to <code>apply()</code>:</p> 
<pre>t.c:11: warning: passing argument 1 of ‘apply’ from incompatible pointer type 
</pre> 
<p>Fixing this warning with a cast to <code>void (*)(void*)</code> is a programmer mistake. The bug indicated by the warning is that there is a risk that <code>fun()</code> will be applied with the wrong type  and this warning is justified here  since <code>fun()</code> will be applied with the wrong type inside function <code>apply()</code>. If we “fix” the program this way:</p> 
<pre>$ tail -3 t.c 
int main(){ 
  apply((void (*)(void*))fun  &amp;data); 
} 
$ gcc -std=c99 -Wall t.c 
$  
</pre> 
<p>The explicit cast to <code>(void (*)(void*)</code> silences the compiler  but the bug is still in the same place  in function <code>apply()</code>.</p> 
<p>Fortunately <code>gcc -std=c99 -Wall</code> is not the only static analyzer we can rely on. Frama-C's value analysis warns where the problem really is  in function <code>apply()</code>  and it warns for both the version with implicit conversion and the version with explicit cast:</p> 
<pre>$ frama-c -val t.c 
… 
[value] computing for function apply &lt;- main. 
        Called from t.c:14. 
t.c:3:[value] warning: Function pointer and pointed function 'fun'  have incompatible types: 
        void (void *) vs. void (int *x). assert(function type matches) 
</pre> 
<p>The correct way to use function <code>apply()</code> without changing it is to make a function with the correct type for it  and to pass that function to <code>apply()</code>:</p> 
<pre>void stub(void *x){ 
  fun(x);   
} 
… 
  apply(stub  &amp;data); 
</pre> 
<p>Note that in the above  <code>x</code> is implicitly converted when passed to function <code>fun()</code>  the same way that <code>&amp;data</code> is implicitly converted to <code>void*</code> when passed to <code>apply()</code>.</p> 
<h2>Conclusion</h2> 
<p>There is almost nothing you can do in C with a function pointer. The feature is still very useful and instills a bit of genericity in an otherwise decidedly low-level language.</p> 
<p>Function pointers are not often used in the standard library  considering: <code>qsort()</code> is  with <code>pthread_create()</code>  another of the few functions that requires a function pointer. Like it  it is often misused: it has its own <a href="http://c-faq.com/lib/qsort2.html">entry in the C FAQ</a>.</p> 
<p>Jens Gustedt provided advice in the writing of this post.</p>
{% endraw %}
