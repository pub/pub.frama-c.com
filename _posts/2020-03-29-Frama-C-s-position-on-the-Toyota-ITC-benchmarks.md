---
layout: post
author: André Maroneze
date: 2020-03-29 16:55 +0200
categories: benchmarks rant Eva
image:
title: "Frama-C's position on the Toyota ITC benchmarks"
published: false
---

The Frama-C team has recently been asked to comment on the results of a
benchmark published in CAV'16, available
[here](http://fsl.cs.illinois.edu/FSL/papers/2016/guth-hathhorn-saxena-rosu-2016-cav/guth-hathhorn-saxena-rosu-2016-cav-public.pdf).
We don't have the time nor the will to comment on every paper that uses Frama-C
on a benchmark, but we believe this benchmark allows us to present some of the
design decisions around Frama-C, as well as detailing why making a benchmark is
hard, and especially why it is hard to compare tools using such benchmarks, and
why one should refrain from drawing quick conclusions from simple comparative
tables. Our conclusion is that the benchmark contains some serious drawbacks
that users should be aware of; besides fixes and changes, we added several
(over 150) comments to the files, in order to explain our patches and the
shortcomings of some tests. In our experience, other test suites such as
[Juliet](https://samate.nist.gov/SRD/testsuite.php) offer a better
cost-benefit ratio for their users.

## Comments on the CAV'16 paper, and clarifying tool usage by examples

The aforementioned CAV'16 paper is just one of several papers which evaluate
Frama-C and other code analysis tools. While this blog post is centered around
the Toyota ITC benchmark suite and its limitations, a few points concerning the
CAV'16 paper are worth mentioning:

- Frama-C's options concerning the reporting of non-undefined behaviors as
  errors were not used; as will be explained later, several of the defects
  present in the benchmark correspond to what Frama-C considers "optional"
  errors: behaviors that *are* authorized by the C standard, but often
  dreaded by C programmers, due to their non-intuitive semantics, or due to
  the fact that they tend to hide problems in the code;
- Frama-C, and the Eva plug-in in particular (which is the one concerned by
  the benchmark), have several options to improve the precision of the analysis,
  at a negligible cost in terms of analysis time; Eva is optimized for large,
  industrial code bases, which may require analyses taking several minutes or
  even hours; for synthetic benchmarks with a few hundred lines of code, the
  increase in analysis time is negligible (for instance, running *all* of the
  tests in the Toyota ITC benchmark suite with very improved precision takes
  15 seconds on a modern laptop), and such options greatly reduce the number
  of false positives;
- Some unrelated warnings were considered as false positives, despite their
  descriptive messages; for instance, the usage of non-exact floating-point
  constants such as `2.14748365e+09F` can lead to approximations and is
  flagged by Frama-C; while most users may not care about this information,
  it is important for safety-critical systems, hence why it is displayed by
  default.

We believe these points have negatively impacted the results of Frama-C; but
we also take into account the fact that proper usage of the tool requires some
effort, both from tool makers and from tool users. To help users, we have
greatly improved the default parametrization options for Eva; Frama-C now
offers more possibilities to filter warnings; and there are more practical
usage examples of Eva applied to case studies, such as in our
[open-source-case-studies](https://github.com/Frama-C)
Github repository. These efforts should hopefully contribute towards better
usage of Frama-C in the future.

With all of that said, we now focus on the main subject of this blog post,
which are the Toyota ITC Benchmarks used in the paper.

## The Toyota ITC Benchmarks

The [Toyota ITC Benchmarks](https://github.com/Toyota-ITC-SSD/Software-Analysis-Benchmark)
(TITCB), mentioned in the CAV'16 paper, are a synthetic benchmark suite
consisting of 634 pairs of test cases for C (plus 4 pairs for C++),
where each pair contains one test with defects and another without them.
We will more precisely describe what the benchmark considers as "defects"
later.

### History of Toyota ITC Benchmarks

One of the authors of the benchmark suite (more precisely, of a 2014 paper
using the benchmark suite to evaluate static analyzers), Shinichi Shiraishi,
contacted the University of Utah professor John Regehr about making the
benchmarks publicly available, as described in
[John Regehr's blog](https://blog.regehr.org/archives/1217).
John put them in a
[repository on Github](https://github.com/regehr/itc-benchmarks) and, for
some time, this was the "official" version of the benchmarks. Some fixes
were applied, and later a pull request was made, but the repository was not
modified since then.

Later, Toyota InfoTechnology Center's researchers published the benchmark under
[Toyota ITC's own Github](https://github.com/Toyota-ITC-SSD/Software-Analysis-Benchmark).
They incorporated the patches applied to John Regehr's repository. The only
differences between both repositories are some header files and a few extr
C++ tests, both of which are not used in our evaluation.
Our version is based on John Regehr's.

Another version of the benchmark is available at one of the CAV'16 paper
authors' Github. It includes more fixes (which are proposed as a pull request
towards John Regehr's repository). Other versions exist on Github, with
different patches, applied by different authors.

### Types of defects in the Toyota ITC Benchmarks

Defining and classifying *defects*, *bugs*, or any kind of pattern that one
intends to avoid, is not easy. [CWEs](https://cwe.mitre.org)
(Common Weakness Enumerations) have been adopted by the industry, but they
suffer from several drawbacks: lack of formal definition, lack of orthogonality,
structural complexity, etc. The [Bugs Framework](https://samate.nist.gov/BF/)
proposed by NIST aims to provide a more structured classification with
many advantages, but it is still not widely adopted. Several other frameworks
exist, but in practice, many software practitioners do not
know them, and end up defining a custom classification according to their
needs. This is what TITCB's researchers did: they provided
a [document](https://github.com/Toyota-ITC-SSD/Software-Analysis-Benchmark/blob/master/docs/spec.pdf)
which specifies the classes of defects they want tools to detect and
report.

Some of the defects are common to most bug frameworks: buffer overflows,
deadlocks, divisions by zero; others are more rarely seen, such as
*free NULL pointer* (which is a well-defined behavior in the C standard).
Some of the defects are related to syntactic aspects, while others
involve the C semantics.

## Guessing intent: why semantic properties are important

One of the main issues with the definition of defects in TITCB is that,
due to the lack of formal definition and the non-reliance on the C standard,
several of them end up addressing the *intent* of a piece of code.
For instance, one of the categories is called *unintentional endless loop*,
but it is never defined what *unintentional* means. Is a `while (1)` loop
in a reactive system unintentional? Probably not; but what if the loop is
written as `do {} while (flag)`, and `flag` is not modified inside the loop?
Is it unintentional? Whatt if `flag` is a volatile supposed to be modified from
another process?

The tests provide several *examples* of what these classifications mean, but
here we start getting into problems: how can one determine if a test is
incorrect, or if it accurately detects what it intends to?
In some cases, the presence of an error is obvious, but in others, not so much.
Let us provide a few concrete examples from the benchmark suite.

### Example of a clear mistake: `deletion_of_data_structure_sentinel_003`

Let us consider test `deletion_of_data_structure_sentinel_003`:

```
/*
* Types of defects: Deletion of a data structure sentinel
* Complexity: Not leaving a place for '\0' which is the
* terminator for the string using memcpy function using a constant.
*/

void deletion_of_data_structure_sentinel_003()
{
    int i;
    char ptr[16];
    char str[16];
    for(i=0;i<15;i++) {
        ptr[i]='a';
    }
    ptr[i]='\0';
    memcpy(str,ptr,16); /*ERROR:Deletion of a data structure sentinel*/
}
```

In this test, we initialize an array of 16 characters by writing 'a'
everywhere but in the last position, where we put a `\0` instead.
A call to `memcpy` then copies all 16 characters (_including_ the `\0`!).
From the test comment, and the category description in the specification,
we are confident the test is wrong: the line marked as "ERROR" actually
contains none. Maybe the `memcpy` was intended to only copy 15 characters;
maybe the `str` array should only have had 15 elements; that, we cannot know
for sure, but at least the surrounding elements (comment, category description,
etc.) provide enough information for us to realize that there is an
unintentional mistake, and not that our tool failed to detect the issue.

### Example of an unclear mistake: `memory_allocation_failure_004`

In the previous test, the error was obvious since the code was short, the
comments clear, and there were no other unintended errors; in other tests,
finding out whether the test is _intentionally_ incorrect is harder.
Our example here is one of the tests from the file
`memory_allocation_failure.c`. For starters, there are a couple of issues
with this file:

- Despite the name, not all of the tests check the return value of `malloc`
  or `calloc`. Some tests do check, but others seem to assume a
  deterministic behavior, as if they know in advance which allocations are
  supposed to succeed and which should fail, based solely on the total
  memory size;
- The tests assume a limited (but entirely deterministic and predictable)
  memory layout. Supposedly, this limite is 2 GB, given the comment in the
  beginning of the file: "MAX Heap memory in Windows 7 32 bit 2GB";
- Several of the test cases invoke undefined behavior via signed overflows
  (mostly related to a constant defined in the file);
  it is not always clear whether the
  test intended such an overflow to take place (to obtain a negative value),
  or if it was accidental.

As an example, here's test case number 16, slightly rewritten for improved
readability:

```
int *gptr1;
int *gptr2;

#define MAX_VAL_4 1073741824
int aux_001(int flag) {
    int ret = 0;
    if (flag == 0)
        ret = MAX_VAL_4;
    else
        ret = 5;
    return ret;
}

void aux_002(int flag) {
    if (flag == 0) {
        ;
    } else {
        gptr1 = (int*)malloc(aux_001(0)*sizeof(int));
        gptr2 = (int*)malloc(aux_001(0)*sizeof(int));/*ERROR:Memory allocation failure */
    }
}

void memory_allocation_failure_016 (){
    int *ptr1 = (int*)malloc(aux_001(0)*sizeof(int));
    int *ptr2 = (int*)malloc(aux_001(0)*sizeof(int));
    *(ptr1+1) = 10;
    aux_002(0);
    free(gptr1);
    free(gptr2);
    free(ptr1);
    free(ptr2);
}
```

This is the most complex test in the file, and convoluted on purpose;
the idea is that function `aux_001` can return either a small or a very large
value; each time we call `malloc`, we may end up consuming 1 GB of memory.
From the test, we can infer that:

1. The first call to `malloc`, with `ptr1`, is supposed to always succeed:
its return value is never checked, and `ptr1` is dereferenced 2 lines below;
2. The second call to malloc is supposed to succeed (no "ERROR" comment);
3. The call to `aux_002` should trigger two more calls to `malloc`, the last
of which should fail and trigger the expected error.

The first, obvious error in this test is that the code containing the error
message is actually unreachable; the call to `aux_002(0)` was probably intended
as `aux_002(1)`.

There is nothing immediately obvious about *why* the fourth call, and only
it, should fail. The comment in the beginning of the file mentions
"MAX heap memory 2GB". Should we use this as a limit? Then the second call
to `malloc` should have failed; why is the error comment present only in
the fourth call? Should the first two allocations happen inside an unreachable
branch of the code, just to complicate the analysis?

It is impossible to know for sure; there is at least one mistake in the code,
and several implicit assumptions about memory layout, finiteness, and other
aspects that are not even handled by the C standard. Fixing such tests
requires a large amount of effort and guesswork. This defeats the purpose
of a benchmark suite, which should help compare tools easily, with as little
ambiguity as possible.

### Inconsistent rule enforcement across tests

Yet another issue with the TITCB suite is that
*enforcement of rules is inconsistent across tests*.
If we look at the tests in `data_lost.c`, whose defect sub-type is
*Integer precision lost because of cast*, we see several examples where there
is no undefined behavior (UB) per the C standard, but there are common examples of
[Arithmetic or Conversion Fault](https://samate.nist.gov/BF/Classes/ARC.html)
caused by converting integer values into smaller types which cannot represent
them. In Frama-C, such cases can be handled via the following kernel options:

- `-warn-signed-overflow` (enabled by default, since it is an UB)
- `-warn-unsigned-overflow`
- `-warn-signed-downcast`
- `-warn-unsigned-downcast`

Such options are detailed in the
[Frama-C user manual](http://frama-c.com/download/user-manual-20.0-Calcium.pdf)
with a few examples. Very broadly, if you are concerned with this kind of
"integer loss of precision" issues, you can just enable these options.

However, when enabling such options on the TITCB suite, we quickly notice
several issues happening in files other than `data_lost.c`: for instance,
in `memory_allocation_failure.c`, there are global initializers such as
`MAX_VAL*2`, in which `MAX_VAL` is defined as `UINT_MAX`; therefore the
initializers themselves perform arithmetic overflow. There are also several
examples in which a function of type `int` returns `UINT_MAX`, again
performing such overflows.

Similar situations happen with uninitialized variables (in a few test cases,
there are accesses which should not be marked as errors, while in others,
they should be, but are not), and with testing the result of `malloc`/`calloc`:
some tests simply forget to do it, while others require it.

Overall, the test suite contains several accidental cases of violation of
the rules it wants enforced, and a few cases in which it is not possible
to actually enforce them without changing the test in a non-trivial way.

### Example of catastrophic mistakes: `pow_related_errors`

We initially did not include the file `pow_related_errors.c` (containing
errors related to the `pow()` function from the C mathematical library,
which computes exponentiation using floating-point numbers), mainly
because the errors in this file are not related to undefined behaviors.
Instead, the tests seek to address corner cases in which one of the three
following situations may happen: "overflow", "underflow", and "losing
precision". The benchmark suite specification contains a large table
with the different corner cases, and the tests seem inspired from it.
However, the number of errors is high, with several different kinds of
errors.

First of all, the tests contain several numerical constants which supposedly
represent large (or small) numbers, most of them expressed using the
following notation: `10^3700`, `-10^36`, `-100^10`, etc. That is,
*using the bitwise XOR operator `^`*, which is **not** an exponentiation
operator in C! This means that the actual numbers they produce are 3170, -46,
110, etc.

Secondly, the numbers themselves represent magnitudes which are way beyond
the scope of double-precision floating-points. If they really wanted to
express `1e3700`, they would have obtained an infinity, and the
test would be moot: it's not the `pow` function that is losing precision,
it's the constant itself! We wonder if such numbers were decided by trial
and error, as a consequence of the previous error in the notation. For
instance, when `10^37` failed to result in an error, `10^370` was tried,
then `10^3700`...

Then, there is a serious issue with *very small numbers*: even when the
comments describe tests as involving e.g. a *very small negative exponent*,
the numbers used never contain the minus sign next to the exponent;
which means the numbers used are always much larger than 1. We do not have
any hypotheses concerning these mistakes.

Speaking of copy-and-pasting errors, the comments in some tests are
wrong, copy-pasted from previous ones and not updated. This already happened
in other files, but here, due to the staggering accumulation of errors,
comments are much more important to reverse engineer what the tests actually
intended. For instance, tests 024 and 025 both contain the same comment,
"Very small negative base and very small odd negative exponent", but the first
declares the following values:

    double base=-0.0004;
    double exponent=-10^35000;

While the second declares:

    double base=10^3300;
    double exponent=0.000004;

Note the exceptionally large values using the bitwise XOR notation.

Another puzzling aspect of these tests is that they seem to not consider
NaNs (*not a number*) as a problem; they do not even acknowledge their existence (none of the
comments mention them, and the table with the "corner cases" mentions
"complex number (not real number)" in the cases where the `pow` function
actually produces NaNs. Confusingly, several tests of the `02_wo_Defects`
directory, which is supposed to contain tests that do *not* contain errors,
actually produce NaNs, such as test 009, which defines:

    double exponent=-2.4;
    double base=-34.50;

`pow(-34.50, -2.4)` results in NaN.
We had to disable several tests in this file, since we
activated a Frama-C option to emit alarms on both the production of
infinities and NaNs; this benchmark is the first situation we encountered
where infinities are considered as errors, but NaNs are not.

Finally, some comments are mathematically wrong: in test 029, for
instance, its comment states:

> Very Small negative base and very small exponent together can lead to underflow

Which is incorrect: such situations either produce NaNs (depending on the
parity of the exponent) or converge towards 1.0, and can never produce
underflows.

Overall, we spent hours trying to fix the tests in this file, and in the end
we did not respect what all of the comments intended; we adjusted the constants
to produce errors where they should happen, and to prevent them where they
should not happen (disabling tests which were obviously producing NaNs).
However, given the amount of errors, concerning both the mathematical aspect of
the exponentiation function, as well as code-level issues (wrong operator, code not
matching comments, copy-pasting mistakes), we believe these files were not
thoroughly tested.

Note that, due to the fact that users put too much trust in their benchmarks,
not fixing such errors leads to problems of unintentional error propagation
later. For intance, other users of the TITCB suite, such as
[Arusoaie et al.](https://ieeexplore.ieee.org/iel7/8528958/8531246/08531281.pdf),
did include `pow_related_errors.c` in their tests and statistics, but they
did not notice a single mistake in these files.

### Fixing the benchmarks, but for whom?

The TITCB suite, like most synthetic benchmarks for static analysis, has been
developed to solve the issues which were most concerning to the developers who
made them. In particular, they were not seeking strict adherence to the C
standard, but trying to illustrate typical examples of bugs they most likely
have found at least once in their code base (or bugs which they would certainly
*not* want to see!). it is futile to claim that strict adherence to the C
standard will prove the solution to all issues. For instance, the C standard
does not consider memory leaks as undefined behavior, yet this is a real
issue that many developers face. Therefore, proposing to fix the benchmarks
using only the C standard as reference will simply render most of the
tests useless: several have no relation to undefined behaviors.

On the other hand, trying to provide a benchmark suite where each test
completely describes what it assumes and what it expects, without using some
formal language, is likely to end up with ambiguities or, in the best case,
with an inordinate amount of text.

Using ACSL, we could add assertions to several tests, to provide a less
ambiguous situation of what is expected at each test; however, this would
not be enough for some tests (for instance, ACSL does not allow specifying
"the total available memory for malloc is X bytes before it fails"), and
it would not be immediately usable by other tools. Encoding properties in
a format such as
[SARIF](https://www.oasis-open.org/committees/tc_home.php?wg_abbrev=sarif)
could be useful in the future. For now, what we decided to do was to
make the benchmark suite *compatible with Frama-C*:

- For the test files which are in the scope of Frama-C/Eva, we ran the
  analysis, identified all unintended undefined behaviors, and fixed the
  code, or commented them out;
- In some tests, we fixed the locations of error messages, which were
  marked a few statements away from their actual cause;
- More importantly, whenever we applied some changes or fixes, we
  added an extensive set of comments on each deviation. These comments
  are mainly of three types: justification of why Frama-C/Eva cannot report
  the error; what is the option required to make Frama-C report it;
  and why the test seems to be erroneous.

For each class of defects that Frama-C/Eva is currently unable to report,
we added a comment on the `main.c` file explaining why they were disabled.
The modified benchmark suite, as well as the Frama-C script to run it,
are available at
our [open-source-case-studies](https://github.com/Frama-C/open-source-case-studies/tree/master/itc-benchmarks)
Github repository, under directory `itc-benchmarks`.

We hope this will help educate users, with practical examples,
about the kinds of properties that Frama-C/Eva is able to report; some
of the options that are needed to match the semantic intention of errors
which are not undefined behaviors; and several pitfalls related to the
complexity of the C semantics, which make it hard to develop good benchmark
suites.

While we are thankful the TITCB authors allowed the publication of their
benchmark, we believe other [test suites](https://samate.nist.gov/SARD/testsuite.php),
such as SAMATE's Juliet for C/C++, offer a more thorough and
standards-compliant alternative for comparing static analysis tools.

## Important semantic issues of the Toyota ITC benchmark

This section is a more direct presentation of several semantic shortcomings of
the TITCB suite, as well as a summary of the results that the current version
of Frama-C/Eva (20.0 - Calcium, at the time of this writing) is able to provide
on our modified version of the test suite. In the future, you can refer to the
[open-source-case-studies](https://github.com/Frama-C/open-source-case-studies/tree/master/itc-benchmarks)
Github repository for up-to-date test results.

## The Toyota ITC Benchmark suite, adapted and annotated

Note: in this post, we do not consider the C++ tests.
For completeness' sake: they comprise a single test file,
with 4 tests related to error handling via try/catch.

The TITCB suite contains 50 C files, totaling 634 tests. Each test file
is more or less self-explanatory, and extended descriptions are available
in comments in the header of each file and in the
[test suite specification](https://github.com/Toyota-ITC-SSD/Software-Analysis-Benchmark/blob/master/docs/spec.pdf).
Instead of using the original classification, we present in the table below
our own categories for each test file, along with extra data, that we explain
below.

| Test name                             | Category               | Enabled? | #tests | #adjusted | #disabled | #adapted | #different-UB | #alarm-but-noUB | #noUB |
|---------------------------------------|------------------------|----------|--------|-----------|-----------|----------|---------------|-----------------|-------|
| `bit_shift`                           | Undefined behavior     | yes      | 17     |           |           |          |               |                 |       |
| `cmp_funcaddr`                        | Syntactic rule         | no       | 2      |           |           |          |               |                 |       |
| `conflicting_cond`                    | Syntactic rule         | no       | 10     |           |           |          |               |                 |       |
| `data_lost`                           | UB / Arithmetic        | yes      | 19     |           |           |          | 2             | 16              | 1     |
| `data_overflow`                       | UB / Arithmetic        | yes      | 25     | 3         |           |          |               | 8               | 1     |
| `data_underflow`                      | UB / Arithmetic        | yes      | 12     | 1         |           | 1        |               | 2               | 2     |
| `dead_code`                           | Syntactic rule         | no       | 13     |           |           |          |               |                 |       |
| `dead_lock`                           | Concurrency            | no       | 5      |           |           |          |               |                 |       |
| `deletion_of_data_structure_sentinel` | Undefined behavior     | no       | 3      |           |           |          |               |                 |       |
| `double_free`                         | Undefined behavior     | yes      | 12     | 1         |           |          |               |                 |       |
| `double_lock`                         | Concurrency            | no       | 4      |           |           |          |               |                 |       |
| `double_release`                      | Concurrency            | no       | 6      |           |           |          |               |                 |       |
| `dynamic_buffer_overrun`              | Undefined behavior     | yes      | 32     |           |           |          |               |                 |       |
| `dynamic_buffer_underrun`             | Undefined behavior     | yes      | 39     | 3         |           | 1        |               |                 |       |
| `endless_loop`                        | Syntactic rule         | yes      | 9      |           |           |          |               |                 |       |
| `free_nondynamic_allocated_memory`    | Undefined behavior     | yes      | 16     | 2         |           |          |               |                 |       |
| `free_null_pointer`                   | Syntactic rule         | no       | 14     |           |           |          |               |                 |       |
| `function_return_value_unchecked`     | Syntactic rule         | no       | 16     |           |           |          |               |                 |       |
| `improper_termination_of_block`       | Syntactic rule         | no       | 4      |           |           |          |               |                 |       |
| `insign_code`                         | Syntactic rule         | no       | 1      |           |           |          |               |                 |       |
| `invalid_extern`                      | Syntactic rule         | no       | 6      |           |           |          |               |                 |       |
| `invalid_memory_access`               | Undefined behavior     | yes      | 17     |           | 1         |          |               |                 |       |
| `littlemem_st`                        | Undefined behavior     | yes      | 11     |           |           |          |               |                 |       |
| `livelock`                            | Concurrency            | no       | 1      |           |           |          |               |                 |       |
| `lock_never_unlock`                   | Concurrency            | no       | 9      |           |           |          |               |                 |       |
| `memory_allocation_failure`           | Undefined behavior     | yes      | 16     |           |           | 3        |               | 11              | 6     |
| `memory_leak`                         | Non-UB memory          | no       | 18     |           |           |          |               |                 |       |
| `not_return`                          | Syntactic rule         | yes      | 4      |           |           |          |               |                 |       |
| `null_pointer`                        | Undefined behavior     | yes      | 17     |           |           |          |               |                 |       |
| `overrun_st`                          | Undefined behavior     | yes      | 54     | 1         |           |          |               |                 |       |
| `ow_memcpy`                           | Undefined behavior     | no       | 2      |           |           |          |               |                 |       |
| `pow_related_errors`                  | Arithmetic, non-UB     | yes      | 29     |           |           | 21       |               | 27              | 2     |
| `ptr_subtraction`                     | Undefined behavior     | yes      | 2      |           |           | 1        |               |                 | 1     |
| `race_condition`                      | Concurrency            | no       | 8      |           |           |          |               |                 |       |
| `redundant_cond`                      | Syntactic rule         | no       | 14     |           |           |          |               |                 |       |
| `return_local`                        | Undefined behavior     | yes      | 2      |           |           |          |               |                 |       |
| `sign_conv`                           | UB / Arithmetic        | yes      | 19     |           |           |          |               | 19              |       |
| `sleep_lock`                          | Concurrency            | no       | 3      |           |           |          |               |                 |       |
| `st_cross_thread_access`              | Concurrency            | no       | 6      |           |           |          |               |                 |       |
| `st_overflow`                         | Architecture-dependent | no       | 7      |           |           |          |               |                 |       |
| `st_underrun`                         | Undefined behavior     | yes      | 7      | 2         | 1         |          |               |                 |       |
| `underrun_st`                         | Undefined behavior     | yes      | 13     |           |           |          |               |                 |       |
| `uninit_memory_access`                | Undefined behavior     | yes      | 15     | 2         |           | 2        |               |                 | 2     |
| `uninit_pointer`                      | Undefined behavior     | yes      | 16     | 2         |           | 1        |               |                 | 1     |
| `uninit_var`                          | Undefined behavior     | yes      | 15     | 2         |           |          |               |                 |       |
| `unlock_without_lock`                 | Concurrency            | no       | 8      |           |           |          |               |                 |       |
| `unused_var`                          | Syntactic rule         | no       | 7      |           |           |          |               |                 |       |
| `wrong_arguments_function_pointer`    | Syntactic rule         | no       | 18     |           |           |          |               |                 |       |
| `zero_division`                       | Undefined behavior     | yes      | 16     |           |           |          |               | 1               |       |
|---------------------------------------|------------------------|----------|--------|-----------|-----------|----------|---------------|-----------------|-------|
|                                       | Total                  | 50       | 634    | 19        | 2         | 30       | 2             | 84              | 16    |
|                                       | Enabled                | 25       | 434    |           |           |          |               |                 |       |
|                                       |                        | 50%      | 68%    |           |           |          |               |                 |       |
|                                       | Handled by Frama-C/Eva |          | 416    | 66%       |           |          |               |                 |       |

The *Enabled?* column lists whether the defects in the file are considered as
handled by Frama-C/Eva. Then we list, in order, annotations added to test cases
indicating the nature of the changes we performed:

- **#tests**: total number of test cases in the file
- **#adjusted**: test cases with minor adjustments to the error line number
- **#disabled**: disabled tests (namely, due to recursive calls, currently
  unsupported by Eva)
- **#adapted**: tests with more significant changes: either multiple errors,
  or adaptations to produce intended errors
- **#different-UB**: the test case actually contains an UB which is not the
  one intended in the test (namely, float-to-integer overflow instead of
  *integer precision loss due to overflow*)
- **#alarm-but-noUB**: the test contains something that is not UB, but Frama-C
  has options to trigger it as an alarm (e.g. unsigned overflows)
- **#noUB**: the test does not contain UB and there are currently no options
  in Frama-C to report the wanted property as an alarm

Note that, in a few test cases, several annotations are possible; for instance,
some *noUB* tests also have *adapted* annotations, indicating the original test
did not contain undefined behavior but a similar statement was added to trigger
an error.

Besides (and in complement of) these annotations, we added over 150 comments
among the test files, explaining the context related to several issues in the
benchmark, such as unintended UBs, why a given test is not an UB, how Frama-C
deals with it, etc.

### Defect categories handled by Frama-C/Eva

We can see that, among the 50 files, Frama-C/Eva handles 25 of them.
The most numerous classes are included, which makes Frama-C/Eva able to
handle about 66% of the total test cases.

The *Categories* in the table roughly correspond to the following:

- **Undefined behavior**: the defects in this file correspond to actual
  undefined behaviors, and are handled by Frama-C;
- **Syntactic rule**: these tests are concerned with syntactic aspects:
  either typing errors that can be detect by parsers/linkers, such as
  functions missing `return` statements, or quality-related code aspects
  such as redundant conditions, dead code, unused variables, etc.
  Frama-C does report about a few of them, but not necessarily under the
  form of alarms (e.g. dead code is reported in number of statements on
  the command line and as red lines in the GUI, but reporting one alarm
  per line requires some scripting);
- **UB / Arithmetic**: these defects are sometimes related to undefined
  behavior (such as signed overflow), but often related to arithmetic errors
  such as loss of precision; some Frama-C options allow reporting them;
- **Concurrency**: these defects are related to concurrency issues
  (deadlock, livelock, etc.) and not currently analyzed by Eva.
  The [Mthread](http://frama-c.com/mthread.html) plug-in can identify some
  of these issues.

The categories above comprise almost all files. The few remaining ones have
each a category of their own, which we describe below:

- **Non-UB memory** (`memory_leak`): memory leaks do not constitute undefined
  behavior, but they are a common issue for C developers. Frama-C does not
  currently track memory leaks.
- **Arithmetic, non-UB** (`pow_related_errors`): these are errors related to
  usage of the floating-point exponentiation function
  (`pow`, defined in `math.h`). The TITCB suite specification contains a table
  showing which kinds of defects are sought. Frama-C/Eva does have an ACSL
  specification for `pow` in its standard library which detects non-finite
  values and thus corresponds to some of the cases in the table, but not all;
  for instance, when the inputs are small and the result is zero, no warning
  is produced. We have therefore adapted the specification by adding an extra
  precondition, so that this situation is detected.
- **Architecture-dependent** (`st_overflow`): despite the file name, these
  tests are concerned with a specific hardware configuration, where *accessing*
  memory in the stack beyond a fixed, pre-defined stack size (15360 bytes)
  leads to an error. Note that this is not at all dealt with in the C standard;
  also, the error does not happen at memory allocation (e.g. when a function
  containing too many local parameters is called), but only when a given
  memory location is accessed. A static analysis tool able to handle this
  kind of defect must have a precise model of stack allocation by the compiler.
  For instance, does it assume register spills (saving values in the stack to
  perform operations) are possible? These are out of the scope of the C
  language, which formally does not even know what *heap* and *stack* are.

Note that, even for the files placed under the *Undefined behavior* category,
not all errors are actually UBs. For instance, in `memory_allocation_failure.c`,
most of the tests are actually related to exceeding a fixed heap memory size,
which is not a semantic property (the C abstract machine not define heap, nor
finite memory). Instead, we *adapted* those tests such that they will produce
unsigned overflows when allocating the memory, and we track those errors
instead. It does not correspond to the original intent of the tests, but we
believe it is better to use those tests somehow than to disable the category.

An ODS (OpenDocument Spreadsheet) file containing the result table, as well as
some scripts to compute the values in the table, are available in our
[open-source-case-studies](https://github.com/Frama-C/open-source-case-studies/tree/master/itc-benchmarks)
Github repository.

## Conclusion

The Toyota ITC Benchmark suite offers a few hundred test cases for C which show
some kinds of defects that the authors believe static analyzers should be able
to identify. Unfortunately, many of these tests do not correspond to
well-defined semantic properties; some are ambiguously defined, requiring
tools to guess the intent of the code in some cases; others contain mistakes
of several kinds, a few of them obvious, others more subtle.

Overall, among the 634 test cases, 416 (66%) are handled by Frama-C/Eva,
after several fixes and adjustments to avoid unintended errors and,
in a few cases, to actually produce errors that are in the scope
of those handled by Frama-C. We strived to adapt as many tests as possible,
to maximize the utility of the benchmark.

However, due to the considerable effort required to understand, fix, adapt and
acknowledge the semantic differences between the tests and the C standard
(over 150 comments), the overall experience with this benchmark was negative.
For each accidental error or undefined behavior, it was necessary to
(1) identify what the test actually expects, (2) check whether the required
property is semantically sound and not based on arbitrary heuristics, and
(3) identify how to patch the test, if needed.
In cases such as `pow_related_errors`, the situation is dire: almost every
test contains a mistake, either syntactic, semantic, or mathematical.

Our previous experience with synthetic benchmarks (Juliet 1.2 and 1.3) had
a more positive result: Frama-C was able to identify some unintended defects
on the tests, but it was proportionally much smaller; and, at the same
time, the benchmark gave indications of precision improvements that were used
to enhance Frama-C/Eva. The TITCB suite, on the other hand, required much
more close inspection and fixing of test cases, while not providing any novel
ideas for improvements to Frama-C/Eva.

We hope this effort will help future readers and users of the TITCB suite,
even if they do not agree with our adaptations concerning the errors.
Knowing the weaknesses of the suite should still be helpful for future
reference. We intend to further continue some of this work to contribute
towards better benchmarks for the C code analysis community.
