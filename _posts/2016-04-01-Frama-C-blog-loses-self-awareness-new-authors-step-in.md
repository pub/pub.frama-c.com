---
layout: post
author: André Maroneze
date: 2016-04-01 13:37 +0200
categories:
image:
title: "Frama-C blog loses self-awareness, new authors step in"
---

The Frama-C blog is back, with an extended set of writers and a different focus: small pieces of (informal) documentation and useful tips for Frama-C users.

During its self-awareness period, the Frama-C blog realized that silence is a
valid option, sometimes better than the [alternatives](https://blogs.microsoft.com/blog/2016/03/25/learning-tays-introduction/).

Still, we thought better to remove its self-awareness and regain control of the
blog, to post useful information for Frama-C users, ranging from beginners to
advanced plug-in developers.

The name of the blog is still relevant: we will keep talking about Frama-C
news and ideas, but with a slightly different focus, dedicating some posts to
usage tips, new features, and general information that we judge useful for the
Frama-C community.

We hope to keep the blog useful and informative. Feel free to post your comments
and questions, either here or on the
[frama-c-discuss](mailto:frama-c-discuss@lists.gforge.inria.fr) list.
