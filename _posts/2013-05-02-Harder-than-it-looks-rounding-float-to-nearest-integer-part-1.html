---
layout: post
author: Pascal Cuoq
date: 2013-05-02 17:14 +0200
categories: floating-point
format: xhtml
title: "Harder than it looks: rounding float to nearest integer, part 1"
summary: 
---
{% raw %}
<p>This post is the first in a series on the difficult task of rounding a floating-point number to an integer. Laugh not! 
The easiest-looking questions can hide unforeseen difficulties, and the most widely accepted solutions can be wrong.</p> 
<h2>Problem</h2> 
<p>Consider the task of rounding a <code>float</code> to the nearest integer. The answer is expected as a <code>float</code>, same as the input. In other words, we are looking at the work done by standard C99 function <code>nearbyintf()</code> when the rounding mode is the default round-to-nearest.</p> 
<p>For the sake of simplicity, in this series of posts, we assume that the argument is positive and we allow the function to round any which way if the float argument is exactly in-between two integers. In other words, we are looking at the ACSL specification below.</p> 
<pre>/*@ requires 0 ≤ f ≤ FLT_MAX ; 
  ensures -0.5 ≤ \esult - f ≤ 0.5 ; 
  ensures \exists integer n; \esult == n; 
*/ 
float myround(float f); 
</pre> 
<p>In the second <code>ensures</code> clause, <code>integer</code> is an ACSL type (think of it as a super-long <code>long long</code>). The formula <code>\exists integer n; \esult == n</code> simply means that <code>\esult</code>, the <code>float</code> returned by function <code>myround()</code>, is a mathematical integer.</p> 
<h2>Via truncation</h2> 
<p>A first idea is to convert the argument <code>f</code> to <code>unsigned int</code>, and then again to <code>float</code>, since that's the expected type for the result:</p> 
<pre>float myround(float f) 
{ 
  return (float) (unsigned int) f; 
} 
</pre> 
<h3>Obvious overflow issue</h3> 
<p>One does not need Frama-C's value analysis to spot the very first issue, an overflow for large <code>float</code> arguments, but it's there, so we might as well use it:</p> 
<pre>$ frama-c -pp-annot  -val r.c -lib-entry -main myround 
... 
r.c:9:[kernel] warning: overflow in conversion of f ([-0. .. 3.40282346639e+38]) 
   from floating-point to integer. assert -1 &lt; f &lt; 4294967296; 
</pre> 
<p>This overflow can be fixed by testing for large arguments. Large floats are all integers, so the function can simply return <code>f</code> in this case.</p> 
<pre>float myround(float f) 
{ 
  if (f &gt;= UINT_MAX) return f; 
  return (float) (unsigned int) f; 
} 
</pre> 
<h3>Obvious rounding issue</h3> 
<p>The cast from <code>float</code> to <code>unsigned int</code> does not round to the nearest integer. It “truncates”, that is, it rounds towards zero. And if you already know this, you probably know too the universally used trick to obtain the nearest integer instead of the immediately smaller one, adding 0.5:</p> 
<pre>float myround(float f) 
{ 
  if (f &gt;= UINT_MAX) return f; 
  return (float) (unsigned int) (f + 0.5f); 
} 
</pre> 
<p><strong>This universally used trick is wrong.</strong></p> 
<h3>An issue when the ULP of the argument is exactly one</h3> 
<p>The Unit in the Last Place, or ULP for short, of a floating-point number is its distance to the floats immediately above and immediately below it. For large enough floats, this distance is one. In that range, floats behave as integers.</p> 
<blockquote><p>There is an ambiguity in the above definition for powers of two: the distances to the float immediately above and the float immediately below are not the same. If you know of a usual convention for which one is called the ULP of a power of two, please leave a note in the comments.</p> 
</blockquote> 
<pre>int main() 
{ 
  f = 8388609.0f; 
  printf(\%f -&gt; %f"  f  myround(f)); 
} 
</pre> 
<p>With a strict IEEE 754 compiler  the simple test above produces the result below:</p> 
<pre>8388609.000000 -&gt; 8388610.000000 
</pre> 
<p>The value passed as argument is obviously representable as a float  since that's the type of <code>f</code>. However  the mathematical sum <code>f + 0.5</code> does not have to be representable as a float. In the worst case for us  when the argument is in a range of floats separated by exactly one  the floating-point sum <code>f + 0.5</code> falls exactly in-between the two representable floats <code>f</code> and <code>f + 1</code>. Half the time  it is rounded to the latter  although <code>f</code> was already an integer and was the correct answer for function <code>myround()</code>. This causes bugs as the one displayed above.</p> 
<p>The range of floating-point numbers spaced every 1.0 goes from 2^23 to 2^24. Half these 2^23 values  that is  nearly 4 millions of them  exhibit the problem.</p> 
<p>Since the 0.5 trick is nearly universally accepted as the solution to implement rounding to nearest from truncation  this bug is likely to be found in lots of places. Nicolas Cellier <a href="http://bugs.squeak.org/view.php?id=7134">identified it in Squeak</a>. He offered a solution  too: switch the FPU to round-downward for the time of the addition <code>f + 0.5</code>. But let us not fix the problem just yet  there is another interesting input for the function as it currently stands.</p> 
<h3>An issue when the argument is exactly the predecessor of 0.5f</h3> 
<pre>int main() 
{ 
  f = 0.49999997f; 
  printf("%.9f -&gt; %.9f"  f  myround(f)); 
} 
</pre> 
<p>This test produces the output <code>0.499999970 -&gt; 1.000000000</code>  although the input <code>0.49999997</code> is clearly closer to <code>0</code> than to <code>1</code>.</p> 
<p>Again  the issue is that the floating-point addition is not exact. The argument <code>0.49999997f</code> is the last <code>float</code> of the interval <code>[0.25 … 0.5)</code>. The mathematical result of <code>f + 0.5</code> falls exactly midway between the last float of the interval <code>[0.5 … 1.0)</code> and <code>1.0</code>. The rule that ties must be rounded to the even choice means that <code>1.0</code> is chosen.</p> 
<h3>A function that works</h3> 
<p>The overflow issue and the first non-obvious issue (when ulp(f)=1) can be fixed by the same test: as soon as the ULP of the argument is one  the argument is an integer and can be returned as-is.</p> 
<p>The second non-obvious issue  with input <code>0.49999997f</code>  can be fixed similarly.</p> 
<pre>float myround(float f) 
{ 
  if (f &gt;= 0x1.0p23) return f; 
  if (f &lt;= 0.5) return 0; 
  return (float) (unsigned int) (f + 0.5f); 
} 
</pre> 
<h3>A better function that works</h3> 
<p>Changing the FPU rounding mode  the suggestion in the Squeak bug report  is slightly unpalatable for such a simple function  but it suggests to add the predecessor of <code>0.5f</code> instead of <code>0.5f</code> to avoid the sum rounding up when it shouldn't:</p> 
<pre>float myround(float f) 
{ 
  if (f &gt;= 0x1.0p23) return f; 
  return (float) (unsigned int) (f + 0.49999997f); 
} 
</pre> 
<p>It turns out that this works  too. It solves the problem with the input <code>0.49999997f</code> without making the function fail its specification for other inputs.</p> 
<h2>To be continued</h2> 
<p>The next post will approach the same question from a different angle. It should not be without its difficulties either.</p>
{% endraw %}
