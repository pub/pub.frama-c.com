---
layout: post
author: Pascal Cuoq
date: 2011-02-28 10:06 +0200
categories: derived-analysis value
format: xhtml
title: "On switch statements"
summary: 
---
{% raw %}
<p>In Carbon 20110201 and earlier versions of Frama-C, if you do not use precautions when analyzing a program with <code>switch</code> statements, you get imprecise results. Consider the example program below.</p> 
<pre>main(int argc, char **argv){ 
  switch(argc){ 
  case 1:  
    Frama_C_show_each_no_args(argc); 
    break; 
  case 2: 
    Frama_C_show_each_exactly_2(argc);     
    /* fall through */ 
  case 3: 
    Frama_C_show_each_2_or_3(argc); 
    break; 
  default: 
    Frama_C_show_each_else(argc); 
    break; 
  } 
  return argc; 
} 
</pre> 
<pre>$ frama-c -val sw.c 
... 
[value] Called Frama_C_show_each_exactly_2([-2147483648..2147483647]) 
[value] Called Frama_C_show_each_no_args([-2147483648..2147483647]) 
[value] Called Frama_C_show_each_else([-2147483648..2147483647]) 
[value] Called Frama_C_show_each_2_or_3([-2147483648..2147483647]) 
... 
</pre> 
<p>This is correct, but disappointingly imprecise. 
Frama-C (the framework) can normalize the code to a large extent so that its plug-ins (here, the value analysis) do not need to handle redundant constructs. A <code>switch</code> construct can be encoded using <code>if</code> and <code>goto</code>. Frama-C option <code>-simplify-cfg</code> does exactly that, for the convenience of plug-in authors. As long as they have already programmed a precise analysis for these constructions, they can always get a precise result by requiring the program to be transformed thus.</p> 
<blockquote><p>Crediting where credit is due, the C code normalization philosophy is inherited from <a href="http://www.cs.berkeley.edu/~necula/cil/" hreflang="en">CIL</a>  which provided the basis of Frama-C's front-end.</p> 
</blockquote> 
<p>The transformed program looks like this:</p> 
<pre>$ frama-c -simplify-cfg -print sw.c 
... 
  if (! (argc == 1)) { 
    if (argc == 2) { goto switch_0_2; } 
    else { 
      if (argc == 3) { goto switch_0_3; } 
      else { goto switch_0_default; goto switch_0_break; } 
    } 
  } 
  switch_0_1: /* internal */ Frama_C_show_each_no_args(argc); 
  goto switch_0_break; 
  switch_0_2: /* internal */ Frama_C_show_each_exactly_2(argc); 
  switch_0_3: /* internal */ Frama_C_show_each_2_or_3(argc); 
  goto switch_0_break; 
  switch_0_default: /* internal */ ; 
  Frama_C_show_each_else(argc); 
  goto switch_0_break; 
  switch_0_break: /* internal */ ; 
  return (argc); 
... 
</pre> 
<p>I did not say the transformed code looked good. But usually  you do not need to look at the generated code; the analysis results are what you are interested in:</p> 
<pre>$ frama-c -simplify-cfg -val sw.c 
... 
[value] Called Frama_C_show_each_no_args({1; }) 
[value] Called Frama_C_show_each_exactly_2({2; }) 
[value] Called Frama_C_show_each_2_or_3({2; 3; }) 
[value] Called Frama_C_show_each_else([-2147483648..2147483647]) 
... 
</pre> 
<p>This time  the value analysis' results show that each time the program point at which we placed the <code>Frama_C_show_each_no_args</code> call is traversed  the value of <code>argc</code> is exactly 1. The program point with the call to <code>Frama_C_show_each_exactly_2</code> is only traversed with <code>argc==2</code>  and so on. These are the expected results.</p> 
<p>Sometimes  however  the value analysis is used as a means to a different end  and option <code>-simplify-cfg</code> can conflict with the final objective. Let's say the example program is modified slightly:</p> 
<pre>int x  t[4]; 
main(int argc  char **argv){ 
  t[0] = 10; 
  t[1] = 11; 
  t[2] = 12; 
  t[3] = 13; 
  switch(argc){ 
  case 1:  
    Frama_C_show_each_no_args(argc); 
    break; 
  case 2: 
    Frama_C_show_each_exactly_2(argc); 
    /* fall through */ 
  case 3: 
    Frama_C_show_each_2_or_3(argc); 
    x = t[argc]; 
    break; 
  default: 
    Frama_C_show_each_else(argc); 
    break; 
  } 
  return x; 
} 
</pre> 
<p>If the task we are actually interested in is obtaining a subset of the statements of the above program  so that this subset  when compiled and executed  returns the same value as the original program  option <code>-simplify-cfg</code> gets in the way. The options to instruct the slicing plug-in to compute this subset are <code>-slice-return main -slice-print</code>. We face the following dilemma:</p> 
<ol> 
<li>not use option <code>-simplify-cfg</code>  give the slicing plug-in access to an unmodified abstract syntax tree  but let it rely on imprecise values; or</li> 
<li>use option <code>-simplify-cfg</code>  get precise values  but have the slicing plug-in work on an abstract syntax tree representing a mangled version of the original program.</li> 
</ol> 
<p>In the first case  we get the sliced program below  which is shaped like the original but retains several statements that could have been removed:</p> 
<pre>$ frama-c sw.c -slice-return main -slice-print  
... 
{ 
  t[0] = 10; 
  t[1] = 11; 
  t[2] = 12; 
  t[3] = 13; 
   switch (argc) { 
    case 1: ; 
    break; 
    case 2: ; 
    case 3: ; 
    /*@ assert (0 ≤ argc) ∧ (argc &lt; 4); 
          // synthesized 
       */ 
    x = t[argc]; 
    break; 
    default: ; 
  } 
  return (x); 
} 
</pre> 
<p>In the second case  some useless initializations are removed  but the transformation makes the sliced program harder to follow:</p> 
<pre>$ frama-c sw.c -slice-return main -slice-print -simplify-cfg 
... 
{ 
  t[2] = 12; 
  t[3] = 13; 
  if (! (argc == 1)) { 
    if (argc == 2) { goto switch_0_2; } 
    else { if (argc == 3) { goto switch_0_3; } } 
  } 
  goto switch_0_break; 
  switch_0_2: /* internal */ ; 
  switch_0_3: /* internal */ ; 
  x = t[argc]; 
  switch_0_break: /* internal */ ; 
  return (x); 
} 
</pre> 
<p>Or... you can use the next version of Frama-C:</p> 
<pre>$ bin/toplevel.opt sw.c -slice-return main -slice-print  
... 
{ 
  t[2] = 12; 
  t[3] = 13; 
  switch (argc) { 
    case 1: ; 
    break; 
    case 2: ; 
    case 3: ; 
    x = t[argc]; 
    break; 
    default: ; 
  } 
  return (x); 
} 
</pre> 
<p>As you can see on this example  from the next release onwards  option <code>-simplify-cfg</code> is no longer necessary to get precise results with the value analysis. This means better slicing of programs that use the <code>switch</code> construct — and one option that you may now forget about.</p> 
<p>Many thanks to Boris  Taker of our Own Value Analysis Medicine extraordinaire  for implementing the new feature described in this post  and to Miguel Ferreira for providing expertise in English idioms.</p>
{% endraw %}
