---
layout: post
author: Pascal Cuoq
date: 2011-04-14 14:45 +0200
categories: unspecified-behavior value
format: xhtml
title: "When is it valid to compare two pointers in C?"
summary: 
---
{% raw %}
<p>This post is about the circumstances in which the value analysis considers comparing pointers is safe, and those in which it considers the comparison is dangerous and emits an alarm. The alarm, an enigmatic <code>assert \pointer_comparable(…, …);</code>, uses an unaxiomatized ACSL predicate. If you use the value analysis regularly, you probably already encountered it (but perhaps only as a false alarm caused by earlier approximations). This posts informally describes the rules applied by the value analysis. It is by these rules false alarms should be sorted from true ones, although a specific programmer may wish the rules were either stricter or relaxed a bit.</p> 
<p>If it were axiomatized, the predicate <code>\pointer_comparable</code> should capture all the conditions expressed in this post.</p> 
<blockquote><p>Note: this discussion takes place in the usual working conditions of the value analysis: we know and assume a bit about the architecture: size of integer and pointer types, <code>NULL</code> is zero, …</p> 
</blockquote> 
<h2>Pointers</h2> 
<p>The expression <code>&amp;a &lt; &amp;b</code> is dangerous. It clearly can evaluate to <code>0</code> or <code>1</code> depending on compiler choices the programmer has no control over. I would have guessed that this constituted <strong>unspecified behavior</strong> (i.e. the expression evaluates to either 0 or 1). In fact, according to paragraph 6.5.8.5 in the C99 standard,  it is <strong>undefined behavior</strong> (anything can happen, including a segmentation fault or the program displaying aberrant behavior from that point onwards).</p> 
<p>Expression <code>&amp;a + 1 == &amp;b</code> is similarly dangerous: variable <code>b</code> may have been placed just after <code>a</code> in memory, or it may not. The comparison may just, in this case, be <strong>unspecified</strong>: this is my reading of paragraph 6.5.9.6.</p> 
<h2>Pointers converted to integers</h2> 
<p>The value analysis employs the same rule for comparisons between expressions that have pointer types and for comparison between expressions that have unsigned integer types but happen to contain addresses. Signed integer inequalities between values that happen to be addresses should be considered dangerous more often, since variables can be placed at will in memory by the compiler. Let me clarify what I mean with an example:</p> 
<pre>int t[10]; 
main(){ 
  return (unsigned int)t &lt;= (unsigned int)(t+8); 
} 
</pre> 
<p>In Frama-C's default target architecture, an `unsigned int` can contain a pointer to int. The comparison above is deemed safe, and always evaluates to <code>1</code> (true), same as the pointer comparison <code>t&lt;=(t+8)</code>.</p> 
<p>Compare to this slightly different example:</p> 
<pre>int t[10]; 
main(){ 
  return (int)t &lt;= (int)(t+8); 
} 
</pre> 
<p>The comparison above is dangerous and should be flagged as such by the value analysis, because the array <code>t</code> could have been placed by the compiler at address <code>0x7ffffffa</code>. That <code>t</code> could, by sheer bad luck, be placed at this address means that <code>(int)(t+8)</code> could be negative while <code>(int)t</code> is positive in the value analysis' permissive model of memory and values. The result of this comparison depending in this way on compiler choices is undesirable, hence the warning.</p> 
<blockquote><p>You can force the value analysis to take into account the possibility that the dangerous comparison <code>(int)t &lt;= (int)(t+8)</code> returns <code>0</code> (false) with option <code>-undefined-pointer-comparison-propagate-all</code>. This is exactly what this option is for.</p> 
</blockquote> 
<p>Let's move on from signed integer inequalities: you can really never do anything safe except <code>(int)&amp;a &lt;= (int)&amp;a</code>. Here is another example:</p> 
<pre>int t[10]; 
main(){ 
  return 0 &lt; (unsigned int)t; 
} 
</pre> 
<p>In the value analysis modelization, this is a contrived way to test whether <code>t</code> is different from <code>NULL</code>, and is therefore always true. This comparison is considered safe.</p> 
<pre>int t[10]; 
main(){ 
  return t-1 &lt;= t; 
} 
</pre> 
<p>This comparison is dangerous: the address <code>0</code> is reserved for <code>NULL</code>, but nothing prevents the compiler from placing <code>t</code> at address <code>2</code>.  Doing so means that the comparison, assumed to be implemented with the unsigned comparison assembly instruction, can return false. It returns true for most other allocations of <code>t</code>, and is therefore unsafe.</p> 
<blockquote><p>Technically, computing <code>t - 1</code> is undefined behavior in C99. The value analysis waits until this value is used in a comparison before warning about it. This is only one of the various places where it is more lenient than the standard. The rationale is that the value analysis is intended for verification of existing programs. Existing programs do this (you wouldn't believe how often!). It would be very easy to warn at the slightest sign that the program is computing <code>t-1</code>, and this would in fact make a lof of things simpler (including this blog post). We are waiting for an industrial customer to ask for this specific feature, and we anticipate their asking, once it's done, how they can disable it locally.</p> 
</blockquote> 
<pre>int t[10]; 
main(){ 
  return 2 &lt;= (unsigned int)t; 
} 
</pre> 
<p>The comparison above, and <code>2 == (unsigned int)t</code>, are both unsafe because, while they usually respectively return <code>1</code> and <code>0</code>, they might return <code>0</code> and <code>1</code> at run-time for some rare compilations.</p> 
<h2>String literals</h2> 
<p>The comparison <code>\tutu" == "tutu"</code> (or  if you prefer  the comparison <code>s == t</code> when <code>s</code> and <code>t</code> have been defined as 
<code>char *s = "tutu"; char *t = "tutu";</code>) is dangerous: the compiler may 
place the anonymous string constants anywhere in memory  and factor them 
at its option. Again  in this case  my reading of the 
standard is that this comparison is only unspecified  but the development 
version of the value analysis emits an alarm for it nevertheless.</p> 
<p><code>char *s = "tutu"; char *t = s; s == t</code> is not dangerous: it should always 
evaluate to <code>1</code>.</p> 
<p><code>"hello" == "world"</code> is not dangerous: the compiler has no opportunity to factor the chains and thus the comparison predictably evaluates to <code>0</code>. <code>"hello" == "hello world"</code> is not dangerous either: the compiler cannot factor the two strings  because the first one must be zero-terminated. The terminating zero clashes with the space in the second string.</p> 
<p><code>"hello" == "hello\000world"</code> is dangerous: the compiler <strong>can</strong> factor the strings  making the comparison return <code>1</code>. It may also not factor them  making the comparison return <code>0</code>.</p> 
<p>Finaly  consider the snippet below:</p> 
<pre>  char *s = "oh  hello"; 
  char *s4 = s+4; 
  char *t = "hello\000hello"; 
</pre> 
<p>In this context  <code>s4 == t + 6</code> is harmless  because although <code>s4</code> points to the string <code>"hello"</code>  the compiler was contrained by the requirement to place "oh  " before it and could not place it at <code>t + 6</code>. This comparison always return <code>0</code>. The comparison <code>s4 == t</code>  on the other hand  is dangerous  because the compiler could have factored the tail of <code>s</code> with the head of <code>t</code>.</p> 
<h2>State of value analysis implementation</h2> 
<p>The rules described in this post are only implemented approximately in Carbon. They will work exactly as described here in the next Frama-C release.</p>
{% endraw %}
