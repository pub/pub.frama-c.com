---
layout: post
author: Virgile Prevosto
date: 2012-05-21 07:45 +0200
categories: CIL OCaml developer facetious-colleagues visitor
format: wiki
title: "Iterating over the AST"
summary: 
---
{% raw %}
<h3>Context</h3> 
<p>A facetious colleague who claims that he should be better writing his thesis but keeps committing Coq and OCaml files on Frama-C's repository, asked me the following question: <q>Is there a function in Frama-C's kernel that can fold<sup>[<a href="#pnote-144-1" id="rev-pnote-144-1">1</a>]</sup> a function over all types that appear in a C program?</q></p> 
<p>As it turns out  nothing is readily available for this exact purpose  but another facetious colleague could have come up with a terse answer: <q>There's a <strong>visitor</strong> for that!</q> Now  we can be a bit more helpful and actually write that visitor. Our end goal is to come up with a function</p> 
<pre> 
fold_typ: (Cil_types.typ -&gt; 'a -&gt; 'a) -&gt; 'a -&gt; 'a 
</pre> 
<p>so that <code>fold_typ f init</code> will be <code>f t_1 (f t_2 (... (f t_n init)...))</code>  where the <code>t_i</code> are all the C types appearing in a given C program (in no particular order).</p> 
<h3>Frama-C's visitors</h3> 
<p>The <a href="http://en.wikipedia.org/wiki/Visitor_pattern">visitor pattern</a> is a well-known object-oriented design pattern that can be used to perform a given action over all nodes of a complex data structure (in our case the Abstract Syntax Tree -AST for short- of a C program). Frama-C provides a generic visitor mechanism  built upon CIL's visitor  
whose entry points can be found in the aptly named <code>src/kernel/visitor.mli</code> file. It is also documented in section 5.14 of <a href="http://frama-c.com/download/plugin-development-guide-Nitrogen-20111001.pdf">the developer manual</a>. In summary  you first define a class that inherits from the generic visitor and overrides the methods corresponding to the nodes you're interested in (in our case  this will be <code>vtype</code> for visiting uses of <code>Cil_types.typ</code>). Then you apply an object of this class to the function from the <code>Visitor</code> module that correspond to the subpart of the AST that you want to inspect (in our case  this will be the whole AST).</p> 
<h3>Basic version</h3> 
<p>The standard visitor does not provide anything to return a value outside of the AST. In fact  all the entry points in <code>Visitor</code> return a node of the same type that the node in which the visit starts (for visitors that don't perform code transformation  this is in fact physically the same node). But if you accept to sneak in a little bit of imperative code into your development -and by using Frama-C you've already accepted that- there is an easy way out: pass to the visitor a reference 
that it can update  and you just have to read the final value that reference is holding after the visit. The visitor then looks like the following:</p> 
<pre> 
class fold_typ_basic f acc = 
object 
  inherit Visitor.frama_c_inplace 
  method vtype ty = acc:= f ty !acc; Cil.DoChildren 
end 
</pre> 
<p>And that's it. Each time the visitor sees a type  it will apply <code>f</code> to <code>ty</code> and the result of the previous computations  stored in <code>acc</code>. <code>fold_typ</code> then just needs to call the visitor over the whole AST and give it a reference initialized with <code>init</code>:</p> 
<pre> 
let fold_typ f init = 
  let racc = ref init in 
  let vis = new fold_typ_basic f racc in 
  Visitor.visitFramacFileSameGlobals vis (Ast.get()); 
  !racc 
</pre> 
<h3>Don't do the same work twice</h3> 
<p>This first version  that is barely more than 10 LoC  works  but we can do a little better. Indeed  <code>f</code> will be called each time a type is encountered in the AST. In most cases  we want to call <code>f</code> once for any given type. This can be done quite simply by memoizing in an instance variable of our visitor the set of types encountered thus far. Frama-C's <code>Cil_datatype</code> module (<code>cil/src/cil_datatype.mli</code>) provides all the needed functions for that:</p> 
<pre> 
class fold_typ f acc = 
object 
  inherit Visitor.frama_c_inplace 
  val mutable known_types = Cil_datatype.Typ.Set.empty 
  method vtype ty = 
    if Cil_datatype.Typ.Set.mem ty known_types then Cil.DoChildren 
    else begin 
      known_types &lt;- Cil_datatype.Typ.Set.add ty known_types; 
      acc:= f ty !acc; 
      Cil.DoChildren 
    end 
end 
</pre> 
<h3>Testing the infrastructure</h3> 
<p>It is now time to test if everything works smoothly. The following function will print the name and size of the type who has the biggest size in the analyzed program.</p> 
<pre> 
let test () = 
  let f ty (maxty maxsize as acc) = 
    try 
      let size = Cil.sizeOf_int ty in 
      if size &gt; maxsize then (ty size) else acc 
    with Cil.SizeOfError _ -&gt; acc 
  in 
  let (ty size) = fold_typ f (Cil.voidType 0) in 
  Format.printf &quot;Biggest type is %a @ with size %d@.&quot;  
    !Ast_printer.d_type ty size 
</pre> 
<p>Since it is only a quick test  we don't do anything special if Cil complains that it cannot compute the size of a given type: we just stick to the maximal value computed 
so far.</p> 
<p>File <a href="/assets/img/blog/imported-posts/fold_typ.ml">fold_typ.ml</a> provides the code for the visitor and the test function. <code>frama-c -load-script fold_typ.ml file.c</code> should output something like</p> 
<pre> 
[kernel] preprocessing with &quot;gcc -C -E -I.  file.c&quot; 
Biggest type is struct Ts  
with size 44 
</pre> 
<h3>Exercises</h3> 
<ol> 
<li>How can you use the <code>fold_typ</code> class to define an <code>iter_typ</code> function that apply a function <code>f</code> returning <code>unit</code> to each type of the AST (<code>val iter_typ: (Cil_types.typ -&gt; unit) -&gt; unit</code>)?</li> 
<li>Writing <code>fold_typ</code> is a bit overkill if you're going to apply it once in your plug-in. Write a specialized visitor that will do the same thing as the <code>test</code> function above.</li> 
</ol> 
<div class="footnotes"><h4>Notes</h4> 
<p>[<a href="#rev-pnote-144-1" id="pnote-144-1">1</a>] For those who are not familiar with functional programming: <a href="http://en.wikipedia.org/wiki/Fold_%28higher-order_function%29" title="http://en.wikipedia.org/wiki/Fold_%28higher-order_function%29">http://en.wikipedia.org/wiki/Fold_%...</a></p></div> 
" 
{% endraw %}
