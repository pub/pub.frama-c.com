---
layout: post
author: André Maroneze
date: 2016-11-22 12:00 +0200
categories: ACSL
image:
title: "Frama-C and ACSL are on GitHub"
---

We are glad to announce the creation of official GitHub repositories for
Frama-C and ACSL, to stimulate contributions from the community, and to better
contribute back to it.

Frama-C is on GitHub
====================

Frama-C now (in fact, since a few months) has an official GitHub repository:

[https://github.com/Frama-C/Frama-C-snapshot](https://github.com/Frama-C/Frama-C-snapshot)

It contains snapshots of each Frama-C release,
starting from Hydrogen all the way up to Aluminium.
It also contains a branch for release candidates.

Issues and pull requests can be submitted via GitHub, for those who prefer it
to MantisBT's interface (which remains the official Frama-C bug tracker).

ACSL is also on GitHub
======================

On a related note, a new repository for the ACSL documentation has also been
created:

[https://github.com/acsl-language/acsl](https://github.com/acsl-language/acsl)

Previously, documentation issues (typos, updates, etc.) had to be reported to
the Frama-C bug tracking system, which was not ideal, since ACSL is not part
of Frama-C, even if Frama-C is currently ACSL's largest user.

This new repository, which is now the official channel for the ACSL language
specification, will help evolve it, by giving contributors direct access to
the source code, and allowing faster creation of issues and pull requests with
updates (typos, corrections, etc.).
