---
layout: post
author: Pascal Cuoq
date: 2012-10-02 12:45 +0200
categories: rers2012 value
format: xhtml
title: "RERS 2012 competition: our solutions for problems 1-9"
summary:
---
{% raw %}
<h2>Previously on this blog</h2>
<p>Although it was so brief that you may have missed it, I previously mentioned here the 2012 RERS Grey Box Challenge an interesting competition where the questions involve programs in C syntax.</p>
<p>I pointed out that some questions were about the reachability of assertions in the programs. As it is documented  Frama-C's value analysis can guarantee that some assertions are not reachable. When its authors are using it  they can ascertain that no approximation is taking place  and that for this reason  any assertion that the analysis diagnoses as perhaps reachable is indeed reachable for some sequence of inputs. Because of the way it work  the analysis is unable to exhibit the sequence of inputs  but one such sequence definitely exist.</p>
<p>We then moved on to properties expressed as LTL formulas. Some of the LTL formulas in the competition <a href="/2012/08/20/Participating-in-the-RERS-2012-competition-reachability-questions.html">expressed reachability properties</a>. It was easy to diagnose those by applying Frama-C's value analysis to an instrumented program. The other properties were more difficult liveness properties. With more instrumentation  <a href="/2012/08/22/Proving-F-oW-true-or-false-alternative-method.html">involving either a transition counter or a non-deterministic predecessor state</a>  it was possible to diagnose these too using the value analysis.</p>
<blockquote><p>Colleagues and I have obtained solutions for all properties in the initial problems 1-9 of the competition. More information about these solutions is available in the last part of this post. First  now that the exercise is done  I would like to revisit its benefits.</p>
</blockquote>
<h2>Features gained  bugs fixed</h2>
<h3>New built-in Frama_C_interval_split()</h3>
<p>A key part of the <code>main()</code> function used for the verification of any kind of property in the competition was:</p>
<pre>  input = unknown_int();
  /*@ assert input == 1 || input == 2 || input == 3 ||
                input == 4 || input == 5 || input == 6 ; */
</pre>
<p>These three lines are intended to replace the following one in the original <code>main()</code> function provided by the organizers. Their line works for interactive sessions where the operator inputs number in the range 1..6.</p>
<pre>        scanf("%d"  &amp;input);
</pre>
<p>The call to <code>unknown_int()</code> could also have been <code>Frama_C_interval(1  6)</code>. Then the value analysis would be able to tell that the assertion is not an assumption  only a hint. Regardless  the assertion is there  together with option <code>-slevel</code>  to make the analyzer study separately what happens in executions where a different value is read for <code>input</code>.</p>
<p>This is a classic trick used in many earlier studies. It is mentioned in <a href="/2011/06/02/Skein-tutorial-part-7-not-dead-but-resting.html">the study of the Skein-256 hash function</a> and in nearly all others since. Still  these assertions do get tiring to write when many cases need to be listed. Until now  there was no shortcut (I have been using Emacs macros myself).</p>
<p>Enter <code>Frama_C_interval_split(l  u)</code>. It does the same thing that <code>Frama_C_interval(l  u)</code> does  but it automatically causes the individual values between <code>l</code> and <code>u</code> inclusive to be propagated separately  without need for a laborious ACSL assertion.
The <code>Frama_C_interval_split()</code> built-in is available in Oxygen (I think)  and since it is a true value analysis built-in  the user does not need to remember to include file <code>builtin.c</code> to make it accessible. A simple prototype <code>int Frama_C_interval_split(int  int);</code> will do.</p>
<h3>The informational message “unrolling up to ... states” could be off</h3>
<p>If you have used option <code>-slevel</code> (and as previously said  if you use Frama-C's value analysis at all  you should definitely use this option from time to time)  you know that it sometimes logs a message to tell how much superposition has been done on a same program statement. It looks like:</p>
<pre>...
[value] Semantic level unrolling superposing up to 100 states
...
[value] Semantic level unrolling superposing up to 200 states
...
</pre>
<p>The algorithm that takes care of this message was like this:</p>
<pre>  target := 100;
  if new number of states attached to the statement ≥ target
  then
    print "Semantic level unrolling superposing up to " target " states"
    target := target + 100
  endif
</pre>
<p>The intention was that the value displayed would indicate the maximum number of superposed states to the nearest hundred.</p>
<p>Suppose the first wave of propagated states to reach a statement contains a thousand of them. The algorithm displays “superposing up to 100 states”  and updates the target to 200. If the second wave contains 2500 more states  the algorithm then displays “superposing up to 200 states” and updates the target to 300. And so on. If states keep arriving in large numbers  variable <code>target</code> gets irremediably behind.</p>
<p>This had not been noticed until this competition  but in model-checking mode  manipulating tons of states  <code>target</code> never gets a chance to catch up and the number displayed can be much lower than the number of states actually superposed.</p>
<p>This bug is present in Frama_C Oxygen. It is fixed in the development version.</p>
<h3>An unnamed option was tested more thoroughly</h3>
<p>Last but proverbially not least  thanks to this competition  the value analysis option that <a href="/2012/09/06/A-value-analysis-option-to-reuse-previous-function-analyses.html">caches</a> previous analyses of a function call in order to re-use them without loss of precision has received more testing. Who knows  someday we may even trust it enough to reveal its name.</p>
<h2>Conclusion</h2>
<p>The next section describes our solutions. If you did not at least attempt the competition yourself  it is unlikely that you will find it interesting: you should stop reading now. Here is my personal conclusion before you go: case studies are fun  and model-checking is a fun sub-field of computer science. At the same time  a piece of software is not an automaton: if you try to specify one as if it was the other you will certainly make mistakes and verify something other than you intended. I think that our experience trying to participate in the competition demonstrates that. If you are not convinced  please try to answer the competition's questions for yourself  and then  and only then  read the next section.</p>
<h2>Our solutions to the 2012 RERS challenge</h2>
<h3>Description</h3>
<p>The reachability of assertions is straightforward to answer with the value analysis  so I won't describe that.
Here is an <a href="/assets/img/blog/imported-posts/eca-automatic.tgz">archive</a> containing our answers for the LTL properties part. The archive contains mostly everything. Many files there were generated from others. In particular  the *.log files are analysis log files that took about one core-month to generate on a 2.66 GHz Intel Xeon X5650 workstation. The last file to be generated was “results”. It contains the predicted statuses of properties as interpreted with the conventions that follow.</p>
<p>A specific LTL property for a specific problem is checked by putting together the problem's transition function (function <code>calculate_output()</code> and its callees)  a file <code>buchimgt.c</code> specific to the problem (this file contains the definition of the predecessor state and functions that manipulate it)  a file <code>main.c</code> that contains a generic main loop  and a file such as <code>output Z occurs after output U until output W.c</code> that is specific to a property (but independent of the problem to which it is linked).</p>
<p>The problem-specific files are generated by the script <code>genallproblemfiles</code>.</p>
<p>The property-specific files such as <code>output Z occurs after output U until output W.c</code> were generated from templates such as <code>output _ occurs after output _ until output _.c</code>. The templates were hand-crafted by my colleague Sébastien Bardin to work together with the rest of the instrumentation.</p>
<p>The current status of the LTL property is maintained by an automaton designed for this kind of work (called a Büchi automaton).
The generic <code>main()</code> function describes our interpretation of the problem. In pseudo-code:</p>
<pre>forever
   read input 1..6
   compute output from input and current state
   compute transition in Büchi automaton from input and output
   if the current trace can no longer fail to satisfy the property
      prune
   if the current trace can no longer satisfy the property
      display current state for verification
      abort analysis
   non-deterministically update predecessor state
</pre>
<p>The actual C file is designed so that the value analysis will remain complete as well as sound when analyzing it with high <code>-slevel</code>. A secondary objective is for the analysis to be as fast as possible despite current limitations. Readability only ranks third as an objective.</p>
<p>Still  regardless of the strange structure of the program  it is basically C. Any C programmer can read the <code>main()</code> and understand what is being verified. The only non-standard function is <code>Frama_C_interval_split()</code>. The informal description of <code>Frama_C_interval_split()</code> is that the function returns a value between the bounds passed to it and that the results provided by the analyzer hold for all values that it can return.
If a simple Büchi automaton is plugged in that fails when output <code>Z</code> is emitted  it is relatively easy to understand on the basis of this <code>main()</code> function what we mean by “<code>Z</code> is emitted” and that <code>Z</code> can never be emitted if the analysis passes without the automaton raising a flag. This is how we can verify  as an example  the LTL property <code>G !oZ</code>.</p>
<p>As you can see on the pseudo-code  the Büchi automaton is updated with an input and the corresponding computed output  which are considered simultaneous. When <code>calculate_output()</code> fails to return an output because of a failed assertion  no pair <code>(input  output)</code> is presented to the automaton. To illustrate  consider the hypothetical property "output X occurs before
input A". This property is weird  because each input A-F can happen at each instant  including the first one. It may seem that the property is tautologically false.
With our interpretation  this property would be true if the system always failed the instant it read A until it had output X. These inputs would not count because the Büchi automaton would not see them.</p>
<p>The above is what we considered the most reasonable interpretation of what the competition organizers intended  at the time we sent our answers. On October 1  the organizers published example solutions that made us realize that they intended something else  and we had to retract our participation.</p>
<h3>Differences with the official interpretation  and remaining unknowns</h3>
<p>As the challenge period was coming to a close  organizers published solutions to additional problems 10 and 11 (the challenge was supposed to close at the end of September. The solutions illustrating the semantics were published on October 1 and the deadline extended).</p>
<p>One can only assume that these solutions also define the semantics of LTL formulas for problems 1-9  since no example is available for these. Unfortunately  there are subtle differences between the two families of programs.</p>
<h5>Outputs corresponding to invalid inputs</h5>
<p>The transition functions in problems 1-9 can return -1 or -2. They only return -2 when the rest of the program state was unchanged. The organizers communicated to us that traces in which -2 is emitted indefinitely should be ignored:</p>
<p><q>The same holds by the way for invalid inputs  i.e.  calculate_output returning 2 [sic]. They are not meant to be part of the traces that LTL properties talk about - a liveness property such as F oW hence does not become false alone by infinitely often executing an invalid input.</q></p>
<p>I did not see this last point made explicit on the website.</p>
<p>For problems 1-9  <strong>since the program state is unchanged when -2 is returned</strong>  it seems that the easiest way to comply is to treat -2 as a fatal error that interrupts the trace. We do not lose much of importance in doing so because any trace with -2 outputs can be shortened into a trace without these outputs. We do lose some inputs during such a shortening  so it is still slightly invasive and we would like to be sure that this is what the organizers intended. Indeed  the <code>main()</code> function provided by the organizers does seem to treat -1 and -2 differently  and does call the inputs that lead to -2 “invalid”.</p>
<pre>        if(output == -2)
                fprintf(stderr  "Invalid input: %d"  input);
        else if(output != -1)
                        printf("%d"  output);
</pre>
<p>The code above matches more or less the “if an unexpected input event is provided  an error message is printed and the ECA system terminates” statement on the competition's website. The code above does not stop on -2 but it seems clear that the “unexpected input” from the explanation is the -2 from the code and that  as informally stated  it interrupts the current trace.</p>
<p>You might expect programs 10-13 to follow the same pattern  but it turns out that instead of sometimes -1 and sometimes -2  they always return -2  both when the program state has been changed by the transition function and when it hasn't. You might still hope to interpret -2 as “error”  but in programs 10 and 11 it happens in the middle of traces that are given as example for the questions on the reachability of assertions.
The solution page says:</p>
<pre>error_46 reachable via input sequence
[E  A]
</pre>
<p>And  when you try it:</p>
<pre>$ gcc Problem10.c
$ ./a.out
5
Invalid input: 5
1
Assertion failed: (0)  function calculate_output  file Problem10.c  line 66.
Abort trap
</pre>
<p>So it does not look that -2 should be treated as an error after all. In the above organizer-provided trace  the error caused by input 5 (E) does not make <code>error_46</code> unreachable (actually  I have it on good authority that error_46 is unreachable when  in the transition function  <code>return -2;</code> is treated as a trace-interrupting error).</p>
<p>In conclusion  for problems 1-9  we treated -1 as just another output letter and -2 as a trace-interrupting error. This does not correspond to what the organizers did in their example solutions to problem 10. We still do not know what the organizers intended.
The transition function returning -2 instead of -1 in problems 10-13 seems particularly inscrutable when the organizers have previously told us by e-mail to ignore the traces with finitely many non-(-2) outputs.</p>
<p>Our aforelinked solutions were computed with this convention. We provide them in case someone wants to use the same convention and compare their results to ours.</p>
<h5>Are inputs and outputs simultaneous or are they different events?</h5>
<p>Again  we have no idea. In our modelization  the output happens at the same time as the input it was computed from.</p>
<p>But in example traces provided by the organizers  it looks like they are distinct events!! An example trace is <code>[iD  oV  iB] ([oY  iC  oV  iB])*</code>.</p>
<p>But if input and output are separate events  why is the first LTL formula for problem 10 expressed as <code>(! iA WU (oU &amp; ! iA))</code> ?
The atom <code>(oU &amp; ! iA)</code> makes no sense  because if the event is any sort of output event (<code>U</code> or another)  then it cannot be an input event. The atom <code>(oU &amp; ! iA)</code> is just equivalent to <code>oU</code>. All LTL properties that talk about inputs and outputs in the same instant are similarly nonsensical.</p>
<p>In our solutions  influenced by the shape of the competition's LTL formulas which talk about input and output in the same instant
we considered that input and corresponding output are simultaneous. Both are passed to the Büchi automaton at the time of computing a transition. An input can happen together with the output -1. If the transition function aborts because of an assertion or returns -2  then the input is not seen by the Büchi automaton (in the case of the -2 output  this would be easy to change by modifying the <code>main()</code> function. The trace could be interrupted just after the Büchi automaton transition instead of just before).</p>
<h5>Is there any reachability LTL property at all in the competition?</h5>
<p>The example solutions all provide infinite traces as counter-examples. This is consistent with the explanation “an LTL property holds for a certain ECA program if and only if all possible runs which do not lead to an error satisfy the LTL property”  but it is a very strange way to approach reachability questions (which indeed are no longer reachability questions at all). To answer a LTL question that would ordinarily have been a reachability question  such as “output <code>W</code> occurs at most twice”  in this setting  one must  when <code>W</code> has been encountered three times on a trace  wonder whether this trace is the prefix of at least one infinite trace. If there is a sequence of inputs that keeps the trace alive  then the trace is a counter-example. If all inputs that can be added to extend to this prefix eventually lead to an error  even if it is at a very far horizon  then the trace is not a counter-example (remember not to take into account the trace that indefinitely outputs -2 when trying to answer this sub-question).</p>
<p>In short  in this setting  no LTL formula ever expresses a reachability property. It does not prevent us to handle these LTL properties (Sébastien assures me that our method theoretically handles all liveness properties. We certainly handle those that are already liveness properties without the weird “all possible runs that do not lead to an error” quantification).</p>
<blockquote><p>This discussion gives me an idea for a powerful static analysis tool. I will call it <a href="http://en.wikipedia.org/wiki/Marvin_the_Paranoid_Android">Marvin</a>. You ask it “Is this software safe? Can this dangerous angle be reached?” and it answers “Who cares? The Sun is going to blow up in five billion years. Good riddance if you ask me. Oh god  I'm so depressed…”</p>
</blockquote>
<h2>Thanks</h2>
<p>As the initiator of this attempt to participate to the competition  I would like to thank my colleagues Virgile Prevosto  Boris Yakobowski and Sébastien Bardin  from whom I personally learnt a lot about various aspects of software verification; my warmest thanks are for the organizers of the 2012 RERS Grey Box Challenge.</p>
{% endraw %}
