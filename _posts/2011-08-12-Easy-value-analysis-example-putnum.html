---
layout: post
author: Pascal Cuoq
date: 2011-08-12 06:55 +0200
categories: value
format: xhtml
title: "Easy value analysis example: putnum()"
summary:
---
{% raw %}
<p>If some of the posts in this blog ever get re-organized into a course-style document, this one will be near the beginning, because it's simple and self-contained.</p>
<h2>The program</h2>
<p>This example was offered on our bug-tracking system:</p>
<pre>int putchar(int);
void print(const char *ptr)
{
  while (*ptr)
    putchar(*ptr++);
}
void putnum(unsigned long n)
{
  char buf[10], *ptr = buf + sizeof(buf);
  *--ptr = 0;
  do {
    *--ptr = \0123456789ABCDEF"[n&amp;15];
    n &gt;&gt;= 4;
  } while(n);
  print(ptr);
}
</pre>
<p>A few minutes of looking at the code should allow you to infer for yourself that function <code>putnum()</code> is for printing unsigned hexadecimal numbers.</p>
<h2>The precision issue</h2>
<p>The reporter was disappointed that the value analysis emitted a number of alarms for this code  which seems trivial. This brings to mind the quip by Richard Feynman that "mathematicians can prove only trivial theorems  because every theorem that's proved is trivial". Excuse the approximate quote: this is another book that I sop up in audio form  and these things are even worse than paper for searching.
But perhaps the case deserves to be called trivial indeed  because in the original program  as provided by the reporter in the BTS  <code>putnum()</code> is called only with a single  concrete value:</p>
<pre>int main(void)
{
  putnum(0x12345);
  putchar('\
');
  return 0;
}
</pre>
<p>This should help the value analysis: it likes to analyze functions with only the values for arguments and globals that appear to be really passed to them at execution. However  remember that most classic  bug-finding static analyzers would analyze function <code>putnum()</code> in abstracto  and tell you what they think pertinent to tell you regardless of whether the function is called only with <code>0x12345</code>  or indeed  never called at all — and therefore  like the plane that always stays on the ground  is extremely safe.</p>
<p>Back to the value analysis. What are these alarms that it finds?</p>
<pre>$ frama-c -val putnum.c
...
putnum.c:16:[kernel] warning: out of bounds write. assert \valid(ptr);
putnum.c:7:[kernel] warning: accessing uninitialized left-value *ptr: assert(Ook)
putnum.c:8:[kernel] warning: accessing uninitialized left-value *tmp: assert(Ook)
putnum.c:7:[kernel] warning: out of bounds read. assert \valid(ptr);
putnum.c:8:[kernel] warning: out of bounds read. assert \valid(tmp);
...
</pre>
<p>Fair enough. The value analysis does emit a number of alarms. Some of them are nearly duplicates. For instance  variable <code>tmp</code> is only  in function <code>print()</code>  the temporary  introduced in the translation of <code>*ptr++</code>. It's by design that <a href="/2011/07/21/Back-to-the-drawing-board.html">the analysis no longer even tries to avoid multiple warnings in this case</a>. On the whole  it does limit its study of function <code>putnum()</code> to the case where <code>n</code> is <code>0x12345</code>. The imprecision comes from somewhere else: the loop inside <code>putnum()</code>. By default  the analyzer summarizes the loop in an unfavorable way. Variable <code>ptr</code> is only inferred to range over <code>{{ &amp;buf + [-1..8] }}</code> at the point where it's dereferenced  so it is not guaranteed to be valid:</p>
<p><img src="/assets/img/blog/imported-posts/putnum/loop_summary.png" alt="loop_summary.png" style="display:block; margin:0 auto;" title="loop_summary.png  août 2011" /></p>
<p>Since the analyzer doesn't know which <code>buf[]</code> element exactly is written to  all it can say is that each element between 0 and 8 may be  <code>[48..70] or UNINITIALIZED</code>:</p>
<p><img src="/assets/img/blog/imported-posts/putnum/loop_buf.png" alt="loop_buf.png" style="display:block; margin:0 auto;" title="loop_buf.png  août 2011" /></p>
<p>Note that <code>48</code> is <code>'0'</code> and <code>70</code> is <code>'F'</code>  so it is not doing too bad  really  despite emitting all these alarms.</p>
<h2>The solution</h2>
<p>What's the step after checking what library functions are used in the program and thinking how to offer equivalent source code to the analyzer? Ah  yes  it is: "if the analysis is fast enough  use option <code>-slevel</code> to make it more precise".</p>
<pre>$ frama-c -val putnum.c -slevel 50
...
[value] Values for function putnum:
          n ∈ {0; }
          buf[0..3] ∈ UNINITIALIZED
             [4] ∈ {49}
             [5] ∈ {50}
             [6] ∈ {51}
             [7] ∈ {52}
             [8] ∈ {53}
             [9] ∈ {0}
          ptr ∈ {{ &amp;buf + {4} }}
</pre>
<p>The alarms all disappear. We can make sure this is not caused by a freakish accident but because the value analysis understand exactly what goes on during execution of this program: the hexadecimal representation of <code>0x12345</code> in the contents of array <code>buf[]</code>  in a snapshot taken at the end of <code>putnum()</code>.</p>
<h2>Generalizing</h2>
<p>The exercise can be made more interesting by considering the safety of <code>putnum()</code> when called with an unknown argument:</p>
<pre>unsigned long unknown();
int main(void)
{
  putnum(unknown());
  putchar('\
');
  return 0;
}
</pre>
<p>I have to admit I made a mistake here when replying to the bug report: I did not try it. Biased by hours spent on difficult case studies  I thought that the value analysis would need to have different execution paths (where the program is valid for slightly different reasons) manually separated in order to conclude that the program is safe. Something like the following hint  placed in a strategic location  according to the principles in <a href="/2010/10/07/Value-analysis-tutorial-part-2.html">this post</a>.</p>
<pre>/*@ assert n &lt;= 15 || 16 &lt;= n &lt;= 65535 || 65536 &lt;= n &lt;= ... ;
</pre>
<p>Such an annotation  typical of those needed for a larger-scale value-analysis-based verification  is not required here (but it does not hurt). Even without it  the value analysis separates the cases where the number is one-digit  two-digits  etc. long by itself  in few millions of processor cycles — that is  in a fraction of a second:</p>
<pre>$ frama-c -val putnum.c -slevel 500
...
[value] Values for function putnum:
          n ∈ {0; }
          buf[0] ∈ UNINITIALIZED
             [1..7] ∈ [48..70] or UNINITIALIZED
             [8] ∈ [48..70]
             [9] ∈ {0}
          ptr ∈ {{ &amp;buf + [1..8] }}
</pre>
<p>For an annotationless analysis  a lot of information is contained in this snapshot of the program's state. The value analysis is strongly suggesting that it is safe to reduce the size of <code>buf[]</code> by one  since <code>ptr</code> never goes lower than <code>buf+1</code> and <code>buf[0]</code> remains uninitialized for all possible <code>putnum()</code> arguments.
All numbers are printed with at least one digit (<code>buf[8]</code> is guaranteed to be initialized and in the range '0'..'F'  and <code>ptr</code> points no higher than <code>buf[8]</code>). On the other hand  elements 1 to 7 are not guaranteed to be written to.</p>
<p>But wait: the value analysis seems to be saying that it's safe to reduce the size of <code>buf[]</code> by one  but perhaps this code is not intended <strong>only</strong> for 32-bit architectures? Perhaps the additional element serves to make the function safe on a 64-bit architecture. Let us try it out:</p>
<pre>$ frama-c -val putnum.c -slevel 500 -machdep x86_64
...
putnum.c:16:[kernel] warning: out of bounds write. assert \valid(ptr);
...
[value] Values for function putnum:
          n ∈ {0; }
          buf[0..7] ∈ [48..70] or UNINITIALIZED
             [8] ∈ [48..70]
             [9] ∈ {0}
          ptr ∈ {{ &amp;buf + [0..8] }}
</pre>
<p>On a 64-bit architecture  all the elements of <code>buf[]</code> seem to be useful  but there may be a problem with <code>ptr</code> at line 16. Determining whether this is a true or false alarm will be left as an exercise to the reader (download file <a href="/assets/img/blog/imported-posts/putnum/putnum.c">putnum.c</a>).</p>
<p>Zaynah Dargaye provided insights based on an early version of this post.</p>
{% endraw %}
