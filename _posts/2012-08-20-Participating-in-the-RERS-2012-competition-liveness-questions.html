---
layout: post
author: Pascal Cuoq
date: 2012-08-20 12:15 +0200
categories: benchmarks obviously-terminates rers2012 value
format: xhtml
title: "Participating in the RERS 2012 competition: liveness questions"
summary:
---
{% raw %}
<p>This third installment discusses the diagnosis of liveness properties of particular C programs, using Frama-C's value analysis, for the RERS 2012 competition.</p>
<p>[Note added in September 2012: the diagnostic found for the example in this post disagrees with the diagnostic found later and sent to the competition organizers. I am unable to reproduce the result obtained here. Perhaps it was obtain with a version of Frama-C that had a bug.]</p>
<h2>Liveness properties 101</h2>
<p>Here is another extract of two properties found in the property list attached to Problem 1 in the competition:</p>
<pre>(G ! oW)
output W does never occur
----------------------------------------
(F oW)
output W occurs eventually
</pre>
<p>To tell you how unaccustomed to temporal properties I am  I first read the second formula as a negation of the first one. There are redundancies in the lists of formulas to evaluate  and I thought this was one more of them. It is of course not the case: <code>(F oW)</code> is not the negation of <code>(G ! oW)</code>.</p>
<p><code>(G ! oW)</code> says that there is no execution path that leads to <code>W</code> being output;  the negation of that is that there is at least one such execution path.
By contrast  <code>(F oW)</code> expresses that all execution  paths have a <code>W</code> being output at some point  a much stronger property.</p>
<p>Indeed  <code>(F oW)</code>  expressing that a state in which <code>W</code> is output is eventually reached  is a typical liveness property. Checking this kind of property is at odds with traditional abstract interpretation practices  where the analysis is considered done when a fixpoint has been reached: the fixpoint does not tell whether all executions eventually leave the loop. It only tells what is guaranteed for those executions that leave the loop.</p>
<h2>Semi-solution: a simple program transformation</h2>
<p>Please bear with me and consider the simple modification of Problem1.c below  applied on top of the <a href="/2012/08/20/Participating-in-the-RERS-2012-competition-reachability-questions.html">previous</a> instrumentation:</p>
<pre>--- Problem1.c	2012-08-08 10:15:00.000000000 +0200
+++ Problem1_terminate_at_W.c	2012-08-21 17:00:38.000000000 +0200
@@ -1 9 +1 8 @@
 void assert(int x)
 {
   if (!x)
     {
-      Frama_C_show_each_assert_reached();
       /*@ assert \false; */
     }
 }
@@ -594 6 +593 8 @@
         // operate eca engine
         output = calculate_output(input);
+	// continue only if output not W yet
+	/*@ assert output != w ; */
     }
 }
</pre>
<p>We remove the user message in our <code>assert()</code> function because we want to temporarily forget about assertions. Besides  by instrumenting the main loop  we make the program stop when the output is <code>W</code>.</p>
<p>You should be able to convince yourself that the modified program terminates if and only if the original program does not admit any infinite execution that never outputs <code>W</code> (for any infinite sequence of values assigned to variable <code>input</code>).</p>
<p>Earlier posts (<a href="/2012/06/07/Verifying-the-termination-of-programs-with-value-analysis-option-obviously-terminates.html">1</a>  <a href="/2012/06/08/To-finish-on-termination.html">2</a>) in this blog pointed out how Frama-C's value analysis could be made to forget that it was an abstract interpreter  and verify the termination of programs.</p>
<h2>Application</h2>
<p>Since everything is ready for this particular property of Problem1.c  let us run the verification.
The command to prove that the program terminates is as follows:</p>
<pre>$ frama-c -val -obviously-terminates -no-val-show-progress Problem1_terminate_at_W.c
...
[value] Semantic level unrolling superposing up to 400 states
[value] done for function main
[value] ====== VALUES COMPUTED ======
</pre>
<p>We should have used a timeout  because the analysis fails to terminate when the analyzer is unable to prove that the target program terminate. Nevertheless  we were lucky  the target program terminates  the analyzer is able to verify that the program terminates  and we can conclude that there are no infinite executions of the original Program1.c that fail to emit <code>W</code>.</p>
<h2>Assertions and consequences on liveness properties</h2>
<p>The previous section only tells us that there is no infinite execution that does not emit <code>W</code>. Depending on the organizers' intentions  this may or may not answer the question of whether <code>(F oW)</code> holds.</p>
<p>It still depends whether you count an execution that reaches one of the <code>assert(0);</code> in the program as  well  an execution. If it count as an execution to which the formula should apply  then an execution that does not emit <code>W</code> and terminates on such an <code>assert(0);</code> certainly does not eventually output <code>W</code>.</p>
<p>There is an argument to say that it shouldn't count  however: although it is impossible to understand what these programs are supposed to do  assertions usually characterize illegal behaviors. Perhaps the LTL formulas are to be evaluated only on the legal  infinite behaviors of the system?</p>
<p>In the latter case  we are done: <code>(F oW)</code> holds because <code>W</code> is eventually emitted in all legal executions. In the former case  the additional question is whether an original <code>assert(0);</code> is reached before <code>W</code> is output  which is a simple reachability question on the instrumented <code>Problem1_terminate_at_W.c</code>. The answer is then that <code>(F oW)</code> does not hold  because there are plenty of <code>assert(0);</code> calls that are reached without <code>W</code> having been emitted.</p>
<p>We e-mailed the organizers and they said it was the first  simpler case. Only infinite executions need be taken into account for evaluating LTL formulas  and interrupted executions can be ignored. Our answer for the formula <code>(F oW)</code> of Problem 1 in the competition is therefore that the property holds.</p>
<h2>Proving that something does not eventually happen</h2>
<p>In the first part of the post  we set out to prove that the liveness property <code>(F oW)</code> was true. Verifying that the property is false requires a different approach.
With the toolset provided by Frama-C  I only see the scheme below. We reduce the problem to the seemingly harder but actually easier problem of computing the maximum number of iterations before W is emitted.</p>
<ol>
<li>Obtain the number N of reachable states for the system by running the value analysis on the original program.</li>
<li>Instrument the program  as previously making execution terminate when W is emitted  and also maintaining a transition counter (incremented at each iteration of the loop).</li>
<li>Using sound and complete propagation on the instrumented program  either reach a fixpoint (with a finite bound computed for the counter) or find an execution with N+1 transitions that does not emit <code>W</code>. The former case means that the program eventually emits <code>W</code>. The latter that  in the transition graph  there is a reachable cycle along which <code>W</code> is not emitted  and therefore an infinite execution that does not output <code>W</code>.</li>
</ol>
<p>This technique likely gets pretty heavy in practice. Adding a counter that may go up to N+1 to a system that already has N states can probably cause the new system to have of the order of N² states. This segues into at least one  perhaps two more blog posts  to answer the questions “what is the value analysis actually doing when it is applied on these questions?”  “how much work would it be to do the same thing closer to the metal  executing the C function as compiled instead of laboriously interpreting it?”  and “why don't we make a C model checker while we are at it?”</p>
{% endraw %}
