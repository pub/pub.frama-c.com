---
layout: post
author: Pascal Cuoq
date: 2013-03-13 19:54 +0200
categories: c11 c99 link undefined-behavior unspecified-behavior
format: xhtml
title: "Reading indeterminate contents might as well be undefined"
summary:
---
{% raw %}
<p>Warning: on a punctiliousness scale ranging from zero to ten, this post is a good nine-and-a-half. There was no tag for that, so I tagged it both “C99” and “C11”. The faithful reader will know what to expect. There is a bit of C90, too.</p>
<p>To summarize, it may appear that according to the letter of modern C standards, it is only dangerous to use uninitialized variables, instead of very dangerous. Nevertheless, this post shows that it does not matter what the standards say: compilers will bite you even when you are arguably right.</p>
<h2>Some context in the form of a link</h2>
<p>In 2012, Xi Wang wrote a nice blog post showing it is not a good idea to use an uninitialized variable as a source of additional entropy when trying to create a random seed.</p>
<p>“Xoring an uninitialized variable with whatever other source of entropy you already have cannot hurt”, the conventional thinking goes. Conventional thinking is wrong. Your typical modern compiler deletes the code that gathers the original entropy, since it is only going to be xored with an uninitialized variable. Hence the title of Xi Wang's blog post, <a href="http://kqueue.org/blog/2012/06/25/more-randomness-or-less/">More Randomness or Less</a>.</p>
<h2>In C90  “indeterminate” was simple</h2>
<p>In the nineties  real men were real men  C standards were short  and reading indeterminate contents(such as uninitialized variables) was listed in the very definition of “undefined behavior”:</p>
<blockquote><p>1.6 DEFINITIONS OF TERMS</p>
<p>
<strong>Unspecified behavior</strong> — behavior  for a correct program construct and correct data  for which the Standard imposes no requirements.</p>
<p>
<strong>Undefined behavior</strong> — behavior  upon use of a nonportable or   erroneous program construct  of erroneous data  or <strong>of   indeterminately-valued objects</strong>  for which the Standard imposes no   requirements.</p>
</blockquote>
<p>“Undefined behavior” means the compiler can do what it wants  so the behavior noticed by Xi Wang can in no way be held against a C90 compiler.</p>
<h2>In 1999  C standards became more complicated</h2>
<p>The C99 standard does not directly list “reading indeterminate contents” as undefined behavior. Instead  it defines indeterminate contents as “either an unspecified value or a trap representation”. Reading a trap representation causes undefined behavior (6.2.6.1:5). The nuance here is that the type <code>unsigned char</code> is guaranteed not to have any trap representations (and thus can always be used to read indeterminate contents).</p>
<h3>Less randomness : the simplified version</h3>
<p>“But my compilation platform does not have trap representations for type <code>int</code>  either  therefore I can use an uninitialized <code>int</code> variable and expect an  unspecified value (a much better prospect than undefined behavior)”  one may think. This line of reasoning is attractive. It could even explain the behavior shown in Xi Wang's blog post and reproduced in simplified form below:</p>
<pre>$ cat i.c
int f(int x)
{
  int u;
  return u ^ x;
}
$ gcc -O2 -std=c99 -S -fomit-frame-pointer i.c
$ cat i.s
…
_f:
Leh_func_begin1:
	ret
…
</pre>
<p>On this 64-bit platform  the argument <code>x</code> passed to <code>f()</code> is in register <code>%edi</code>  and the result of <code>f()</code> is expected in register <code>%eax</code>. Thus  by executing instruction <code>ret</code> directly  function <code>f()</code> is not even giving us back the entropy we provided it. It is instead giving us the current contents of <code>%eax</code>  which may not be random at all.</p>
<p>(Giving us back the entropy we passed to it would have been <code>mov %edi  %eax</code> followed by <code>ret</code>  a longer sequence.)</p>
<p>One may argue that the compiler has only opportunistically chosen the most convenient value for variable <code>u</code>  that is  <code>x</code> xored with the current contents of <code>%eax</code>  so that <code>u</code> xored with <code>x</code> is just the current contents of register <code>%eax</code>. This fits the interpretation of “unspecified value” for C99's definition of “indeterminate contents”. It is a good argument  but just wait until you have seen the next example.</p>
<h3>The next example</h3>
<pre>#include &lt;stdio.h&gt;
int main(int c  char **v)
{
  unsigned int j;
  if (c==4)
    j = 1;
  else
    j *= 2;
  printf("j:%u " j);
  printf("c:%d" c);
}
</pre>
<p>If fewer than three command-line arguments are passed to the program  it should display an unspecified <strong>even</strong> number for <code>j</code>  right?</p>
<pre>$ gcc -v
Using built-in specs.
Target: x86_64-linux-gnu
…
gcc version 4.4.3 (Ubuntu 4.4.3-4ubuntu5.1)
$ gcc -O2 t.c
$ ./a.out
j:1 c:1
</pre>
<p>GCC version 4.4.3 has decided that since the “else” branch was reading from an uninitialized variable <code>j</code>  only the “then” branch was worth compiling. This is acceptable if reading uninitialized variable <code>j</code> is <strong>undefined behavior</strong>  but not if it is <strong>unspecified behavior</strong>. Let us insist:</p>
<pre>$ gcc -Wall -O2 -std=c99 t.c
$ ./a.out
j:1 c:1
</pre>
<p>Although we are requesting the C99 standard to be followed by GCC  the program is not printing for variable <code>j</code> the even unspecified value we are entitled to.</p>
<p>(In passing  a proper static analyzer would know that if it is going to show variable <code>j</code> as containing <code>1</code>  it might as well show <code>c</code> as containing <code>4</code>. Also  a proper static analyzer would remind you that your program must  in essence  only be used with three command-line arguments. The reason compilers do not do this is covered elsewhere</a>)</p>
<h2>Between 1999 and 2011  C standards did not get shorter</h2>
<p>In 2007  Rich Peterson  working at HP  was disappointed to find that the “Not a Thing” (NaT) value that registers can have on the Itanium architecture could <a href="http://www.open-std.org/jtc1/sc22/wg14/www/docs/dr_338.htm">not be used to implement an uninitialized unsigned char</a>.</p>
<p>One thing led to another  and the C11 standard was amended with the phrase “If the lvalue designates an object of automatic storage duration that could have been declared with register storage class (never had its address taken)  and that object is uninitialized (not declared with an initializer  and no assignment to it has been performed prior to the use)  the behavior is undefined.”</p>
<p>That would have been my reaction too  if I was paid by the word. Anyway  this additional sentence re-introduces undefined behavior where there was none in C99.</p>
<p>In the example above  the address of <code>j</code> was never taken  so maybe that's GCC's excuse. Let us check:</p>
<pre>#include &lt;stdio.h&gt;
int main(int c  char **v)
{
  unsigned int j;
  unsigned int *p = &amp;j;
  if (c==4)
    j = 1;
  else
    j *= 2;
  printf("j:%u " j);
  printf("c:%d" c);
}
$ gcc -O2 t.c
$ ./a.out
j:1 c:1
</pre>
<p>No  GCC is still acting as if <code>j *= 2;</code> was undefined.</p>
<h2>Conclusion</h2>
<p>I am not saying that this is not a bug in GCC. Perhaps it was fixed in later versions (in fact  that version does not accept <code>-std=c11</code>  so it must be rather old). My thesis is that you might as well avoid reading from uninitialized variables <strong>as if it was undefined behavior</strong>  because otherwise compilers will bite you. This statement holds even if what we have witnessed here is a bug in GCC version 4.4.3.</p>
<p>Also  if this is a bug in GCC 4.4.3  this is the first time I identify a bug in GCC without the assistance of a random program generator. In other words  compiler bugs are rare  but they become surprisingly common if you stick to a strict interpretation of a necessarily ambiguous standard. And speaking of <a href="https://github.com/csmith-project/csmith">Csmith</a>  if there is indeed a GCC bug here  said bug cannot be detected with Csmith  which does not generate programs like mine.</p>
{% endraw %}
