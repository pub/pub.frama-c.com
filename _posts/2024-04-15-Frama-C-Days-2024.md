---
layout: post
author: Frama-C Team
date: 2024-04-15 12:00 +0100
categories: event cybersecurity
title: "Frama-C Days 2024 | June, 13 and 14 <br> Maison de la Radio et de la Musique"
image: /assets/img/blog/frama-c-days-2024/banner.jpg
---

After 5 years of absence, the Frama-C Days are back!

On June 13 and 14, will be held the Frama-C Days at Maison de la Radio et de la
Musique in Paris.

What to expect? Many exciting interventions from both academic and industrial
users of the Frama-C environment, for sharing experiences and discussing
perspectives: talks by the Frama-C community, talks about the current and
future developments by the Frama-C team, posters from junior contributors and
demos of new or experimental features.

The complete program is available through [this link](https://cea.invityou.com/frama-c-days).

Save the date and stay tuned! Registration to follow shortly.
