---
layout: post
author: Pascal Cuoq
date: 2013-10-09 16:06 +0200
categories: conversions-and-promotions undefined-behavior
format: xhtml
title: "The overflow when converting from float to integer is undefined behavior"
summary:
---
{% raw %}
<h2>Integer overflows in C</h2>
<p>A <a href="/2013/07/09/Arithmetic-overflows-in-Fluorine.html">previous post</a> on this blog was a reminder that in C  signed integer arithmetic overflow is undefined behavior. In contrast  the behavior of overflows in conversions from integer type to signed integer type is implementation-defined. The C99 standard allows for an implementation-defined signal to be raised  but in practice  the widespread compilation platforms provide two's complement behavior. And you can trust that they will continue to do so  because it's implementation-defined. Compiler makers cannot change their mind willy-nilly as if it was undefined behavior:</p>
<blockquote><p><strong>6.3.1.3</strong> Signed and unsigned integers</p>
<p>
<strong>1</strong> When a value with integer type is converted to another integer type other than <code>_Bool</code>  if the value can be represented by the new type  it is unchanged.</p>
<p>
<strong>2</strong> Otherwise  if the new type is unsigned  the value is converted by repeatedly adding or subtracting one more than the maximum value that can be represented in the new type until the value is in the range of the new type.</p>
<p>
<strong>3</strong> Otherwise  the new type is signed and the value cannot be represented in it; either the result is implementation-defined or an implementation-defined signal is raised.</p>
</blockquote>
<h2>Floating-point overflows in C</h2>
<p>The C standard does not mandate IEEE 754 floating-point arithmetic. Still  in practice  modern compilation platforms  if they provide floating-point features at all  provide either exactly IEEE 754 binary32 and binary64 formats and computations  or the same formats and a close approximation of the same computations.</p>
<p>IEEE 754 floating-point defines <code>+inf</code> and <code>-inf</code> values  so that any real number can be approximated in the target IEEE 754 format (albeit  when it ends up represented as an infinity  not precisely). This means that for C compilation platforms that implement IEEE 754 for floating-point  the condition “the value can be represented in the new type” is always true. There is no reason to worry of undefined behavior caused by overflow in either floating-point arithmetic or in the conversion of a <code>double</code> to a <code>float</code>.</p>
<p>Or indeed  in a constant. Consider GCC's warning here:</p>
<pre>$ cat t.c
#include &lt;stdio.h&gt;
int main()
{
  double big = 0x1.0p5000;
  printf("%f"  big);
}
$ gcc-172652/bin/gcc -std=c99 -Wall t.c &amp;&amp; ./a.out
t.c: In function ‘main’:
t.c:5:3: warning: floating constant exceeds range of ‘double’ [-Woverflow]
inf
</pre>
<p>The number 2^5000  represented in C as <code>0x1.0p5000</code>  is totally in the range of <code>double</code>  which goes up to <code>inf</code>. Clang similarly warns that “magnitude of floating-point constant too large for type double”. A proper warning message would be that 2^5000 cannot be represented precisely  instead of implying that it cannot be represented at all.</p>
<h2>Floating-point ↔ integer conversion overflows in C</h2>
<p>But enough pedantry contests with compilers.
The range of floating-point representations being what it is  we are left with only overflows in conversions from floating-point to integer to consider.</p>
<p>Suspense… (for the reader who did not pay attention to the title)</p>
<p>Overflows in conversions from floating-point to integer are undefined behavior. Clause 6.3.1.4 in the C99 standard make them so:</p>
<blockquote><p><strong>6.3.1.4</strong> Real floating and integer</p>
<p>
<strong>1</strong> When a finite value of real ﬂoating type is converted to an integer type other than <code>_Bool</code>  the fractional part is discarded (i.e.  the value is truncated toward zero). If the value of the integral part cannot be represented by the integer type  the behavior is undefined.</p>
</blockquote>
<p>What can happen in practice when a C program invokes this particular flavor of undefined behavior? It is as bad as dereferencing an invalid address  mostly harmless like signed integer arithmetic overflow  or what? Let us find out.</p>
<p>The following program converts the <code>double</code> representation of 2^31  the smallest positive integer that does not fit a 32-bit <code>int</code>  to <code>int</code>.</p>
<pre>int printf(const char *  ...);
int main()
{
  int i = 0x1.0p31;
  printf("%d"  i);
}
</pre>
<p>Frama-C's value analysis warns about undefined behavior in this program:</p>
<pre>$ frama-c -val t.c
warning: overflow in conversion of 0x1.0p31 (2147483648.)
   from floating-point to integer.
   assert -2147483649 &lt; 0x1.0p31 &lt; 2147483648;
</pre>
<blockquote><p>Fine-tuning the assertion <code>-2147483649 &lt; 0x1.0p31 &lt; 2147483648</code> was a riot  by the way. Do you see why?</p>
</blockquote>
<p>My aging (but still valiant) PowerPC-based Mac appears to think that saturation is the way to go: the variable <code>i</code> is set to <code>INT_MAX</code>:</p>
<pre>$ gcc -std=c99 t.c &amp;&amp; ./a.out
2147483647
</pre>
<p>Dillon Pariente was first to draw our attention to overflow in floating-point-to-integer conversions  which caused CPU exceptions on the target CPU for the code he was analyzing. I understood that target CPU to also be a PowerPC  so I suspect the behavior must be configurable on that architecture.</p>
<blockquote><p>Dillon Pariente's example was along the lines of <code>float f = INT_MAX; int i = f;</code> which is also hilarious if you are into that sort of humor.</p>
</blockquote>
<p>In order to really show how weird things can get on Intel processors  I need to modify the test program a bit:</p>
<pre>int printf(const char *  ...);
volatile double v = 0;
int main()
{
  int i1 = 0x1.0p31;
  int i2 = 0x1.0p31 + v;
  printf("%d %d"  i1  i2);
}
</pre>
<p>The <code>volatile</code> type qualifier precludes optimization  but there is no hardware or thread to change the value of variable <code>v</code>. The two expressions <code>0x1.0p31</code> and <code>0x1.0p31 + v</code> are both expressions of type <code>double</code> that evaluate to 2^31.</p>
<p>Still GCC and Clang  like a single compiler  think that these two expressions needn't result in the same value when converted to <code>int</code>:</p>
<pre>$ gcc t.c &amp;&amp; ./a.out
2147483647 -2147483648
$ clang  t.c &amp;&amp; ./a.out
2147483647 -2147483648
</pre>
<p>The results are different because one conversion was evaluated statically to be placed in <code>%esi</code> (2147483647) whereas the other was evaluated at run-time in <code>%edx</code> with the <code>cvttsd2si</code> instruction:</p>
<pre>$ clang -S -O t.c  &amp;&amp; cat t.s
...
_main:                                  ## @main
...
	movsd	_v(%rip)  %xmm0
	addsd	LCPI0_0(%rip)  %xmm0
	cvttsd2si	%xmm0  %edx
	leaq	L_.str(%rip)  %rdi
	movl	$2147483647  %esi       ## imm = 0x7FFFFFFF
	xorb	%al  %al
	callq	_printf
...
L_.str:                                 ## @.str
	.asciz	 "%d %d"
</pre>
<p>Only undefined behavior allows GCC and Clang to produce different values for <code>i1</code> and <code>i2</code> here: the values of these two variables are computed by applying the same conversion to the same original <code>double</code> number  and should be identical if the program was defined.</p>
<p>Generally speaking  <code>cvttsd2si</code> always produces <code>-0x80000000</code> in cases of overflow. That is almost like saturation  except that floating-point numbers that are too positive are wrapped to <code>INT_MIN</code>. One may think of it as saturating to either <code>-0x80000000</code> or <code>0x80000000</code>  and in the latter case  wrapping around to <code>-0x80000000</code> because of two's complement. I do not know whether this rationale bears any resemblance to the one Intel's engineers used to justify their choice.</p>
<p>So one might think that this is the end of the story: as long as the conversion is done at run-time on an Intel platform  the compiler uses the <code>cvttsd2si</code> instruction. Overflows  if overflows there are  “saturate to INT_MIN” as the convention is on this platform. This can be confirmed experimentally with the following program variant:</p>
<pre>#include &lt;stdio.h&gt;
#include &lt;stdlib.h&gt;
int main(int c  char **v)
{
  int i = 0x1.0p31 + strtod(v[1]  0);
  printf("%d"  i);
}
</pre>
<p>This new program takes a number from the command-line and adds it to 2^31  so that there is no opportunity for compile-time evaluation. We expect the conversion to saturate to <code>INT_MIN</code>  and it does:</p>
<pre>$ gcc -std=c99 t.c &amp;&amp; ./a.out 1234 &amp;&amp; ./a.out 12345 &amp;&amp; ./a.out 123456
-2147483648
-2147483648
-2147483648
</pre>
<p>Wait! It gets more amusing still. Let us change the program imperceptibly:</p>
<pre>int main(int c  char **v)
{
  unsigned int i = 0x1.0p32 + strtod(v[1]  0);
  printf("%u"  i);
}
</pre>
<p>The behavior of run-time overflow in the conversion from <code>double</code> to integer changes completely:</p>
<pre>$ gcc -m64 -std=c99 t.c &amp;&amp; ./a.out 1234 &amp;&amp; ./a.out 123456 &amp;&amp; ./a.out 12345678999
1234
123456
3755744407
</pre>
<p>But conversion saturates again  at zero this time  for the same program  when targeting IA-32:</p>
<pre>$ gcc -m32 -std=c99 t.c &amp;&amp; ./a.out 1234 &amp;&amp; ./a.out 123456 &amp;&amp; ./a.out 12345678999
0
0
0
</pre>
<blockquote><p>Do you have an explanation for this one? Leave a message in the comments section below. The fastest author of a complete explanation wins a static analyzer license.</p>
</blockquote>
<h2>Conclusion</h2>
<p>In conclusion  the overflow in the conversion from floating-point to integer is rather on the nasty side of C's undefined behavior spectrum. It may appear to behave consistently if the compilation targets an architecture where the underlying assembly instruction(s) saturate. Saturation is the behavior that compilers GCC and Clang implement when they are able to evaluate the conversion at compile-time. In these conditions  a lucky programmer may not actually observe anything strange.</p>
<p>The idiosyncrasies of other architectures may lead to very different results for overflowing conversions depending on parameters outside the programmer's control (constant propagation  for instance  is more or less efficient depending on the optimization level and may be difficult to predict  as we already <a href="/2013/07/15/More-on-FLT_EVAL_METHOD_2.html">complained about when discussing Clang targeting the 387 FPU</a>).</p>
<p>Acknowledgements: In addition to Dillon Pariente  I discussed this topic with Boris Yakobowski  John Regehr  Stephen Canon  and StackOverflow users tenos  Sander De Dycker and Mike Seymour prior to writing this blog post.</p>
{% endraw %}
