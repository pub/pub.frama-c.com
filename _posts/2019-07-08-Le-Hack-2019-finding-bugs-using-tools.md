---
layout: post
author: André Maroneze
date: 2019-07-08 20:11 +0200
categories: collaboration
image:
title: "Le Hack 2019: finding bugs, using tools"
published: false
---

The [SPARTA project](https://www.sparta.eu) held a booth in the
[Carrefour numérique²](http://www.cite-sciences.fr/fr/au-programme/lieux-ressources/carrefour-numerique2/)
at Cité des Sciences et de l'Industrie, in Paris, during
[Le Hack 2019](https://lehack.org/en/2019-edition/venue),
a large "hacking fest" in Paris, with several security-related talks,
workshops and challenges.
The SPARTA booth was there to disseminate information about
cybersecurity R&D in Europe and, among others, promote the usage of
formal methods and tools to help improve the security of computer systems.
We give here some feedback about what we observed, and how
we believe Frama-C can participate in the process.

## Hacking challenges

The sponsor area in Le Hack 2019 included several companies and French
government organizations presenting their works in cybersecurity, job offers,
and several [CTF-style wargames](https://en.wikipedia.org/wiki/Capture_the_flag#Computer_security)
for fun and profit. Prizes included, besides obvious swag points and potential
job interviews, security-related equipment, such as
[SDR dongles](https://en.wikipedia.org/wiki/Software-defined_radio)
and [IoT](https://en.wikipedia.org/wiki/Internet_of_things) analysis hardware.

For instance, one of such challenges presented itself as a Pastebin link to
a page containing a long hexadecimal string. The rest must be inferred by the
hacker. Other challenges include trying to login into deliberately vulnerable
servers, and finding out the meaning of obscure messages written in the event
badge itself.

## Coding challenge: bug finding

The SPARTA booth proposed several challenges, whose results were
announced on its [Twitter](https://twitter.com/sparta_eu) account.
Two such challenges were related to coding: given some faulty C programs,
try to find as many bugs as possible within the hour.
The first person to find a bug publishes a Github pull request with a fix and
earns a point. At the end of the hour, the participant with the most points
earns a token reward (e.g. a gift card).

Note that there are no restrictions whatsoever relating to the usage of
tools or external information: participants can user Google and whatever
tools they prefer. This includes no tools at all: a printed version of
part of the code was made available at the booth, for those who prefer
manual inspection.

The code was put on a
[Github repository](https://github.com/Frama-C/le-hack-2019), inside the
Frama-C group, which might serve as a hint for participants:
*maybe, just maybe, Frama-C can help finding these bugs*.
Of course, users are free to use whatever they want.
For instance, some of the bugs can be found with
[Valgrind](http://valgrind.org/),
[MemorySanitizer](https://clang.llvm.org/docs/MemorySanitizer.html), and other
static analysis tools.

Ideally, we would like the challenge to be exhaustive: *no bugs should remain
at the end*. However, scoring one point per bug is more fine-grained, and
therefore fairer, as in every effort is rewarded. In a more strict setting,
we may consider that a single bug is enough for a breach, so eliminating *all*
bugs is also a justifiable measure.

In our challenge, finding the bug is only part of the solution: the challenger
must also *fix* it, proposing a patch.
This means that, for instance, even if someone finds out that some of the bugs
were described in a
[previous blog post](http://blog.frama-c.com/index.php?post/2019/02/26/Finding-unexpected-bugs-in-the-DARPA-CGC-corpus),
they still need to provide their own patch, and then check that it does
not introduce new bugs.

## Experimental results

After explaining what is SPARTA, how tools and formal methods can help
cybersecurity, and presenting the challenge itself, we expected challengers to
consider using tools to help them. Our intended goal was to disseminate
better coding-related security practices, while helping programmers to learn
about new tools and techniques.

However, the results we obtained were quite different:

- Participants were eager to find the bugs by manual inspection of the source
  code;
- Using tools to find the bugs was interpreted by some as being "cheating".

Several factors contribute to this result:

1. The participants in question were either students or had recently graduated.
   Bug-finding as a manual exercice (that is, when you are given a piece of code
   that has a *deliberate* bug in it) is similar to the experience they had at
   school. Systematic usage of static analysis tools as part of a standard
   development process is rarely present in the curriculum;

2. Most hacking-related challenges (especially those of the kind "spot the bug")
   involve lots of manual work and small programms; even when they involve some
   automated parts (e.g., using Python scripts to quickly convert
   between hexadecimal and decimal formats), the "fun" in such challenges is
   more often derived from the intellectual, problem-solving effort than
   from the final result itself. Therefore our coding challenge was expected
   to work in a similar manner: participants did not expect to be able to solve
   most of it in a quasi-automated fashion.

3. Few visitors at the *Carrefour numérique²* had brought their laptops with
   them, to be able to go to Github download the files and run the tools.
   Indeed, the nearby workshops had desktops available for the participants.

Finally, it must be noted that the majority of hackers in Le Hack 2019
remained in the Wargame zone for the entire duration of the event,
or in the other area, where the talks, workshops and sponsor challenges
took place.

## Good practices help, even without formal justification

The winning participant was able to identify the 2 bugs present in the paper
version of the challenge (which contained only the initial portion of the code
of "HackJack", adapted from DARPA CGC's "WhackJack") and to propose solutions.
However, the bug due to lack of initialization was not justified in terms of the
C standard (e.g. "local variables are uninitialized by default, so we should
add an initializer"), but in terms of "I've been taught to always `bzero`
variables after declaration, as a good practice".

In practice, modern compilers may obtain similar results from both codes,
even though the `bzero` version is longer to write and more error-prone
(the length parameter, `n`, must be compatible with the buffer to be zeroed;
while the static initialization is handled by the compiler).

The main advantage of the "`bzero` pattern" is that it stipulates a more
robust and memorable mantra: whether the variable is local or global,
has just been declared or comes from the caller, the pattern will ensure
the memory is zeroed. For instance, some refactoring in which an
implicitly-initialized global variable becomes local can lead to
initialization issues. In this regard, the `bzero` approach is akin to
defensive programming.

Overall, given the complexity of the C language and the amount of footguns it
provides, it is important to ease the cognitive load of the programmer, be it
via a "mantra" ("always bzero after declaration"), or via tools which can help
detect such cases. We believe the latter approach is a better long-term
strategy, but we appreciate every effort towards more robust and bug-resistant
C code.

## Conclusion

Overall, being at Le Hack 2019 and proposing a few coding challenges has been
a positive experience, allowing us to get a glimpse of a part of the
programming community that we do not encounter very often. We got some ideas
for future challenges which, we hope, will help us highlight even better the
importance of formal methods and tools, not only for complex and critical
systems, but also in everyday hacking.
