---
layout: post
author: Virgile Prevosto
date: 2017-09-21 18:11 +0200
categories: collaboration
image:
title: "Frama-C appears on European Commission's Innovation Radar"
---

# tl;dr
Frama-C is among the 10 innovative products selected in the ["Tech for society" category](https://ec.europa.eu/futurium/en/innovation-radar-prize/tech-for-society). Be sure to cast your vote to help your favorite static analyzer making its way through the final in Budapest.

# Stance Project

Between 2012 and 2016, CEA Tech List was the coordinator of the [STANCE project](https://cordis.europa.eu/project/id/317753), funded by the European Union under the 7th Framework Programme (FP7). The main goal of STANCE was to show how formal verification tools, including of course Frama-C, could be used for assessing security properties in sensitive programs.

Among many other results, the project has seen the development of various Frama-C plug-ins by CEA Tech List, including Frama-Clang, E-ACSL, EVA, WP, Slicing and From.
Other partners (Dassault Aviation, Thales, Technical University of Graz, and Infineon) did use Frama-C over their own, and wrote themselves a certain number of plug-ins to help their analyses, while Search Labs provided a plug-in to combine Frama-C with their own fuzz testing tool, Flinder, in order to generate test cases triggering an alarm emitted by Value Analysis. Finally, Trusted Labs evaluated the benefits brought by Frama-C in the context of a Common Criteria security evaluation.

# EC's Innovation Radar

All in all, it seems that the project was evaluated very positively by the European Commission, which has just selected Frama-C among the [10  best innovators](https://ec.europa.eu/futurium/en/innovation-radar-prize/tech-for-society) in the Tech for Society category. We would like to thank all the participants to the STANCE project for this great achievement, and we hope that is a good omen for STANCE's successor, [VESSEDIA](https://cordis.europa.eu/project/id/731453), which gather many partners already present in STANCE and is more directed towards safety and security in industrial control systems.

# Call to the Public

Now, our presence in this list is not the end. Indeed, a public vote will now select 4 finalists who will defend their project in November in Budapest, where a jury will choose the winner. If you think that cybersecurity is important and formal methods such as advocated by Frama-C should play a major role in it, don't hesitate to [cast your vote](https://ec.europa.eu/futurium/en/tech-society/list-institute) (and share the link in all social networks you're connected to, of course). In the meantime, if you have project ideas in which Frama-C could fit, we would of course be glad to [hear from you](mailto:allan.blanchard@cea.fr,virgile.prevosto@cea.fr)
