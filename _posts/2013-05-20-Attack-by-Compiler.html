---
layout: post
author: Pascal Cuoq
date: 2013-05-20 11:21 +0200
categories: cybersecurity link undefined-behavior
format: xhtml
title: "Attack by Compiler"
summary: 
---
{% raw %}
<p>The title of this post, “Attack by Compiler”, has been at the back of my mind for several weeks. It started with a comment by jduck on a post earlier this year. The post's topic, the practical undefinedness of reading from uninitialized memory, and jduck's comment, awakened memories from a 2008 incident with the random number generator in OpenSSL.</p> 
<p>As I am writing this, if I google “attack by compiler”, the first page of results include the classic essay <a href="https://www.cs.cmu.edu/~rdriley/487/papers/Thompson_1984_ReflectionsonTrustingTrust.pdf">Reflections on Trusting Trust</a> by Ken Thompson  Wikipedia's definition of a <a href="http://en.wikipedia.org/wiki/Backdoor_(computing)">backdoor in computing</a>  an <a href="http://www.acsa-admin.org/2005/abstracts/47.html">article</a> by David A. Wheeler for countering the attack described by Thompson  a <a href="http://www.schneier.com/blog/archives/2006/01/countering_trus.html">commentary</a> by Bruce Schneier on Wheeler's article  and a Linux Journal <a href="http://www.linuxjournal.com/article/7929">article</a> by David Maynor on the practicality of the attack described by Thompson on the widespread GNU/Linux platform.</p> 
<p>This post is about a slightly different idea.</p> 
<h2>Initial conditions: trustworthy compiler  peer-reviewed changes</h2> 
<p>Suppose that we start with a trustworthy compiler  widely distributed in both source and binary form. Some people are in the position to make changes to the compiler's source code and could  like Thompson in his essay  attempt to insert a backdoor in the compiler itself.</p> 
<p>But now  each change is scrutinized by innumerable witnesses from the Open-Source community. I say “witnesses”  in fact  they  are  mostly students   but  we  will probably be forced to assume that they can't all be mischievous.</p> 
<p>The attackers could try to obfuscate the backdoor as they insert it  but the problem is that some of the witnesses are bound to possess this character trait that the less they understand a change  the more they investigate it. Furthermore  once these witnesses have uncovered the truth  loss of credibility will ensue for the person who tried to sneak the backdoor in. This person will lose eir commit privilege to the compiler's sources  and people will recover untainted compilers in source and binary form from their archives. This kind of approach is risky and may only result in a temporary advantage—which may still be enough.</p> 
<h2>The underhanded approach</h2> 
<p>The 2013 edition of the <a href="http://underhanded-c.org/_page_id_25.html">Underhanded C Contest</a> is under way. The contest defines underhanded code as:</p> 
<blockquote><p>code that is as readable  clear  innocent and straightforward as possible  and yet [fails] to perform at its apparent function</p> 
</blockquote> 
<p>Underhandedness is exactly what an attacker with commit access to the source code of the widely used compiler should aim for. If the commit is underhanded enough  the committer may not only enjoy full <strong>deniability</strong>  but ey may obtain that the incriminating change <strong>stays in ulterior versions</strong> of the compiler  as a “good” change. This implies that all affected security-sensitive applications  like the “login” program in Thompson's essay  must be updated to work around the now official backdoor in the compiler. In this scenario  even after the attack has been discovered  anytime someone unknowingly compiles an old version of “login” with a recent compiler  it's another win for the attacker.</p> 
<p>Fortunately  we agree with Scott Craver that the C programming language is a very good context to be underhanded in  and a C compiler is even better. How about the following ideas?</p> 
<ol> 
<li>making pseudo-random number generators that rely on uninitialized memory <strong>less random</strong>  in the hope that this will result in weaker cryptographic keys for those who do not know about the flaw;</li> 
<li>optimizing a NULL test out of kernel code when it is one of several <strong>defenses that need to be bypassed</strong>;</li> 
<li>optimizing <code>buffer + len &gt;= buffer_end || buffer + len &lt; buffer</code> overflow tests out from application code  so that <strong>buffer overflows do take place</strong> in code that is guarded thus;</li> 
<li>optimizing source code that was written to take constant-time into binary code that <strong>reveals secrets by terminating early</strong>.</li> 
</ol> 
<p>I am not being very original.  According to this <a href="http://kqueue.org/blog/2012/06/25/more-randomness-or-less/">post</a> by Xi Wang  idea (1) is only waiting for someone to give the compiler one last well-calibrated shove. The NULL test optimization was already implemented in the compiler when it was needed for a famous Linux kernel <a href="http://lwn.net/Articles/342330/">exploit</a>. The interesting scenario would have been if someone had found that the code in <code>tun_chr_poll()</code> was almost exploitable and had submitted the GCC optimization to activate the problem  but it did not happen in this order. Idea (3) really <a href="http://lwn.net/Articles/278137/">happened</a>.</p> 
<p>Idea (4) has not been exploited that I know of  but it is only only a matter of time. If I google for “constant-time memcmp”  I may find an implementation such as follows:</p> 
<pre>int memcmp_nta(const void *cs  const void *ct  size_t count) 
{ 
  const unsigned char *su1  *su2; 
  int res = 0; 
  for (su1 = cs  su2 = ct; 0 &lt; count; ++su1  ++su2  count--) 
  res |= (*su1 ^ *su2); 
  return res; 
} 
</pre> 
<p>Nothing in the C language definition forces a compiler to compile the above function into a function that does not return as soon as variable <code>res</code> contains <code>(unsigned char)-1</code>  not to mention the possibilities if the compiler first inlines the function in a site where its result is only compared to 0  and then optimizes the code for early termination. If I was trying to sneak in a compiler change that defeats the purpose of this <code>memcmp_nta()</code> function  I would bundle it with auto-vectorization improvements. It is a fashionable topic  and quite exciting if one does not care about non-functional properties such as execution time.</p> 
<h2>Conclusion</h2> 
<p>The impracticability of the described attack is counter-balanced by some unusual benefits: at the time of the attack  someone may already have audited the pseudo-random number generator or function <code>memcmp_nta()</code> we used as examples. The audit may have considered both source and generated assembly code  and involved actual tests  at a time when the code was “safely” compiled. But the auditor is not going to come back to the review again and again each time a new compiler comes out. Like Monty Python's <a href="http://www.youtube.com/watch?v=Tym0MObFpTI">Spanish Inquisition</a>  nobody expects the compiler-generated backdoor.</p> 
<p>Three of my four examples involve undefined behavior. More generally  my examples all involve unwarranted programmer expectations about C idioms. This is the key to plausibly deniable compiler changes that reveal latent security flaws. What other unwarranted expectations should we take advantage of for an “attack by compiler”?</p>
{% endraw %}
