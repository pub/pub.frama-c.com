#!/bin/sh -eu

JSON_API="git@git.frama-c.com:frama-c/frama-c-api-json.git"

export GIT_SSH=$PWD/ci_scripts/ssh.sh
echo "$JSON_API_READ_KEY" | base64 -d > ci_scripts/id_ed25519
chmod 400 ci_scripts/id_ed25519
git clone $JSON_API _data/_api
