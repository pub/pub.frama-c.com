#!/bin/sh -eu

FRAMAC_MASTER="https://git.frama-c.com/api/v4/projects/780/repository/files"

chmod +x ./generator/generate
curl $FRAMAC_MASTER/Changelog/raw?ref=master > ./assets/Changelog
curl $FRAMAC_MASTER/src%2Fplugins%2Fe-acsl%2Fdoc%2FChangelog/raw?ref=master >> ./assets/Changelog
curl $FRAMAC_MASTER/src%2Fplugins%2Fwp%2FChangelog/raw?ref=master >> ./assets/Changelog
./generator/generate ./assets/Changelog -o ./html/changelog.html
