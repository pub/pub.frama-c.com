---
layout: default
date: 06-10-2022
short_title: Frama-Clang 0.0.13
title: Release of Frama-Clang 0.0.13
internal_link: /fc-plugins/frama-clang.html
---

Frama-Clang 0.0.13 is out. Download it [here](/fc-plugins/frama-clang.html).
