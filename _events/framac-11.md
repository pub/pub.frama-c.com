---
layout: default
date: 06-03-2015
short_title: Frama-C 11.0 (Sodium)
title: Release of Frama-C 11.0 (Sodium)
internal_link: /fc-versions/sodium.html
---

Frama-C 11.0 (Sodium) is available.

This new major version includes too many bug fixes and improvements to
list here: details are available at http://frama-c.com/Changelog.html.

The main highlights are:

- **kernel**: Frama-C standard library is included by default (-no-frama-c-stdlib
    for the previous behavior)
- **kernel**: When preprocessor supports it, expansions of macros in
   annotations (-pp-annot) is now done by default. Efficiency of
   -pp-annot has been greatly improved.
- **kernel**: the default machdep no longer assumes the compiler is gcc. See
    'frama-c -machdep help'
- Homogenization of collections options (eg: -cpp-extra-args, -slevel-function)
- **value**: much-improved pretty-printing of pointer abstract values
- **value**: logic ranges are now evaluated using a dedicated domain,
    resulting in faster analysis and more precise results
- **value**: the parameters of a function call may be reduced if they are
    constrained by the callee
- **kernel, value**: support for \dangling predicate
- **kernel, value, WP**: support for const variables
- **rte, value**: alarms are now emitted for casts from floating-point to integer
    that overflow
- **from**: assigns/from acsl clauses of functions with a body can now be verified
    through option -from-verify-assigns
- **from**: major performance improvements on large input programs.
- **semantic constant folding**: better propagation on pointer variables

For plug-in developers:

- New API for collections options
- New AST nodes Throw and TryCatch for dealing with exception. The C
   front-end does not generate any such node. A code transformation can
   compile such nodes away if needed (see src/kernel/exn_flow.mli for
   more information).
