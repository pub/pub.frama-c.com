---
layout: default
date: 01-12-2016
short_title: Frama-C 14.0 (Silicon)
title: Release of Frama-C 14.0 (Silicon)
internal_link: /fc-versions/silicon.html
---

Frama-C 14.0 (Silicon) is available.

Main changes are:

Kernel:

- refactoring of ACSL extensions + allow extensions in loop annotations
- rename multiple types of the logic AST for more coherence

Libc:

- The implementations of some functions of the standard library are now available in share/libc/*.c. The .c and .h files in share/libc are deprecated.

Eva/Value plugin:

- two now (experimental) analysis domains are available. Gauges infer affine relations between variables in loops. Symbolic locations keep track of the contents of l-values such as *p or t[i].
- new builtins are available for dynamic allocation, and some functions of string.h and. Default builtins can be activated through option -val-builtins-auto.

WP plugin:

- unified variable usage for all models
- WP now honors the kernel option -warn-(signed|unsigned)-(overflow|downcast). The cint and cfloat are used by default

Rte plugin:

- new option -rte-pointer-call, to generate annotations for calls through function pointers.

Nonterm plugin:

- overall increase in precision, especially on compound statements (if, switch, loops...).

Changes in the compilation process:

- OCamlGraph is no longer packaged within Frama-C, and must be installed to build Frama-C from source.
- OCaml version greater or equal than 4.02.3 required..

A complete changelog can be found [here](/html/changelog.html#Silicon-14.0).
