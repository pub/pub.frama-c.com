---
layout: default
date: 15-11-2024
short_title: frama-clang v0.0.17~beta
title: frama-clang v0.0.17~beta for Frama-C 30.0~ Zinc
internal_link: /fc-plugins/frama-clang.html
---

[frama-clang](/fc-plugins/frama-clang.html) [v0.0.17~beta](https://git.frama-c.com/pub/frama-clang/-/releases/0.0.17-beta) is out.

Frama-Clang is an experimental C++ front-end for Frama-C, based on the clang compiler
See its [homepage](/fc-plugins/frama-clang.html) for more information.

Main changes in this release include:

- Compatibility with Frama-C 30 Zinc
