---
layout: default
date: "26-10-2023"
short_title: Frama-C 28.0~beta (Nickel)
title: Beta release of Frama-C 28.0~beta (Nickel)
internal_link: /fc-versions/nickel.html
---

Frama-C 28.0~beta (Nickel) is out. Download it [here](/fc-versions/nickel.html).

Main changes with respect to Frama-C 27 (Cobalt) include:

#### Kernel
- Frama-C can now generate more default clauses (in particular terminates and exits)
- Removed deprecated options `-no-type` and `-no-obj`

#### Alias
- New plugin Alias, implements a points-to analysis

#### E-ACSL
- More efficient code arithmetic calculations

#### Eva
- Support for simple `\let` bindings
- Removed deprecated Db.Value API
- Fixed unsoundness about initialization with goto and switch

#### WP
- New ACSL extensions for defining automatic proof strategies
- WP generates default exits and terminates, removed old options `-wp-*-terminate`
- Fixed cache for interactive provers

#### Ivette
- Basic component for WP
- Many bug fixes
