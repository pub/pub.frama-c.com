---
layout: default
date: 12-04-2022
short_title: Frama-Clang 0.0.12
title: Release of Frama-Clang 0.0.12
internal_link: /fc-plugins/frama-clang.html
---

Frama-Clang 0.0.12 is out. Download it [here](/fc-plugins/frama-clang.html).
