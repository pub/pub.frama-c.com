---
layout: default
date: 17-09-2019
short_title: Frama-C 19.1
title: Release of Frama-C 19.1 (Potassium)
internal_link: /fc-versions/potassium.html
---

Frama-C 19.1 (Potassium) is out. Download it [here](/fc-versions/potassium.html).

This minor release merely restores compatibility with OCaml 4.08.1 (and the new
4.09.0), and fixes a few issues with lablgtk3.
