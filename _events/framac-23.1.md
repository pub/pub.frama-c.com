---
layout: default
date: "21-07-2021"
short_title: Frama-C 23.1 (Vanadium)
title: Release of Frama-C 23.1 (Vanadium)
internal_link: /fc-versions/vanadium.html
---

Frama-C 23.1 (Vanadium) is out. Download it [here](/fc-versions/vanadium.html).

Main changes with respect to Frama-C 23.0 (Vanadium) include:

#### E-ACSL

- Fix crash related to several ACSL constructs
- Fix crash when raising some user errors


#### WP

- Fix a crash related to opaque structures memory typing
