---
layout: default
date: 29-11-2017
short_title: Frama-C 16.0 (Sulfur)
title: Release of Frama-C 16.0 (Sulfur)
internal_link: /fc-versions/sulfur.html
---

Frama-C 16.0 (Sulfur) is out. Download it [here](/fc-versions/sulfur.html).


Main changes with respect to Frama-C 15 - Phosphorus include:

#### Kernel
- Extra type checking verifications
  (e.g. const on local variables, qualifiers in function calls)

#### EVA
- Precision and efficiency improvements
- Better feedback for abstract domains
- Scripts to help analyze large programs
  (in $FRAMAC_SHARE/analysis-scripts)

#### WP
- New tacticals for TIP
  (for dealing with modulus, bit operations, equality rewriting, etc)
- Several new simplifications

#### RTE
- Emission of more alarms (\initialized)

#### Studia
- New plug-in for case studies with EVA, integrated in the GUI

#### GUI
- Display of local callgraphs (useful for large programs)

A complete changelog can be found [here](/html/changelog.html#Sulfur-16.0).
