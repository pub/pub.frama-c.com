---
layout: default
date: 25-06-2020
short_title: Frama-C 21.1
title: Release of Frama-C 21.1 (Scandium)
internal_link: /fc-versions/scandium.html
---

Frama-C 21.1 (Scandium) is out. Download it [here](/fc-versions/scandium.html).

This minor release fixes a few issues in WP.
