---
layout: default
date: 21-12-2017
short_title: Frama-Clang 0.0.4
title: Release of Frama-Clang 0.0.4
internal_link: /fc-plugins/frama-clang.html
---

Frama-Clang 0.0.4, compatible with Frama-C 16, is out.
Download it [here](/fc-plugins/frama-clang.html).
