---
layout: default
date: 02-08-2017
short_title: Frama-Clang 0.0.3
title: Release of Frama-Clang 0.0.3
internal_link: /fc-plugins/frama-clang.html
---

Frama-Clang 0.0.3, compatible with Frama-C 15, is out.
Download it [here](/fc-plugins/frama-clang.html).
