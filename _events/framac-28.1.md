---
layout: default
date: "01-03-2024"
short_title: Frama-C 28.1 (Nickel)
title: Release of Frama-C 28.1 (Nickel)
internal_link: /fc-versions/nickel.html
---

Frama-C 28.1 (Nickel) is out. Download it [here](/fc-versions/nickel.html).

Main changes with respect to Frama-C 28.0 (Nickel) include:

#### Kernel

- Fix Cil.isConstant on lvalues with offset.

#### Ivette

- Fix Ivette shell wrapper on macOS.

#### WP

- Fix interactive prover startup.
