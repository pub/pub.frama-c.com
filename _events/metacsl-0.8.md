---
layout: default
date: 11-12-2024
short_title: MetAcsl v0.8
title: MetAcsl v0.8 for Frama-C 30.0 Zinc
internal_link: /fc-plugins/metacsl.html
---

[MetAcsl](/fc-plugins/metacsl.html) [v0.8](https://git.frama-c.com/pub/meta/-/releases/0.8) is out.

MetAcsl intends to provide simple and compact ways to express properties that
would demand peppering the code with thousands of annotations in plain ACSL.
Its main use cases focus on security properties (notably ensuring that
write and read accesses to sensitive memory locations are guarded appropriately).
See its [homepage](/fc-plugins/metacsl.html) for more information.

Main changes in this release include:

- compatibility with Frama-C 30.0 Zinc
