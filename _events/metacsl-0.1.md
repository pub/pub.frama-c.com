---
layout: default
date: 09-12-2020
short_title: MetAcsl 0.1
title: First release of MetAcsl plugin
internal_link: /fc-plugins/metacsl.html
---

Following the release of Frama-C 22.0 (Titanium), [MetAcsl](/fc-plugins/metacsl.html) [v0.1](https://git.frama-c.com/pub/meta/-/releases/0.1) is out and ready to be installed via `opam install frama-c-metacsl`.

MetAcsl intends to provide simple and compact ways to express properties that
would demand peppering the code with thousands of annotations in plain ACSL.
Its main use cases focus on security properties (notably ensuring that
write and read accesses to sensitive memory locations are guarded appropriately).
Feel free to consult its [homepage](/fc-plugins/metacsl.html) for more information.
