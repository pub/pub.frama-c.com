---
layout: default
date: 31-05-2017
short_title: Frama-C 15.0 (Phosphorus)
title: Release of Frama-C 15.0 (Phosphorus)
internal_link: /fc-versions/phosphorus.html
---

Frama-C 15.0 (Phosphorus) is out. Download it [here](/fc-versions/phosphorus.html).

Main changes with respect to Frama-C 14 - Silicon include:

#### Kernel
- E-ACSL is now included in the standard distribution
- Better handling of Variable-Length Arrays (VLA)
- ZArith is now a required dependency. Support of Big_int has been dropped
- Bash and Zsh completion for Frama-C options
- new AST nodes to explicitly mark local variable initialization

#### EVA
- better set of default options
- dropped support for legacy version of Value Analysis

#### WP
- Interactive Proof Editor in the GUI
- Extensible Proof Engine via Tactics and Strategies
- More powerful simplifications of goals
- Dynamic API is deprecated in favor of static API
- Fatally flawed support of generalized invariants (`-wp-invariants`)
has been dropped

#### E-ACSL
- included in the standard Frama-C distribution
- use of a (much more efficient) shadow memory model by default
- much better support of unstructured control flow (complex goto, ...)

#### Variadic
- translation of variadic calls is now enabled by default
- option names have changed to avoid confusion with EVA

A complete changelog can be found [here](/html/changelog.html#Phosphorus-15.0).
