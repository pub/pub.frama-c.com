---
layout: default
date: 07-09-2023
short_title: Frama-Clang 0.0.14
title: Release of Frama-Clang 0.0.14
internal_link: /fc-plugins/frama-clang.html
---

[Frama-Clang](/fc-plugins/frama-clang.html) 0.0.14 is out. You can download the sources [here](https://git.frama-c.com/pub/frama-clang/-/releases/0.0.14).

This release brings compatibility with Frama-C 27, Clang 15 and 16 (but removes support for Clang 10).
In addition, it has now an official `opam` [package](https://opam.ocaml.org/packages/frama-clang/).
