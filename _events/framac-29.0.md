---
layout: default
date: "06-06-2024"
short_title: Frama-C 29.0 (Copper)
title: Release of Frama-C 29.0 (Copper)
internal_link: /fc-versions/copper.html
---

Frama-C 29.0 (Copper) is out. Download it [here](/fc-versions/copper.html).

Main changes with respect to Frama-C 28 (Nickel) include:

#### Kernel
- Introduce \plugin:: prefix for ACSL extensions
- Refactor current location handling mechanism
- Removal of Db (Db.Main.extend is deprecated). Features related to asynchronous
  interactions are now handled in module Async
- Various fixes and improvements

#### Alias
- Better analysis results in the presence of structures
- Rework the API and improved the documentation

#### Eva
- Better reporting for garbled mix
- Improved handling of \valid and \valid_read

#### E-ACSL
- Fix TLS segment start address and size
- Remove option -e-acsl-version

#### WP
- Generation of counter examples, see option -wp-counter-examples
- Upgrade to Why3 1.7.x and improved prover selection
- Extended support for Ivette

#### Ivette
- Revamped workspace (tabs, views, dock, alerts, notifications, …)
- Types and Globals navigation
- Better feedback on Eva values evaluation
- Extended support for WP
- Improve performances
