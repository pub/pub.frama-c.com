---
layout: default
date: "08-08-2024"
short_title: The Frama-C Book is available
title: The Guide to Software Verification with Frama-C is available
external_link: https://link.springer.com/book/10.1007/978-3-031-55608-1
---

The Guide to Software Verification with Frama-C is now available. This book
gathers contributions from 39 authors. We hope that it will help you with
code analysis using Frama-C!

#### Abstract

Frama-C is a popular open-source toolset for analysis and verification of C
programs, largely used for teaching, experimental research, and industrial
applications. With the growing complexity and ubiquity of modern software,
there is increasing interest in code analysis tools at various levels of
formalization to ensure safety and security of software products. Acknowledging
the fact that no single technique will ever be able to fit all software
verification needs, the Frama-C platform features a wide set of plug-ins that
can be used or combined for solving specific verification tasks.

This guidebook presents a large panorama of basic usages, research results, and
concrete applications of Frama-C since the very first open-source release of the
platform in 2008. It covers the ACSL specification language, core verification
plug-ins, advanced analyses and their combinations, key ingredients for
developing new plug-ins, as well as successful industrial case studies in which
Frama-C has helped engineers verify crucial safety or security properties.

**[Click here to reach the book]({{ page.external_link }})**.
