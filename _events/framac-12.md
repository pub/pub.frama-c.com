---
layout: default
date: 15-01-2016
short_title: Frama-C 12.0 (Magnesium)
title: Release of Frama-C 12.0 (Magnesium)
internal_link: /fc-versions/magnesium.html
---

Frama-C 12.0 (Magnesium) is available.

This new major version includes too many bug fixes and improvements to
list here: details are available at http://frama-c.com/Changelog.html.

The main highlights are:

  - **value**: brand new GUI, found in the "Values" tab
  - **value**: new builtins for floating-point functions of the standard library
  - **value**: more fine-grained control on the value of padding after initialisation
  - **value**: multiple improvements to option -subdivide-float-var
  - **wp**: many improvements for user experience (see Changelog)
  - **wp**: many new or improved simplifications in Qed (see Changelog)
  - **wp**: support for global const (see -wp-init-const option)
  - **wp**: refined memory access and compound encoding
  - **wp**: new memory model 'Caveat' for unit-proofs
  - **wp**: new (less precise) integer model 'rg' to simplify integral ranges
  - **wp**: more ACSL builtins (\subset, \is_NaN, \is_finite, \is_infinite, \is_plus_infinity, \is_minus_infinity)
  - **report**: new report in .csv format
