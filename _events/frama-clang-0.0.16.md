---
layout: default
date: 05-09-2024
short_title: Frama-Clang v0.0.16
title: Frama-Clang v0.0.16 for Frama-C 29.0 Copper
link: /fc-plugins/frama-clang.html
---

[Frama-Clang](/fc-plugins/frama-clang.html) [v0.0.16](https://git.frama-c.com/pub/frama-clang/-/releases/0.0.16) is out.

Frama-Clang is an experimental C++ front-end for Frama-C, based on the clang compiler
See its [homepage](/fc-plugins/frama-clang.html) for more information.

Main changes in this release include:

- Better handling of ACSL constructions
- Compatibility with Clang 18
- Compatibility with Frama-C 29 Copper
