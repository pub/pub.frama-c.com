---
layout: default
date: 08-04-2022
short_title: LTest is out
title: Release of LTest 0.1
internal_link: /fc-plugins/ltest.html
---

The LTest toolset is available in `opam`. More information [here](/fc-plugins/ltest.html).
