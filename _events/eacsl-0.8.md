---
layout: default
date: 17-01-2017
short_title: E-ACSL 0.0.8
title: Release of E-ACSL 0.0.8
---

Version 0.8 of the E-ACSL plugin is available.