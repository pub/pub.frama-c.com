---
layout: default
date: "28-04-2023"
short_title: Cyberhackathon Frama-C + Binsec
title: Cyberhackathon Frama-C + Binsec
internal_link: /2023/03/27/cyberhackathon-frama-c-binsec.html
---

If you are near Paris, come to the Cyber-hackathon **Frama-C + [Binsec](https://binsec.github.io)**, on **28/04** from **9h to 17h**, at [CEA List](https://list.cea.fr), in the Paris-Saclay campus ([Nano-Innov, 2 bd Thomas Gobert, 91120 Palaiseau](https://www.openstreetmap.org/?mlat=48.71264&mlon=2.19218#map=17/48.71264/2.19218&layers=H))!

**Registration is closed**
(the form is in French, but feel free to contact us directly in English if you prefer) or, **for more details, [click here](/2023/03/27/cyberhackathon-frama-c-binsec.html)**.
