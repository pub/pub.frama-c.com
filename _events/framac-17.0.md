---
layout: default
date: 31-05-2018
short_title: Frama-C 17.0 (Chlorine)
title: Release of Frama-C 17.0 (Chlorine)
internal_link: /fc-versions/chlorine.html
---

Frama-C 17.0 (Chlorine) is out. Download it [here](/fc-versions/chlorine.html).

Main changes with respect to Frama-C 16 - Sulfur include:

#### Kernel
*   Added option -inline-calls for syntactic inlining
*   Introduced warning categories: possibility of disabling some warnings, converting warnings into errors and vice-versa, and more detailed warning messages
*   Added support for CERT EXP46-C
*   Extra-type checking verifications (e.g. in function pointer compatibility and lvalues assignments)
*   Added support for JSON compilation databases, a.k.a. compile_commands.json (optional: requires package 'yojson')

#### EVA
*   Added support for infinite floats and NaN (via option -warn-special-float)
*   Added a new panel "Red alarms" in the GUI that shows all properties for which a red status has been emitted for some states during the analysis. They may not be completely invalid, but should often be investigated first
*   Evaluate the preconditions of the functions for which a builtin is used; builtins do not emit alarms anymore
*   The subdivision of evaluations (through the option -val-subdivide-non-linear) can subdivide the values of several lvalues simultaneously (on expressions such as x*x - 2*x*y + y*y)
*   Various improvements in the equality domain which is now inter-procedural (equalities can be propagated through function calls)

#### WP
*   Support for ACSL math builtins (\sqrt, \exp, \log, etc.) and _Bool type
*   Improved Qed simplifications in many domains
*   Upgrade reference versions for provers (Alt-Ergo 2.0.0, Coq 8.7.1 and Why-3 0.88.3)
*   New and/or enhanced tactics available from the graphical user-interface
*   Searching for strategies from the command line

#### E-ACSL
*   New option -e-acsl-validate-format-strings to detect format string vulnerabilities in printf-like functions

A complete changelog can be found [here](/html/changelog.html#Chlorine-17.0).
