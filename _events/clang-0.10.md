---
layout: default
date: 08-03-2021
short_title: Frama-Clang 0.0.10
title: Release of Frama-Clang 0.0.10
internal_link: /fc-plugins/frama-clang.html
---

Frama-Clang 0.0.10 is out. Download it [here](/fc-plugins/frama-clang.html).
