---
layout: default
date: "18-07-2023"
short_title: Frama-C 27.1 (Cobalt)
title: Release of Frama-C 27.1 (Cobalt)
internal_link: /fc-versions/cobalt.html
---

Frama-C 27.1 (Cobalt) is out. Download it [here](/fc-versions/cobalt.html).

Main changes with respect to Frama-C 27.0 (Cobalt) include:

#### Kernel
- Fixes a crash and a freeze in the GTK GUI
- Add a wrapper in `frama-c-script` for `make_machdep.py`

#### Ivette
- Fixes a crash with multiple instances of Ivette
