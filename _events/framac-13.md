---
layout: default
date: 31-05-2016
short_title: Frama-C 13.0 (Aluminium)
title: Release of Frama-C 13.0 (Aluminium)
internal_link: /fc-versions/aluminium.html
---

Frama-C 13.0 (Aluminium) is available.

This major version fixes many bugs and includes improvements
and three new plug-ins.

The main highlights are:

- **variadic**: new plug-in which translates variadic functions, calls and
  macros to allow analyses to handle them more easily.
- **loop**: new plug-in which estimates loop bounds and -slevel-function
  parameters.
- **nonterm**: new plug-in for detection of definite non-termination based on
  Value.
- **value**: major reimplementation of large parts of the plugin. New analysis
  domains are available (see options -eva-equality-domain and
  -eva-bitwise-domain), and the analysis of conditionals has been
  improved. 'direct' and 'indirect' annotations are now used to evaluate
  assigns clauses. Better propagation strategy for nested loops and changes
  in the widening strategy for frontiers of integer types.
- **cil**: various improvements to the handling of empty structs, zero-length
  arrays, and flexible array members.
- **kernel**: automatic generation of assigns from GCC's extended asm.
- **ACSL**: new predicate \valid_function, requiring the compatibility
  between the type of the pointer and the function being pointed (currently
  supported by Value), notation { x, y, z } for defining sets and built-in
  operators for lists (currently supported by WP).
