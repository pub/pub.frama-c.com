---
layout: default
date: 23-07-2018
short_title: Frama-Clang 0.0.6
title: Release of Frama-Clang 0.0.6
internal_link: /fc-plugins/frama-clang.html
---

Frama-Clang 0.0.6, compatible with Frama-C 17 Chlorine, is out.
Download it [here](/fc-plugins/frama-clang.html).
