---
layout: default
date: 07-11-2023
short_title: MetAcsl 0.5
title: MetAcsl for Frama-C 27.x Cobalt
internal_link: /fc-plugins/metacsl.html
---

Following the release of Frama-C 27.0 (Cobalt), [MetAcsl](/fc-plugins/metacsl.html) [v0.5](https://git.frama-c.com/pub/meta/-/releases/0.5) is out. The corresponding `opam` package should be available soon.

MetAcsl intends to provide simple and compact ways to express properties that
would demand peppering the code with thousands of annotations in plain ACSL.
Its main use cases focus on security properties (notably ensuring that
write and read accesses to sensitive memory locations are guarded appropriately).
Feel free to consult its [homepage](/fc-plugins/metacsl.html) for more information.

Main changes in this release include
- new meta-variables `\lhost_read` and `\lhost_written` for `\reading` and `\writing` contexts respectively
- new option `-check-callee-assigns f` to treat calls to `f` as writing to the locations mentioned in `f`'s `assigns` and reading to its `\from`
