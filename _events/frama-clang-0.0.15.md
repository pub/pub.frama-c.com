---
layout: default
date: 25-03-2024
short_title: Frama-Clang v0.0.15
title: Frama-Clang v0.0.15 for Frama-C 28.0 Nickel
internal_link: /fc-plugins/frama-clang.html
---

[Frama-Clang](/fc-plugins/frama-clang.html) [v0.0.15](https://git.frama-c.com/pub/frama-clang/-/releases/0.0.15) is out.


See its [homepage](/fc-plugins/frama-clang.html) for more information.

Main changes in this release include:

- Better handling of mixed C/C++ code and `extern "C"` declarations
- Compatibility with Clang 17
- Compatibility with Frama-C 28.x Nickel
