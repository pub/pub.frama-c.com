---
layout: default
date: "15-02-2023"
short_title: Frama-C 26.1 (Iron)
title: Release of Frama-C 26.1 (Iron)
internal_link: /fc-versions/iron.html
---

Frama-C 26.1 (Iron) is out. Download it [here](/fc-versions/iron.html).

This minor release fixes some issues related to the compilation and installation
of Frama-C.

Other changes with respect to Frama-C 26.0 (Iron) include:

#### Kernel
- Accepts \ghost attribute in logic annotations
- Fixes issue in pretty-printing ranges

#### WP
- Fixes 'terminates' goals generation when some 'terminates' or 'decreases'
  clauses are missing.
