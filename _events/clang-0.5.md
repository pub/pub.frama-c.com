---
layout: default
date: 19-02-2018
short_title: Frama-Clang 0.0.5
title: Release of Frama-Clang 0.0.5
internal_link: /fc-plugins/frama-clang.html
---

Frama-Clang 0.0.5, fixing compatibility issue with Debian/Ubuntu, is out.
Download it [here](/fc-plugins/frama-clang.html).
