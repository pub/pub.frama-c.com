---
layout: default
date: 09-12-2024
short_title: Frama-Clang v0.0.17
title: Frama-Clang v0.0.17 for Frama-C 30.0~ Zinc
internal_link: /fc-plugins/frama-clang.html
---

[Frama-Clang](/fc-plugins/frama-clang.html) [v0.0.17](https://git.frama-c.com/pub/frama-clang/-/releases/0.0.17) is out.

Frama-Clang is an experimental C++ front-end for Frama-C, based on the clang compiler
See its [homepage](/fc-plugins/frama-clang.html) for more information.

Main changes in this release include:

- Compatibility with Frama-C 30 Zinc
