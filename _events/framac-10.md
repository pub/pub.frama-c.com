---
layout: default
date: 07-03-2014
short_title: Frama-C 10.0 (Neon)
title: Release of Frama-C 10.0 (Neon)
internal_link: /fc-versions/neon.html
---

Frama-C 10.0 (Neon) is available.

This new major version includes too many bug fixes and improvements to
list here: details are available at http://frama-c.com/Changelog.html.

The main highlights are:

- The Value plugin is more efficient (computation and cache have been
  optimized). The experimental option -val-show-perf helps estimating
  which part of the C code takes time to analyze. One can send SIGUSR1
  signal to a Frama-C process for stopping and saving the
  partial results of the Value plugin.
- The From plugin computes separately data dependencies and indirect
  (address, control) dependencies with option -show-indirect-deps
- The axiomatizations used by the WP plugin are now shared between the
  different prover outputs and mainly realized in Coq thanks to a better
  integration with Why3.

For plug-in developers:

- The api for Dataflow have been greatly simplified.
- This major release changes several Frama-C APIs
  in an incompatible way. Some of the plugin-side changes can
  be automatically applied by using the script bin/fluorine2neon.sh of
  the source distribution. Complex plug-ins should be reviewed for
  compatibility.
