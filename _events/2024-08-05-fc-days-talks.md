---
layout: default
date: "05-08-2024"
short_title: Frama-C Days 2024 Slides
title: The slides presented at Frama-C Days 2024 are available
internal_link: /html/publications/frama-c-days-2024/index.html
---

The slides presented at the Frama-C Days 2024 are now available. We would like
to thank again all participants for this amazing event!

**[Click here to reach the gallery](/html/publications/frama-c-days-2024/index.html)**.
