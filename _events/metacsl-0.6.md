---
layout: default
date: 16-02-2024
short_title: MetAcsl v0.6
title: MetAcsl v0.6 for Frama-C 28.0 Nickel
internal_link: /fc-plugins/metacsl.html
---

[MetAcsl](/fc-plugins/metacsl.html) [v0.6](https://git.frama-c.com/pub/meta/-/releases/0.6) is out.

MetAcsl intends to provide simple and compact ways to express properties that
would demand peppering the code with thousands of annotations in plain ACSL.
Its main use cases focus on security properties (notably ensuring that
write and read accesses to sensitive memory locations are guarded appropriately).
See its [homepage](/fc-plugins/metacsl.html) for more information.

Main changes in this release include:

- compatibility with Frama-C 28.x Nickel
- `-meta-check-callee-assigns` can now also be given declared functions
