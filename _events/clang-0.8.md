---
layout: default
date: 10-03-2020
short_title: Frama-Clang 0.0.8
title: Release of Frama-Clang 0.0.8
internal_link: /fc-plugins/frama-clang.html
---

Frama-Clang 0.0.8 is out. Download it [here](/fc-plugins/frama-clang.html).
