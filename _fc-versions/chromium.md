---
layout: version
number: 24
name: Chromium
acsl: 17
releases:
  - number: 0
    categories:
    - name: Frama-C v24.0 Chromium
      files:
      - name: Source distribution
        link: /download/frama-c-24.0-Chromium.tar.gz
        help: Compilation instructions
        help_link: https://git.frama-c.com/pub/frama-c/-/blob/24.0/INSTALL.md
      - name: User manual
        link: /download/user-manual-24.0-Chromium.pdf
      - name: Plug-in development guide
        link: /download/plugin-development-guide-24.0-Chromium.pdf
        help: Hello plug-in tutorial archive
        help_link: /download/hello-24.0-Chromium.tar.gz
      - name: API Documentation
        link: /download/frama-c-24.0-Chromium-api.tar.gz
      - name: ACSL 1.17 (Chromium implementation)
        link: /download/acsl-implementation-24.0-Chromium.pdf
    - name: Plug-in Manuals
      sort: true
      files:
      - name: Aoraï manual
        link: /download/aorai-manual-24.0-Chromium.pdf
        help: Aoraï example
        help_link: /download/aorai-example-24.0-Chromium.tgz
      - name: Metrics manual
        link: /download/metrics-manual-24.0-Chromium.pdf
      - name: Rte manual
        link: /download/rte-manual-24.0-Chromium.pdf
      - name: Eva manual
        link: /download/eva-manual-24.0-Chromium.pdf
      - name: WP manual
        link: /download/wp-manual-24.0-Chromium.pdf
      - name: E-ACSL manual
        link: /download/e-acsl/e-acsl-manual-24.0-Chromium.pdf
---
