---
layout: version
number: 11
name: Sodium
acsl: 9
releases:
  - number: 0
    changelog: 20150201
    categories:
    - name: Frama-C v11.0 Sodium
      files:
        - name: Source distribution
          link: /download/frama-c-Sodium-20150201.tar.gz
          help: Compilation instructions
          help_link: /html/installations/sodium.html
        - name: User manual
          link: /download/user-manual-Sodium-20150201.pdf
        - name: Plug-In development guide
          link: /download/plugin-development-guide-Sodium-20150201.pdf
        - name: API Documentation
          link: /download/frama-c-Sodium-20150201_api.tar.gz
        - name: ACSL 1.9 (Sodium implementation)
          link: /download/acsl-implementation-Sodium-20150201.pdf
    - name: Plug-in Manuals
      sort: true
      files:
        - name: Aoraï manual
          link: /download/aorai-manual-Sodium-20150201.pdf
        - name: Metrics manual
          link: /download/metrics-manual-Sodium-20150201.pdf
        - name: Rte manual
          link: /download/rte-manual-Sodium-20150201.pdf
        - name: Value Analysis manual
          link: /download/value-analysis-Sodium-20150201.pdf
        - name: WP manual
          link: /download/wp-manual-Sodium-20150201.pdf
    - name: External Plug-ins
      files:
        - name: E-ACSL version 0.5
          link: /download/e-acsl/e-acsl-0.5.tar.gz
---