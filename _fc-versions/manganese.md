---
layout: version
number: 25
name: Manganese
acsl: 18
releases:
  - number: 0
    categories:
    - name: Frama-C v25.0 Manganese
      files:
      - name: Source distribution
        link: /download/frama-c-25.0-Manganese.tar.gz
        help: Compilation instructions
        help_link: https://git.frama-c.com/pub/frama-c/-/blob/25.0/INSTALL.md
      - name: User manual
        link: /download/user-manual-25.0-Manganese.pdf
      - name: Plug-in development guide
        link: /download/plugin-development-guide-25.0-Manganese.pdf
        help: Hello plug-in tutorial archive
        help_link: /download/hello-25.0-Manganese.tar.gz
      - name: API Documentation
        link: /download/frama-c-25.0-Manganese-api.tar.gz
      - name: ACSL 1.18 (Manganese implementation)
        link: /download/acsl-implementation-25.0-Manganese.pdf
    - name: Plug-in Manuals
      sort: true
      files:
      - name: Aoraï manual
        link: /download/aorai-manual-25.0-Manganese.pdf
        help: Aoraï example
        help_link: /download/aorai-example-25.0-Manganese.tgz
      - name: Metrics manual
        link: /download/metrics-manual-25.0-Manganese.pdf
      - name: Rte manual
        link: /download/rte-manual-25.0-Manganese.pdf
      - name: Eva manual
        link: /download/eva-manual-25.0-Manganese.pdf
      - name: WP manual
        link: /download/wp-manual-25.0-Manganese.pdf
      - name: E-ACSL manual
        link: /download/e-acsl/e-acsl-manual-25.0-Manganese.pdf
---
