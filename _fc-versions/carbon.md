---
layout: version
number: 6
name: Carbon
acsl: 5
releases:
  - number: 2
    changelog: 20110201
    categories:
    - name: Frama-C v6.0 Carbon
      files:
        - name: Source distribution
          link: /download/frama-c-Carbon-20110201.tar.gz
          help: Compilation instructions
          help_link: /html/installations/carbon.html
        - name: User manual
          link: /download/user-manual-Carbon-20110201.pdf
        - name: Value Analysis manual
          link: /download/value-analysis-Carbon-20110201.pdf
        - name: Rte manual
          link: /download/rte-manual-Carbon-20110201.pdf
        - name: ACSL 1.5 (Carbon implementation)
          link: /download/acsl-implementation-Carbon-20110201.pdf
        - name: API Documentation
          link: /download/frama-c-Carbon-20110201_api.tar.gz
        - name: Plug-In development guide
          link: /download/plugin-development-guide-Carbon-20110201.pdf
        - name: Plug-in WP 0.3 Source distribution
          link: /download/wp-0.3-Carbon-20110201.tar.gz
          help: Installation instructions
          help_link: /html/installations/carbon-wp.html
        - name: WP manual
          link: /download/wp-0.3-Carbon-20110201.pdf
        - name: Mac OS X Intel binary package
          link: /download/frama-c-Carbon-20110201pl1-why_2.29-OSX_intel.tar.bz2
          help: Installation instructions
          help_link: /html/installations/carbon-OSX.html
  - number: 1
    changelog: 20101202
    categories:
    - name: Frama-C v6~beta-2 Carbon
      files:
        - name: Source distribution
          link: /download/frama-c-Carbon-20101202-beta2.tar.gz
          help: Compilation instructions
          help_link: /html/installations/carbon-beta.html
        - name: User manual
          link: /download/user-manual-Carbon-20101202-beta2.pdf
        - name: Value Analysis manual
          link: /download/value-analysis-Carbon-20101202-beta2.pdf
        - name: Jessie tutorial
          link: /download/jessie-tutorial-Carbon-20101202-beta2.pdf
        - name: WP manual
          link: /download/wp-manual-Carbon-20101202-beta2.pdf
        - name: Rte manual
          link: /download/rte-manual-Carbon-20101202-beta2.pdf
        - name: ACSL 1.5 (Carbon implementation)
          link: /download/acsl-implementation-Carbon-20101202-beta2.pdf
        - name: API Documentation
          link: /download/frama-c-Carbon-20101202-beta2_api.tar.gz
        - name: Plug-In development guide
          link: /download/plugin-development-guide-Carbon-20101202-beta2.pdf
  - number: 0
    changelog: 20101201
    categories:
    - name: Frama-C v6~beta-1 Carbon
      files:
        - name: Source distribution
          link: /download/frama-c-Carbon-20101201-beta1.tar.gz
          help: Compilation instructions
          help_link: /html/installations/carbon-beta.html
        - name: User manual
          link: /download/user-manual-Carbon-20101201-beta1.pdf
        - name: Value Analysis manual
          link: /download/value-analysis-Carbon-20101201-beta1.pdf
        - name: Jessie tutorial
          link: /download/jessie-tutorial-Carbon-20101201-beta1.pdf
        - name: WP manual
          link: /download/wp-manual-Carbon-20101201-beta1.pdf
        - name: Rte manual
          link: /download/rte-manual-Carbon-20101201-beta1.pdf
        - name: ACSL 1.5 (Carbon implementation)
          link: /download/acsl-implementation-Carbon-20101201-beta1.pdf
        - name: API Documentation
          link: /download/frama-c-Carbon-20101201-beta1_api.tar.gz
        - name: Plug-In development guide
          link: /download/plugin-development-guide-Carbon-20101201-beta1.pdf
---

The Mac OS X Intel binary package includes Value analysis patchlevel 1, Jessie, Why 2.29, Alt-Ergo 0.92.2 and an OCaml 3.12.0/LablGtk development environment. 