---
layout: version
number: 20
name: Calcium
acsl: 14
releases:
  - number: 0
    categories:
    - name: Frama-C v20.0 Calcium
      files:
      - name: Source distribution
        link: /download/frama-c-20.0-Calcium.tar.gz
        help: Compilation instructions
        help_link: https://git.frama-c.com/pub/frama-c/-/blob/20.0/INSTALL.md
      - name: User manual
        link: /download/user-manual-20.0-Calcium.pdf
      - name: Plugin-In development guide
        link: /download/plugin-development-guide-20.0-Calcium.pdf
        help: Hello plug-in tutorial archive
        help_link: /download/hello-20.0-Calcium.tar.gz
      - name: API Documentation
        link: /download/frama-c-20.0-Calcium-api.tar.gz
      - name: ACSL 1.14 (Calcium implementation)
        link: /download/acsl-implementation-20.0-Calcium.pdf
    - name: Plug-in Manuals
      sort: true
      files:
      - name: Aoraï manual
        link: /download/aorai-manual-20.0-Calcium.pdf
      - name: Metrics manual
        link: /download/metrics-manual-20.0-Calcium.pdf
      - name: Rte manual
        link: /download/rte-manual-20.0-Calcium.pdf
      - name: Eva manual
        link: /download/eva-manual-20.0-Calcium.pdf
      - name: WP manual
        link: /download/wp-manual-20.0-Calcium.pdf
      - name: E-ACSL manual
        link: /download/e-acsl/e-acsl-manual-20.0-Calcium.pdf
---
