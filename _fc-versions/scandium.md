---
layout: version
number: 21
name: Scandium
acsl: 15
releases:
  - number: 1
    categories:
    - name: Frama-C v21.1 Scandium
      files:
      - name: Source distribution
        link: /download/frama-c-21.1-Scandium.tar.gz
        help: Compilation instructions
        help_link: https://git.frama-c.com/pub/frama-c/-/blob/21.1/INSTALL.md
      - name: User manual
        link: /download/user-manual-21.1-Scandium.pdf
      - name: Plugin-In development guide
        link: /download/plugin-development-guide-21.1-Scandium.pdf
        help: Hello plug-in tutorial archive
        help_link: /download/hello-21.1-Scandium.tar.gz
      - name: API Documentation
        link: /download/frama-c-21.1-Scandium-api.tar.gz
      - name: ACSL 1.15 (Scandium implementation)
        link: /download/acsl-implementation-21.1-Scandium.pdf
    - name: Plug-in Manuals
      sort: true
      files:
      - name: Aoraï manual
        link: /download/aorai-manual-21.1-Scandium.pdf
      - name: Metrics manual
        link: /download/metrics-manual-21.1-Scandium.pdf
      - name: Rte manual
        link: /download/rte-manual-21.1-Scandium.pdf
      - name: Eva manual
        link: /download/eva-manual-21.1-Scandium.pdf
      - name: WP manual
        link: /download/wp-manual-21.1-Scandium.pdf
      - name: E-ACSL manual
        link: /download/e-acsl/e-acsl-manual-21.1-Scandium.pdf
---
