---
layout: version
number: 26
name: Iron
acsl: 18
releases:
- number: 1
  categories:
  - name: Frama-C v26.1 Iron
    files:
    - name: Source distribution
      link: /download/frama-c-26.1-Iron.tar.gz
      help: Compilation instructions
      help_link: https://git.frama-c.com/pub/frama-c/-/blob/26.1/INSTALL.md
    - name: User manual
      link: /download/user-manual-26.1-Iron.pdf
    - name: Plug-in development guide
      link: /download/plugin-development-guide-26.1-Iron.pdf
      help: Hello plug-in tutorial archive
      help_link: /download/hello-26.1-Iron.tar.gz
    - name: API Documentation
      link: /download/frama-c-26.1-Iron-api.tar.gz
    - name: Server API Documentation
      link: /download/frama-c-server-26.1-Iron-api.tar.gz
    - name: ACSL 1.18 (Iron implementation)
      link: /download/acsl-implementation-26.1-Iron.pdf
  - name: Plug-in Manuals
    sort: true
    files:
    - name: Aoraï manual
      link: /download/aorai-manual-26.1-Iron.pdf
      help: Aoraï example
      help_link: /download/aorai-example-26.1-Iron.tar.gz
    - name: Metrics manual
      link: /download/metrics-manual-26.1-Iron.pdf
    - name: Rte manual
      link: /download/rte-manual-26.1-Iron.pdf
    - name: Eva manual
      link: /download/eva-manual-26.1-Iron.pdf
    - name: WP manual
      link: /download/wp-manual-26.1-Iron.pdf
    - name: E-ACSL manual
      link: /download/e-acsl/e-acsl-manual-26.1-Iron.pdf
---
