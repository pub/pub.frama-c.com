---
layout: version
number: 28
name: Nickel
acsl: 20
releases:
- number: 1
  categories:
  - name: Frama-C v28.1 Nickel
    files:
    - name: Source distribution
      link: /download/frama-c-28.1-Nickel.tar.gz
      help: Compilation instructions
      help_link: https://git.frama-c.com/pub/frama-c/-/blob/28.1/INSTALL.md
    - name: User manual
      link: /download/user-manual-28.1-Nickel.pdf
    - name: Plug-in development guide
      link: /download/plugin-development-guide-28.1-Nickel.pdf
      help: Hello plug-in tutorial archive
      help_link: /download/hello-28.1-Nickel.tar.gz
    - name: API Documentation
      link: /download/frama-c-28.1-Nickel-api.tar.gz
    - name: Server API Documentation
      link: /download/frama-c-server-28.1-Nickel-api.tar.gz
    - name: ACSL 1.20 (Nickel implementation)
      link: /download/acsl-implementation-28.1-Nickel.pdf
  - name: Plug-in Manuals
    sort: true
    files:
    - name: Aoraï manual
      link: /download/aorai-manual-28.1-Nickel.pdf
      help: Aoraï example
      help_link: /download/aorai-example-28.1-Nickel.tar.gz
    - name: Metrics manual
      link: /download/metrics-manual-28.1-Nickel.pdf
    - name: Rte manual
      link: /download/rte-manual-28.1-Nickel.pdf
    - name: Eva manual
      link: /download/eva-manual-28.1-Nickel.pdf
    - name: WP manual
      link: /download/wp-manual-28.1-Nickel.pdf
    - name: E-ACSL manual
      link: /download/e-acsl/e-acsl-manual-28.1-Nickel.pdf
  - name: Ivette Packages (experimental)
    files:
    - name: Linux x86-64 AppImage
      link: /download/frama-c-ivette-linux-x86-64-28.1-Nickel.AppImage
      help: README install
      help_link: /download/frama-c-ivette-app-install-28.1-Nickel.md
    - name: Linux ARM64 AppImage
      link: /download/frama-c-ivette-linux-ARM64-28.1-Nickel.AppImage
      help: README install
      help_link: /download/frama-c-ivette-app-install-28.1-Nickel.md
    - name: macOS universal
      link: /download/frama-c-ivette-macos-universal-28.1-Nickel.dmg
      help: README install
      help_link: /download/frama-c-ivette-app-install-28.1-Nickel.md
---
