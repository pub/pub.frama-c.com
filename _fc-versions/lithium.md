---
layout: version
number: 3
name: Lithium
acsl: 4
releases:
  - number: 1
    changelog: 20081201
    categories:
    - name: Frama-C v3.0 Lithium
      files:
        - name: Source distribution
          link: /download/frama-c-Lithium-20081201.tar.gz
          help: Compilation instructions
          help_link: /html/installations/lithium.html
        - name: Windows binary installer
          link: /download/frama-c-Lithium-20081201_installer.exe
          help: Installation instructions
          help_link: /html/installations/lithium-windows.html
        - name: Mac OS X 10.5 (Leopard) Intel
          link: /download/frama-c-Lithium-20081201_OSX_intel.tar.bz2
          help: Installation instructions
          help_link: /html/installations/lithium-OSX.html
        - name: Mac OS X 10.5 (Leopard) PowerPC
          link: /download/frama-c-Lithium-20081201_OSX_PPC.tar.bz2
          help: Installation instructions
          help_link: /html/installations/lithium-OSX.html
        - name: ACSL 1.4 (Lithium implementation)
          link: /download/acsl-implementation-Lithium.pdf
  - number: 0
    changelog: 20081002
    categories:
    - name: Frama-C v3.0~beta-1 Lithium
      files:
        - name: Source distribution (beta-1)
          link: /download/frama-c-Hydrogen-20080301.tar.gz
---