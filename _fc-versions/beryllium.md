---
layout: version
number: 4
name: Beryllium
acsl: 4
releases:
  - number: 2
    changelog: 20090902
    categories:
    - name: Frama-C v4.1 Beryllium
      files:
        - name: Source distribution
          link: /download/frama-c-Beryllium-20090902-why-2.21.tar.gz
          help: Compilation instructions
          help_link: /html/installations/beryllium-why-2.21.html
        - name: Source distribution without Jessie
          link: /download/frama-c-Beryllium-20090902.tar.gz
          help: Compilation instructions
          help_link: /html/installations/beryllium.html
        - name: Windows binary installer (does not include Why nor Jessie)
          link: /download/frama-c-Beryllium-20090902_installer.exe
          help: Installation instructions
          help_link: /html/installations/beryllium-windows-20090902.html
        - name: Mac OS X 10.5 or 10.6 Intel
          link: /download/frama-c-Beryllium-20090902-why_2.23-OSX_intel.tar.bz2
          help: Installation instructions
          help_link: /html/installations/beryllium-OSX.html
        - name: Mac OS X 10.5 or 10.6 Intel
          link: /download/frama-c-Beryllium-20090902_OSX_intel.tar.bz2
          help: Installation instructions
          help_link: /html/installations/beryllium-OSX.html
        - name: Mac OS X 10.5 (Leopard) PowerPC
          link: /download/frama-c-Beryllium-20090902_OSX_PPC.tar.bz2
          help: Installation instructions
          help_link: /html/installations/beryllium-OSX.html
        - name: User manual
          link: /download/user-manual-Beryllium-20090902.pdf
        - name: Value Analysis manual
          link: /download/value-analysis-Beryllium-20090902.pdf
        - name: ACSL 1.4 (Beryllium implementation)
          link: /download/acsl-implementation-Beryllium-20090902.pdf
        - name: API Documentation
          link: /download/frama-c-Beryllium-20090902_api.tar.gz
        - name: Plug-In development guide
          link: /download/plugin-developer-Beryllium-20090902.pdf
  - number: 1
    changelog: 20090901
    categories:
    - name: Frama-C v4.0 Beryllium
      files:
        - name: Source distribution
          link: /download/frama-c-Beryllium-20090901.tar.gz
          help: Compilation instructions
          help_link: /html/installations/beryllium.html
        - name: Windows binary installer
          link: /download/frama-c-Beryllium-20090901_installer.exe
          help: Installation instructions
          help_link: /html/installations/beryllium-windows.html
        - name: Value Analysis guide
          link: /download/value-analysis-Beryllium.pdf
        - name: ACSL 1.4 (Beryllium implementation)
          link: /download/acsl-implementation-Beryllium.pdf
        - name: API Documentation
          link: /download/frama-c-Beryllium-20090901_api.tar.gz
        - name: Plug-In development guide
          link: /download/plugin-developer-Beryllium.pdf
  - number: 0
    changelog: 20090601
    categories:
    - name: Frama-C v4.0~beta-1 Beryllium
      files:
        - name: Source distribution
          link: /download/frama-c-Beryllium-20090601-beta1.tar.gz
          help: Compilation instructions
          help_link: /html/installations/beryllium.html
        - name: Plug-In development guide
          link: /download/frama-c-plugin-dev-guide-Beryllium-20090601-beta1.pdf
        - name: ACSL 1.4 (Beryllium implementation)
          link: /download/acsl-implementation-Beryllium-beta1.pdf
---
