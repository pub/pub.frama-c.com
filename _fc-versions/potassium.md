---
layout: version
number: 19
name: Potassium
acsl: 14
releases:
  - number: 0
    categories:
    - name: Frama-C v19.1 Potassium
      files:
      - name: Source distribution
        link: /download/frama-c-19.1-Potassium.tar.gz
        help: Compilation instructions
        help_link: /html/installations/potassium.html
      - name: User manual
        link: /download/user-manual-19.1-Potassium.pdf
      - name: Plugin-In development guide
        link: /download/plugin-development-guide-19.1-Potassium.pdf
        help: Hello plug-in tutorial archive
        help_link: /download/hello-19.1-Potassium.tar.gz
      - name: API Documentation
        link: /download/frama-c-19.1-Potassium-api.tar.gz
      - name: ACSL 1.14 (Potassium implementation)
        link: /download/acsl-implementation-19.1-Potassium.pdf
    - name: Plug-in Manuals
      sort: true
      files:
      - name: Aoraï manual
        link: /download/aorai-manual-19.1-Potassium.pdf
      - name: Metrics manual
        link: /download/metrics-manual-19.1-Potassium.pdf
      - name: Rte manual
        link: /download/rte-manual-19.1-Potassium.pdf
      - name: Eva manual
        link: /download/eva-manual-19.1-Potassium.pdf
      - name: WP manual
        link: /download/wp-manual-19.1-Potassium.pdf
      - name: E-ACSL manual
        link: /download/e-acsl/e-acsl-manual-19.1-Potassium.pdf
---