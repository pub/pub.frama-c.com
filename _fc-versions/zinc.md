---
layout: version
number: 30
name: Zinc
acsl: 21
releases:
- number: 0
  categories:
  - name: Frama-C v30.0 Zinc
    files:
    - name: Source distribution
      link: /download/frama-c-30.0-Zinc.tar.gz
      help: Compilation instructions
      help_link: https://git.frama-c.com/pub/frama-c/-/blob/30.0/INSTALL.md
    - name: User manual
      link: /download/user-manual-30.0-Zinc.pdf
    - name: Plug-in development guide
      link: /download/plugin-development-guide-30.0-Zinc.pdf
      help: Hello plug-in tutorial archive
      help_link: /download/hello-30.0-Zinc.tar.gz
    - name: API Documentation
      link: /download/frama-c-30.0-Zinc-api.tar.gz
    - name: Server API Documentation
      link: /download/frama-c-server-30.0-Zinc-api.tar.gz
    - name: ACSL 1.21 (Zinc implementation)
      link: /download/acsl-implementation-30.0-Zinc.pdf
  - name: Plug-in Manuals
    sort: true
    files:
    - name: Aoraï manual
      link: /download/aorai-manual-30.0-Zinc.pdf
      help: Aoraï example
      help_link: /download/aorai-example-30.0-Zinc.tar.gz
    - name: Metrics manual
      link: /download/metrics-manual-30.0-Zinc.pdf
    - name: Rte manual
      link: /download/rte-manual-30.0-Zinc.pdf
    - name: Eva manual
      link: /download/eva-manual-30.0-Zinc.pdf
    - name: WP manual
      link: /download/wp-manual-30.0-Zinc.pdf
    - name: E-ACSL manual
      link: /download/e-acsl/e-acsl-manual-30.0-Zinc.pdf
  - name: Ivette Packages (experimental)
    files:
    - name: Linux x86-64 AppImage
      link: /download/frama-c-ivette-linux-x86-64-30.0-Zinc.AppImage
      help: Installation instructions
      help_link: https://git.frama-c.com/pub/frama-c/-/blob/30.0/INSTALL.md#installing-ivette-via-the-online-packages-on-linux
    - name: Linux ARM64 AppImage
      link: /download/frama-c-ivette-linux-ARM64-30.0-Zinc.AppImage
      help: Installation instructions
      help_link: https://git.frama-c.com/pub/frama-c/-/blob/30.0/INSTALL.md#installing-ivette-via-the-online-packages-on-linux
    - name: macOS universal
      link: /download/frama-c-ivette-macOS-universal-30.0-Zinc.dmg
      help: Installation instructions
      help_link: https://git.frama-c.com/pub/frama-c/-/blob/30.0/INSTALL.md#installing-ivette-via-the-online-packages-on-macos
---
