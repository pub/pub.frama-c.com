---
layout: version
number: 1
name: Hydrogen
acsl: 2
releases:
  - number: 2
    changelog: 20080502
    categories:
    - name: Frama-C v1.0 Hydrogen
      files:
        - name: Source distribution
          link: /download/frama-c-Hydrogen-20080502.tar.gz
          help: Compilation instructions
          help_link: /html/installations/hydrogen.html
        - name: Windows binary installer
          link: /download/frama-c-Hydrogen-20080502_installer.exe
        - name: ACSL 1.2 (Hydrogen implementation)
          link: /download/acsl-implementation-Hydrogen.pdf
  - number: 1
    changelog: 20080302
    categories:
    - name: Frama-C v1.0~beta-2 Hydrogen
      files:
        - name: Source distribution (beta-2)
          link: /download/frama-c-Hydrogen-20080501.tar.gz
  - number: 0
    changelog: 20080301
    categories:
    - name: Frama-C v1.0~beta-1 Hydrogen
      files:
        - name: Source distribution (beta-1)
          link: /download/frama-c-Hydrogen-20080301.tar.gz
---