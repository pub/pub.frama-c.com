---
layout: version
number: 7
name: Nitrogen
acsl: 5
releases:
  - number: 0
    changelog: 20111001
    categories:
    - name: Frama-C v7.0 Nitrogen
      files:
        - name: Source distribution
          link: /download/frama-c-Nitrogen-20111001.tar.gz
          help: Compilation instructions
          help_link: /html/installations/nitrogen.html
        - name: Binary package for Mac OS X Intel
          link: /download/frama-c-Nitrogen-20111001pl1-OSX_intel.tar.bz2
          help: Readme
          help_link: /html/installations/nitrogen-OSX-intel.html
        - name: Binary package for Mac OS X PowerPC
          link: /download/frama-c-Nitrogen-20111001pl2-OSX_ppc.tar.bz2
          help: Readme
          help_link: /html/installations/nitrogen-OSX-ppc.html
        - name: User manual
          link: /download/user-manual-Nitrogen-20111001.pdf
        - name: Plug-In development guide
          link: /download/plugin-development-guide-Nitrogen-20111001.pdf
        - name: API Documentation
          link: /download/frama-c-Nitrogen-20111001_api.tar.gz
        - name: ACSL 1.5 (Nitrogen implementation)
          link: /download/acsl-implementation-Nitrogen-20111001.pdf
    - name: Plug-in Manuals
      sort: true
      files:
        - name: Aoraï manual
          link: /download/aorai-manual-Nitrogen-20111001.pdf
        - name: Metrics manual
          link: /download/metrics-manual-Nitrogen-20111001.pdf
        - name: Rte manual
          link: /download/rte-manual-Nitrogen-20111001.pdf
        - name: Value Analysis manual
          link: /download/value-analysis-Nitrogen-20111001.pdf
        - name: WP 0.4 manual
          link: /download/wp-manual-Nitrogen-20111001.pdf
    - name: External Plug-ins
      sort: true
      files:
        - name: E-ACSL version 0.1
          link: /download/e-acsl/e-acsl-0.1.tar.gz
        - name: WP version 0.5
          link: /download/nitrogen/wp-0.5-Nitrogen-20111001.tgz
          help: Compilation instructions
          help_link: /html/installations/nitrogen-wp.html
        - name: WP 0.5 manual
          link: /download/frama-c-wp-manual.pdf
---
