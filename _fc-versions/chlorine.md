---
layout: version
number: 17
name: Chlorine
acsl: 13
releases:
  - number: 1
    changelog: 20180502
    categories:
    - name: Frama-C v17.1 Chlorine
      files:
        - name: Source distribution
          link: /download/frama-c-Chlorine-20180502.tar.gz
          help: Compilation instructions
          help_link: /html/installations/chlorine.html
        - name: User manual
          link: /download/user-manual-Chlorine-20180501.pdf
        - name: Plugin-In development guide
          link: /download/plugin-development-guide-Chlorine-20180501.pdf
          help: Hello plug-in tutorial archive
          help_link: /download/hello-Chlorine-20180501.tar.gz
        - name: API Documentation
          link: /download/frama-c-Chlorine-20180501_api.tar.gz
        - name: ACSL 1.13 (Chlorine implementation)
          link: /download/acsl-implementation-Chlorine-20180501.pdf
    - name: Plug-in Manuals
      sort: true
      files:
        - name: Aoraï manual
          link: /download/aorai-manual-Chlorine-20180501.pdf
        - name: Metrics manual
          link: /download/metrics-manual-Chlorine-20180501.pdf
        - name: Rte manual
          link: /download/rte-manual-Chlorine-20180501.pdf
        - name: Eva manual
          link: /download/value-analysis-Chlorine-20180501.pdf
        - name: WP manual
          link: /download/wp-manual-Chlorine-20180501.pdf
        - name: E-ACSL manual
          link: /download/e-acsl/e-acsl-manual_Chlorine-20180501.pdf
---
