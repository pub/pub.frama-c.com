---
layout: version
number: 5
name: Boron
acsl: 4
releases:
  - number: 0
    changelog: 20100401
    categories:
    - name: Frama-C v5.0 Boron
      files:
        - name: Source distribution
          link: /download/frama-c-Boron-20100401-why-2.24.tar.gz
          help: Compilation instructions
          help_link: /html/installations/boron-why-2.24.html
        - name: Source distribution without Jessie
          link: /download/frama-c-Boron-20100401.tar.gz
          help: Compilation instructions
          help_link: /html/installations/boron.html
        - name: Windows binary installer
          link: /download/frama-c-Boron-20100401.exe
          help: Installation instructions
          help_link: /html/installations/boron-windows.html
        - name: Mac OS X Intel binary  package
          link: /download/frama-c-Boron-20100401-why_2.26-OSX_intel.tar.bz2
          help: Installation instructions
          help_link: /html/installations/boron-OSX.html
        - name: User manual
          link: /download/user-manual-Boron-20100401.pdf
        - name: Value Analysis manual
          link: /download/value-analysis-Boron-20100401.pdf
        - name: Jessie tutorial
          link: /download/jessie-tutorial-Boron-20100401.pdf
        - name: ACSL 1.4 (Boron implementation)
          link: /download/acsl-implementation-Boron-20100401.pdf
        - name: API Documentation
          link: /download/frama-c-Boron-20100401_api.tar.gz
        - name: Plug-In development guide
          link: /download/plugin-developer-Boron-20100401.pdf
---
The Windows binary installer and the Mac OS X Intel binary package includes Jessie, Why 2.26, Alt-Ergo 0.91 and an OCaml 3.11.2/LabGtk development environment.
