---
layout: version
number: 12
name: Magnesium
acsl: 10
releases:
  - number: 0
    changelog: 20151002
    categories:
    - name: Frama-C v12.0 Magnesium
      files:
        - name: Source distribution
          link: /download/frama-c-Magnesium-20151002.tar.gz
          help: Compilation instructions
          help_link: /html/installations/magnesium.html
        - name: User manual
          link: /download/user-manual-Magnesium-20151002.pdf
        - name: Plug-In development guide
          link: /download/plugin-development-guide-Magnesium-20151002.pdf
        - name: API Documentation
          link: /download/frama-c-Magnesium-20151002_api.tar.gz
        - name: ACSL 1.10 (Magnesium implementation)
          link: /download/acsl-implementation-Magnesium-20151002.pdf
    - name: Plug-in Manuals
      sort: true
      files:
        - name: Aoraï manual
          link: /download/aorai-manual-Magnesium-20151002.pdf
        - name: Metrics manual
          link: /download/metrics-manual-Magnesium-20151002.pdf
        - name: Rte manual
          link: /download/rte-manual-Magnesium-20151002.pdf
        - name: Value Analysis manual
          link: /download/value-analysis-Magnesium-20151002.pdf
        - name: WP manual
          link: /download/wp-manual-Magnesium-20151002.pdf
    - name: External Plug-ins
      files:
        - name: E-ACSL version 0.6
          link: /download/e-acsl/e-acsl-0.6.tar.gz
---