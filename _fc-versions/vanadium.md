---
layout: version
number: 23
name: Vanadium
acsl: 17
releases:
  - number: 1
    categories:
    - name: Frama-C v23.1 Vanadium
      files:
      - name: Source distribution
        link: /download/frama-c-23.1-Vanadium.tar.gz
        help: Compilation instructions
        help_link: https://git.frama-c.com/pub/frama-c/-/blob/23.1/INSTALL.md
      - name: User manual
        link: /download/user-manual-23.1-Vanadium.pdf
      - name: Plug-in development guide
        link: /download/plugin-development-guide-23.1-Vanadium.pdf
        help: Hello plug-in tutorial archive
        help_link: /download/hello-23.1-Vanadium.tar.gz
      - name: API Documentation
        link: /download/frama-c-23.1-Vanadium-api.tar.gz
      - name: ACSL 1.17 (Vanadium implementation)
        link: /download/acsl-implementation-23.1-Vanadium.pdf
    - name: Plug-in Manuals
      sort: true
      files:
      - name: Aoraï manual
        link: /download/aorai-manual-23.1-Vanadium.pdf
        help: Aoraï example
        help_link: /download/aorai-example-23.1-Vanadium.tgz
      - name: Metrics manual
        link: /download/metrics-manual-23.1-Vanadium.pdf
      - name: Rte manual
        link: /download/rte-manual-23.1-Vanadium.pdf
      - name: Eva manual
        link: /download/eva-manual-23.1-Vanadium.pdf
      - name: WP manual
        link: /download/wp-manual-23.1-Vanadium.pdf
      - name: E-ACSL manual
        link: /download/e-acsl/e-acsl-manual-23.1-Vanadium.pdf
---
