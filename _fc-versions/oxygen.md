---
layout: version
number: 8
name: Oxygen
acsl: 6
releases:
  - number: 0
    changelog: 20120901
    categories:
    - name: Frama-C v8.0 Oxygen
      files:
        - name: Source distribution
          link: /download/frama-c-Oxygen-20120901.tar.gz
          help: Compilation instructions
          help_link: /html/installations/oxygen.html
        - name: User manual
          link: /download/user-manual-Oxygen-20120901.pdf
        - name: Plug-In development guide
          link: /download/plugin-development-guide-Oxygen-20120901.pdf
        - name: API Documentation
          link: /download/frama-c-Oxygen-20120901_api.tar.gz
        - name: ACSL 1.6 (Oxygen implementation)
          link: /download/acsl-implementation-Oxygen-20120901.pdf
    - name: Plug-in Manuals
      sort: true
      files:
        - name: Aoraï manual
          link: /download/aorai-manual-Oxygen-20120901.pdf
        - name: Metrics manual
          link: /download/metrics-manual-Oxygen-20120901.pdf
        - name: Rte manual
          link: /download/rte-manual-Oxygen-20120901.pdf
        - name: Value Analysis manual
          link: /download/value-analysis-Oxygen-20120901.pdf
        - name: WP 0.6 manual
          link: /download/wp-manual-Oxygen-20120901.pdf
        - name: Mthread 0.9 manual
          link: /download/mthread-manual-Oxygen-20120901.pdf
---
