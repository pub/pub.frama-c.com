---
layout: version
number: 9
name: Fluorine
acsl: 7
releases:
  - number: 2
    changelog: 20130601
    categories:
    - name: Frama-C v9.2 Fluorine 3
      files:
        - name: Source distribution
          link: /download/frama-c-Fluorine-20130601.tar.gz
          help: Compilation instructions
          help_link: /html/installations/fluorine-20130601.html
        - name: User manual
          link: /download/user-manual-Fluorine-20130601.pdf
        - name: Plug-In development guide
          link: /download/plugin-development-guide-Fluorine-20130601.pdf
        - name: API Documentation
          link: /download/frama-c-Fluorine-20130601_api.tar.gz
        - name: ACSL 1.7 (Fluorine implementation)
          link: /download/acsl-implementation-Fluorine-20130601.pdf
    - name: Plug-in Manuals
      sort: true
      files:
        - name: Aoraï manual
          link: /download/aorai-manual-Fluorine-20130601.pdf
        - name: Metrics manual
          link: /download/metrics-manual-Fluorine-20130601.pdf
        - name: Rte manual
          link: /download/rte-manual-Fluorine-20130601.pdf
        - name: Value Analysis manual
          link: /download/value-analysis-Fluorine-20130601.pdf
        - name: WP 0.7 manual
          link: /download/wp-manual-Fluorine-20130601.pdf
    - name: External Plug-ins
      files:
        - name: E-ACSL version - 0.3
          link: /download/e-acsl/e-acsl-0.3.tar.gz
  - number: 1
    changelog: 20130501
    categories:
    - name: Frama-C v9.1 Fluorine
      files:
        - name: Source distribution
          link: /download/frama-c-Fluorine-20130501.tar.gz
          help: Compilation instructions
          help_link: /html/installations/fluorine-20130501.html
        - name: User manual
          link: /download/user-manual-Fluorine-20130501.pdf
        - name: Plug-In development guide
          link: /download/plugin-development-guide-Fluorine-20130501.pdf
        - name: API Documentation
          link: /download/frama-c-Fluorine-20130501_api.tar.gz
        - name: ACSL 1.7 (Fluorine implementation)
          link: /download/acsl-implementation-Fluorine-20130501.pdf
        - name: Aoraï manual
          link: /download/aorai-manual-Fluorine-20130501.pdf
        - name: Metrics manual
          link: /download/metrics-manual-Fluorine-20130501.pdf
        - name: Rte manual
          link: /download/rte-manual-Fluorine-20130501.pdf
        - name: Value Analysis manual
          link: /download/value-analysis-Fluorine-20130501.pdf
        - name: WP 0.7 manual
          link: /download/wp-manual-Fluorine-20130501.pdf
  - number: 0
    Changelog: 20130401
    categories:
    - name: Frama-C v9.0 Fluorine
      files:
        - name: Source distribution
          link: /download/frama-c-Fluorine-20130401.tar.gz
          help: Compilation instructions
          help_link: /html/installations/fluorine-20130401.html
        - name: User manual
          link: /download/user-manual-Fluorine-20130401.pdf
        - name: Plug-In development guide
          link: /download/plugin-development-guide-Fluorine-20130401.pdf
        - name: API Documentation
          link: /download/frama-c-Fluorine-20130401_api.tar.gz
        - name: ACSL 1.7 (Fluorine implementation)
          link: /download/acsl-implementation-Fluorine-20130401.pdf
        - name: Aoraï manual
          link: /download/aorai-manual-Fluorine-20130401.pdf
        - name: Metrics manual
          link: /download/metrics-manual-Fluorine-20130401.pdf
        - name: Rte manual
          link: /download/rte-manual-Fluorine-20130401.pdf
        - name: Value Analysis manual
          link: /download/value-analysis-Fluorine-20130401.pdf
        - name: WP 0.7 manual
          link: /download/wp-manual-Fluorine-20130401.pdf
---