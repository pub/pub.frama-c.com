---
layout: version
number: 14
name: Silicon
acsl: 12
releases:
  - number: 0
    changelog: 20161101
    categories:
    - name: Frama-C v14.0 Silicon
      files:
        - name: Source distribution
          link: /download/frama-c-Silicon-20161101.tar.gz
          help: Compilation instructions
          help_link: /html/installations/silicon.html
        - name: User manual
          link: /download/user-manual-Silicon-20161101.pdf
        - name: Plug-In development guide
          link: /download/plugin-development-guide-Silicon-20161101.pdf
        - name: API Documentation
          link: /download/frama-c-Silicon-20161101_api.tar.gz
        - name: ACSL 1.12 (Silicon implementation)
          link: /download/acsl-implementation-Silicon-20161101.pdf
    - name: Plug-in Manuals
      sort: true
      files:
        - name: Aoraï manual
          link: /download/aorai-manual-Silicon-20161101.pdf
        - name: Metrics manual
          link: /download/metrics-manual-Silicon-20161101.pdf
        - name: Rte manual
          link: /download/rte-manual-Silicon-20161101.pdf
        - name: Eva manual
          link: /download/value-analysis-Silicon-20161101.pdf
        - name: WP manual
          link: /download/wp-manual-Silicon-20161101.pdf
    - name: External Plug-ins
      files:
        - name: E-ACSL version 0.8
          link: /download/e-acsl/e-acsl-0.8.tar.gz
---