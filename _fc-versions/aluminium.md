---
layout: version
number: 13
name: Aluminium
acsl: 11
releases:
  - number: 0
    changelog: 20160502
    categories:
    - name: Frama-C v13.0 Aluminium
      files:
        - name: Source distribution
          link: /download/frama-c-Aluminium-20160502.tar.gz
          help: Compilation instructions
          help_link: /html/installations/aluminium.html
        - name: User manual
          link: /download/user-manual-Aluminium-20160501.pdf
        - name: Plug-In development guide
          link: /download/plugin-development-guide-Aluminium-20160501.pdf
        - name: API Documentation
          link: /download/frama-c-Aluminium-20160501_api.tar.gz
        - name: ACSL 1.11 (Aluminium implementation)
          link: /download/acsl-implementation-Aluminium-20160501.pdf
    - name: Plug-in Manuals
      sort: true
      files:
        - name: Aoraï manual
          link: /download/aorai-manual-Aluminium-20160501.pdf
        - name: Metrics manual
          link: /download/metrics-manual-Aluminium-20160501.pdf
        - name: Rte manual
          link: /download/rte-manual-Aluminium-20160501.pdf
        - name: Value Analysis manual
          link: /download/value-analysis-Aluminium-20160501.pdf
        - name: WP manual
          link: /download/wp-manual-Aluminium-20160501.pdf
---

Thre is no E-ACSL plug-in version for Frama-C Aluminium.