---
layout: version
number: 27
name: Cobalt
acsl: 19
releases:
- number: 1
  categories:
  - name: Frama-C v27.1 Cobalt
    files:
    - name: Source distribution
      link: /download/frama-c-27.1-Cobalt.tar.gz
      help: Compilation instructions
      help_link: https://git.frama-c.com/pub/frama-c/-/blob/27.1/INSTALL.md
    - name: User manual
      link: /download/user-manual-27.1-Cobalt.pdf
    - name: Plug-in development guide
      link: /download/plugin-development-guide-27.1-Cobalt.pdf
      help: Hello plug-in tutorial archive
      help_link: /download/hello-27.1-Cobalt.tar.gz
    - name: API Documentation
      link: /download/frama-c-27.1-Cobalt-api.tar.gz
    - name: Server API Documentation
      link: /download/frama-c-server-27.1-Cobalt-api.tar.gz
    - name: ACSL 1.19 (Cobalt implementation)
      link: /download/acsl-implementation-27.1-Cobalt.pdf
  - name: Plug-in Manuals
    sort: true
    files:
    - name: Aoraï manual
      link: /download/aorai-manual-27.1-Cobalt.pdf
      help: Aoraï example
      help_link: /download/aorai-example-27.1-Cobalt.tar.gz
    - name: Metrics manual
      link: /download/metrics-manual-27.1-Cobalt.pdf
    - name: Rte manual
      link: /download/rte-manual-27.1-Cobalt.pdf
    - name: Eva manual
      link: /download/eva-manual-27.1-Cobalt.pdf
    - name: WP manual
      link: /download/wp-manual-27.1-Cobalt.pdf
    - name: E-ACSL manual
      link: /download/e-acsl/e-acsl-manual-27.1-Cobalt.pdf
  - name: Ivette Packages (experimental)
    files:
    - name: Linux AppImage
      link: /download/frama-c-ivette-linux-27.1-Cobalt.AppImage
      help: README install
      help_link: /download/frama-c-ivette-app-install-27.1-Cobalt.md
    - name: macOS x86
      link: /download/frama-c-ivette-macos-x86-27.1-Cobalt.tar.gz
      help: README install
      help_link: /download/frama-c-ivette-app-install-27.1-Cobalt.md
    - name: macOS ARM
      link: /download/frama-c-ivette-macos-arm-27.1-Cobalt.tar.gz
      help: README install
      help_link: /download/frama-c-ivette-app-install-27.1-Cobalt.md
---
