---
layout: doc_page
title: Bug reporting
---
# Report an issue with Frama-C

If Frama-C crashes or behaves abnormally, you are invited to report an issue via the [Frama-C Gitlab repository](https://git.frama-c.com).

 The [New Issue page](https://git.frama-c.com/pub/frama-c/issues/new)
allows creating a new report, but you will need an account.

Unless you have an account provided by Frama-C the team, you need to sign in
using a [Github](https://github.com) account.

When creating a new issue, choose the `bug_report` template next to
*Title*, then enter the title and fill the template.

Bug reports can be marked as public or confidential. Public bug reports
can be read by anyone and may be indexed by search engines. Confidential
bug reports are only shown to developers.

Please fill the template as precisely as possible, *in English*[^1],
which helps the team more quickly understand, reproduce and respond to
the issue. The form uses Markdown syntax and you can attach source files
and screenshots to the issue.

[^1]: French is also a possible language choice for private entries.

Replies and updates concerning your issue are sent by e-mail by Gitlab.
