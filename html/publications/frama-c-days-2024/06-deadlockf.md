---
layout: gallery_element
title: "Finding Deadlocks and Data Races with Frama-C"
authors:
  - name: Tomáš Dacík
    bio: "
      is a Ph.D. student at Brno University of Technology. His research focuses
      on the static analysis of programs that manipulate dynamically allocated
      memory and the development of decision procedures for separation logic.
      He is also interested in lightweight methods for the detection of
      concurrency bugs.
      "
file: /download/publications/frama-c-days-2024/06-deadlockf.pdf
img:  06-deadlockf.png
---

This talk presents two Frama-C plugins designed for the lightweight detection
of data races (RacerF) and deadlocks (DeadlockF) in multi-threaded C programs.
Both plugins can operate in a fast mode, utilizing only syntactic information,
or in a more precise mode, leveraging results from the value analysis computed
by EVA. Our experimental evaluation on medium-sized programs demonstrates that
both plugins can detect known bugs while maintaining a very low false positive
rate.
