---
layout: gallery_element
title: "Where We Are, Where We Go"
authors:
  - name: Allan Blanchard
    bio: "
      is a researcher at CEA List and deputy head of the Software Safety and
      Security Laboratory for the topic related to analysis of software at
      source code level. His work mainly focuses on the development of the WP
      plug-in and the kernel of Frama-C.
      "
file: /download/publications/frama-c-days-2024/17-frama-c.pdf
img:  17-frama-c.png
---

This talk presents what we have done since the last Frama-C Days in
2019, in particular our work on usability through better user interfaces,
better analysis feedback and improved analysis and specification methods.
It also describes what we plan for the upcoming years.
