---
layout: gallery_element
title: "Building Automated Proofs of Refinement between State Machines and C"
authors:
  - name: Samuel Pollard
    bio: "
      is a computer scientist at Sandia National Laboratories, a United States
      Federally Funded Research and Development Center. Samuel’s work focuses on
      formal methods as it applies to high-performance computing,
      high-consequence embedded systems, and floating-point arithmetic.
      "
file: /download/publications/frama-c-days-2024/13-qframework.pdf
img:  13-qframework.png
---

Q Framework is a verification framework used at Sandia National Laboratories.
Q is a collection of tools used to verify safety and correctness properties of
high-consequence embedded systems. This correctness argument hinges on
constructing refinement relations between multiple state machine models, as
well as state machines and C implementations. This talk focuses on the latter
refinement to C code, which is achieved via automatically generated ACSL
annotations and proved via Frama-C. We give an overview of Q Framework and
describe how these ACSL annotations provide a refinement relation.
