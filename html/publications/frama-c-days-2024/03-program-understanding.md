---
layout: gallery_element
title: "Tools for Program Understanding"
authors:
  - name: André Maroneze
    bio: "
      is a researcher at CEA List in the Frama-C/Eva team. His current work
      include making Frama-C easier to run on new code bases and industrial case
      studies.
      "
file: /download/publications/frama-c-days-2024/03-program-understanding.pdf
img:  03-program-understanding.png
---
This presentation is the support for some live demos of plugins and tools
available in Frama-C, to help the user understand what a given code does, how
to navigate it, and how to explore analysis results.

Associated book chapter: [Tools for Program Understanding](https://link.springer.com/chapter/10.1007/978-3-031-55608-1_8).
