---
layout: gallery
title: "Frama-C Days 2024 Gallery"
---

![](/assets/img/blog/frama-c-days-2024/banner.jpg)

The Frama-C Days 2024 took place at the Maison de la Radio et de la Musique on
the 13 and 14 of June. There were many amazing talks by industrial and academic
partners as well as Frama-C developers. We would like to thank all speakers and
participants, this was truly a great moment!

In particular, we presented the brand new Frama-C Book that covers the most
important topics related to Frama-C. A lot of the talks of these Frama-C Days
were related to some chapters of the book. When this is the case, we give the
corresponding reference.

Feel free to download the slides of the different talks. We hope to see you at
some future Frama-C Days!
