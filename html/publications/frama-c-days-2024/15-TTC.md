---
layout: gallery_element
title: "Trust-Type Checker"
authors:
  - name: Benoît Boyer
    bio: "
      is researcher in computer science and formal methods at Mitsubishi
      Electric R&D Centre Europe (MERCE). He has a strong background in proof
      assistants (Coq) and static analysis.
      "
file: /download/publications/frama-c-days-2024/15-TTC.pdf
img:  15-TTC.png
---

TTC (Trust-Type Checker) is a static analyzer, designed as a Frama-C plugin,
for C programs. It is compositional in the sense that when TTC analyzes a
piece of code, it only uses the specification of the functions called (not the
function’s definition). TTC augments C’s “type system” with a notion of trust.
The analysis is akin to taint analysis, a kind of analysis found in security.
It lets users specify their data using annotations. The data is either unsafe
or (some flavor of) trusted, and TTC verifies that when a function is
expecting trusted data, the actual parameter contains data that is at least as
trusted as the function’s specification.
