---
layout: gallery_element
title: "Overview of the Frama C-Book"
authors:
  - name: Virgile Prevosto
    bio: "
      is a researcher at CEA List, senior expert in program analysis
      and formal methods. He has been involved in the development of Frama-C
      since 2006 and is supervising the kernel development as well as various
      specification plug-ins.
      "
  - name: Nikolai Kosmatov
    bio: "
      obtained a PhD in Mathematics in 2001 and a Habilitation in Computer
      Science in 2018. Currently, he leads the Formal Methods team at Thales
      Research & Technology, whose main goal is to bring innovative solutions in
      verification and validation to operational entities of Thales.
      "
  - name: Julien Signoles
    bio: "
      is a research director at Université Paris-Saclay, CEA, List. He is one of
      the main contributors to Frama-C since 2006. His research interests
      include formal methods for code safety and security and, more
      particularly, runtime annotation checking.
      "
file: /download/publications/frama-c-days-2024/01-book.pdf
img:  01-book.png
---
We present the brand new [Guide to Software Verification with Frama-C](https://link.springer.com/book/10.1007/978-3-031-55608-1),
a collective effort over several years to get a guidebook that presents a large
panorama of basic usages, research results, and concrete applications of Frama-C
since the very first open-source release of the platform in 2008. It covers the
ACSL specification language, core verification plug-ins, advanced analyses and
their combinations, key ingredients for developing new plug-ins, as well as
successful industrial case studies in which Frama-C has helped engineers verify
crucial safety or security properties.
