---
layout: gallery_element
title: "Towards Verification of Linux Kernel Code"
authors:
  - name: Julia Lawall
    bio: "
      is a researcher at Inria working at the crossroads of programming
      languages, software engineering, and operating systems. She is the
      designer and maintainer of Coccinelle, that has been extensively used to
      improve Linux kernel code.
      "
file: /download/publications/frama-c-days-2024/07-linux.pdf
img:  07-linux.png
---

The many bugs found regularly in the Linux kernel suggests that formal
verification of some core components could be valuable. However, a major
stumbling block is that fact that the rapid rate of change of Linux kernel
code implies that any such proof is soon out of date. We would like to
investigate the kinds of changes that occur in practice and the practical
impact of these changes on proofs. In this talk, we describe an initial
case study, using Frama-C and the WP plugin, considering the entire
history of the Linux kernel scheduler function should_we_balance.
