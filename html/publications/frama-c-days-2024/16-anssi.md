---
layout: gallery_element
title: "Necessary Static Code Analysis for Highest Level of Certification"
authors:
  - name: Franck Sadmi
    bio: "
      is the head of the French certification body at ANSSI
      "
file: /download/publications/frama-c-days-2024/16-anssi.pdf
img:  16-anssi.png
---

This talk presents how static analysis and formal methods improve the security
of the products and the situations where they are required in the French
scheme for Common Criteria certification.
