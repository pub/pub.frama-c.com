---
layout: gallery_element
title: "Specification and Verification of High-Level Properties"
authors:
  - name: Virgile Prevosto
    bio: "
      is a researcher at CEA List, senior expert in program analysis
      and formal methods. He has been involved in the development of Frama-C
      since 2006 and is supervising the kernel development as well as various
      specification plug-ins.
      "
file: /download/publications/frama-c-days-2024/08-hl-spec.pdf
img:  08-hl-spec.png
---

This talk, which is an introduction to Chap. 10 of the Frama-C book
presents three Frama-C plug-ins that provide Domain-Specific Languages (DSL)
for expressing different kinds of properties that are difficult to state as
plain ACSL annotations. First, MetAcsl is dedicated to pervasive properties,
that need to be checked at many points of the program. Second, RPP lets the
user state relational properties, that express a relation between several
function calls, as opposed to a function contract that only describe a single
call. Finally, Aoraï focuses on temporal properties, and more precisely on the
sequences of calls that are supposed to happen during the program execution.

Associated book chapter: [Specification and Verification of High-Level Properties](https://link.springer.com/chapter/10.1007/978-3-031-55608-1_10).
