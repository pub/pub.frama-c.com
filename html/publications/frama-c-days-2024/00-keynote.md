---
layout: gallery_element
title: "The VerCors verifier: a verifier for multiple concurrent programming languages"
authors:
  - name: Marieke Huisman
    bio: "
      is a professor of Software Reliability at the University of Twente, where
      she leads the Formal Methods and Tools Group. She is well-known for her
      research on reliability and correctness of concurrent and distributed
      software, which is supported by the VerCors verifier.
      "
file: /download/publications/frama-c-days-2024/00-keynote.pdf
img:  00-keynote.png
---

In this talk, I have given an overview of how VerCors is used for the
verification of concurrent software. Some small examples were shown to
illustrate the approach. Then I zoomed in on some subprojects. First, I
discussed how Alpinist, a tool which takes as input a verified program, and
then optimises both the program and specification, such that the resulting
program can be verified again. Then we discussed the Vesuv tool, which encodes
SystemC programs into input for the VerCors verifier. A particular challenge
here is that we wish to verify properties that relate multiple processes.
Finally, we discussed how we are working on support for the verification LLVM
programs, as a way to build verifiers for multiple programming languages in an
easy way. Finally, I concluded the talk by an outlook of multiple future
research directions.
