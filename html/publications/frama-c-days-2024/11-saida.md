---
layout: gallery_element
title: "An Exercice in Mind Reading: Automatic Contract Inference for Frama-C"
authors:
  - name: Jesper Amilon
    bio: "
      is a PhD student at the KTH Royal Institute of Technology in Sweden under
      the supervision of Dilian Gurov, working with formal verification of
      embedded systems software in collaboration with the heavy-vehicles
      manufacturer Scania.
      "
file: /download/publications/frama-c-days-2024/11-saida.pdf
img:  11-saida.png
---

We present the experimental Saida plugin aimed at inferring ACSL functions for
C programs. The plugin takes as input a program where only the main function
is annotated with an ACSL contract, and then attempts to infer ACSL contracts
for all functions that are called (possibly transitively) from the main
function. The inference engine is the TriCera tool, which is a Horn clause
based model checker for C programs. TriCera encodes the program as a set of
Horn clauses and then searches for a solution. From the solution, one may
extract contracts for any function that is called in the program. Naturally,
model checking may always scale, but we expect that, in many cases, the
contract inference technique may reduce the manual overhead when using
Frama-C.

Associated book chapter: [An Exercise in Mind Reading: Automatic Contract Inference for Frama-C](https://link.springer.com/chapter/10.1007/978-3-031-55608-1_13).
