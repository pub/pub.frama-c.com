---
layout: gallery_element
title: "Cegarmc: Integrating Software Model Checking into Frama-C"
authors:
  - name: Artjom Plaunov
    bio: "
      is currently a Master's student at the City University in New York. His
      current research interests are in automatic verification techniques
      (Abstract Interpretation and Model Checking), but is more broadly
      interested in formal verification of software.
      "
file: /download/publications/frama-c-days-2024/05-cegarmc.pdf
img:  05-cegarmc.png
---

Cegarmc is a Frama-C plugin for integrating software model checking into
Frama-C. The name is derived from CEGAR, or counterexample guided abstraction
refinement, which is one of the main techniques used by software model
checking to handle the state space explosion problem (However there are others
as well, such as bounded model checking and k-induction). The Cegarmc plugin
interoperates with other Frama-C plugins, such as EVA in order to provide
sound static analysis results to aid the model checking procedure.

Associated book chapter: [Combining Analyses Within Frama-C](https://link.springer.com/chapter/10.1007/978-3-031-55608-1_9).
