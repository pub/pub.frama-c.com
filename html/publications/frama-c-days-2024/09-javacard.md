---
layout: gallery_element
title: "Proof of Security Properties: Application to JavaCard Virtual Machine"
authors:
  - name: Adel Djoudi
    bio: "
      obtained a PhD in Computer Science at Université Paris-Saclay in 2016. He
      is interested in formal verification and analysis of critical software. He
      works as formal methods expert at Thales Digital Identity & Security.
      "
  - name: Nikolai Kosmatov
    bio: "
      obtained a PhD in Mathematics in 2001 and a Habilitation in Computer
      Science in 2018. Currently, he leads the Formal Methods team at Thales
      Research & Technology, whose main goal is to bring innovative solutions in
      verification and validation to operational entities of Thales.
      "
file: /download/publications/frama-c-days-2024/09-javacard.pdf
img:  09-javacard.png
---

This talk presents a retrospective of four years of successful application of
formal verification in an industrial context at Thales for security
certification. Its achievements allowed to consolidate a stringent software
development process of smart card products using deductive verification with
Frama-C/WP of a JavaCard virtual machine implementation. The project evolved
from initial uncertainties on feasibility at early stages up to the successful
Common Criteria certification at the highest level of security assurance
(EAL7). We present the verification methodology, share some results and best
practices we have learned during this journey, and outline some ongoing and
future work. This is a joint work with Martin Hána.

Associated book chapter: [Proof of Security Properties: Application to JavaCard Virtual Machine](https://link.springer.com/chapter/10.1007/978-3-031-55608-1_16).
