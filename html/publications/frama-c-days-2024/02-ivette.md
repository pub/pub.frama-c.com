---
layout: gallery_element
title: "Ivette, a new GUI for Frama-C"
authors:
  - name: Loïc Correnson
    bio: "
      is Senior Expert at CEA in formal methods and software verification. He is
      the main designer and implementer of the Frama-C/WP plug-in for deductive
      verification, but was also involved for a long time in user-interface
      development and initiated the complete redesign of frama-c-gui into Ivette.
      "
file: /download/publications/frama-c-days-2024/02-ivette.pdf
img:  02-ivette.png
---
We present Ivette, the new graphical user interface for Frama-C.
Ivette is a complete redesign of our oldish frama-c-gui. Based on a modern
web-originated software stack, Ivette offers much more flexibility, benefits
from a wide ecosystem, and allows us to welcome external contributors much more
easily than its predecessor.

Moreover, contrarily to our old frama-c-gui, the new design of Ivette is based
on a clear separation from the Frama-C side (OCaml based, sequential) and the
User Interface side (Typescript & React based, asynchronous). The Frama-C side,
supported by its new Server plug-in, allows for further integration in other
contexts, typically code editors.

Frama-C Server requests are auto-documented and easy to implement by Frama-C
plug-in developers. Ivette is built upon React and Electron, and is easily
extensible in Typescript thanks to a rich toolkit of ready-to-used components
named Dome. A rich middleware is available, which makes using Frama-C Server
requests straightforward and quite efficient.

Currently, Ivette has built-in support for only EVA and WP and it is still under
active development. Although, we already use it on a day-to-day basis for all of
our new projects and we plan to deprecate the old frama-c-gui very quickly.
