---
layout: gallery_element
title: "Advanced Memory and Shape Analyses"
authors:
  - name: Matthieu Lemerre
    bio: "
      is a researcher at CEA List, expert in programming languages, static
      analysis and system software verification. He supervised 8 theses and
      led more than 15 research projects. He regularly publishes papers in
      reference conferences.
      "
file: /download/publications/frama-c-days-2024/12-shape.pdf
img:  12-shape.png
---

The talk discusses the fundamental role of memory analysis when analyzing
general-purpose C code and how to structure a static analyzer able to analyze
memory. It then discusses two advanced memory analyzes implemented as Frama-C
plugins: one based on separation logic reasoning about strong invariants, and
one based on advanced type systems reasoning about less strong (but more
general) invariants. Both analyses are modular (analyzing functions without
having to know the whole program), which is key to scalability, integrating
static analysis in the development phase, or to analyze reusable code such as
libraries.

Associated book chapter: [Advanced Memory and Shape Analyses](https://link.springer.com/chapter/10.1007/978-3-031-55608-1_11).
