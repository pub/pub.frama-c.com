---
layout: gallery_element
title: "Numerical Filter Code Analysis"
authors:
  - name: Pierre-Yves Piriou
    bio: "
      is a research engineer at EDF. His work focus on the development of formal
      methods to demonstrate the reliability of complex critical systems.
      "
file: /download/publications/frama-c-days-2024/14-filters.pdf
img:  14-filters.png
---

This talk presents how to produce some complex loop invariants for numerical
embedded code and and how Frama-C can be used to verify them. Numerical
embedded code usually defines an endless loop that takes inputs from sensors
and that emits outputs for actuators. Moreover, such a code commonly uses
some floating-point global memories to keep track of the input or output
values from the previous loop cycles. These memories store a summary of
previous values to filter the input or the output over time. Hence,
recursive linear or Infinite Impulse Response (IIR) filters are very common
in such code. Analysing filter code with Frama-C is challenging, since
finding a loop invariant with complex relationships between the variables is
never an evident task. Hence, this chapter presents different approaches to
find and organize inductive invariants for such numerical reactive systems
and shows how Frama-C can prove them with its Eva and Wp plug-ins. It also
exposes optimal theoretical results to compare the quality of results of the
different solutions. Several examples and several concrete solutions
illustrate the generation of such inductive invariants and their formal
verification.

Associated book chapter: [Analysis of Embedded Numerical Programs in the Presence of Numerical Filters](https://link.springer.com/chapter/10.1007/978-3-031-55608-1_12).
