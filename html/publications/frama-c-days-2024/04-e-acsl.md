---
layout: gallery_element
title: "Runtime Annotation Checking with Frama-C: The E-ACSL Plug-in"
authors:
  - name: Julien Signoles
    bio: "
      is a research director at Université Paris-Saclay, CEA, List. He is one of
      the main contributors to Frama-C since 2006. His research interests
      include formal methods for code safety and security and, more
      particularly, runtime annotation checking.
      "
file: /download/publications/frama-c-days-2024/04-e-acsl.pdf
img:  04-e-acsl.png
---

Runtime Annotation Checking (RAC) is a lightweight formal method consisting
in checking code annotations written in the source code during the program
execution. While static formal methods aim for guarantees that hold for any
execution of the analyzed program, RAC only provides guarantees about the
particular execution it monitors. This allows RAC-based tools to be used to
check a wide range of properties with minimum intervention from the user.
Frama-C can perform RAC on C programs with the plug-in E-ACSL.

This talk presents RAC through practical use with E-ACSL, shows advanced uses of
E-ACSL leveraging the collaboration with other plug-ins, and sheds some
light on the internals of E-ACSL and the technical difficulties of
implementing RAC.

Associated book chapter: [Runtime Annotation Checking with Frama-C: The E-ACSL Plug-in](https://link.springer.com/chapter/10.1007/978-3-031-55608-1_5).
