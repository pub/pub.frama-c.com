---
layout: feature
css: plugin
title: Ivette - Frama-C
description: "Ivette: a new Graphical User Interface for Frama-C"
active: ivette
---

# Ivette: a new Graphical User Interface for Frama-C


**Ivette** is a new Graphical User Interface for Frama-C. It is aimed at
replacing in a midterm basis the brave, although old, GTK-based `frama-c-gui`
user interface.

As many Frama-C developers and users, we really want a desktop application. We
finally ended up with the following strong design directions:

- The GUI shall be a desktop application based on an HTML/CSS engine.
- The GUI shall be coded in a language that User Interface experts know
about, with very good community support and open-source tools.
- The GUI and the Static Analyzer shall live in independent processes, with
middlewares and protocols to connect them with each other.
- GUI components shall be as much as possible agnostic to the precise meaning
of static analyzer data; no complex semantic treatment over data shall be
performed on the GUI side.
- Static Analyzer plug-ins shall be ready to interact with some external User
Environment, but without any dependency on the middleware that will be
actually responsible for the connection.

## Overview

![](/assets/img/gui/ivette-all.png)

**Ivette** has been designed with modern HTML5 technologies in order to explore
new ways of interacting with the Frama-C platform. It is still experimental but
ready to use for EVA-centric usage. This [blog post](/2022/07/12/frama-c-25-ivette.html)
provides a gentle introduction to **Ivette**. Any feedback is very welcomed!

## Architecture

The technical stack we have chosen so far consists of the following frameworks:

- A GUI desktop application using the HTML5 and [NodeJS](https://nodejs.org)
  JavaScript runtime engine of the [Electron](https://www.electronjs.org)
  platform.
- A GUI code base written in [TypeScript](https://www.typescriptlang.org/) with
the Reactive Programming framework [ReactJS](https://react.dev/).
- A [Frama-C/Server plug-in](/fc-plugins/server.html) written in OCaml, that
provides an asynchronous, JSON-based, strongly typed Request System.
- Each Frama-C plug-in will then register new requests in the Server plug-in,
  independently of any communication protocol with any external User Environment.
- A collection of middlewares and protocols written in different languages to
  specifically connect the Frama-C/Server plug-in and the ReactJS code with
  each other.

The GUI code itself is entirely written in TypeScript and is split into three
parts:

- The Dome framework, which is a collection of carefully designed and themed
  high-level components, offering a predefined choice of features.
- The Ivette framework, which is an application built with Electron and Dome
  featuring the main GUI environment and managing the connection with Frama-C.
- The Frama-C plug-in components for Ivette are build from Dome components
  and interact with the Frama-C static analyzer plug-ins.

More details can be found in [this article](/download/publications/correnson_fide_2022.pdf).
