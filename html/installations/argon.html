---
layout: installation_page
version: argon
title: Installation instructions for 18.0 Argon
---

<h1 id="installing-frama-c">Installing Frama-C Argon (released on 2018-10-29)</h1>
<h2 id="table-of-contents">Table of Contents</h2>
<ul>
<li><a href="#installing-frama-c">Installing Frama-C</a>
<ul>
<li><a href="#table-of-contents">Table of Contents</a></li>
<li><a href="#installing-frama-c-via-opam">Installing Frama-C via
opam</a>
<ul>
<li><a href="#installing-opam">Installing opam</a></li>
<li><a href=
"#installing-custom-versions-of-frama-c-via-opam">Installing Custom
Versions of Frama-C via opam</a></li>
<li><a href=
"#installing-frama-c-on-windows-via-cygwin--opam">Installing
Frama-C on Windows (via Cygwin + opam)</a></li>
<li><a href="#installing-frama-c-on-macos">Installing Frama-C on
macOS</a></li>
</ul>
</li>
<li><a href=
"#installing-frama-c-via-your-linux-distribution-debianubuntufedora">
Installing Frama-C via your Linux distribution
(Debian/Ubuntu/Fedora)</a></li>
<li><a href="#compiling-from-source">Compiling from source</a>
<ul>
<li><a href="#quick-start">Quick Start</a></li>
<li><a href="#full-compilation-guide">Full Compilation
Guide</a></li>
</ul>
</li>
<li><a href="#available-resources">Available resources</a>
<ul>
<li><a href="#executables-in-install_dirbin">Executables: (in
<code>/INSTALL_DIR/bin</code>)</a></li>
<li><a href=
"#shared-files-in-install_dirshareframa-c-and-subdirectories">Shared
files: (in <code>/INSTALL_DIR/share/frama-c</code> and
subdirectories)</a></li>
<li><a href=
"#documentation-files-in-install_dirshareframa-cdoc">Documentation
files: (in <code>/INSTALL_DIR/share/frama-c/doc</code>)</a></li>
<li><a href="#object-files-in-install_dirlibframa-c">Object files:
(in <code>/INSTALL_DIR/lib/frama-c</code>)</a></li>
<li><a href="#plugin-files-in-install_dirlibframa-cplugins">Plugin
files: (in <code>/INSTALL_DIR/lib/frama-c/plugins</code>)</a></li>
<li><a href="#man-files-in-install_dirmanman1">Man files: (in
<code>/INSTALL_DIR/man/man1</code>)</a></li>
</ul>
</li>
<li><a href="#installing-additional-plugins">Installing Additional
Plugins</a></li>
<li><a href="#have-fun-with-frama-c">HAVE FUN WITH
FRAMA-C!</a></li>
</ul>
</li>
</ul>
<h2 id="installing-frama-c-via-opam">Installing Frama-C via
opam</h2>
<p><a href="http://opam.ocaml.org/">opam</a> is the OCaml package
manager. Every Frama-C release is made available via an opam
package.</p>
<p>First you need to install opam, then you may install Frama-C
using opam:</p>
<pre><code>opam install frama-c
</code></pre>
<p><strong>Note:</strong> make sure your opam version is &gt;=
1.2.2. Also, it is highly recommended that you install an external
solver for opam, such as <code>aspcud</code>, otherwise unexpected
dependency errors may occur during installation.</p>
<h3 id="installing-opam">Installing opam</h3>
<p>Several Linux distributions already include an <code>opam</code>
package.</p>
<p>OSX has opam through Homebrew.</p>
<p>A <a href=
"https://fdopen.github.io/opam-repository-mingw/installation">Cygwin-based
opam</a> is available on Windows. It is less stable than it is for
the other OSes, but should work.</p>
<p>If your system does not have an opam package &gt;= 1.2.2 you can
compile it from source, or use the provided opam binaries available
at:</p>
<p><a href="http://opam.ocaml.org/doc/Install.html" class=
"uri">http://opam.ocaml.org/doc/Install.html</a></p>
<h3 id="installing-frama-c-from-opam">Installing Frama-C from
opam</h3>
<p>The Frama-C package in opam is called <code>frama-c</code>,
which includes both the command-line <code>frama-c</code>
executable and the graphical interface
<code>frama-c-gui</code>.</p>
<p>(Note: before version 16 Sulfur, there were two packages,
<code>frama-c-base</code> and <code>frama-c</code>, which were
merged together.)</p>
<p><code>frama-c</code> includes non-OCaml dependencies, such as
Gtk and GMP. In most systems, opam can take care of these external
dependencies through its <code>depext</code> plug-in: issuing the
two commands</p>
<pre><code>opam install depext
opam depext frama-c
</code></pre>
<p>will install the appropriate system packages (this of course
requires administrator rights on the system).</p>
<p>If your system is not supported by <code>depext</code>, you will
need to install Gtk, GtkSourceView, GnomeCanvas and GMP, including
development libraries, separately. If you do so, please consider
providing the system name and list of packages (e.g. via a <a href=
"https://github.com/Frama-C/Frama-C-snapshot/issues/new">Github
issue</a>) so that we can add it to the Frama-C <code>depext</code>
package.</p>
<h3 id="known-working-configuration">Known working
configuration</h3>
<p>The following set of packages is known to be a working
configuration for Frama-C 18 (Argon):</p>
<ul>
<li>OCaml 4.05.0</li>
<li>alt-ergo.1.30 or, under a non-commercial license,
alt-ergo.2.0.0 (pin recommended)</li>
<li>apron.20160125 (optional)</li>
<li>coq.8.7.2 (optional; pin recommended)</li>
<li>lablgtk.2.18.5</li>
<li>mlgmpidl.1.2.7 (optional)</li>
<li>ocamlgraph.1.8.8</li>
<li>why3.0.88.3</li>
<li>yojson.1.4.1 (optional)</li>
<li>zarith.1.7</li>
</ul>
<p>Note: <em>pin recommended</em> indicates packages likely to
become incompatible in future releases; <code>opam pin</code> is
recommended to prevent them from breaking.</p>
<h3 id="installing-custom-versions-of-frama-c-via-opam">Installing
Custom Versions of Frama-C via opam</h3>
<p>If you have a <strong>non-standard</strong> version of Frama-C
available (with proprietary extensions, custom plugins, etc.), you
can use opam to install Frama-C's dependencies and compile your own
sources directly:</p>
<pre>
<code># optional: remove the standard frama-c package if it was installed
opam remove --force frama-c frama-c-base

# install Frama-C's dependencies
opam install depext
opam depext frama-c
opam install --deps-only frama-c

# install custom version of frama-c
opam pin add --kind=path frama-c &lt;dir&gt;
</code></pre>
<p>where <code>&lt;dir&gt;</code> is the root of your unpacked
Frama-C archive. See <code>opam pin</code> for more details.</p>
<p>If your extensions require other libraries than the ones already
used by Frama-C, they must of course be installed as well.</p>
<h3 id="installing-frama-c-on-windows-via-cygwin--opam">Installing
Frama-C on Windows (via Cygwin + opam)</h3>
<p>Windows is not officially supported by the Frama-C team (as in,
we may not have the time to fix all issues), but Frama-C has been
successfully compiled in Windows with the following tools:</p>
<ul>
<li>Cygwin (for shell and installation support only; the compiled
binaries do not depend on Cygwin)</li>
<li>opam for Windows (currently experimental)</li>
<li>OCaml MinGW-based compiler</li>
</ul>
<p>You may follow these instructions for installing OCaml for
Windows:</p>
<p><a href=
"https://fdopen.github.io/opam-repository-mingw/installation/"
class=
"uri">https://fdopen.github.io/opam-repository-mingw/installation/</a></p>
<p>Note that <code>lablgtk</code> (used by Frama-C) requires
installing <code>depext</code> and <code>depext-cygwinports</code>,
as indicated in the page.</p>
<p>Once the Windows-based opam repository is configured, simply
run:</p>
<pre><code>opam install frama-c
</code></pre>
<p>Some (now obsoleted) compilation instructions for older versions
of Frama-C on Windows are available on the Frama-C wiki:</p>
<p><a href=
"https://bts.frama-c.com/dokuwiki/doku.php?id=mantis:frama-c:compiling_from_source"
class=
"uri">https://bts.frama-c.com/dokuwiki/doku.php?id=mantis:frama-c:compiling_from_source</a></p>
<h3 id="installing-frama-c-on-macos">Installing Frama-C on
macOS</h3>
<p><a href="https://opam.ocaml.org">opam</a> works perfectly on
macOS via <a href="https://brew.sh">Homebrew</a>. We recommend to
rely on it for the installation of Frama-C.</p>
<ol>
<li>
<p>Install <em>required</em> general macOS tools for OCaml:</p>
<pre class="shell"><code>brew install autoconf pkg-config opam
</code></pre>
<p>Do not forget to <code>opam init</code> and <code>eval `opam
config env`</code> for a proper opam installation (if not already
done before on your machine).</p>
</li>
<li>
<p>Install <em>required</em> dependencies for Frama-C:</p>
<pre class="shell">
<code>brew install gmp gtk+ gtksourceview libgnomecanvas
</code></pre></li>
<li>
<p>Install <em>recommended</em> dependencies for Frama-C:</p>
<pre class="shell"><code>brew install graphviz
opam install why3
</code></pre></li>
<li>
<p>Install <em>optional</em> dependencies for Frama-C/WP:</p>
<pre class="shell"><code>opam install coq coqide
</code></pre></li>
<li>
<p>Install Frama-C:</p>
<pre class="shell"><code>opam install frama-c
</code></pre></li>
</ol>
<h2 id=
"installing-frama-c-via-your-linux-distribution-debianubuntufedora">
Installing Frama-C via your Linux distribution
(Debian/Ubuntu/Fedora)</h2>
<p><strong>NOTE</strong>: Distribution packages are updated later
than opam packages, so if you want access to the most recent
versions of Frama-C, opam is currently the recommended
approach.</p>
<p>Also note that it is <strong>not</strong> recommended to mix
OCaml packages installed by your distribution with packages
installed via opam. When using opam, we recommend uninstalling all
<code>ocaml-*</code> packages from your distribution, and then
installing, exclusively via opam, an OCaml compiler and all the
OCaml packages you need. This ensures that only those versions will
be in the PATH.</p>
<p>The advantage of using distribution packages is that
dependencies are almost always handled by the distribution's
package manager. The disadvantage is that, if you need some
optional OCaml package that has not been packaged in your
distribution (e.g. <code>landmarks</code>, which is distributed via
opam), it may be very hard to install it, since mixing opam and
non-opam packages often fails (and is <strong>strongly</strong>
discouraged).</p>
<p>Debian/Ubuntu: <code>apt-get install frama-c</code></p>
<p>Fedora: <code>dnf install frama-c</code></p>
<p>Arch Linux: <code>yaourt -S frama-c</code></p>
<h2 id="compiling-from-source">Compiling from source</h2>
<p><strong>Note</strong>: These instructions are no longer required
in the vast majority of cases. They are kept here mostly for
historical reference.</p>
<h3 id="quick-start">Quick Start</h3>
<ol>
<li>
<p>Install OCaml, OCamlfind, OCamlGraph and Zarith if not already
installed. Note that OCaml &gt;= 4.02.3 is needed in order to
compile Frama-C.</p>
</li>
<li>
<p>(Optional) For the GUI, also install Gtk, GtkSourceView,
GnomeCanvas and Lablgtk2 if not already installed. See section
'REQUIREMENTS' below for indications on the names of the packages
to install, or use 'opam depext' as explained in section 'Opam'
above.</p>
</li>
<li>
<p>On Linux-like distributions:</p>
<pre>
<code> ./configure &amp;&amp; make &amp;&amp; sudo make install
</code></pre>
<p>See section <em>Configuration</em> below for options.</p>
</li>
<li>
<p>On Windows+Cygwin:</p>
<pre>
<code> ./configure --prefix="$(cygpath -a -m &lt;installation path&gt;)" &amp;&amp; make &amp;&amp; make install
</code></pre></li>
<li>
<p>The binary <code>frama-c</code> (and <code>frama-c-gui</code> if
you have lablgtk2) is now installed.</p>
</li>
<li>
<p>Optionally, test your installation by running:</p>
<pre><code> frama-c -val tests/misc/CruiseControl*.c
 frama-c-gui -val tests/misc/CruiseControl*.c # if frama-c-gui is available
</code></pre></li>
</ol>
<h3 id="full-compilation-guide">Full Compilation Guide</h3>
<h4 id="frama-c-requirements">Frama-C Requirements</h4>
<ul>
<li>GNU make version &gt;= 3.81</li>
<li>OCaml &gt;= 4.02.3</li>
<li>a C compiler with standard C and POSIX headers and
libraries</li>
<li><a href="http://ocamlgraph.lri.fr">OCamlGraph</a> &gt;=
1.8.8</li>
<li><a href=
"http://projects.camlcity.org/projects/findlib.html">findlib</a>
&gt;= 1.6.1</li>
<li><a href="http://github.com/ocaml/Zarith">Zarith</a></li>
</ul>
<p>The Frama-C GUI also requires:</p>
<ul>
<li>Gtk (&gt;= 2.4)</li>
<li>GtkSourceView 2.x</li>
<li>GnomeCanvas 2.x</li>
<li>LablGtk &gt;= 2.18.5</li>
</ul>
<p>Plugins may have their own requirements. Consult their specific
documentations for details.</p>
<h4 id="configuration">Configuration</h4>
<p>Frama-C is configured by <code>./configure [options]</code>.</p>
<p><code>configure</code> is generated by <code>autoconf</code>, so
that the standard options for setting installation directories are
available, in particular <code>--prefix=/path</code>.</p>
<p>A plugin can be enabled by <code>--enable-plugin</code> and
disabled by <code>--disable-plugin</code>. By default, all
distributed plugins are enabled. Those who default to 'no' are not
part of the Frama-C distribution (usually because they are too
experimental to be released as is).</p>
<p>See <code>./configure --help</code> for the current list of
plugins, and available options.</p>
<h5 id="under-cygwin">Under Cygwin</h5>
<p>Use <code>./configure --prefix="$(cygpath -a -m &lt;installation
path&gt;)"</code>.</p>
<p>(using Unix-style paths without the drive letter will probably
not work)</p>
<h4 id="compilation">Compilation</h4>
<p>Type <code>make</code>.</p>
<p>Some Makefile targets of interest are:</p>
<ul>
<li><code>doc</code> generates the API documentation.</li>
<li><code>top</code> generates an OCaml toplevel embedding Frama-C
as a library.</li>
<li><code>oracles</code> sets up the Frama-C test suite oracles for
your own configuration.</li>
<li><code>tests</code> performs Frama-C's own tests.</li>
</ul>
<h4 id="installation">Installation</h4>
<p>Type <code>make install</code> (depending on the installation
directory, this may require superuser privileges. The installation
directory is chosen through <code>--prefix</code>).</p>
<h4 id="testing-the-installation">Testing the Installation</h4>
<p>This step is optional.</p>
<p>Test your installation by running:</p>
<pre><code>frama-c -val tests/misc/CruiseControl*.c
frama-c-gui -val tests/misc/CruiseControl*.c (if frama-c-gui is available)
</code></pre>
<h4 id="api-documentation">API Documentation</h4>
<p>For plugin developers, the API documentation of the Frama-C
kernel and distributed plugins is available in the file
<code>frama-c-api.tar.gz</code>, after running <code>make
doc-distrib</code>.</p>
<h4 id="uninstallation">Uninstallation</h4>
<p>Type <code>make uninstall</code> to remove Frama-C and all the
installed plugins. (Depending on the installation directory, this
may require superuser privileges.)</p>
<h2 id="available-resources">Available resources</h2>
<p>Once Frama-C is installed, the following resources should be
installed and available:</p>
<h3 id="executables-in-install_dirbin">Executables: (in
<code>/INSTALL_DIR/bin</code>)</h3>
<ul>
<li><code>frama-c</code></li>
<li><code>frama-c-gui</code> if available</li>
<li><code>frama-c-config</code> displays Frama-C configuration
paths</li>
<li><code>frama-c.byte</code> bytecode version of frama-c</li>
<li><code>frama-c-gui.byte</code> bytecode version of frama-c-gui,
if available</li>
<li><code>ptests.opt</code> testing tool for Frama-c</li>
<li><code>frama-c.toplevel</code> if 'make top' previously
done</li>
</ul>
<h3 id=
"shared-files-in-install_dirshareframa-c-and-subdirectories">Shared
files: (in <code>/INSTALL_DIR/share/frama-c</code> and
subdirectories)</h3>
<ul>
<li>some <code>.h</code> and <code>.c</code> files used as preludes
by Frama-C</li>
<li>some <code>Makefiles</code> used to compile dynamic
plugins</li>
<li>some <code>.rc</code> files used to configure Frama-C</li>
<li>some image files used by the Frama-C GUI</li>
<li>some files for Frama-C/plug-in development (autocomplete
scripts, Emacs settings, scripts for running Eva, ...)</li>
</ul>
<h3 id="documentation-files-in-install_dirshareframa-cdoc">
Documentation files: (in
<code>/INSTALL_DIR/share/frama-c/doc</code>)</h3>
<ul>
<li>files used to generate dynamic plugin documentation</li>
</ul>
<h3 id="object-files-in-install_dirlibframa-c">Object files: (in
<code>/INSTALL_DIR/lib/frama-c</code>)</h3>
<ul>
<li>object files used to compile dynamic plugins</li>
</ul>
<h3 id="plugin-files-in-install_dirlibframa-cplugins">Plugin files:
(in <code>/INSTALL_DIR/lib/frama-c/plugins</code>)</h3>
<ul>
<li>object files of available dynamic plugins</li>
</ul>
<h3 id="man-files-in-install_dirmanman1">Man files: (in
<code>/INSTALL_DIR/man/man1</code>)</h3>
<ul>
<li><code>man</code> files for <code>frama-c</code> (and
<code>frama-c-gui</code> if available)</li>
</ul>
<h2 id="installing-additional-plugins">Installing Additional
Plugins</h2>
<p>Plugins may be released independently of Frama-C.</p>
<p>The standard way for installing them should be:</p>
<pre><code>./configure &amp;&amp; make &amp;&amp; make install
</code></pre>
<p>Plugins may have their own custom installation procedures.
Consult their specific documentation for details.</p>
<h2 id="have-fun-with-frama-c">HAVE FUN WITH FRAMA-C!</h2>
