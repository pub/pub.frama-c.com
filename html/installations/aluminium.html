---
layout: installation_page
version: aluminium
title: Installation instructions for Aluminium
---

<h1 id="installing-frama-c">Installing Frama-C Aluminium (released on 2016-05-02)</h1>
<h2 id="table-of-contents">Table of Contents</h2>
<ul>
<li><a href="#recommended-mode-opam">Recommended mode: OPAM</a></li>
<li><a href="#frama-c-requirements">Frama-C Requirements</a></li>
<li><a href="#installing-opam">Installing OPAM</a></li>
<li><a href="#installing-frama-c-from-opam">Installing Frama-C from OPAM</a></li>
<li><a href="#installing-custom-versions-of-frama-c-via-opam">Installing Custom Versions of Frama-C via OPAM</a></li>
<li><a href="#installing-frama-c-on-windows-via-cygwin--opam">Installing Frama-C on Windows (via Cygwin + OPAM)</a></li>
<li><a href="#installing-frama-c-on-mac-os-x">Installing Frama-C on Mac OS X</a></li>
<li><a href="#installing-frama-c-via-your-linux-distribution-debianubuntufedora">Installing Frama-C via your Linux distribution (Debian/Ubuntu/Fedora)</a></li>
<li><a href="#debianubuntu">Debian/Ubuntu</a></li>
<li><a href="#fedora">Fedora</a></li>
<li><a href="#compiling-from-source">Compiling from source</a></li>
<li><a href="#quick-start">Quick Start</a></li>
<li><a href="#full-compilation-guide">Full Compilation Guide</a></li>
<li><a href="#available-resources">Available resources</a></li>
<li><a href="#installing-additional-plugins">Installing Additional Plugins</a></li>
</ul>
<h2 id="recommended-mode-opam">Recommended mode: OPAM</h2>
<p>The preferred method of installation for Frama-C is using<br />
<a href="http://opam.ocaml.org/">OPAM</a> (v1.2 or newer).</p>
<p>First you need to install OPAM, then you may install Frama-C using OPAM:</p>
<pre><code>opam install frama-c</code></pre>
<p><strong>Note:</strong> make sure your OPAM version is &gt;= 1.2.<br />
Also, it is highly recommended that you install an external solver<br />
for OPAM, such as <code>aspcud</code>, otherwise unexpected dependency errors<br />
may occur during installation.</p>
<h3 id="frama-c-requirements">Frama-C Requirements</h3>
<ul>
<li>OCaml 4.xx (incompatible with versions 4.02.2, 4.02.0 and 4.00.0)</li>
<li>A C compiler (gcc, clang, CompCert, etc.)</li>
<li>GNU Make</li>
</ul>
<h3 id="installing-opam">Installing OPAM</h3>
<p>Several Linux distributions already include an <code>opam</code> package.</p>
<p>OSX has OPAM through Homebrew.</p>
<p>A Windows OPAM is currently being developed, but it is not yet stable.</p>
<p><strong>Note</strong>: Some distributions include an old version of OPAM (&lt;= 1.1).<br />
It cannot be used to reliably install Frama-C due to conflicts<br />
between dependencies.</p>
<p>If your system does not have an OPAM package, you can compile it from source,<br />
or use the provided OPAM binaries available at:</p>
<p><a href="http://opam.ocaml.org/doc/Install.html" class="uri">http://opam.ocaml.org/doc/Install.html</a></p>
<h3 id="installing-frama-c-from-opam">Installing Frama-C from OPAM</h3>
<p>There are two Frama-C packages in OPAM:</p>
<ul>
<li><code>frama-c-base</code>: minimal Frama-C installation, without GUI; few dependencies</li>
<li><code>frama-c</code>: includes all GUI-related dependencies, plus other recommended<br />
packages.</li>
</ul>
<p>The <code>frama-c</code> package recommends the installation of optional packages, e.g.<br />
external provers for WP, such as <code>why3</code> and <code>coq</code>.</p>
<p>To install <code>frama-c</code>, you may need to install Gtk, GtkSourceView and<br />
GnomeCanvas separately.<br />
These are C libraries with OCaml bindings used by the GUI.<br />
To get the exact list of packages that are needed, use:</p>
<pre><code>opam install depext
opam depext frama-c</code></pre>
<p>and install the packages listed as missing.</p>
<h3 id="installing-custom-versions-of-frama-c-via-opam">Installing Custom Versions of Frama-C via OPAM</h3>
<p>If you have a <strong>non-standard</strong> version of Frama-C available<br />
(with proprietary extensions, custom plugins, etc.),<br />
you can install it through OPAM using these commands:</p>
<pre><code># remove the previous version of frama-c
opam remove --force frama-c frama-c-base

# optional packages, but recommended (for efficiency, and for the GUI)
opam install depext
opam depext zarith lablgtk conf-gtksourceview conf-gnomecanvas
opam install zarith lablgtk conf-gtksourceview conf-gnomecanvas

# install custom version of frama-c
opam pin add frama-c-base &lt;dir&gt;</code></pre>
<p>where <code>&lt;dir&gt;</code> is the root of your unpacked Frama-C archive.</p>
<h3 id="installing-frama-c-on-windows-via-cygwin-opam">Installing Frama-C on Windows (via Cygwin + OPAM)</h3>
<p>Windows is not officially supported by the Frama-C team<br />
(as in, we may not have the time to fix all issues),<br />
but Frama-C has been succesfully compiled in Windows with the following tools:</p>
<ul>
<li>Cygwin (for shell and installation support only;<br />
the compiled binaries do not depend on Cygwin)</li>
<li>OPAM for Windows (currently experimental)</li>
<li>OCaml MinGW-based compiler</li>
</ul>
<p>Installation instructions are described (and updated continuously) on the<br />
Frama-C wiki:</p>
<p><a href="https://bts.frama-c.com/dokuwiki/doku.php?id=mantis:frama-c:compiling_from_source" class="uri">https://bts.frama-c.com/dokuwiki/doku.php?id=mantis:frama-c:compiling_from_source</a></p>
<p>(<em>Note: Your browser may complain about the self-signed certificate.</em>)</p>
<p>Frama-C Windows releases are periodically made available on the non-official<br />
OPAM MinGW repository:</p>
<p><a href="https://github.com/fdopen/opam-repository-mingw" class="uri">https://github.com/fdopen/opam-repository-mingw</a></p>
<h3 id="installing-frama-c-on-mac-os-x">Installing Frama-C on Mac OS X</h3>
<p>OPAM works perfectly on Mac OS via Homebrew.</p>
<p>Recommended installation:</p>
<p>General Mac OS tools for OCaml:</p>
<pre><code>xcode-select --install
open http://brew.sh
brew install git autoconf meld opam</code></pre>
<p>Graphical User Interface:</p>
<pre><code>brew install gtk+ --with-jasper
brew install gtksourceview libgnomecanvas graphviz
opam install lablgtk ocamlgraph</code></pre>
<p>Recommended for Frama-C:</p>
<pre><code>brew install gmp
opam install zarith</code></pre>
<p>Necessary for Frama-C/WP:</p>
<pre><code>opam install alt-ergo</code></pre>
<p>Also recommended for Frama-C/WP:</p>
<pre><code>opam install altgr-ergo coq coqide why3</code></pre>
<h2 id="installing-frama-c-via-your-linux-distribution-debianubuntufedora">Installing Frama-C via your Linux distribution (Debian/Ubuntu/Fedora)</h2>
<p><strong>NOTE</strong>: Distribution packages are not as up-to-date as OPAM packages.<br />
We recommend using OPAM if possible.</p>
<h3 id="debianubuntu">Debian/Ubuntu</h3>
<p>If you are using Debian &gt;= Squeeze 6.0 or Ubuntu &gt;= Lucid Lynx 10.04 then<br />
a Frama-C package is provided:</p>
<pre><code>sudo apt-get install frama-c</code></pre>
<p>or, if you don't want the Gtk-based GUI:</p>
<pre><code>sudo apt-get install frama-c-base</code></pre>
<h3 id="fedora">Fedora</h3>
<p>If you are using Fedora &gt;= 13 then a Frama-C package is provided:</p>
<pre><code>yum install frama-c</code></pre>
<h2 id="compiling-from-source">Compiling from source</h2>
<p><strong>Note</strong>: These instructions are no longer required in the vast majority<br />
of cases. They are kept here mostly for historical reference.</p>
<h3 id="quick-start">Quick Start</h3>
<ol>
<li><p>Install OCaml if not already installed.</p></li>
<li><p>(Optional) For the GUI, also install Gtk, GtkSourceView, GnomeCanvas and<br />
Lablgtk2 if not already installed. If possible, also install Zarith.<br />
See section 'REQUIREMENTS' below for indications on the names of the<br />
packages to install, or use 'opam depext' as explained in section 'Opam'<br />
above.</p></li>
<li><p>On Linux-like distributions:</p>
<pre><code>./configure &amp;&amp; make &amp;&amp; sudo make install</code></pre>
<p>See section <em>Configuration</em> below for options.</p></li>
<li><p>On Windows+Cygwin or Windows+MinGW+msys:</p>
<pre><code>./configure --prefix C:/windows/path/with/direct/slash/no/space &amp;&amp; make &amp;&amp; make install</code></pre></li>
<li><p>The binary <code>frama-c</code> (and <code>frama-c-gui</code> if you have lablgtk2) is now installed.</p></li>
<li><p>Optionally, test your installation by running:</p>
<pre><code>frama-c -val tests/misc/CruiseControl*.c
frama-c-gui -val tests/misc/CruiseControl*.c (if frama-c-gui is available)</code></pre></li>
</ol>
<h3 id="full-compilation-guide">Full Compilation Guide</h3>
<h4 id="requirements">Requirements</h4>
<ul>
<li>GNU make version &gt;= 3.81</li>
<li>OCaml 4.x (except 4.02.2, 4.02.0 and 4.00.0)</li>
<li>a C compiler with standard C and POSIX headers and libraries</li>
</ul>
<p>The Frama-C GUI also requires:</p>
<ul>
<li>Gtk (&gt;= 2.4)</li>
<li>GtkSourceView 2.x</li>
<li>GnomeCanvas 2.x</li>
<li>LablGtk &gt;= 2.14.0 (and &gt;= 2.18.2 if you use OCaml &gt;= 4.02.1)</li>
</ul>
<p>If <a href="http://ocamlgraph.lri.fr">OcamlGraph 1.8.5 or 1.8.6</a> is already installed,<br />
then it will be used by Frama-C.<br />
Otherwise the distributed local copy (file <code>ocamlgraph.tar.gz</code>) will be used.</p>
<p>If <a href="http://forge.ocamlcore.org/projects/zarith">Zarith</a> is installed, it will be used by Frama-C.<br />
Otherwise another equivalent but less efficient library will be used.</p>
<p>Plugins may have their own requirements.<br />
Consult their specific documentations for details.</p>
<h5 id="ubuntu">Ubuntu</h5>
<p>If you are using Ubuntu &gt;= Precise Pangolin 12.04 then an optimal list of<br />
packages is installed by:</p>
<pre><code>sudo apt-get install ocaml ocaml-native-compilers graphviz \
             libzarith-ocaml-dev libfindlib-ocaml-dev \
             liblablgtksourceview2-ocaml-dev liblablgtk2-gnome-ocaml-dev</code></pre>
<h5 id="fedora-1">Fedora</h5>
<p>If you are using a recent Fedora, an optimal list of packages can be installed<br />
through (replace <code>dnf</code> by <code>yum</code> in older versions of Fedora):</p>
<pre><code>sudo dnf install ocaml graphviz \
             ocaml-zarith-devel ocaml-findlib ocaml \
             ocaml-lablgtk-devel gtksourceview2-devel libgnomecanvas-devel</code></pre>
<h5 id="other-linux-systems">Other Linux systems</h5>
<p>Some other Linux systems provide packages for the required tools and libraries.<br />
Please send us patches to update this section for your favorite distro.</p>
<h4 id="configuration">Configuration</h4>
<p>Frama-C is configured by <code>./configure [options]</code>.</p>
<p><code>configure</code> is generated by <code>autoconf</code>, so that the standard options for setting<br />
installation directories are available, in particular <code>--prefix=/path</code>.</p>
<p>A plugin can be enabled by <code>--enable-plugin</code> and disabled by <code>--disable-plugin</code>.<br />
By default, all distributed plugins are enabled. Those who defaults to 'no'<br />
are not part of the Frama-C distribution (usually because they are too<br />
experimental to be released as is).</p>
<p>See <code>./configure --help</code> for the current list of plugins, and available options.</p>
<h5 id="under-cygwin-or-mingw">Under Cygwin or MinGW</h5>
<p>Use <code>./configure --prefix C:/windows/path/with/direct/slash</code>.</p>
<h4 id="compilation">Compilation</h4>
<p>Type <code>make</code>.</p>
<p>Some Makefile targets of interest are:</p>
<ul>
<li><code>doc</code> generates the API documentation</li>
<li><code>top</code> generates an OCaml toplevel embedding Frama-C as a library.</li>
<li><code>oracles</code> sets up the Frama-C test suite oracles for your own configuration.</li>
<li><code>tests</code> performs Frama-C's own tests</li>
</ul>
<h5 id="under-cygwin-or-mingw-1">Under Cygwin or MinGW</h5>
<p>Use: <code>make FRAMAC_TOP_SRCDIR=&quot;$(cygpath -a -m $PWD)&quot;</code></p>
<h4 id="installation">Installation</h4>
<p>Type <code>make install</code><br />
(depending on the installation directory, this may require superuser<br />
privileges. The installation directory is chosen through <code>--prefix</code>).</p>
<h4 id="testing-the-installation">Testing the Installation</h4>
<p>This step is optional.</p>
<p>Test your installation by running:</p>
<pre><code>frama-c -val tests/misc/CruiseControl*.c
frama-c-gui -val tests/misc/CruiseControl*.c (if frama-c-gui is available)</code></pre>
<h4 id="api-documentation">API Documentation</h4>
<p>For plugin developers, the API documentation of the Frama-C kernel and<br />
distributed plugins is available in the file <code>frama-c-api.tar.gz</code>, after running<br />
<code>make doc-distrib</code>.</p>
<h4 id="uninstallation">Uninstallation</h4>
<p>Type <code>make uninstall</code> to remove Frama-C and all the installed plugins.<br />
(Depending on the installation directory, this may require superuser<br />
privileges.)</p>
<h2 id="available-resources">Available resources</h2>
<p>Once Frama-C is installed, the following resources should be installed and<br />
available:</p>
<h3 id="executables-in-install_dirbin">Executables: (in <code>/INSTALL_DIR/bin</code>)</h3>
<ul>
<li><code>frama-c</code></li>
<li><code>frama-c-gui</code> if available</li>
<li><code>frama-c-config</code> displays Frama-C configuration paths</li>
<li><code>frama-c.byte</code> bytecode version of frama-c</li>
<li><code>frama-c-gui.byte</code> bytecode version of frama-c-gui, if available</li>
<li><code>ptests.opt</code> testing tools for Frama-c</li>
<li><code>frama-c.toplevel</code> if 'make top' previously done</li>
</ul>
<h3 id="shared-files-in-install_dirshareframa-c-and-subdirectories">Shared files: (in <code>/INSTALL_DIR/share/frama-c</code> and subdirectories)</h3>
<ul>
<li>some <code>.h</code> and <code>.c</code> files used as preludes by Frama-C</li>
<li>some <code>Makefiles</code> used to compile dynamic plugins</li>
<li>some <code>.rc</code> files used to configure Frama-C</li>
<li>some image files used by the Frama-C GUI</li>
</ul>
<h3 id="documentation-files-in-install_dirshareframa-cdoc">Documentation files: (in <code>/INSTALL_DIR/share/frama-c/doc</code>)</h3>
<ul>
<li>files used to generate dynamic plugin documentation</li>
</ul>
<h3 id="object-files-in-install_dirlibframa-c">Object files: (in <code>/INSTALL_DIR/lib/frama-c</code>)</h3>
<ul>
<li>object files used to compile dynamic plugins</li>
</ul>
<h3 id="plugin-files-in-install_dirlibframa-cplugins">Plugin files: (in <code>/INSTALL_DIR/lib/frama-c/plugins</code>)</h3>
<ul>
<li>object files of available dynamic plugins</li>
</ul>
<h3 id="man-files-in-install_dirmanman1">Man files: (in <code>/INSTALL_DIR/man/man1</code>)</h3>
<ul>
<li><code>man</code> files for <code>frama-c</code> (and <code>frama-c-gui</code> if available)</li>
</ul>
<h2 id="installing-additional-plugins">Installing Additional Plugins</h2>
<p>Plugins may be released independently of Frama-C.</p>
<p>The standard way for installing them should be:</p>
<pre><code>./configure &amp;&amp; make &amp;&amp; sudo make install</code></pre>
<p>Plugins may have their own custom installation procedures.<br />
Consult their specific documentations for details.</p>
<h2 id="have-fun-with-frama-c">HAVE FUN WITH FRAMA-C!</h2>
