---
layout: plugin
title: JCard
description: JavaCard Front-End for Frama-C
key: front
distrib_mode: obsolete
---

## Overview

JCard is a JavaCard front-end for Frama-C, based on
[JavaLib and Sawja](https://opam.ocaml.org/packages/javalib/javalib.2.3.4/).
It is meant to transform Java bytecode into Frama-C's internal C
representation. After that, main analysis plug-ins can operate as
usual.

## Usage

Once installed, JCard will automatically consider any `.java`
or `.class` file passed on the Frama-C command line as java input
and attempt to parse them.
JCard is not publicly released, and there is no guarantee that it
is compatible with the current Frama-C version. It is usually made available
as part of collaborative projects or through direct partnerships.
