---
layout: plugin
title: Metrics calculation
short: Metrics
description: Allows the user to compute various metrics from the source code.
key: reporting
distrib_mode: main
manual_pdf: /download/frama-c-metrics-manual.pdf
api_doc: frama-c-metrics
---

## Overview

The **Metrics** plug-in allows the automatic computation of various measures
on the source code.

It can be used to compute the following measures:

- McCabe's cyclomatic complexity;

- Halstead complexity;

- Eva coverage estimate.

## Usage


This plug-in can be partly used with the graphical user interface.
In batch mode, the command line may look like:

    frama-c -metrics file1.c file2.c

## Dependencies

This plug-in depends on results of the [Eva](eva.html) plug-in for the
`-metrics-eva-cover` option.

## Further Reading

The use of this plug-in is detailed in its
[short documentation]({{page.manual_pdf}}).
