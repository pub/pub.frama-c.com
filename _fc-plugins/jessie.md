---
layout: plugin
title: Jessie
description: A deductive verification plug-in.
key: specialized
distrib_mode: obsolete
publications_id: jessie
---

## Overview

**Warning** Jessie is currently not actively maintained and
probably incompatible with the latest Frama-C versions.

The **Jessie** plug-in allows deductive verification of C programs annotated
with [ACSL](/html/acsl.html). It uses internally the languages and tools of the
[Why3](https://www.why3.org/) environment.
The generated verification conditions can be submitted to many external
automatic provers including
[Alt-Ergo](https://alt-ergo.ocamlpro.com/),
[Z3](https://github.com/Z3Prover) and [CVC4](https://cvc4.github.io/).

For more complex situations, interactive theorem provers can be used to
establish validity of VCs. While pages related to Jessie are now mostly
inaccessible, a few examples are still available on
[this page](https://toccata.gitlabpages.inria.fr/toccata/gallery/jessieplugin.en.html)

## Usage

The plug-in is activated with the following command line:

    frama-c -jessie [options] <file>.c
