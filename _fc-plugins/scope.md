---
layout: plugin
title: Scope & Data-flow browsing
short: Scope
description: Allows the user to navigate the dataflow of the program, from definition to use or from use to definition.
key: browsing
distrib_mode: main
api_doc: frama-c-scope
---

## Overview

The **Scope** plug-in computes information about dependencies on a
user-specified lvalue `D` at a user-specified program point `L`.
It provides the following three features, which are presented in
greater detail below.

- **ShowDefs**: gives information about the statements that may contribute to the
  value of `D` at `L`;

- **DataScope**: shows the statements where `D` is guaranteed to have the same
  value than at `L`;

- **Zones**: shows which data is needed in the previous statements to compute
  `D` at `L`.

### Show Defs

**ShowDefs** selects the statements that contribute to define the value of `D`
at `L`, and prints a message if a part of `D` might be undefined.
Notice that *undefined* here only means that it has not been defined on some
path from the beginning of the function.

### DataScope

**DataScope** selects sets of statements around a user-specified program point
`L` where a user-specified lvalue `D` is guaranteed to have the same value than
at `L`.

- Backward selected statements are colored in green: if a statement `s` belongs
  to this selection, then for all executions of the function, the value of `D`
  is not modified between the execution of `s` and the **next** time `L` is
  reached.

- Forward selected statements are colored in blue: if a statement `s` belongs
  to this selection, then for all executions of the function, the value of `D`
  is not modified between the **last** time `L` is reached and the time `s` is
  reached.

- Statements that are in both selections are colored in purple.

- The starting statement is underlined and is always part of the forward set.

### Zones

**Zones** computes, for each point `Li` of the function, the data `Di` needed
to know the value of `D` at `L`. After this computation, the result `Di` will
be printed in the information window each time a statement `Li` is selected.
Of course, the only interesting program points `Li` are the ones reached
before `L`.

The statements that contribute to the computation are highlighted so that you
should get the same result `Di` for successive statements that are not
highlighted.

## Usage

Those features are available through the graphical user interface once the
value analysis ([Eva](eva.html) plug-in) has been done. They can be called from
the menu **Dependencies** in the popup menu that appears when right-clicking
a statement:

- The program point `L` is the one **before** the selected statement;

- The lvalue `D` for which computations are done is either the one that is
  currently selected, if any, or an lvalue entered by the user in a dialog
  window.

Results are presented by coloring the statements in the graphical interface as
explained above. In addition, when something has been selected through these
dependency features, a message in the information window serves as a reminder
of where it comes from.

Note that all the variables that appear in the user-provided lvalue are taken
into account, so that selecting `T[0]` and `T[i]` might lead to different
results, even if the analysis is performed at a point where `i=0`.


## Dependencies

The **Scope** plug-in only takes into account the executions that have been
considered by [Eva](eva.html). In particular, the alarms emitted by **Eva** may
correspond to executions that it ceased to consider. The alarms emitted by
**Eva** should therefore be carefully examined by the user.
