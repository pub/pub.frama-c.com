---
layout: plugin
title: Semantic constant folding
description: Makes use of the results of the EVA plug-in to replace, in the source code, the constant expressions by their values.
key: code
distrib_mode: main
api_doc: frama-c-constant_propagation
---

## Overview

The **Semantic constant folding** plug-in produces an output program where C
expressions which are established as constant by the [Eva](eva.html) plug-in
are replaced by their value. Because it relies on Eva, it is able to do more of
these simplifications than a syntactic analysis would. The output program is
guaranteed to be compilable C code, and to have the same semantics as the
original program.

## Usage

The command-line options related to constant folding are:

- `-semantic-const-folding`:
  Activates semantic constant folding and pretty print the new source code.

- `-semantic-const-fold f1,...,fn`:
  Replaces constant expressions in functions `f1,...,fn`.

- `-cast-from-constant`:
  Allows introduction of new casts from a folded constant.

## Example

Consider the code fragment:
```
p = &x;
x = 3;
send(*p+4);
```

The **Semantic constant folding** plug-in produces:
```
p = &x;
x = 3;
send(7);
```

If you need to remove the (now useless) first two statements, you may make
use of the [Spare Code analysis](spare-code.html) plug-in.

## Technical Notes

The plug-in performs propagation of constant integers and addresses,
but at this time, it does not handle floating-point values.

## Dependencies

This plug-in depends on results of the [Eva](eva.html) plug-in.
