---
layout: plugin
title: Server
description: Frama-C Server Protocol
key: reporting
distrib_mode: main
api_doc: frama-c-server
---

## Overview

**Server** plug-in turns Frama-C into a server. It is designed for
IDE integration or scripting.

## Usage

Server is part of the main distribution of Frama-C. It consists of two main distinct parts: (1) a rich API for plug-ins to programmatically register _Requests_ in the server; (2) a collection of protocols to
expose registered _Requests_ to the external clients.

## Server Request API

Each Frama-C plug-in can register _Requests_ in the server. A request
is a normal OCaml routine that interact with the platform. The Server API also offers a typed collection of JSON converters in order to adapt to external clients outside of the OCaml ecosystem.

All registered _Requests_ are programmatically documented and the Server plug-in can generate a complete HTML documentation of all registered requests, with the JSON format of their input and output parameters.

## Server Protocols

The Server plug-in provides three built-in protocols: one based on Unix sockets, one based on ZeroMQ sockets, and one based on static JSON script files. New protocols can be defined _via_ custom Frama-C plug-ins.

## Ivette Integration

The new GUI of Frama-C, [Ivette](/html/ivette.html), uses the Server plug-in to interact with the platform.
