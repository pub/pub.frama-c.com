---
tab-id: e-acsl
title: E-ACSL
images: [ "/assets/img/plugins/e-acsl.png" ]
---
The **E-ACSL** plug-in provides **Runtime Annotation Checking (RAC)**, a
lightweight formal method consisting in checking code annotations during the
program execution. While static formal methods aim for guarantees that hold
for any execution, RAC only **provides guarantees about the particular execution**
it monitors. This allows RAC-based tools to be used with
**minimum intervention from the user**.

#### Runtime assertion checking with the E-ACSL plug-in

E-ACSL is able to translate (Executable-)ACSL annotations into C code, so that
they can be verified at runtime. E-ACSL is often **used in combination** with
other plug-ins of Frama-C, *e.g.* Eva and WP. It can be used to understand
**why a proof with WP fails**, or to **monitor alarms after Eva analysis**.
E-ACSL builds upon the results from the other plug-ins: it does not instrument
annotations that have already been proved valid by a static analyzer.

#### Formally correct monitoring

Translating ACSL is not simple as it may seem. The generated C code must
**not introduce any new bug in the code**. Thus, E-ACSL relies on the
[RTE](/fc-plugins/rte.html) plug-in, on GMP integers and on a
**high performance shadow memory** to capture **all possible runtime error**.
Since all of this can be costly, E-ACSL optimizes these constructs when
possible, this guarantees that **E-ACSL has a reasonable runtime overhead**.

Not all ACSL constructs can be translated into C code (for example quantifiers
must be bounded), one has to restrict to the *executable* fragment of the
language. More details can be found on
[the page dedicated to E-ACSL](/fc-plugins/e-acsl.html).

\* The actual transformation is slightly more complex than this illustration.
