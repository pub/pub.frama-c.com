---
tab-id: eva
title: Eva
images: [ "/assets/img/plugins/eva-overview.png" ]
---

The **Eva** plug-in aims at **proving the absence of runtime errors**
caused by C undefined behaviors, such as invalid memory accesses,
reads of uninitialized memory, integer overflows, divisions by zero,
dangling pointers…

#### Based on abstract interpretation

Eva relies on abstract interpretation to perform a **sound static analysis**
of the entire program and capture all possible behaviors of its executions.
It is thus able to **report all errors that might happen** — in the class of
undefined behaviors supported by the analysis.

During its analysis, Eva infers many properties about the analyzed program,
including an over-approximation of **the possible values for each variable**
at each program point. The Frama-C **graphical user interface** can then be used
to browse the analyzed code, review the list of emitted alarms, display the
inferred ranges for any variable, highlight dead code, and more.

#### Highly configurable analysis

Although the Eva analysis is **automatic**, many parameters are available to
finely configure its behavior, impacting its results accuracy and analysis time.
More information on this plug-in and how to use it are available on the
[dedicated page](/fc-plugins/eva.html) and in the [Eva user manual]({{page.manual_pdf}}).
