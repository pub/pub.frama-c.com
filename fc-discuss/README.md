This directory contains the raw archives of the old frama-c-discuss mailing list hosted by Inria's gforge until September 2020. do make to generate
the html version that will be displayed on the public site. You need
[MHonArc](https://mhonarc.org) for that.
