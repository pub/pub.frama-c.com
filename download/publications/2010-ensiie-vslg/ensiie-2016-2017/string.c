int f(char* dest, int len, const char* src) {
  int i = 0;
  while (src[i] != 0 && src[i] != ' ' && i < len) {
    dest[i] = src[i];
    i++;
  }
  dest[i] = 0;
  return i-1;
}

int main() {
  const char* test[] = "Example of character string";
  char result1[10], result2[10];
  int res = f(result1, 9, test);
  f(result2, 9, test + res + 1);
  return 0;
}
