#include <stdio.h>
#include "KeccakNISTInterface.h"

#define HASHLEN 64

BitSequence msg[80] = "hello, world";

int main(void) {
  BitSequence hash[HASHLEN/8];
  unsigned long i;
  hashState context;
  Init(&context, HASHLEN);
  Update(&context,msg,80*8);
  Final(&context,hash);
  for (i=0; i<HASHLEN/(8*sizeof(unsigned long)); i++)
       printf("%lu\n",((unsigned long *)hash)[i]);
  return 0;
}
