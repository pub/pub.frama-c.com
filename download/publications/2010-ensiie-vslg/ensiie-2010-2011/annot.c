int x, y, z;

void f(int c) {
  int* p = 0;

  if (c == 1) p = &x;
  if (c == 2) p = &y;
  if (c == 3) p = &z; 

  if (0<c && c <4) *p=42;
}
