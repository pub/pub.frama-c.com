#include "fact.c"

volatile int entropy;

int interval(int min, int max) {
  int res = entropy;
  if (res < min) res = min;
  if (res > max) res = max;
  return res;
}

int main (void) {
  int x = interval(0,100);
  return fact(x);
}
