void *memcpy(void *src, void* dst, int size) {
  unsigned long dst_val = ((unsigned long) dst) % 4 ;
  int todo = size;
  char *dstc = (char *) dst;
  char *srcc = (char *) src;
  for (int i=0; i < dst_val && todo > 0; i++) {
    *dstc++ = *srcc++;
    todo--;
  }
  int *dsti = (int *) dstc;
  int *srci = (int *) srcc;
  while (todo>=4) {
    *dstc++=*srci++;
    todo-=4;
  }
  dstc=(char*)dsti;
  srcc=(char*)srci;
  for (int i=0; i < todo; i++) {
    *dstc++=*srci++;
  }
  return dst;
}
