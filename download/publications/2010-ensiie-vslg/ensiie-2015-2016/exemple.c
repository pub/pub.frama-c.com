#define MAX 128

int sieve[MAX];

int main() {
  for (int i = 2; i <= MAX+1; i++) {
    if (sieve[i-2]==0) {
      for(int j = i - 2; j < MAX; j+=i) {
        sieve[j] = i;
      }
    }
  }
}
