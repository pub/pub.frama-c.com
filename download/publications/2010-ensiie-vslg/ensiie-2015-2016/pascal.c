#define MAX 21

int triangle[MAX][MAX];

void fill(int n) {
if (n <= 0 || n > MAX) return;

for (int i =0; i < n; i++) {
  triangle[i][0] = 1;
  for (int j = 1; j<=i; j++) {
    triangle[i][j]=triangle[i-1][j] + triangle[i-1][j-1];
  }
 }
}

int main() {
  fill(MAX);
  return triangle[MAX][MAX/2];
}

