#include <stdio.h>

void m_func (int N, int a[], int b[], int c[]) {
  int i=0,j=0,k=0,s=0;
  int *ptr_a,*ptr_b,*ptr_c;
  
  for (i=0;i<N;i++){
    for (j=0;j<N;j++){
      ptr_a = a+ (i*N);
      ptr_b = b+ j;
      ptr_c = c+ (i*N + j);
      s=0;
      for (k=0;k<N;k++){
	s= s + *ptr_a * *ptr_b;
	ptr_a++;
	ptr_b += N;
      }
      *ptr_c=s;
    }
  }
}  

int a[4]={2,3,4,5};
int b[4]={2,0,0,1};
int c[4];
int main () {
  m_func(2,a,b,c);
  //printf(" (%d %d) \n (%d %d) \n",c[0],c[1],c[2],c[3]);
  return 0;
 }
