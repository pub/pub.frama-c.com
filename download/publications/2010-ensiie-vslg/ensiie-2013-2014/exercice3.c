
int f(int x) {
  int S = 0;
  for (int i=0; i<=x; i++) {
    if (S>=i) S-=i; else S+=i;
  }
  return S;
}

int main() {
  int x = 10;
  int res = f(x);
  return res;
}
