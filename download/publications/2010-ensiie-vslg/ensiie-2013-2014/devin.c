
#include "builtin.h"

int devine(int secret, int max) {
  int tentative = max / 2, inf = 0, sup = max, nb = 1;
  while(tentative != secret) {
    nb++;
    if (tentative < secret) inf = tentative+1;
    if (tentative > secret) sup = tentative-1;
    tentative = (inf+sup) / 2;
  }
  return nb;
}
