
int x,y, *z, *idx, a[101], t;
float f;

int main (int c) {

  if(0<= c && c<=9) z = &x; else z = &y;

  if(c==5) x = 1; else x = 2;

  switch (c) {
    case 0: y = 3; break;
    case 1: y = 42; break;
    case 2: y = 36; break;
    case 3: y = 25; break;
    case 4: y = 10; break;
    case 5: y = 100; break;
    case 6: y = 18; break;
    case 7: y = 75; break;
    case 8: y = 83; break;
    case 9: y = 98; break;
    default: y = 2; break;
  }

  idx = &a[y];
    
  if (c) f = 1.5; else f = 0x1p-2;
  
  return y + *z;
}
