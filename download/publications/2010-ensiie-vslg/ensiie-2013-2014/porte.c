
#include "builtin.h"

int portes[100];

int main() {
  int N = Frama_C_interval(2,100);
  for(int i = 1; i<= N; i++) {
    for(int j=i; j<=N; j+=i) portes[j-1] = 1 - portes[j-1];
  }
}
