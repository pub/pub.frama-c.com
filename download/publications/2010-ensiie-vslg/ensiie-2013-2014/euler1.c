
int total = 0;

void add(int max, int x) { total += x; }

void sum(int max) {
  int i=1;
  while ( i <= max / 3) {
    add(max,3*i);
    if (i%3 !=0) add(max, 5*i);
    i++;
  }
}

int main() {
  sum(1000);
  return total;
}
