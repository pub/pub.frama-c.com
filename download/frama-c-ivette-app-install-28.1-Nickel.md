# Installing the Binary Distribution of Ivette

Requirement: an installed `frama-c` binary.

**Warning:** if you already have an `ivette` script along with `frama-c`, that
script is used for bootstrapping the installation of Ivette from source through
your internet connection.  The instructions provided here are intended to
_replace_ the installation procedure from source. Hence, it is highly
recommended for you to remove the bootstrapping `ivette` script if you want to
use the binary distribution of Ivette.

## Ivette for Linux

Requirement: libfuse2 must be installed.

Download the binary distribution (for now, only x86-64 and ARM64 are supported). Install it
wherever you want:

```sh
cp frama-c-ivette-linux-<arch>-28.1-Nickel.AppImage <IVETTE-INSTALL-PATH>/ivette.AppImage
```

Then add an alias `ivette` that just runs the AppImage:

```sh
alias ivette=<ABSOLUTE-IVETTE-INSTALL-PATH>/ivette.AppImage
```

## Ivette for macOS

Download the universal binary distribution of Ivette and install it, typically
in `/Applications/Ivette.app`. To launch Ivette from the command line, you will
need your own `ivette` script, like the following one:

```sh
#! /usr/bin/env sh
exec open -na <IVETTE-INSTALL>/Ivette.app --args\
  --command <FRAMAC-INSTALL>/frama-c\
  --working $PWD $*
```

Simply replace `<IVETTE-INSTALL>` and `<FRAMAC-INSTALL>` in the code above with
the (absolute) paths to your `Ivette.app` and `frama-c` binaries,
respectively. Then, make your `ivette` script executable and simply use it like
the `frama-c` command-line binary!
