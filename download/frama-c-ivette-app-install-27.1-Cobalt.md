# Installing Binary Distribution of Ivette

You shall have an installed Frama-C binary.

**Warning:** if you already have an `ivette` script along with `frama-c`, this
script is used for bootstrapping the installation of Ivette from source through
your internet connection.  The instructions provided here are intended to
_replace_ this installation procedure from source. Hence, it is highly
recommended for you to remove this bootstrapping `ivette` script if you want to
use the binary distribution of Ivette.

## Ivette for Linux

Download the binary distribution (x86 only for now).

Install it wherever you want and make it executable:

```sh
cp frama-c-ivette-linux-27.0-Cobalt.AppImage <IVETTE-INSTALL-PATH>/ivette.AppImage
chmod a+x <IVETTE-INSTALL-PATH>/ivette.AppImage
```

Then add an alias `ivette` that just runs the AppImage:

```sh
alias ivette=<ABSOLUTE-IVETTE-INSTALL-PATH>/ivette.AppImage
```

## Ivette for macOS

Download the suitable binary distribution of `Ivette.app` (x86 or arm64).

You can install Ivette binaries at any location, typically
`/Applications/Ivette.app` or `~/mylib/Ivette.app`.  To launch Ivette from the
command line, you will need your own `ivette` script, like the
following one:

```sh
#! /usr/bin/env sh
exec open -na <IVETTE-INSTALL>/Ivette.app --args\
  --command <FRAMAC-INSTALL>/frama-c\
  --working $PWD $*
```

Simply replace `<IVETTE-INSTALL>` and `<FRAMAC-INSTALL>` in the code above with
the (absolute) paths to your `Ivette.app` and `frama-c` binaries,
respectively. Then, make your `ivette` script executable and simply use it like
a `frama-c` command-line binary!
