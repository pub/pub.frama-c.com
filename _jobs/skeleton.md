---
layout: job
title: <Postdoc, Permanent, Intern, ...> Position at CEA LIST - LSL
short_title: <to be displayed in the timeline>
short: "
  <short description, two or three lines, plain text>
  <displayed on: jobs page, events>
"
date: 13-10-2024
filled: false
keywords: <a few keywords>
---

#### Context: CEA LIST, Software Security and Reliability Lab

The Software Safety and Security Laboratory (LSL) at CEA List has an ambitious goal: help
designers, developers and validation experts ship high-confidence systems and software. Objects in
our surroundings are getting more and more complex, and we have built a reputation for efficiently
using formal reasoning to demonstrate their trustworthiness. Within the CEA List Institute, LSL is
dedicated to inventing the best possible means to conduct formal verification. In collaboration
with the most creative people in academia and the industry, we design methods and tools that
leverage innovative approaches to ensure that real-world systems can comply with the highest
safety and security standards

Our organizational structure is simple: those who pioneer new concepts are the ones who get to
implement them. We are a forty-person team, and your work will have a direct and visible impact on
the state of formal verification. CEA LIST's offices are located at the heart of Campus Paris-Saclay
(aka the _Silicon Plateau_), in the largest European cluster of public and private research.

#### Work Description

<Description>

#### Application

**Qualifications:**

- *Minimal*
  - <Level: Bachelor, M1, M2, ...>
  - <required skills>
  - Ability to work in a team

- *Preferred*
  - <optional skills>
  
**Duration:** <xx-yy> months

**Location:** [CEA Nano-INNOV](https://www.openstreetmap.org/#map=19/48.71238/2.19335), Paris-Saclay Campus, France (Work From Home possible up to 2-3 days/week)

**Compensation:**

- €850 (Bac+3/Bachelor), €1300 (Bac+4/M1), €1400 (Bac+5/M2) monthly stipend
- Maximum €229 housing and travel expense monthly allowance (in case a relocation is needed)
- CEA shuttle in Paris region and 75% refund of transit pass
- Subsidized lunches

**Availability:** Position is opened for the academic year 2024-2025; Please note that
a 3-month procedure for administrative and security purposes is required

**Contact:**

For further information or details about the internship before applying, please contact:

- First Last (<first.last@cea.fr>)
