---
layout: job
title: 3-year Engineer Position at CEA LIST - LSL
short_title: 3-year Engineer
short: "
  Develop and extend Ivette, the new Electron-based Frama-C graphical user
  interface
"
date: 28-02-2023
filled: false
keywords: GUI, TypeScript, OCaml, Analysis Environment
---

#### Mission

At the heart of the Paris-Saclay campus, [CEA List](http://www-list.cea.fr/en/)'s Software Safety and Security Laboratory develops analysis tools to verify and enforce sensitive properties of software. Our goal is to provide trust in mission-critical and sensitive systems. We are open-source believers, and we take pride in seeing our science-fueled tools used for industry-grade applications.

We are looking for a 3-year engineer to join our [Frama-C](https://www.frama-c.com) team starting as soon as possible. [Frama-C](https://www.frama-c.com) is a sound static and dynamic analysis platform targeting C/C++/JavaCard source code. It relies on advanced techniques giving high level of confidence about source code. The platform has been used in academia and the industry to verify the safety and security of control systems and communication protocols, on small and large codebases.

The main goal of this position is to improve the [new Frama-C graphical user interface](https://www.frama-c.com/html/ivette.html). The platform is used by developers, auditors, and evaluators to validate security properties, and we want to provide them with beyond-state-of-the-art methods, and tools to support their mission. For this, we need a modern graphical user interface, providing efficient and meaningful visualization of the results of the analyses, with advanced ways of dealing with complex code bases that nowadays become larger and larger.

#### Responsibilities

- Develop new features and improvements for the new [Frama-C GUI](https://www.frama-c.com/html/ivette.html)
- Interact with practitioners, prescribers, and the open-source community
- Contribute to the development of the team
- Contribute to the dissemination of the team results in top scientific and technical venues

#### Qualifications

- Master's degree
- Ability to develop in TypeScript or JavaScript
- Knowledge of a framework React-like
- Knowledge of a functional language

#### Application

Every potential candidate feeling they could fit the description are welcome to contact us or apply by emailing the persons indicated below. We try hard to run an inclusive team, so candidates from under-represented groups are certainly welcome. The selection process will include scientific, technical, and HR interviews.

- [Allan Blanchartd](mailto:allan.blanchard@cea.fr)
- [Patricia Mouy](mailto:patricia.mouy@cea.fr)
- [Loic Correnson](mailto:loic.correnson@cea.fr)
