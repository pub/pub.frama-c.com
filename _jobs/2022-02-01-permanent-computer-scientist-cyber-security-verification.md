---
layout: job
title: Permanent Computer Scientist Position at CEA LIST - LSL
short_title: Permanent Computer Scientist Position
short: Develop and extend the applicability of Frama-C for cybersecurity purposes
date: 01-02-2022
filled: true
keywords: cybersecurity, software analysis, formal methods, open source
---

#### Mission

At the heart of the Paris-Saclay campus, [CEA List](http://www-list.cea.fr/en/)'s Software Safety and Security Laboratory develops analysis tools to verify and enforce sensitive properties of software. Our goal is to provide trust in mission-critical and sensitive systems. We are open-source believers and we take pride in seeing our science-fueled tools used for industry-grade applications.

We are looking for a permanent computer scientist to join our [Frama-C](https://www.frama-c.com) team. [Frama-C](https://www.frama-c.com) is a sound static and dynamic analysis platform targeting C/C++/JavaCard source code. It relies on advanced techniques such as abstract interpretation and theorem proving. The platform has been used in academia and the industry to verify the safety and security of control systems and communication protocols, to prevent various CWE occurrences, and to prove functional properties on small and large codebases. Some of those results are displayed in [OSCS](https://git.frama-c.com/pub/open-source-case-studies), an open-source case study repository we maintain.

The main goal of this position is to develop and extend the applicability of [Frama-C](https://www.frama-c.com) for cybersecurity purposes. The platform is used by developers, auditors, and evaluators to validate security properties and we want to provide them with beyond-state-of-the-art methods, techniques, and tools to support their mission. This will be achieved by defining which security properties to target, developing the platform to make their verification possible, and providing relevant proof artefacts.

#### Responsibilities

- Develop new features and improvements for [Frama-C](https://www.frama-c.com)
- Run case studies for partners and [OSCS](https://git.frama-c.com/pub/open-source-case-studies)
- Interact with practitioners, prescribers, and the open-source community
- Contribute to the dissemination of the team results in top scientific and technical venues
- Contribute to the development of the team

#### Qualifications

- PhD or more than three years of experience in a research-intensive team
- Top scientific/technical publications or significant software developments
- Ability to develop in OCaml or functional languages
- Practice of software cybersecurity
- Knowledge of formal methods
- Knowledge of the C language and ability to quickly grasp new ones

#### Application

This position will be filled as soon as possible. Every potential candidate feeling they could fit the description are welcome to contact us or apply by emailing the persons indicated below. We try hard to run an inclusive team, so candidates from under-represented groups are certainly welcome. The selection process will include scientific, technical, and HR interviews.

- [Patricia Mouy](mailto:patricia.mouy@cea.fr)
- [Allan Blanchard](mailto:allan.blanchard@cea.fr)
- [Andre Maroneze](mailto:andre.maroneze@cea.fr)
