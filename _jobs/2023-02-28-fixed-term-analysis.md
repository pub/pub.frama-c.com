---
layout: job
title: 3-year Computer Scientist Position at CEA LIST - LSL
short_title: 3-year Computer Scientist
short: "
  Develop and extend the formal analysis capability of Frama-C.
  In particular, provide new features either in the Eva plug-in or
  WP plug-in
"
date: 28-02-2023
filled: false
keywords: Software Analysis, Formal Methods, OCaml
---

#### Mission

At the heart of the Paris-Saclay campus, [CEA List](http://www-list.cea.fr/en/)'s Software Safety and Security Laboratory develops analysis tools to verify and enforce sensitive properties of software. Our goal is to provide trust in mission-critical and sensitive systems. We are open-source believers, and we take pride in seeing our science-fueled tools used for industry-grade applications.

We are looking for a 3-year computer scientist to join our [Frama-C](https://www.frama-c.com) team starting as soon as possible. [Frama-C](https://www.frama-c.com) is a sound static and dynamic analysis platform targeting C/C++/JavaCard source code. It relies on advanced techniques such as abstract interpretation and theorem proving. The platform has been used in academia and the industry to verify the safety and security to prevent various vulnerabilities, and to prove functional properties on small and large codebases.

The main goal of this position is to develop and extend the main static analyzers of [Frama-C](https://www.frama-c.com), namely [Eva](https://www.frama-c.com/fc-plugins/eva.html) and [WP](https://www.frama-c.com/fc-plugins/wp.html).

#### Responsibilities

- Develop new features and improvements for [Frama-C](https://www.frama-c.com)
- Interact with practitioners, prescribers, and the open-source community
- Contribute to the dissemination of the team results in top scientific and technical venues
- Contribute to the development of the team

#### Qualifications

- PhD or more than three years of experience in a research-intensive team
- Ability to develop in OCaml or functional languages
- Practice of software cybersecurity
- Knowledge of formal methods
- Knowledge of the C language and ability to quickly grasp new ones

#### Application

Every potential candidate feeling they could fit the description are welcome to contact us or apply by emailing the persons indicated below. We try hard to run an inclusive team, so candidates from under-represented groups are certainly welcome. The selection process will include scientific, technical, and HR interviews.

- [Allan Blanchard](mailto:allan.blanchard@cea.fr)
- [Patricia Mouy](mailto:patricia.mouy@cea.fr)
- [David Buhler](mailto:david.buhler@cea.fr)
- [Loic Correnson](mailto:loic.correnson@cea.fr)
