---
layout: job
title: Permanent Computer Scientist Position at CEA LIST - LSL
short_title: Permanent Computer Scientist Position
short: "
  Join the development team of Eva, the Frama-C plug-in based on abstract
  interpretation
"
date: 26-10-2023
filled: true
keywords: abstract interpretation, software analysis, formal methods, open source
---

#### LSL Laboratory

At the heart of the Paris-Saclay campus, [CEA List](http://www-list.cea.fr/en/)'s
Software Safety and Security Laboratory helps developers and validation experts
ship high-confidence software and systems. With everyday objects getting more
and more complex, we have built a reputation for efficiently applying formal
reasoning techniques to establish their trustworthiness. We are open-source
believers, and we take pride in seeing our science-fueled tools used for
industry-grade applications.

Teams at LSL are researching the best possible means to conduct formal
verification. We design tools such as [Binsec](https://binsec.github.io/) or
Frama-C, that ensure production-level systems can comply with the highest safety
and cybersecurity expectations. And in doing so, we get to interact with the
most creative people in academia and the industry.

Our organizational structure is simple: those who pioneer new concepts are the
ones who get to lead their implementation. Your work will have a direct and
visible impact on the state of software verification.

#### Role

We need you to help us develop [Eva](https://www.frama-c.com/fc-plugins/eva.html),
the Frama-C's abstract interpretation plug-in, both by improving current analyses
and by designing new approaches. You will contribute to growing the community of
users, handling feedback and helping real people solve real problems.

You will take an active role in research and development activities and
industrial partnerships, alongside other members of the laboratory. This can
include writing proposals, managing projects, writing and reviewing code,
publishing papers, as well as attending scientific and technical events
worldwide.

#### Requirements

- Background in abstract interpretation and theory of programming languages.
- Hands-on experience with significant OCaml developments - other languages
  are fine too, but you'll need to convince us you can adapt in a snap.
- Self-organized, with an ability to prioritize effectively.
- Team-minded - you know when to let someone else take the lead.

#### Pluses

Various areas of our overall activity can also benefit from specific skill sets.

- Break new ground with us:
  * Hands-on expertise in the fields of software security.
  * Flawless understanding of C, C++, assembly languages or hardware interfaces.
- Help us spread the word:
  * Strong proficiency in foreign languages.
  * A knack for writing and editing longform content.

#### Applying

Every potential candidate feeling they could fit the description are welcome to
contact us or apply by emailing the persons indicated below. We try hard to run
an inclusive team, so candidates from under-represented groups are certainly
welcome. The selection process will include scientific, technical, and HR interviews.

- [David Buhler](mailto:david.buhler@cea.fr)
- [Allan Blanchard](mailto:allan.blanchard@cea.fr)
- [Patricia Mouy](mailto:patricia.mouy@cea.fr)
