---
layout: job
title: Postdoc Position at CEA List - LSL
short_title: Postdoc Position
short: Outline Runtime Annotation Checking
date: 20-01-2025
filled: false
keywords: runtime annotation checking, outline monitoring, compilation, source code generation
---

# {{ page.short }}

[Full description](https://julien-signoles.fr/positions/postdoc-outline-rac.pdf)

#### Context: CEA List, Software Safety and Security Lab

[CEA List](http://www-list.cea.fr/en/)'s offices are located at the heart of Université Paris-Saclay, the
largest European cluster of public and private research. Within [CEA List](http://www-list.cea.fr/en/), the
Software Safety and Security Lab has an ambitious goal: help designers,
developers and validation experts ship high-confidence systems and software.

Systems in our surroundings are getting more and more complex, and we have built
a reputation for efficiently using formal reasoning to demonstrate their
trustworthiness through the design of methods and tools to ensure that
real-world systems can comply with the highest safety and security standards. In
doing so, we get to interact with the most creative people in academia and the
industry, worldwide.

Our organizational structure is simple: those who pioneer new concepts are the
ones who get to implement them. We are a fifty-person team, and your work will
have a direct and visible impact on the state of formal verification.

#### Work Description

Our team develops [Frama-C](www.frama-c.com), a
code analysis platform for C programs that provides several analyzers as
plug-ins. [Frama-C](www.frama-c.com) itself is developed in OCaml. [Frama-C](www.frama-c.com) allows the user to
annotate C programs with formal specifications written in the [ACSL](https://www.frama-c.com/html/acsl.html)
specification language. [Frama-C](www.frama-c.com) can then ensure that a C
program satisfies its formal specification by relying on several techniques
including abstract interpretation, weakest preconditions calculus, and runtime
annotation checking.

[E-ACSL](https://www.frama-c.com/fc-plugins/e-acsl.html) is the
[Frama-C](www.frama-c.com) plug-in dedicated to runtime annotation
checking. It converts a C program
extended with formal annotations written in a subset of [ACSL](https://www.frama-c.com/html/acsl.html) into a new C
program which checks the validity of annotations at runtime: by default, the
program execution stops whenever one annotation is violated, or behaves in the
same way than the input program if all its annotations are valid. For doing so,
[E-ACSL](https://www.frama-c.com/fc-plugins/e-acsl.html) performs an heavy implementation of the original source code to insert
its own code that monitors the [ACSL](https://www.frama-c.com/html/acsl.html) annotations. This technique is usually
referred to as *(online) inline runtime verification*. However, depending on the context of
application, this heavy instrumentation may lead to prohibitive memory and
runtime overheads, as well as security concerns.

The expected work aims at designing and implementing an *outline runtime
verification technique* for [E-ACSL](https://www.frama-c.com/fc-plugins/e-acsl.html), compatible with the existing inline
technique. Outline runtime verification consists in placing the monitor in an
external entity (e.g., another thread, or a remote server) for limiting the
instrumentation to communication activities with the remote
monitor. While this technique is well known and often
applied to monitoring of temporal properties, it was never applied to runtime
annotation checking, which raises several challenges regarding the data that
need to be monitored. The expected contributions are:

- designing a new instrumentation scheme and a communication protocol suitable for outline runtime annotation checking;
- designing a new internal representation for the monitor, usable for both inline and outline runtime annotation checking;
- implementing the new representation and the new instrumentation scheme inside [E-ACSL](https://www.frama-c.com/fc-plugins/e-acsl.html);
- evaluating the new outline runtime annotation checker on representative benchmarks and/or use cases;
- designing and implementing optimization strategies to reduce the time and memory overheads, as well as the network communications between the monitor and the program under scrutiny.

#### Qualification

Knowledge in the following fields is warmly welcome:

- OCaml programming (at least, functional programming)
- C programming
- compilation
- runtime verification
- formal semantics of programming languages

#### Application

This position will be filled as soon as possible; yet  a 3+-month procedure for
administrative and security purposes is required.

- [Julien Signoles](mailto:julien.signoles@cea.fr)

Please join a detailed CV, a motivation letter and at least one recommendation letter.
